﻿
namespace EnumKeys
{
    #region 通用
    public enum StateKeys { Normal, Stop, Delete, Already, Duplicate }
    public enum ServiceStateKeys { TimeOut, Success, Fail, NoPermiss, Exists, NotExists, InUse, NotInUse, HardErr, UserErr, PostBack }
    //public enum SessionKeys { USER }

    public static class SessionKey
    {
        public static string USER = "users";
        public static string ONLINEUSERS = "onlineusers";
        public static string ADMIN = "admin";
        public static string SESSIONID = "sessionId";
    }
    /// <summary>
    /// 正常（在售在租等） 停售 删除 已售 重复等
    /// </summary>
    #endregion
    #region 模块用
    public class EnumKeysClass
    {
        public static string[] HouseState = { "在售", "停售", "已售", "删除" };
        public static string[] CMState = {"未开始", "正常", "终止", "完成" };
        public static string[] HouseFaceKeys = { "多层", "小高层", "高层", "公寓", "商住", "门面", "商铺", "别墅", "厂房", "写字楼", "车库", "多层复式", "高层复式" };
        public static string[] HouseDecoration = {"毛坯","简装","中装","豪装","精装" };
        /// <summary>
        /// 获得房屋类型的id
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static int GetFaceId(string key) {
            if (key == "室内商铺") key = "商铺";
            else  if (key == "独立别墅") key = "别墅";
            else if (key == "连体别墅") key = "别墅";
            else if (key == "双拼别墅") key = "别墅";
            else if (key == "沿街门面") key = "门面";
            else if (key == "商住楼") key = "商住";
            else if (key == "【车库】") key = "车库";
            else if (key == "车位") key = "车库";
            else if (key == "其它") key = "多层";
            else if (key == "其他") key = "多层";
            else if (key == "多层住宅") key = "多层";
            else if (key == "高层住宅") key = "高层";
            else if (key == "花园洋房") key = "别墅";
            else if (key == "复式") key = "多层复式";
            for (var i = 0; i < HouseFaceKeys.Length; i++) {
                if (HouseFaceKeys[i] == key) {
                    return i;
                }
            }
            return 0;
        }
        /// <summary>
        /// 某种装修的id
        /// </summary>
        /// <param name="decoration"></param>
        /// <returns></returns>
        public static int GetDecorationId(string decoration)
        {
            if (decoration == "普装") decoration = "简装";
            else if (decoration == "豪装") decoration = "精装";
            else if (decoration == "豪华装") decoration = "精装";
            for (var i = 0; i < HouseDecoration.Length; i++)
            {
                if (HouseDecoration[i] == decoration)
                {
                    return i;
                }
            }
            return 1;
        }
        /// <summary>
        /// 获取房屋状态id
        /// </summary>
        /// <param name="decoration"></param>
        /// <returns></returns>
        public static int GetHouseState(string State)
        {
            if (State == "出售") State = "在售";
            if (State == "出租") return -1;
            for (var i = 0; i < HouseState.Length; i++)
            {
                if (HouseState[i] == State)
                {
                    return i;
                }
            }
            return 0;
        }
    }
    #endregion
}