using log4net;
using System;
using System.Configuration;
namespace Maticsoft.DBUtility
{

    public class PubConstant
    {
        private static ILog logger = LogManager.GetLogger("PubConstant");
        /// <summary>
        /// 获取连接字符串
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                string _connectionString = ConfigurationManager.AppSettings["ConnectionString"];
                logger.Info("_connectionString=" + _connectionString);
                return _connectionString;
            }
        }

        /// <summary>
        /// 得到web.config里配置项的数据库连接字符串。
        /// </summary>
        /// <param name="configName"></param>
        /// <returns></returns>
        public static string GetConnectionString(string configName)
        {
            string connectionString = ConfigurationManager.AppSettings[configName];

            return connectionString;
        }


    }
}
