﻿
namespace Webapp
{
    public class RegularKeys
    {
        public static string email { get { return @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"; } }
        public static string url { get { return @"[a-zA-z]+://[^\s]*"; } }
        /// <summary>
        /// 以http开头的网址
        /// </summary>
        public static string httpurl { get { return @"^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*"; } }
        public static string cnCode { get { return @"\u4e00-\u9fa5"; } }
        /// <summary>
        /// 双字节字符
        /// </summary>
        public static string uniCode { get { return @"^\x00-\xff"; } }
        /// <summary>
        /// 以HTML结尾
        /// </summary>
        public static string endHTML { get { return @"/<(.*)>.*<\/\1>|<(.*) \/>/ "; } }
        public static string tel { get { return @"^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$"; } }
        public static string mobile { get { return @"^((\(\d{2,3}\))|(\d{3}\-))?1\d\d{9}$"; } }
        public static string zip { get { return @"^[1-9]\d{5}$"; } }
        public static string enCode { get { return @"^[A-Za-z]+$"; } }
        public static string doubleStr { get { return @"^[-\+]?\d+(\.\d+)?$"; } }
        public static string intStr { get { return @"^[-\+]?\d+$"; } }
        public static string qq { get { return @"^[1-9]\d{4,8}$"; } }
        public static string num { get { return @"^\d+$/"; } }
        public static string currencyStr { get { return @"^\d+(\.\d+)?$"; } }
        public static string ip { get { return @"\d+[\.|．]\d+[\.|．]\d+[\.|．]\d+"; } }
        /// <summary>
        /// 正整数
        /// </summary>
        public static string positiveInt { get { return @"^[1-9]\d*$"; } }
        /// <summary>
        /// 负整数
        /// </summary>
        public static string nInt { get { return @"^-[1-9]\d*$"; } }
        /// <summary>
        /// 日期时间
        /// </summary>
        public static string datetimeStr { get { return @"^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-)) (20|21|22|23|[0-1]?\d):[0-5]?\d:[0-5]?\d$"; } }
        public static string dateStr { get { return @"\d{2,4}-\d{1,2}-\d{1,2}"; } }
    }
}
