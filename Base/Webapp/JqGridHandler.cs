﻿using System.Web;
namespace Webapp
{
    /// <summary>
    /// 在服务器端统一获取客户端查询参数并统一处理，返回分页信息供BLL的GetLists使用
    /// </summary>
    public class JqGridHandler
    {
        private int _p;

        public int P
        {
            get { return _p; }
            set { _p = value; }
        }
        private int _recordcount;

        public int Recordcount
        {
            get { return _recordcount; }
            set { _recordcount = value; }
        }
        private string _strWhere;

        public string StrWhere
        {
            get { return _strWhere; }
            set { _strWhere = value; }
        }
        private int _pagesize;

        public int Pagesize
        {
            get { return _pagesize; }
            set { _pagesize = value; }
        }
        private string _order;

        public string Order
        {
            get { return _order; }
            set { _order = value; }
        }
        private int _totalPage;

        public int TotalPage
        {
            get { return _totalPage; }
            set { _totalPage = value; }
        }

        private int _startIndex;

        public int StartIndex
        {
            get { return _startIndex; }
            set { _startIndex = value; }
        }
        private int _endIndex;

        public int EndIndex
        {
            get { return _endIndex; }
            set { _endIndex = value; }
        }
        public JqGridHandler(string strWhere)
        {
            if (strWhere.Trim() == "") strWhere = " 1=1 ";
            //string searchField = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["searchField"]));
            //string searchString = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["searchString"]));
            //string searchOper = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["searchOper"]));
            //string jqGridWhere = Oper(searchOper, searchField, searchString);

            //if (jqGridWhere != "")
            //    _strWhere = strWhere + " AND " + jqGridWhere;
            //else
            _strWhere = strWhere;
            ///
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strWhere"></param>
        /// <param name="page">第几页</param>
        /// <param name="recordcount">共多少条记录</param>
        /// <param name="row">每页多少条记录</param>
        /// <param name="sidx">排序的字段,如果为空默认处理浏览器参数：sort order</param>
        /// <param name="sord">desc或空</param>
        public void init(string strWhere, int page, int recordcount, int row, string sidx, string sord)
        {
            ///jqGrid搜索条件
            _p = page;
            if (sidx != "") _order = sidx + " " + sord;
            else
            {
                //处理默认排序
                string sort = (StringUtil.GetNullToString(HttpContext.Current.Request["sort"]));
                string order = (StringUtil.GetNullToString(HttpContext.Current.Request["order"]));
                _order = "";
                if (sort != "")
                {
                    if (sort.IndexOf(",") > -1)
                    {
                        string[] sorts = sort.Split(',');
                        string[] orders = order.Split(',');
                        for (int i = 0; i < sorts.Length; i++)
                        {
                            if (sorts[i] == "") continue;
                            if (_order != "") _order += " , ";

                            _order +=StringUtil.TBCode( sorts[i]) + " ";// +orders[i];
                            if (i < orders.Length)
                            {
                                _order +=StringUtil.TBCode( orders[i]);
                            }
                        }
                    }
                    else
                    {
                        _order = StringUtil.TBCode(sort);
                        if (order != "") _order += " " +StringUtil.TBCode( order);
                    }
                }
            }
            if (_p < 1) _p = 1;
            if (_pagesize < 1) _pagesize = 20;
            if (_pagesize > 1000000) _pagesize = 1000000;

            // JqGrid j = new JqGrid();
            _pagesize = row;
            _recordcount = recordcount;
            _totalPage = (_recordcount - 1) / _pagesize + 1;
            _startIndex = (_p - 1) * _pagesize;
            _endIndex = _p * _pagesize;
        }

        /// <summary>
        /// 计算符，在查询的时候用
        /// </summary>
        /// <param name="inputOp"></param>
        /// <returns></returns>
        public static string Oper(string inputOp, string field, string val)
        {
            switch (inputOp)
            {
                case "eq":
                    return field + "='" + val + "'";
                case "ne":
                    return field + "<>'" + val + "'";
                case "lt":
                    return field + "<'" + val + "'";
                case "le":
                    return field + "<='" + val + "'";
                case "gt":
                    return field + ">'" + val + "'";
                case "ge":
                    return field + ">='" + val + "'";
                case "bw"://开始于
                    return field + " LIKE '%" + val + "'";
                case "bn"://不开始于
                    return field + " NOT LIKE '%" + val + "'";
                case "in"://属于
                    return field + " LIKE '%" + val + "%'";
                case "ni"://不属于
                    return field + " NOT LIKE '%" + val + "%'";
                case "ew"://结束于
                    return field + " LIKE '" + val + "%'";
                case "en"://不结束于
                    return field + " NOT LIKE '" + val + "%'";
                case "cn"://包含
                    return field + " LIKE '%" + val + "%'";
                case "nc"://不包含
                    return field + " NOT LIKE '%" + val + "%'";
                default:
                    return "";
            }
        }
    }
}