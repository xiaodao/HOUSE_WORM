﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace Webapp
{
    public class PassMD5
    {
        /// <summary>
        /// 得到MD5加密的值
        /// </summary>
        /// <param name="ps"></param>
        /// <returns></returns>
        public static string getMD5(string ps)
        {
            string pass = "";
            try
            {
                if (ps == "") return "";
                byte[] result = Encoding.Default.GetBytes(ps);
                MD5 md5 = MD5.Create();
                //MD5 md5 = new MD5CryptoServiceProvider();
                byte[] by = md5.ComputeHash(result);
                
                foreach(byte b in by)
                {
                    pass += b.ToString("X");
                }
            }
            catch { }
            return pass;
        }
    }
}
