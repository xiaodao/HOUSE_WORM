using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Text;

namespace xundh.Common
{
    /// <summary>
    /// Summary description for upload
    /// </summary>
    public class UploadCs 
    {
        public UploadCs()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        /// <summary>
        /// 加文字水印
        /// </summary>
        /// <param name="Path"></param>
        /// <param name="Path_sy"></param>
        public static void AddWater(string Path, string Path_sy)
        {
            string addText = "";
            System.Drawing.Image image = System.Drawing.Image.FromFile(Path);
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(image);
            g.DrawImage(image, 0, 0, image.Width, image.Height);
            System.Drawing.Font f = new System.Drawing.Font("Verdana", 10);
            System.Drawing.Brush b = new System.Drawing.SolidBrush(System.Drawing.Color.Green);

            g.DrawString(addText, f, b, 14, 14);
            g.Dispose();

            image.Save(Path_sy);
            image.Dispose();
        }
        /// 〈summary>
        /// 在图片上生成图片水印
        /// 〈/summary>
        /// 〈param name="Path">原服务器图片路径〈/param>
        /// 〈param name="Path_syp">生成的带图片水印的图片路径〈/param>
        /// 〈param name="Path_sypf">水印图片路径〈/param>
        public static void AddWaterPic(string Path, string Path_syp, string Path_sypf)
        {
            System.Drawing.Image image = System.Drawing.Image.FromFile(Path);
            System.Drawing.Image copyImage = System.Drawing.Image.FromFile(Path_sypf);
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(image);
            g.DrawImage(copyImage, new System.Drawing.Rectangle(image.Width - copyImage.Width, image.Height - copyImage.Height, copyImage.Width, copyImage.Height), 0, 0, copyImage.Width, copyImage.Height, System.Drawing.GraphicsUnit.Pixel);
            g.Dispose();

            image.Save(Path_syp);
            image.Dispose();
        }
        /// 〈summary>
        /// 生成缩略图
        /// 〈/summary>
        /// 〈param name="originalImagePath">源图路径（物理路径）〈/param>
        /// 〈param name="thumbnailPath">缩略图路径（物理路径）〈/param>
        /// 〈param name="width">缩略图宽度〈/param>
        /// 〈param name="height">缩略图高度〈/param>
        /// 〈param name="mode">生成缩略图的方式〈/param>    
        public static void MakeThumbnail(string originalImagePath, string thumbnailPath, int width, int height, string mode)
        {
            System.Drawing.Image originalImage = System.Drawing.Image.FromFile(originalImagePath);

            int towidth = width;
            int toheight = height;

            int x = 0;
            int y = 0;
            int ow = originalImage.Width;
            int oh = originalImage.Height;

            switch (mode)
            {
                case "HW"://指定高宽缩放（可能变形）                
                    break;
                case "W"://指定宽，高按比例                    
                    toheight = originalImage.Height * width / originalImage.Width;
                    break;
                case "H"://指定高，宽按比例
                    towidth = originalImage.Width * height / originalImage.Height;
                    break;
                case "Cut"://指定高宽裁减（不变形）                
                    if ((double)originalImage.Width / (double)originalImage.Height > (double)towidth / (double)toheight)
                    {
                        oh = originalImage.Height;
                        ow = originalImage.Height * towidth / toheight;
                        y = 0;
                        x = (originalImage.Width - ow) / 2;
                    }
                    else
                    {
                        ow = originalImage.Width;
                        oh = originalImage.Width * height / towidth;
                        x = 0;
                        y = (originalImage.Height - oh) / 2;
                    }
                    break;
                default:
                    break;
            }

            //新建一个bmp图片
            System.Drawing.Image bitmap = new System.Drawing.Bitmap(towidth, toheight);

            //新建一个画板
            System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);

            //设置高质量插值法
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;

            //设置高质量,低速度呈现平滑程度
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;

            //清空画布并以透明背景色填充
            g.Clear(System.Drawing.Color.Transparent);

            //在指定位置并且按指定大小绘制原图片的指定部分
            g.DrawImage(originalImage, new System.Drawing.Rectangle(0, 0, towidth, toheight),
                new System.Drawing.Rectangle(x, y, ow, oh),
                System.Drawing.GraphicsUnit.Pixel);

            try
            {
                //以jpg格式保存缩略图
                bitmap.Save(thumbnailPath, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (System.Exception e)
            {
                throw e;
            }
            finally
            {
                originalImage.Dispose();
                bitmap.Dispose();
                g.Dispose();
            }
        }
        /// <summary>
        /// 获得文件夹大小
        /// </summary>
        /// <param name="dirp"></param>
        /// <returns></returns>
        public static int getsize(string dirp)
        {
            DirectoryInfo mydir = new DirectoryInfo(dirp);
            int str;
            str = 0;
            if (mydir.Exists)
            {
                foreach (FileSystemInfo fsi in mydir.GetFileSystemInfos())
                {
                    if (fsi is FileInfo)
                    {
                        FileInfo fi = (FileInfo)fsi;
                        str += int.Parse(fi.Length.ToString());
                    }
                    else
                    {
                        DirectoryInfo di = (DirectoryInfo)fsi;
                        string new_dir = di.FullName;
                        getsize(new_dir);
                    }
                }
            }
            else
                str = -1;
            return str;
        }
        /// <summary>
        /// 上传一个文件
        /// </summary>
        /// <param name="fu"></param>
        /// <param name="isPic"></param>
        /// <param name="limitsize"></param>
        /// <param name="treePath">上传路径,相对路径</param>
        /// <param name="phsictreepath">上传路径,物理路径</param>
        /// <param name="waterpath">水印路径</param>
        /// <param name="sourname">原文件名,如果重名就覆盖</param>
        /// <returns></returns>
        public static string upOneFile(FileUpload fu, string treePath, string phsictreepath, Int32 limitsize, string sourname, string waterpath, bool isPic)
        {
            ///实现的逻辑
            ///删除原位置,在新的位置另建一个,不删除文件夹,只删除文件
            
            string fileContentType = fu.PostedFile.ContentType;

            string name = fu.PostedFile.FileName;  //客户端名称连路径
            FileInfo file = new FileInfo(name);             //服务器端路径
            int upFileLen = fu.PostedFile.ContentLength;

            string fileName = file.Name;    //文件名称
            //如果是图片就自动生成文件名
            string extname = Path.GetExtension(name).ToLower();
            if(extname==".gif" || extname==".jpg" || extname=="jpeg" || extname=="bmp" || extname=="png")
                fileName = System.DateTime.Now.ToString("yyyyMMddhhmmss") + (new Random().Next(20)) + extname;

            //服务器文件路径,将要写的文件路径带文件名
            string webFilePath;

            //计算目前文件夹已使用的大小
            int currentSize = UploadCs.getsize(phsictreepath);
            if (currentSize < 0)
            {
                //文件夹不存在
                //创建文件夹
                DirectoryInfo di = Directory.CreateDirectory(phsictreepath);
            }

            ///sourname是相对路径,要转为物理路径
            string phsicsourname ;
            try
            {
                phsicsourname = HttpContext.Current.Server.MapPath(sourname);
                if (File.Exists(phsicsourname))
                    File.Delete(phsicsourname);
            }
            catch
            {
                sourname = "";
                phsicsourname = "";
            }
            string phsicwebFilePath;    //要存的物理路径带文件名
            phsicwebFilePath = phsictreepath + fileName;
            webFilePath = treePath + "/" + fileName;

            if ((currentSize + upFileLen < limitsize) || limitsize<0)
            {
                try
                {
                    fu.SaveAs(phsicwebFilePath);    //存到绝对路径上
                    //取得水印
                    if (waterpath != "" && isPic)
                    {
                        try
                        {

                        //分离文件路径和文件名
                            string filepath = phsicwebFilePath.Substring(0, phsicwebFilePath.LastIndexOf("\\"));
                            string filename = phsicwebFilePath.Substring(phsicwebFilePath.LastIndexOf("\\") + 1,
                                phsicwebFilePath.Length - phsicwebFilePath.LastIndexOf("\\") - 1);
                            string tempphsicFilePath = filepath + "\\s_" + filename;
                            UploadCs.AddWaterPic(phsicwebFilePath, tempphsicFilePath, waterpath);
                            File.Replace(tempphsicFilePath, phsicwebFilePath, null);
                            File.Delete(tempphsicFilePath);
                        }
                        catch
                        {

                        }
                    }
                    else if (waterpath != "")
                    {
                        fu.SaveAs(phsicwebFilePath);
                    }
                    //uploadCs.MakeThumbnail(webSourceFilePath, webFilePath_s, imageW, imageH, "W");
                }
                catch
                {
                    return "";
                }
             return webFilePath;
            }
            else
                return "";
        }

        /// <summary>
        /// PDF转SWF
        /// </summary>
        /// <param name="pdfPath"></param>
        /// <param name="swfPath"></param>
        public static void ConvertToSwf(string exe, string pdfPath, string swfPath)
        {
            //try
            //{
                //string exe = HttpContext.Current.Server.MapPath("PDF2SWF/pdf2swf.exe");
                //string exe = @"d:\SWFTools\pdf2swf.exe ";
                //string exe = Server.MapPath("pdf2swf.exe");//获取转换工具的安装路径
                if (!File.Exists(exe))
                {
                    throw new ApplicationException("Can not find: " + exe);
                }
                StringBuilder sb = new StringBuilder();
                sb.Append(" -o \"" + swfPath + "\"");//output  
                sb.Append(" -z");
                sb.Append(" -s flashversion=9");//flash version  
                sb.Append(" -s disablelinks");//禁止PDF里面的链接  
                //sb.Append(" -p " + "1" + "-" + page);//page range  
                sb.Append(" -j 100");//Set quality of embedded jpeg pictures to quality. 0 is worst (small), 100 is best (big). (default:85)  
                sb.Append(" \"" + pdfPath + "\"");//input  
                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                proc.StartInfo.FileName = exe;
                proc.StartInfo.Arguments = sb.ToString();
                proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                proc.Start();
                proc.WaitForExit();
                proc.Close();
            //}

            //catch (Exception ex)
            //{
            //    throw ex;

            //}
        }

    }
}