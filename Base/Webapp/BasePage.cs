﻿using System;
using System.Collections;
using System.Web;

namespace xundh.Common
{
    public partial class BasePage : System.Web.UI.Page
    {
        //private JiaYuanERP.Model.ts_admin _adminModel;

        //public JiaYuanERP.Model.ts_admin AdminModel
        //{
        //    get { return _adminModel; }
        //    set { _adminModel = value; }
        //}
        //private JiaYuanERP.Model.tb_company _companyModel;

        //public JiaYuanERP.Model.tb_company CompanyModel
        //{
        //    get { return _companyModel; }
        //    set { _companyModel = value; }
        //}

        //private JiaYuanERP.Model.tb_employee _employeeModel;

        //public JiaYuanERP.Model.tb_employee EmployeeModel
        //{
        //    get { return _employeeModel; }
        //    set { _employeeModel = value; }
        //}
        //private JiaYuanERP.Model.ts_role _roleModel;

        //public JiaYuanERP.Model.ts_role RoleModel
        //{
        //    get { return _roleModel; }
        //    set { _roleModel = value; }
        //}

        //protected internal Model.siteconfig config = new BLL.siteconfig().loadConfig(Utils.GetXmlMapPath(DTKeys.FILE_SITE_XML_CONFING), true);
        //protected internal Model.userconfig uconfig = new BLL.userconfig().loadConfig(Utils.GetXmlMapPath(DTKeys.FILE_USER_XML_CONFING));
        /// <summary>
        /// 父类的构造函数
        /// </summary>
        public BasePage()
        {
            this.Init += new EventHandler(UserPage_Init); //加入IInit事件
        }
        /// <summary>
        /// 页面处理虚方法
        /// </summary>
        protected virtual void ShowPage()
        {
            //虚方法代码
        }

        /// <summary>
        /// OnInit事件,检查用户是否登录
        /// </summary>
        void UserPage_Init(object sender, EventArgs e)
        {
            //IsUserLogin();
            //if (!IsUserLogin())
            //{
            //    //跳转URL
            //    HttpContext.Current.Response.Redirect("login.htm");
            //    return;
            //}
            //_adminModel =s_user.GetAdminModel();
            //_companyModel = s_user.GetCompanyModel();
            //_roleModel = s_user.GetRoleModel();
            //_employeeModel = s_user.GetEmployeeModel();
            //if (_adminModel == null) HttpContext.Current.Response.Redirect("/login.htm");
        }

        #region 通用处理方法
        /// <summary>
        /// 判断用户是否已经登录(解决Session超时问题)
        /// </summary>
        //public void IsUserLogin()
        //{
        //    //如果Session为Null
        //    if (HttpContext.Current.Session[Enums.SESSIONS.ADMIN.ToString()] != null)
        //    {
        //        // return true;
        //    }
        //    else
        //    {
        //        //跳转URL
        //        HttpContext.Current.Response.Redirect("/login.htm");
        //    }

        //}

        /// <summary>
        /// 取得用户信息
        /// </summary>
        //public Model.users GetUserInfo()
        //{
        //    if (IsUserLogin())
        //    {
        //        Model.users model = HttpContext.Current.Session[DTKeys.SESSION_USER_INFO] as Model.users;
        //        if (model != null)
        //        {
        //            //为了能查询到最新的用户信息，必须查询最新的用户资料
        //            model = new BLL.users().GetModel(model.id);
        //            return model;
        //        }
        //    }
        //    return null;
        //}
        #endregion


    }
}
