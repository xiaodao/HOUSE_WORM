﻿using System;
using System.Data;

namespace Webapp
{
    /// <summary>
    /// 服务器端API下不直接返回DataTable，而使用本类，封装分页等信息
    /// </summary>
    [Serializable()]
    public class JsonTable
    {
        private DataTable _dt;
        private int _page;

        public int Page
        {
            get { return _page; }
            set { _page = value; }
        }
        private int _total;
        private int _records;

        public int Records
        {
            get { return _records; }
            set { _records = value; }
        }
        

        public DataTable Table
        {
            get { return _dt; }
            set { _dt = value; }
        }

        public int Total
        {
            get { return _total; }
            set { _total = value; }
        }
        public JsonTable()
        {
        }
        public JsonTable(DataTable dt, int total)
        {
            _page = 1;
            _records = total;
            _dt = dt;
            _total = total;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="total">总页数</param>
        /// <param name="page">第几页</param>
        /// <param name="records">总记录数</param>
        public JsonTable(DataTable dt, int total,int page,int records)
        {
            _page = page;
            _records = records;
            _dt = dt;
            _total = total;
        }
    }
}