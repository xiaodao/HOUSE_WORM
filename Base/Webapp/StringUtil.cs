﻿using System;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
namespace Webapp
{
    /// <summary>
    /// 黄建华（2007.9）
    /// 字符串的处理，字符的转换
    /// </summary>
    public static class StringUtil
    {

        /// <summary>
        /// add by shan 防止sql注入
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CutBadSqlInfo(string str)
        {
            str = str.Replace("add", "");
            str = str.Replace("exec", "");
            str = str.Replace("master", "");
            str = str.Replace("xp_cmdshell", "");
            str = str.Replace("localgroup", "");
            str = str.Replace("net", "");
            str = str.Replace("administrators", "");
            str = str.Replace("select", "");
            str = str.Replace("count", "");
            str = str.Replace("Asc", "");
            str = str.Replace("char", "");
            str = str.Replace("mid", "");
            str = str.Replace("insert", "");
            str = str.Replace("localgroup", "");
            str = str.Replace("delete", "");
            str = str.Replace("from", "");
            str = str.Replace("drop", "");
            str = str.Replace("table", "");
            str = str.Replace("update", "");
            str = str.Replace("truncate", "");
            str = str.Replace("%", "");
            str = str.Replace("´", "");
            str = str.Replace("&", "");
            return str;
        }
        /// <summary>
        /// 字符传的转换 用在查询 登陆时 防止恶意的盗取密码
        /// </summary>
        /// <param name="strtb"></param>
        /// <returns></returns>
        public static string TBCode(string strtb)
        {
            strtb = strtb.Replace("!", "");
            strtb = strtb.Replace("@", "");
            strtb = strtb.Replace("#", "");
            strtb = strtb.Replace("$", "");
            strtb = strtb.Replace("%", "");
            strtb = strtb.Replace("^", "");
            strtb = strtb.Replace("&", "");
            strtb = strtb.Replace("*", "");
            strtb = strtb.Replace("(", "");
            strtb = strtb.Replace(")", "");
            strtb = strtb.Replace("_", "");
            strtb = strtb.Replace("+", "");
            strtb = strtb.Replace("|", "");
            strtb = strtb.Replace("?", "");
            strtb = strtb.Replace("/", "");
            strtb = strtb.Replace(".", "");
            strtb = strtb.Replace(">", "");
            strtb = strtb.Replace("<", "");
            strtb = strtb.Replace("{", "");
            strtb = strtb.Replace("}", "");
            strtb = strtb.Replace("[", "");
            strtb = strtb.Replace("]", "");
            strtb = strtb.Replace("-", "");
            strtb = strtb.Replace("=", "");
            strtb = strtb.Replace(",", "");
            return strtb;
        }
        #region 过滤特殊字符
        /// <summary>
        /// 过滤特殊字符
        /// </summary>
        /// <param name="inputStr">字符串</param>
        /// <returns>string</returns>
        public static string cutBadStr(string inputStr)
        {
            if (inputStr == null)
                inputStr = "";

            inputStr = inputStr.ToLower().Replace("<", "&lt;");
            inputStr = inputStr.ToLower().Replace(">", "&gt;");
            inputStr = inputStr.ToLower().Replace("%", "");
            inputStr = inputStr.ToLower().Replace(".", "．");
            inputStr = inputStr.ToLower().Replace(":", "");
            inputStr = inputStr.ToLower().Replace("#", "");
            inputStr = inputStr.ToLower().Replace("&", "");
            inputStr = inputStr.ToLower().Replace("$", "");
            inputStr = inputStr.ToLower().Replace("^", "");
            inputStr = inputStr.ToLower().Replace("*", "");
            inputStr = inputStr.ToLower().Replace("`", "");
            inputStr = inputStr.ToLower().Replace(" ", "");
            inputStr = inputStr.ToLower().Replace("~", "");
            inputStr = inputStr.ToLower().Replace("or", "");
            inputStr = inputStr.ToLower().Replace("and", "");
            return inputStr;

        }
        #endregion

        #region 对,33,33,33数据进行处理，字符串的扩展方法。
        //第一个参数必须加上this
        public static bool ItemIsContain(this string _input, string item)
        {
            string source = "," + _input + ",";
            item = "," + item + ",";
            return source.IndexOf(item) > -1;
        }
        public static bool ItemIsContain(this string _input, int item)
        {
            string source = "," + _input + ",";
            string str = "," + item.ToString() + ",";
            return source.IndexOf(str) > -1;
        }

        /// <summary>
        /// 向,323,里加新的
        /// </summary>
        /// <param name="source"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string ItemAdd(this string _input, string item)
        {
            string[] itemArray = item.Split(',');
            string source = "," + _input + ",";

            foreach (string s in itemArray)
            {
                if (source.IndexOf("," + s + ",") > -1)
                {
                }
                else
                {
                    source = source + s + ',';
                }
            }
            return source.Trim(',');
        }
        /// <summary>
        /// 向,323,里加新的，向_input里加item
        /// </summary>
        /// <param name="source"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string ItemAdd(this string _input, int item)
        {
            string source = "," + _input + ",";
            if (source.IndexOf("," + item + ",") > -1)
            {
            }
            else
            {
                source = source + item;
            }
            return source.Trim(',');
        }
        /// <summary>
        /// 向,323,里加新的
        /// </summary>
        /// <param name="source"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string ItemRemove(this string _input, string item)
        {
            string[] itemArray = item.Split(',');
            string source = "," + _input + ",";
            source = source.Replace("," + item + ",", "");
            return source.Trim(',');
        }
        /// <summary>
        /// 向,323,里加新的
        /// </summary>
        /// <param name="source"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string ItemRemove(this string _input, int item)
        {
            string source = "," + _input + ",";
            source = source.Replace("," + item + ",", "");
            return source.Trim(',');
        }
        #endregion
        /// <summary>
        /// 小函数，将字符串转换成float型，若字符串为空，返回0
        /// </summary>
        /// <param name="str">要转换的字符串</param>
        /// <returns>转后的float值</returns>
        public static float GetFloatValue(object o)
        {
            try
            {
                if (o == null)
                    return 0;
                if ((o.ToString()).Length == 0)
                    return 0;
                else
                {
                    if (float.Parse(o.ToString()) == 0)
                        return 0;
                    else
                        return float.Parse(o.ToString());
                }
            }
            catch
            {
                // WriteMessage("数据转换失败，请检查数据是否合理！", true, true);
                return 0;
            }
        }

        /// <summary>
        /// 小函数，将字符串转换成float型，若字符串为空，返回0
        /// </summary>
        /// <param name="str">要转换的字符串</param>
        /// <returns>转后的float值</returns>
        public static decimal GetDecimal(object o)
        {
            try
            {
                if (o == null)
                    return 0;
                if ((o.ToString()).Length == 0)
                    return 0;
                else
                {
                    if (decimal.Parse(o.ToString()) == 0)
                        return 0;
                    else
                        return decimal.Parse(o.ToString());
                }
            }
            catch
            {
                // WriteMessage("数据转换失败，请检查数据是否合理！", true, true);
                return 0;
            }
        }
        /// <summary>
        /// 转换成double型
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static double GetDouble(object o)
        {
            try
            {
                if (o == null)
                    return 0;
                if ((o.ToString()).Length == 0)
                    return 0;
                else
                {
                    if (decimal.Parse(o.ToString()) == 0)
                        return 0;
                    else
                        return double.Parse(o.ToString());
                }
            }
            catch
            {
                return 0;
            }
        }


        /// <summary>
        /// 小函数，将字符串转换成int型，若字符串为空，返回0
        /// </summary>
        /// <param name="str">要转换的字符串</param>
        /// <returns>转后的int值</returns>
        public static int GetIntValue(object o)
        {
            try
            {
                if (o == null)
                    return 0;
                if ((o.ToString()).Length == 0)
                    return 0;
                else
                {
                    if (Int32.Parse(o.ToString()) == 0)
                        return 0;
                    else
                        return Int32.Parse(o.ToString());
                }
            }
            catch
            {
                // WriteMessage("数据转换失败，请检查数据是否合理！", true, true);
                return 0;
            }
        }
        /// <summary>
        /// 小函数，将字符串转换成int型，若字符串为空，返回0
        /// </summary>
        /// <param name="str">要转换的字符串</param>
        /// <returns>转后的int值</returns>
        public static int Get_1Value(object o)
        {
            try
            {
                if (o == null)
                    return -1;
                if ((o.ToString()).Length == 0)
                    return -1;
                else
                {
                    if (Int32.Parse(o.ToString()) == 0)
                        return 0;
                    else
                        return Int32.Parse(o.ToString());
                }
            }
            catch
            {
                // WriteMessage("数据转换失败，请检查数据是否合理！", true, true);
                return -1;
            }
        }
        /// <summary>
        /// 小函数，将字符串转换成int型，若字符串为空，返回0
        /// </summary>
        /// <param name="str">要转换的字符串</param>
        /// <returns>转后的int值</returns>
        public static long GetLongValue(object o)
        {
            try
            {
                if (o == null) return 0;
                if ((o.ToString()).Length == 0)
                    return 0;
                else
                {
                    if (long.Parse(o.ToString()) == 0)
                        return 0;
                    else
                        return long.Parse(o.ToString());
                }
            }
            catch
            {
                // WriteMessage("数据转换失败，请检查数据是否合理！", true, true);
                return 0;
            }
        }
        /// <summary>
        /// 函数，是null就返回空，否则返回原字符串
        /// </summary>
        /// <param name="str">需要处理的字符串</param>
        /// <returns>转换后的值</returns>
        public static string GetNullToString(object o)
        {
            try
            {
                if (o == null)
                    return "";
                else
                    return o.ToString();
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 转换成日期，如果为空就返回当前日期
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static DateTime GetNullToDate(object o)
        {
            try
            {
                if (o == null)
                    return DateTime.Now;
                else
                    return (DateTime.Parse(o.ToString()));
            }
            catch
            {
                //WriteMessage("失败", true, true);
                return DateTime.Now;
            }
        }
        public static Boolean isDate(string s)
        {
            string reg =RegularKeys.dateStr;
            Regex re = new Regex(reg);
            return (re.IsMatch(s));
        }
        /// <summary>
        /// 是空或字母就返回０,否则返回数字
        /// </summary>
        /// <param name="str">需要处理的字符串</param>
        /// <return>转换后的值</return>
        public static Int32 isNum(string str)
        {
            if (str == null)
                return 0;
            else if (IsNumBool(str))
                return Int32.Parse(str);
            else
                return 0;
        }

        /// <summary>
        /// 判断是否是正整数
        /// </summary>
        /// <param name="str>需要处理的字符串</param>
        /// <return>转换后的值</return>
        public static bool IsPInt(string inString)
        {
            Regex regex = new Regex("^[0-9]*[1-9][0-9]*$");
            return regex.IsMatch(inString.Trim());
        }
        public static string GetShortDate(DateTime? d)
        {
            try
            {
                return DateTime.Parse(d.ToString()).ToShortDateString();
            }
            catch (Exception) { return ""; }
        }
        /// <summary>
        /// 判断是否是整数
        /// </summary>
        /// <param name="str>需要处理的字符串</param>
        /// <return>转换后的值</return>
        public static bool IsInt(string inString)
        {
            Regex regex = new Regex("^-?\\d+$");
            return regex.IsMatch(inString.Trim());
        }

        /// <summary>
        /// 判断是否是数字
        /// </summary>
        /// <param name="str>需要处理的字符串</param>
        /// <return>转换后的值</return>
        public static bool IsNumBool(string str)
        {
            Regex regex = new Regex("^[0-9]+$");
            return regex.IsMatch(str);
        }

        /// <summary>
        /// 判断是否是手机号码
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static bool IsMobilePhone(string input)
        {
            if (input=="")
            {
                return true;
            }
            Regex regex = new Regex(@"\d{4}-?\d{4,}");
            return regex.IsMatch(input);
        }
        ///<summary>
        ///截字符串，汉字一个算两个字符，英文算一个字符,功能未完善
        /// </summary>
        ///<param name="str">原字符串</param>
        ///<param name="strlen">截取长度</param>
        ///<return>截取后的字符串</return>

        public static string getLeftn(string inputString, int len)
        {
            if (len < inputString.Length)
                inputString = inputString.Substring(0, len);
            return inputString;
        }


        ///<summary>
        ///清除字符HTML语句
        /// </summary>
        ///<param name="str">原字符串</param>
        ///<return>截取后的字符串</return>
        public static string NoHTML(string Htmlstring)
        {
            //删除脚本  
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML  
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([\r\n])[\s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(\d+);", "", RegexOptions.IgnoreCase);
            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("\r\n", "");
            Htmlstring = HttpContext.Current.Server.HtmlEncode(Htmlstring).Trim();
            return Htmlstring;
        }

        /// <summary>
        /// 重复字符串
        /// </summary>
        /// <param name="str"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string RepeatString(string str, int n)
        {
            char[] arr = str.ToCharArray();
            char[] arrDest = new char[arr.Length * n];
            for (int i = 0; i < n; i++)
            {
                Buffer.BlockCopy(arr, 0, arrDest, i * arr.Length * 2, arr.Length * 2);
            }
            return new string(arrDest);
        }

        #region 生成由日期组成的唯一的文件名
        /// <summary>
        /// 生成由日期组成的唯一的文件名
        /// </summary>
        /// <returns>string</returns>
        /// 
        public static string makeFileName()
        {
            string newFileName;
            string dateName = System.DateTime.Now.ToString("yyyyMMddhhmmss");
            System.Random srd = new Random();
            int srdName = srd.Next(1000);
            newFileName = dateName + srdName.ToString();
            return newFileName;
        }
        #endregion

        #region 转换成html
        public static string htmlStr(string strContent)
        {
            //strContent = strContent.Replace(";", "");
            strContent = strContent.ToLower().Replace("■", ",");
            strContent = strContent.ToLower().Replace("∴", "'");
            //strContent = strContent.ToLower().Replace("∴", "’");
            strContent = strContent.Replace("&lt", "<");
            strContent = strContent.Replace("&gt", ">");
            strContent = strContent.Replace("<;", "<");
            strContent = strContent.Replace(">;", ">");
            //strContent = strContent.Replace("■", ";");
            //strContent = strContent.Replace("&amp", "&");
            //strContent = strContent.Replace("&lt","chr(60)");
            //strContent = strContent.Replace("&gt","chr(37)");
            //strContent = strContent.Replace("&quot","\"");
            //strContent = strContent.Replace("<br />","\n");
            //strContent = strContent.Replace("&nbsp"," ");
            return strContent;
        }
        #endregion

        #region 过滤html标记
        /// <summary>
        /// 过滤html标记
        /// </summary>
        /// <param name="HTMLStr">要过滤的字符串</param>
        /// <returns>string</returns>
        /// 

        public static string CutHTML(string strHtml)
        {
            Regex htmlReg = new Regex(@"<[^>]+>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex htmlSpaceReg = new Regex("\\&nbsp\\;", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex spaceReg = new Regex("\\s{2,}|\\ \\;", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex styleReg = new Regex(@"<style(.*?)</style>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            Regex scriptReg = new Regex(@"<script(.*?)</script>", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            strHtml = styleReg.Replace(strHtml, string.Empty);
            strHtml = scriptReg.Replace(strHtml, string.Empty);
            strHtml = htmlReg.Replace(strHtml, string.Empty);
            strHtml = htmlSpaceReg.Replace(strHtml, " ");
            strHtml = spaceReg.Replace(strHtml, " ");
            return strHtml.Trim();
        }
        #endregion

        #region 标题固定长度


        /// <summary>
        /// <table style="font-size:12px">
        /// <tr><td><b>功能描述</b>：填充或截断原始字符串为指定长度 </td></tr>
        /// <tr><td><b>创 建 人</b>： </td></tr>
        /// <tr><td><b>创建时间</b>：</td></tr>
        /// </table>
        /// </summary>
        /// <param name="strOriginal">原始字符串</param>
        /// <param name="maxTrueLength">字符串的字节长度</param>
        /// <param name="chrPad">填充字符</param>
        /// <param name="blnCutTail">字符串的字节长度超过maxTrueLength时截断多余字符</param>
        /// <returns>填充或截断后的字符串</returns>
        static public string PadRightTrueLen(string strOriginal, int maxTrueLength, char chrPad, bool blnCutTail)
        {
            string strNew = strOriginal;

            if (strOriginal == null || maxTrueLength <= 0)
            {
                strNew = "";
                return strNew;
            }

            int trueLen = TrueLength(strOriginal);
            if (trueLen > maxTrueLength)//超过maxTrueLength
            {
                if (blnCutTail)//截断
                {
                    for (int i = strOriginal.Length - 1; i > 0; i--)
                    {
                        strNew = strNew.Substring(0, i);
                        if (TrueLength(strNew) == maxTrueLength)
                            break;
                        else if (TrueLength(strNew) < maxTrueLength)
                        {
                            strNew += chrPad.ToString();
                            break;
                        }
                    }
                }
            }
            else//填充
            {
                for (int i = 0; i < maxTrueLength - trueLen; i++)
                {
                    strNew += chrPad.ToString();
                }
            }

            return strNew;
        }

        //主方法
        public static string CutStringTitle(string inputString, int i)
        {
            return PadRightTrueLen(inputString, i, ' ', true);
        }

        /// <summary>
        /// <table style="font-size:12px">
        /// <tr><td><b>功能描述</b>：字符串的字节长度 </td></tr>
        /// <tr><td><b>创 建 人</b>： </td></tr>
        /// <tr><td><b>创建时间</b>：</td></tr>
        /// </table>
        /// </summary>
        /// <param name="str">字符串</param>
        /// <returns>字符串的字节长度</returns>
        static public int TrueLength(string str)
        {
            int lenTotal = 0;
            int n = str.Length;
            string strWord = "";
            int asc;
            for (int i = 0; i < n; i++)
            {
                strWord = str.Substring(i, 1);
                asc = Convert.ToChar(strWord);
                if (asc < 0 || asc > 127)
                    lenTotal = lenTotal + 2;
                else
                    lenTotal = lenTotal + 1;
            }

            return lenTotal;
        }
        #endregion

        #region 替换特殊字符
        /// <summary>
        /// 特殊字符串替换
        /// </summary> 
        public static string repString(string strTemp)
        {
            if (strTemp == null)
                strTemp = "";
            strTemp = strTemp.Replace(" ", "");
            strTemp = strTemp.Replace("*", "");
            strTemp = strTemp.Replace("?", "");
            strTemp = strTemp.Replace("#", "");
            strTemp = strTemp.Replace("@", "");
            strTemp = strTemp.Replace("^", "");
            strTemp = strTemp.Replace("&", "");
            strTemp = strTemp.Replace("+", "");
            strTemp = strTemp.Replace("-", "");
            strTemp = strTemp.Replace("(", "");
            strTemp = strTemp.Replace(")", "");
            strTemp = strTemp.Replace("!", "");
            strTemp = strTemp.Replace("`", "");
            strTemp = strTemp.Replace("~", "");
            strTemp = strTemp.Replace("<", "");
            strTemp = strTemp.Replace(">", "");
            strTemp = strTemp.Replace("'", "");
            strTemp = strTemp.Replace("\"", "");
            strTemp = strTemp.Replace("\\", "");
            strTemp = strTemp.Replace("|", "");
            strTemp = strTemp.Replace("=", "");
            strTemp = strTemp.Replace(",", "");
            return strTemp;
        }
        #endregion

        /// <summary>
        /// 读取一个xml的远程文件
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static XmlDocument getUrlXml(string url)
        {
            try
            {
                string s = getUrlString(url, "get");
                XmlTextReader r = new XmlTextReader(new StringReader(s));
                //StringBuilder sb = new StringBuilder();
                //XmlTextWriter w = new XmlTextWriter(new StringWriter(sb));
                //w.WriteNode(r, false);
                //w.Flush();
                XmlDocument xml = new XmlDocument();
                xml.Load(r);
                return xml;
            }
            catch (Exception)
            {
                return null;
            }
        }
        /// <summary>
        /// 取得url的内容
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <returns></returns>
        public static string getUrlString(string url, string method)
        {
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
            req.UseDefaultCredentials = true;
            req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; zh-CN; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12";
            req.Method = method.ToUpper();
            HttpWebResponse result;
            try
            {
                result = (HttpWebResponse)req.GetResponse();
            }
            catch (WebException)
            {
                return "";
            }
            //得到的流是网页内容   
            Stream ReceiveStream = result.GetResponseStream();
            StreamReader readerOfStream = new StreamReader(ReceiveStream,
                            System.Text.Encoding.GetEncoding("GB2312"));

            string aa;
            try
            {
                aa = readerOfStream.ReadToEnd();
            }
            catch (Exception)
            {
                return "";
            }
            return aa;

        }
        #region 删除html格式
        /// <summary>
        /// 清除html特殊字符
        /// </summary>
        /// <param name="strContent"></param>
        /// <returns></returns>
        public static string clearHtml(string strContent)
        {
            strContent = strContent.Replace("&", "");
            strContent = strContent.Replace("´", "");
            strContent = strContent.Replace("<", "");
            strContent = strContent.Replace(">", "");
            strContent = strContent.Replace("chr(60)", "");
            strContent = strContent.Replace("chr(37)", "");
            strContent = strContent.Replace("\"", "");
            strContent = strContent.Replace(";", "");
            strContent = strContent.Replace("\n", "<br/>");
            strContent = strContent.Replace("\\", "");
            return strContent;
        }
        #endregion

        #region md5 加密
        /// <summary>
        /// md5 默认unicode
        /// </summary>
        /// <param name="cleanString"></param>
        /// <returns></returns>
        public static string EncryptMD5(string cleanString)
        {
            Byte[] clearBytes = new UnicodeEncoding().GetBytes(cleanString);
            Byte[] hashedBytes = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(clearBytes);
            return BitConverter.ToString(hashedBytes).Replace("-", "");
        }
        ///   <summary>
        ///   给一个字符串进行MD5加密
        ///   </summary>
        ///   <param   name="strText">待加密字符串</param>
        ///   <returns>加密后的字符串</returns>
        public static string EncryptMD5NotUnicode(string strText)
        {
            string Encrypt = System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(strText, "MD5");
            return Encrypt;
        }

        #endregion

        #region DEC 加密过程
        ///
        public static string Encrypt(string pToEncrypt, string sKey)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();　//把字符串放到byte数组中

            byte[] inputByteArray = Encoding.Default.GetBytes(pToEncrypt);
            //byte[]　inputByteArray=Encoding.Unicode.GetBytes(pToEncrypt);

            des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);　//建立加密对象的密钥和偏移量
            des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);　 //原文使用ASCIIEncoding.ASCII方法的GetBytes方法
            MemoryStream ms = new MemoryStream();　　 //使得输入密码必须输入英文文本
            CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();

            StringBuilder ret = new StringBuilder();
            foreach (byte b in ms.ToArray())
            {
                ret.AppendFormat("{0:X2}", b);
            }
            ret.ToString();
            return ret.ToString();
        }
        #endregion

        #region  DEC 解密过程

        public static string Decrypt(string pToDecrypt, string sKey)
        {
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();

            byte[] inputByteArray = new byte[pToDecrypt.Length / 2];
            for (int x = 0; x < pToDecrypt.Length / 2; x++)
            {
                int i = (Convert.ToInt32(pToDecrypt.Substring(x * 2, 2), 16));
                inputByteArray[x] = (byte)i;
            }

            des.Key = ASCIIEncoding.ASCII.GetBytes(sKey);　//建立加密对象的密钥和偏移量，此值重要，不能修改
            des.IV = ASCIIEncoding.ASCII.GetBytes(sKey);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(), CryptoStreamMode.Write);

            cs.Write(inputByteArray, 0, inputByteArray.Length);
            cs.FlushFinalBlock();

            StringBuilder ret = new StringBuilder();　//建立StringBuild对象，CreateDecrypt使用的是流对象，必须把解密后的文本变成流对象

            return System.Text.Encoding.Default.GetString(ms.ToArray());
        }
        ///其中的sKey非常重要,定义的时候定义成string然后赋值等于八个字母或数字,注意,必须8个
        ///这个也很实用,譬如你想进入文章页面,传入的参数的aid=10000
        ///这时把10000给加密
        ///然后接受的时候解密.这样能有效的防止sql注入攻击!!!Ò®æÙ
        #endregion

        #region DataSet的序列化和反序列化
        /// <summary>      
        /// 序列化DataSet对象并压缩      
        /// </summary>      
        /// <param name="ds"></param>    
        public static void DataSetSerializerCompression(DataSet ds, string savePath, string saveName)
        {
            IFormatter formatter = new BinaryFormatter();//定义BinaryFormatter以序列化DataSet对象   
            MemoryStream ms = new MemoryStream();//创建内存流对象   
            formatter.Serialize(ms, ds);//把DataSet对象序列化到内存流   
            byte[] buffer = ms.ToArray();//把内存流对象写入字节数组   
            ms.Close();//关闭内存流对象   
            ms.Dispose();//释放资源   
            if (!Directory.Exists(savePath))//如果储存文件夹不存在则创建文件夹   
                Directory.CreateDirectory(savePath);
            FileStream fs = File.Create(savePath + saveName);//创建文件   
            GZipStream gzipStream = new GZipStream(fs, CompressionMode.Compress, true);//创建压缩对象   
            gzipStream.Write(buffer, 0, buffer.Length);//把压缩后的数据写入文件   
            gzipStream.Close();//关闭压缩流   
            gzipStream.Dispose();//释放对象   
            fs.Close();//关闭流   
            fs.Dispose();//释放对象   
        }
        /// <summary>      
        /// 不压缩直接序列化DataSet      
        /// </summary>      
        /// <param name="ds"></param>      
        public static void DataSetSerializer(DataSet ds, string savePath, string saveName)
        {
            IFormatter formatter = new BinaryFormatter();//定义BinaryFormatter以序列化DataSet对象   
            if (!Directory.Exists(savePath))//如果储存文件夹不存在则创建文件夹   
                Directory.CreateDirectory(savePath);
            FileStream fs = File.Create(savePath + saveName);//创建文件   
            formatter.Serialize(fs, ds);//把DataSet对象序列化到文件   
            fs.Close();//关闭流   
            fs.Dispose();//释放对象   
        }
        /// <summary>   
        /// 反序列化压缩的DataSet   
        /// </summary>   
        /// <param name="_filePath"></param>   
        /// <returns></returns>   
        public static DataSet DataSetDeserializeDecompress(string _filePath)
        {
            FileStream fs = File.OpenRead(_filePath);//打开文件   
            fs.Position = 0;//设置文件流的位置   
            GZipStream gzipStream = new GZipStream(fs, CompressionMode.Decompress);//创建解压对象   
            byte[] buffer = new byte[4096];//定义数据缓冲   
            int offset = 0;//定义读取位置   
            MemoryStream ms = new MemoryStream();//定义内存流   
            while ((offset = gzipStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                ms.Write(buffer, 0, offset);//解压后的数据写入内存流   
            }
            BinaryFormatter sfFormatter = new BinaryFormatter();//定义BinaryFormatter以反序列化DataSet对象   
            ms.Position = 0;//设置内存流的位置   
            DataSet ds;
            try
            {
                ds = (DataSet)sfFormatter.Deserialize(ms);//反序列化   
            }
            catch
            {
                throw;
            }
            finally
            {
                ms.Close();//关闭内存流   
                ms.Dispose();//释放资源   
            }
            fs.Close();//关闭文件流   
            fs.Dispose();//释放资源   
            gzipStream.Close();//关闭解压缩流   
            gzipStream.Dispose();//释放资源   
            return ds;
        }

        /// <summary>   
        /// 反序列化未压缩的DataSet   
        /// </summary>   
        /// <param name="_filePath"></param>   
        /// <returns></returns>   
        public static DataSet DataSetDeserialize(string _filePath)
        {
            FileStream fs = File.OpenRead(_filePath);//打开文件   
            fs.Position = 0;//设置文件流的位置   
            BinaryFormatter sfFormatter = new BinaryFormatter();//定义BinaryFormatter以反序列化DataSet对象   
            DataSet ds;
            try
            {
                ds = (DataSet)sfFormatter.Deserialize(fs);//反序列化   
            }
            catch
            {
                throw;
            }
            finally
            {
                fs.Close();//关闭内存流   
                fs.Dispose();//释放资源   
            }
            fs.Close();//关闭文件流   
            fs.Dispose();//释放资源   
            return ds;
        }
        #endregion


    }

}