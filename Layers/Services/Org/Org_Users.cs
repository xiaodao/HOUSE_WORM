﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.Org
{
    /// <summary>
    ///Org_Users 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Org_Users : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int UserId, string UserName, int Marry, string HomeAddress, string Mem,
            string School, string Thumb, string EnterDeate, string LevelDate, int State, int Education,
           string BirthDay
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            if (UserId == 0)
            {//添加
                API.RBAC.RBAC_Users serviceRBACUsers = new RBAC.RBAC_Users();
                xundh.Model.RBAC_Users modelRBACUser;
                modelRBACUser = serviceRBACUsers.QueryName(UserName);
                if (modelRBACUser == null)
                    return ServiceStateKeys.NotExists;
                else
                    UserId = modelRBACUser.UserId;
            }
            ///参数处理
            HomeAddress = StringUtil.CutBadSqlInfo(HomeAddress);
            Mem = StringUtil.CutBadSqlInfo(Mem);
            School = StringUtil.CutBadSqlInfo(School);
            Thumb = StringUtil.CutBadSqlInfo(Thumb);

            xundh.BLL.Org_Users bll = new xundh.BLL.Org_Users();
            xundh.Model.Org_Users m;

            bool isAdd = false;
            m = bll.GetModel(UserId);
            if (m == null)
            {
                m = new Model.Org_Users();
                isAdd = true;
            }
            m.UserId = UserId;
            m.Marry = Marry;
            m.HomeAddress = HomeAddress;
            m.Mem = Mem;
            m.School = School;
            m.Thumb = Thumb;
            m.LastTime = NOW;
            m.LastUserId = U.UserId;
            m.LastUser = U.UserName;
            m.Education = Education;
            if (StringUtil.isDate(BirthDay))
                m.BirthDay = DateTime.Parse(BirthDay);
            if (StringUtil.isDate(EnterDeate))
                m.EnterDeate = DateTime.Parse(EnterDeate);
            if (StringUtil.isDate(LevelDate))
                m.LevelDate = DateTime.Parse(LevelDate);
            m.State = State;
            if (!isAdd)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string username = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(REQUEST["txtUserName"]));
            int DepartId = StringUtil.GetIntValue(REQUEST["txtDeparts"]);
            int userid = StringUtil.GetIntValue(REQUEST["UserId"]);
            if (username != "")
            {
                string condition = "'%" + username + "%'";
                where += " AND (UserName LIKE " + condition + " OR UserMobile LIKE " + condition + " OR UserEmail LIKE " + condition + ")";
            }
            if (DepartId > 0)
            {
                API.Org.Org_Departs serviceOrgDeparts = new Org_Departs();
                string temp = serviceOrgDeparts.GetStringsDepartsAndSons(DepartId);
                temp = temp.ItemAdd(DepartId.ToString());
                where += " AND DepartId in (" + DepartId + ")";
            }
            if (userid > 0)
                where += " AND UserId=" + userid;
            string table = "v_Org_Users_RBAC";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
            string cols = "UserId,UserName,UserSex,Marry,HomeAddress,Mem,School,Thumb,EnterDeate,LevelDate,State,UserState,DepartId,UserMobile,UserEmail";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "UserId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Org_Users Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Org_Users bll = new xundh.BLL.Org_Users();
            xundh.Model.Org_Users m = bll.GetModel(id);
            API.RBAC.RBAC_Users servicerbacusers = new RBAC.RBAC_Users();
            m.UserName = servicerbacusers.Query(id).UserName;
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Org_Users bll = new xundh.BLL.Org_Users();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }

}