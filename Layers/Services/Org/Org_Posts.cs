﻿using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
using System;
using System.Collections.Generic;
namespace xundh.API.Org
{
    /// <summary>
    ///Org_Posts 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Org_Posts : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int Postid, string Mem, string PostName, bool IsDelete
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理
            Mem = StringUtil.CutBadSqlInfo(Mem);
            PostName = StringUtil.CutBadSqlInfo(PostName);

            xundh.BLL.Org_Posts bll = new xundh.BLL.Org_Posts();
            xundh.Model.Org_Posts m;

            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();

            if (Postid > 0)
            {
                if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "职位", "改")) return ServiceStateKeys.NoPermiss;
                m = bll.GetModel(Postid);
            }
            else
            {
                if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "职位", "增")) return ServiceStateKeys.NoPermiss;
                m = new xundh.Model.Org_Posts();
                m.AddUserId = mUser.UserId;
                m.AddUser = mUser.UserName;
                m.AddDepartId = mUser.DepartId;
                m.AddTime = now;
                m.AddIp = ip;
            }
            m.LastUserId = mUser.UserId;
            m.LastUser = mUser.UserName;
            m.LastDepartId = mUser.DepartId;
            m.LastTime = now;
            m.LastIp = ip;
            m.Mem = Mem;
            m.PostName = PostName;
            m.PostNameEn = ChineseForFirstCode.IndexCode(PostName);
            m.IsDelete = IsDelete;

            if (Postid > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];

            xundh.BLL.Org_Posts bll = new xundh.BLL.Org_Posts();
            using (DataSet ds = bll.GetList(int.MaxValue, "", "PostId"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet=true)]
        public xundh.Model.Org_Posts Query(int id)
        {
            if (U == null) return null;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            xundh.BLL.Org_Posts bll = new xundh.BLL.Org_Posts();
            xundh.Model.Org_Posts m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.Org_Posts QueryName(string post)
        {
            if (U == null) return null;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];

            xundh.BLL.Org_Posts bll = new xundh.BLL.Org_Posts();
            List<xundh.Model.Org_Posts> ms = bll.GetModelList("PostName='" + post + "'");
            if (ms.Count > 0) return ms[0];
            return null;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "职位管理", "删")) return ServiceStateKeys.NoPermiss;

            xundh.BLL.Org_Posts bll = new xundh.BLL.Org_Posts();
            xundh.Model.Org_Posts m = bll.GetModel(id);
            m.IsDelete = true;
            return ServiceStateKeys.Success;
        }

    }
}