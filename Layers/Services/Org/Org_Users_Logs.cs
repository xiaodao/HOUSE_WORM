﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.Org
{
    /// <summary>
    ///Org_Users_Logs 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Org_Users_Logs : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int LogId, int UserId, int PostId, int DepartId
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理

            xundh.BLL.Org_Users_Logs bll = new xundh.BLL.Org_Users_Logs();
            xundh.Model.Org_Users_Logs m;
            if (LogId > 0)
                m = bll.GetModel(LogId);
            else
                m = new xundh.Model.Org_Users_Logs();
            m.UserId = UserId;
            m.PostId = PostId;
            m.DepartId = DepartId;
            if (LogId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "Org_Users_Logs";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "LogId,UserId,PostId,DepartId";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "LogId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.Org_Users_Logs Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Org_Users_Logs bll = new xundh.BLL.Org_Users_Logs();
            xundh.Model.Org_Users_Logs m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Org_Users_Logs bll = new xundh.BLL.Org_Users_Logs();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}