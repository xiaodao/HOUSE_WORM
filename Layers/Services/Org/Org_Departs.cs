﻿using EnumKeys;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
namespace xundh.API.Org
{
    /// <summary>
    ///Org_Departs 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class Org_Departs : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int Org_DepartsId, int ParentId, string DepartName, string Telphone, string IsDelete, 
            string IsVirtual, string Mem
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            DepartName = StringUtil.CutBadSqlInfo(DepartName);
            IsDelete = StringUtil.TBCode(IsDelete);
            IsVirtual = StringUtil.TBCode(IsVirtual);
            Telphone = StringUtil.TBCode(Telphone);
            Mem = StringUtil.CutBadSqlInfo(Mem);

            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();

            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            xundh.Model.Org_Departs m;
            if (Org_DepartsId > 0)
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "部门设置", "改")) return ServiceStateKeys.NoPermiss;
                int count = bll.GetRecordCount("DepartName='" + DepartName + "' AND DepartId<>" + Org_DepartsId);
                if (count > 0) return ServiceStateKeys.Exists;
                m = bll.GetModel(Org_DepartsId);
            }
            else
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "部门设置", "增")) return ServiceStateKeys.NoPermiss;
                int count = bll.GetRecordCount("DepartName='" + DepartName + "'");
                if (count > 0) return ServiceStateKeys.Exists;
                m = new xundh.Model.Org_Departs
                {
                    AddDepartId = U.DepartId,
                    AddIp = IP,
                    AddTime = NOW,
                    AddUser = U.UserName,
                    AddUserId = U.UserId,
                    IsDelete = false
                };

            }
            m.ParentId = ParentId;
            m.Telphone = Telphone;
            m.DepartName = DepartName;
            m.DepartNameEn = ChineseForFirstCode.IndexCode(DepartName);
            m.IsDelete = IsDelete == "true";
            m.IsVirtual = IsVirtual == "true";
            m.LastDepartId = U.LastDepartId;
            m.LastIp = IP;
            m.LastTime = NOW;
            m.LastUser = U.UserName;
            m.LastUserId = U.UserId;
            m.Mem = Mem;
            if (Org_DepartsId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            return GetListsBase();
        }

        public JsonTable GetListsBase()
        {
            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            using (DataSet ds = bll.GetList(""))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        /// <summary>
        /// 获取所有底层部门,即没有下级部门的
        /// </summary>
        /// <returns></returns>
        public JsonTable GetListsEndBranch()
        {
            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            using (DataSet ds = bll.GetList("IsVirtual=0 and isdelete=0"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount("IsVirtual=0 and isdelete=0"));
            }
        }
        /// <summary>
        /// 获取所有部门
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetListsNotEndBranch()
        {
            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            using (DataSet ds = bll.GetList("IsDelete=0"))
            {
                return (new JsonTable(ds.Tables[0], bll.GetRecordCount("isdelete=0")));
            }
        }
        List<xundh.Model.Org_Departs> ms;
        List<xundh.Model.Org_Departs> msTarget;
        /// <summary>
        /// 所有下级部门
        /// </summary>
        /// <returns></returns>
        public List<xundh.Model.Org_Departs> GetListsDepartsAndSons(int DepartId)
        {
            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            string strWhere = "1=1";
            ms = bll.GetModelList(strWhere);
            msTarget = new List<Model.Org_Departs>();
            FindSons(DepartId);
            return msTarget;
        }
        /// <summary>
        /// 返回下级部门 id字符串
        /// </summary>
        /// <param name="DepartId"></param>
        /// <returns></returns>
        public string GetStringsDepartsAndSons(int DepartId)
        {
            List<xundh.Model.Org_Departs> ms = GetListsDepartsAndSons(DepartId);
            string s = "";
            foreach (xundh.Model.Org_Departs m in ms)
            {
                s += m.DepartId + ",";
            }
            return s.Trim(',');
        }
        private void FindSons(int DepartId)
        {
            foreach (xundh.Model.Org_Departs model in ms)
            {
                if (model.ParentId == DepartId)
                {
                    msTarget.Add(model);
                    FindSons(model.DepartId);
                }
            }
        }

        List<xundh.Model.Org_Departs> ms1;
        List<xundh.Model.Org_Departs> msTarget1;
        /// <summary>
        /// 直线上级部门
        /// </summary>
        /// <returns></returns>
        public List<xundh.Model.Org_Departs> GetListsParent(int DepartId)
        {
            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            string strWhere = "1=1";
            ms1 = bll.GetModelList(strWhere);
            msTarget1 = new List<Model.Org_Departs>();
            FindParents(DepartId);
            return msTarget1;
        }
        /// <summary>
        /// 返回上级部门id字符串
        /// </summary>
        /// <param name="DepartId"></param>
        /// <returns></returns>
        public string GetStringsParent(int DepartId)
        {
            List<xundh.Model.Org_Departs> ms = GetListsParent(DepartId);
            string s = "";
            foreach (xundh.Model.Org_Departs m in ms)
            {
                s += m.DepartId + ",";
            }
            return s.Trim(',');
        }
        private void FindParents(int DepartId)
        {
            xundh.Model.Org_Departs model = QueryFromLists(DepartId);
            msTarget1.Add(model);
            if (model.ParentId == 0) return;
            xundh.Model.Org_Departs modeParent = QueryFromLists(model.ParentId ?? 0);

            FindParents(modeParent.DepartId);
        }
        private xundh.Model.Org_Departs QueryFromLists(int DepartId)
        {
            xundh.Model.Org_Departs result = null;
            foreach (xundh.Model.Org_Departs model in ms1)
            {
                if (model.DepartId == DepartId)
                {
                    result = model;
                    break;
                }
            }
            return result;
        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Org_Departs Query(int id)
        {
            if (U == null) return null;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            xundh.Model.Org_Departs m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.Org_Departs QueryName(string Name)
        {
            if (U == null) return null;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            List<xundh.Model.Org_Departs> ms = bll.GetModelList("DepartName='" + Name + "'");
            if (ms.Count == 0) return null;
            return ms[0];
        }
        [ScriptMethod(UseHttpGet = true)]
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Disable(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "部门设置", "改")) return ServiceStateKeys.NoPermiss;

            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            xundh.Model.Org_Departs m = bll.GetModel(id);
            m.IsDelete = true;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }
        [ScriptMethod(UseHttpGet = true)]
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Enable(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "部门设置", "改")) return ServiceStateKeys.NoPermiss;
            xundh.BLL.Org_Departs bll = new xundh.BLL.Org_Departs();
            xundh.Model.Org_Departs m = bll.GetModel(id);
            m.IsDelete = false;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }
    }
}