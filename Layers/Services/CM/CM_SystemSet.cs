﻿using System.Web;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.CM
{
    /// <summary>
    ///Update 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class CM_SystemSet : BaseService
    {
        [System.ComponentModel.ToolboxItem(false)]
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Save()
        {           
            //用户权限  
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)HttpContext.Current.Session[EnumKeys.SessionKey.USER];

            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "高级设置", "改")) return ServiceStateKeys.NoPermiss;

            string CMShowList = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["CMShowList"]));
            string IsOrderFollow = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsOrderFollow"]));

            SystemSetBool("CM", "CMShowList", CMShowList == "true");
            SystemSetBool("CM", "IsOrderFollow", IsOrderFollow == "true");
            return ServiceStateKeys.Success;
        }
    }
}