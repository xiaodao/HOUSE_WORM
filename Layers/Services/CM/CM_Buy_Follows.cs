﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.CM
{
    /// <summary>
    ///CM_Buy_Follows 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class CM_Buy_Follows : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int CMId, string Follow, int FollowCMState
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Follow = StringUtil.CutBadSqlInfo(Follow);

            xundh.BLL.CM_Buy_Follows bll = new xundh.BLL.CM_Buy_Follows();
            xundh.Model.CM_Buy_Follows m;
            m = new xundh.Model.CM_Buy_Follows();
            m.CMId = CMId;
            m.Follow = Follow;
            m.FollowCMState = FollowCMState;
            m.AddUserId = U.UserId;
            m.AddUser = U.UserName;
            m.AddTime = NOW;
            m.AddIp = IP;
            m.AddDepartId = U.DepartId;
            bll.Add(m);
            //有权限的，直接改合同状态
            API.CM.CM_Buy_Items serviceBuyItems = new CM_Buy_Items();
            serviceBuyItems.UpdateState(CMId, FollowCMState);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int CMId)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            where += " AND CMId=" + CMId;
            string table = "CM_Buy_Follows";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "FollowId,CMId,Follow,FollowCMState,AddUserId,AddUser,AddTime,AddIp,AddDepartId";
            datagrid.init(datagrid.StrWhere, 1, recordcount, int.MaxValue, "", "");
            if (datagrid.Order == "") datagrid.Order = "FollowId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.CM_Buy_Follows Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.CM_Buy_Follows bll = new xundh.BLL.CM_Buy_Follows();
            xundh.Model.CM_Buy_Follows m = bll.GetModel(id);
            return m;
        }

    }
}