﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
using System.Collections.Generic;
namespace xundh.API.CM
{
    /// <summary>
    ///CM_Rent_Items 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class CM_Rent_Items : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int CMId, int CustomerId, string CustomerName, string CustomerTel, string CustomerPersonID,
            int SignUserId, string SignUser, int SignDepartId, string CMNumber, decimal ChargeCustomer,
            string Mem, int HouseId, decimal TotalPrice, int RentRegionArea, decimal HouseArea,
            string HouseTel, string HouseCustomerName, string HousePersonID
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            //todo:权限处理
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            xundh.BLL.CM_Rent_Items bll = new xundh.BLL.CM_Rent_Items();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "租赁合同", "增")) return ServiceStateKeys.NoPermiss;
            //是否已存在
            List<xundh.Model.CM_Rent_Items> ms = bll.GetModelList("CMNumber='" + CMNumber + "'");

            ///参数处理
            CustomerName = StringUtil.CutBadSqlInfo(CustomerName);
            CustomerTel = StringUtil.CutBadSqlInfo(CustomerTel);
            CustomerPersonID = StringUtil.CutBadSqlInfo(CustomerPersonID);
            SignUser = StringUtil.CutBadSqlInfo(SignUser);
            CMNumber = StringUtil.CutBadSqlInfo(CMNumber);
            Mem = StringUtil.CutBadSqlInfo(Mem);
            HouseTel = StringUtil.CutBadSqlInfo(HouseTel);
            HouseCustomerName = StringUtil.CutBadSqlInfo(HouseCustomerName);
            HousePersonID = StringUtil.CutBadSqlInfo(HousePersonID);

            xundh.Model.CM_Rent_Items m;
            if (CMId > 0)
            {
                
                    m = bll.GetModel(CMId);
            }
            else
            {
                if (ms.Count > 0)
                    return ServiceStateKeys.Exists;
                else
                {
                    m = new xundh.Model.CM_Rent_Items();
                    m.AddUserId = U.UserId;
                    m.AddUser = U.UserName;
                    m.AddDepartId = U.DepartId;
                    m.AddTime = NOW;
                    m.AddIp = IP;
                }
            }
            m.CustomerId = CustomerId;
            m.CustomerName = CustomerName;
            m.CustomerTel = CustomerTel;
            m.CustomerPersonID = CustomerPersonID;
            m.SignUserId = SignUserId;
            m.SignUser = SignUser;
            m.SignDepartId = SignDepartId;
            m.PayType = -1;
            m.ChargePayType = 1;
            m.CMNumber = CMNumber;
            m.ChargeHouse = 0;
            m.ChargeTotal = m.ChargeCustomer = ChargeCustomer;
            m.State = (int)CMState.Normal;
            m.Mem = Mem;
            m.LastUserId = U.UserId;
            m.LastUser = U.UserName;
            m.LastDepartId = U.DepartId;
            m.LastTime = NOW;
            m.LastIp = IP;
            m.TotalPrice = TotalPrice;
            m.HouseId = HouseId;
            m.HouseRegionArea = RentRegionArea;
            m.HouseArea = HouseArea;
            m.HouseTel = HouseTel;
            m.HouseCustomerName = HouseCustomerName;
            m.HousePersonID = HousePersonID;
            if (CMId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            //todo:权限处理
            if (U == null) return null;
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (SystemGetBool("CM", "CMShowList"))
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "租赁合同", "查"))
                    return null;
            }
            int DepartId = StringUtil.GetIntValue(REQUEST["SignDepartId"]);
            int SignUserId = StringUtil.GetIntValue(REQUEST["SignUserId"]);
            string CMNumber = StringUtil.GetNullToString(StringUtil.CutBadSqlInfo(REQUEST["CMNumber"]));
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";

            if (serviceOperation.CheckResouceOperationLimit(U.UserId, "租赁合同", "查所有"))
            {
                if (DepartId > 1)
                    where += " AND SignDepartId=" + DepartId;
                else if (SignUserId > 0)
                    where += " AND SignUserId=" + SignUserId;
                if (CMNumber != "")
                    where += " AND CMNumber='" + CMNumber + "'";
            }
            else if (serviceOperation.CheckResouceOperationLimit(U.UserId, "租赁合同", "查部门"))
            {
                where += " AND SignDepartId=" + U.DepartId;
                if (SignUserId > 0)
                    where += " AND SignUserId=" + SignUserId;
                if (CMNumber != "")
                    where += " AND CMNumber='" + CMNumber + "'";
            }
            else if (SignUserId == U.UserId || SignUserId == U.AddUserId)
            {
                where += " AND SignUserId=" + SignUserId;
                if (CMNumber != "")
                    where += " AND CMNumber='" + CMNumber + "'";
            }
            else
            {
                return null;
            }
            if (U.UserAdmin)
            {
                where += " AND State <>" + 2;
            }
            string table = "v_CM_Rents";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "CMId,CustomerId,BuildingName,CustomerName,CustomerPersonID,SignUser,SignUserId,SignDepartId,ChargeTotal,CMNumber,ChargeCustomer,AddUser,AddDepartId,AddTime,HouseId,LastTime,TotalPrice,HouseRegionArea,HouseArea,HouseCustomerName,HousePersonID,HouseAddUser,State";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "CMId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.CM_Rent_Items Query(int id)
        {
            if (U == null) return null;
            //todo:权限处理
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "租赁合同", "查"))
            {
                //没有查看权限
                return null;
            }
            xundh.BLL.CM_Rent_Items bll = new xundh.BLL.CM_Rent_Items();
            xundh.Model.CM_Rent_Items m = bll.GetModel(id);
            bool hasLimit = false;
            if (m.AddUserId == U.UserId || m.SignUserId == U.UserId)
                hasLimit = true;
            else if (m.SignDepartId == U.DepartId || m.AddDepartId == U.DepartId)
            {
                //有没有查看权限
                if (serviceOperation.CheckResouceOperationLimit(U.UserId, "租赁合同", "查部门"))
                {
                    hasLimit = true;
                }
            }
            else
            {
                if (serviceOperation.CheckResouceOperationLimit(U.UserId, "租赁合同", "查所有"))
                {
                    hasLimit = true;
                }
            }
            if (!hasLimit) return null;
            return m;
        }
        public ServiceStateKeys UpateState(int id, int state)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.CM_Rent_Items bll = new BLL.CM_Rent_Items();
            xundh.Model.CM_Rent_Items m = bll.GetModel(id);
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "租赁合同", "改")) return ServiceStateKeys.NoPermiss;
            m.State = state;
            m.LastDepartId = U.DepartId;
            m.LastIp = IP;
            m.LastTime = NOW;
            m.LastUser = U.UserName;
            m.LastUserId = U.UserId;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.CM_Rent_Items bll = new xundh.BLL.CM_Rent_Items();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }
    }
}