﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.CM
{
    /// <summary>
    ///CM_Logs 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class CM_Buy_Steps_Logs : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int LogId, int CMId, int StepId, string Description, int State)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Description = StringUtil.CutBadSqlInfo(Description);

            xundh.BLL.CM_Buy_Steps_Logs bll = new xundh.BLL.CM_Buy_Steps_Logs();
            xundh.Model.CM_Buy_Steps_Logs m;
            if (LogId > 0)
            {
                m = bll.GetModel(LogId);
            }
            else
            {
                m = new xundh.Model.CM_Buy_Steps_Logs();
                //不能重复
                int count = bll.GetRecordCount("CMId=" + CMId + " AND StepId=" + StepId);
                if (count > 0) return ServiceStateKeys.Exists;
                m.AddUserId = U.UserId;
                m.AddUser = U.UserName;
                m.AddDepartId = U.DepartId;
                m.AddTime = NOW;
                m.AddIp = IP;
                m.StepId = StepId;
            }
            m.CMId = CMId;
            m.Description = Description;
            m.State = State;
            //todo:权限处理
            if (LogId > 0)
                bll.Update(m);
            else
            {
                bll.Add(m);
            }
            ///更新合同状态
            API.CM.CM_Buy_Items serviceCMBuyItems = new CM_Buy_Items();
            serviceCMBuyItems.UpdateStep(CMId, StepId);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int CMId)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            where += " AND CMId=" + CMId;
            //todo:权限处理
            string table = "CM_Buy_Steps_Logs";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "LogId,AddIp,CMId,StepId,State,Description,AddUserId,AddUser,AddDepartId,AddTime";
            datagrid.init(datagrid.StrWhere, 1, recordcount, int.MaxValue, "", "");
            if (datagrid.Order == "") datagrid.Order = "LogId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.CM_Buy_Steps_Logs Query(int id)
        {//todo:权限处理
            if (U == null) return null;
            xundh.BLL.CM_Buy_Steps_Logs bll = new xundh.BLL.CM_Buy_Steps_Logs();
            xundh.Model.CM_Buy_Steps_Logs m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {//todo:权限处理
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.CM_Buy_Steps_Logs bll = new xundh.BLL.CM_Buy_Steps_Logs();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}