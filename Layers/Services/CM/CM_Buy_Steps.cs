﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.CM
{
    /// <summary>
    ///CM_Steps 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class CM_Buy_Steps : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int StepId, string Title)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Title = StringUtil.CutBadSqlInfo(Title);

            xundh.BLL.CM_Buy_Steps bll = new xundh.BLL.CM_Buy_Steps();
            xundh.Model.CM_Buy_Steps m;
            if (StepId > 0)
                m = bll.GetModel(StepId);
            else
                m = new xundh.Model.CM_Buy_Steps();
            m.Title = Title;
            if (StepId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "CM_Buy_Steps";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "StepId,Title,Description";
            datagrid.init(datagrid.StrWhere, 1, recordcount, int.MaxValue, "", "");
            if (datagrid.Order == "") datagrid.Order = "StepId";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        public int GetMaxStepId()
        {
            if (U == null) return 0;
            xundh.BLL.CM_Buy_Steps bll = new xundh.BLL.CM_Buy_Steps();
            using (DataTable dt = bll.GetList(1, "", "StepId DESC").Tables[0])
            {
                if (dt.Rows.Count > 0) return StringUtil.GetIntValue(dt.Rows[0]["StepId"].ToString());
            }
            return 0;
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.CM_Buy_Steps Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.CM_Buy_Steps bll = new xundh.BLL.CM_Buy_Steps();
            xundh.Model.CM_Buy_Steps m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.CM_Buy_Steps bll = new xundh.BLL.CM_Buy_Steps();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}