﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.CM
{
    /// <summary>
    ///CM_Rent_FeeTypes 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class CM_Rent_FeeTypes : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int FeeTypeId, int CMTypeId, string Title, string Functions, int FISubjectId
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Title = StringUtil.CutBadSqlInfo(Title);
            Functions = StringUtil.CutBadSqlInfo(Functions);

            xundh.BLL.CM_Rent_FeeTypes bll = new xundh.BLL.CM_Rent_FeeTypes();
            xundh.Model.CM_Rent_FeeTypes m;
            if (FeeTypeId > 0)
                m = bll.GetModel(FeeTypeId);
            else
                m = new xundh.Model.CM_Rent_FeeTypes();
            m.CMTypeId = CMTypeId;
            m.Title = Title;
            m.Functions = Functions;
            m.FISubjectId = FISubjectId;
            if (FeeTypeId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "CM_Rent_FeeTypes";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "FeeTypeId,CMTypeId,Title,Functions,FISubjectId";
            datagrid.init(datagrid.StrWhere, 1, recordcount, int.MaxValue, "", "");
            if (datagrid.Order == "") datagrid.Order = "FeeTypeId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.CM_Rent_FeeTypes Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.CM_Rent_FeeTypes bll = new xundh.BLL.CM_Rent_FeeTypes();
            xundh.Model.CM_Rent_FeeTypes m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.CM_Rent_FeeTypes bll = new xundh.BLL.CM_Rent_FeeTypes();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}