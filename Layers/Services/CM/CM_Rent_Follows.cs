﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.CM
{
    /// <summary>
    ///CM_Rent_Follows 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class CM_Rent_Follows : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int CMId, string Follow, int FollowCMState)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Follow = StringUtil.CutBadSqlInfo(Follow);

            xundh.BLL.CM_Rent_Follows bll = new xundh.BLL.CM_Rent_Follows();
            xundh.Model.CM_Rent_Follows m;
            m = new xundh.Model.CM_Rent_Follows();
            m.CMId = CMId;
            m.Follow = Follow;
            m.FollowCMState = FollowCMState;
            m.AddUserId = U.UserId;
            m.AddUser = U.UserName;
            m.AddTime = NOW;
            m.AddIp = IP;
            m.AddDepartId = U.DepartId;
            bll.Add(m);
            //直接改租赁合同状态
            API.CM.CM_Rent_Items servicesRentItems = new CM_Rent_Items();
            servicesRentItems.UpateState(CMId, FollowCMState);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int CMId)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "CM_Rent_Follows";
            where += " AND CMId=" + CMId;
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "FollowId,CMId,Follow,FollowCMState,AddUserId,AddUser,AddTime,AddIp,AddDepartId";
            datagrid.init(datagrid.StrWhere, 1, recordcount, int.MaxValue, "", "");
            if (datagrid.Order == "") datagrid.Order = "FollowId DESC";
            //datagrid.Pagesize = int.MaxValue;
            //datagrid.P = 1;
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.CM_Rent_Follows Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.CM_Rent_Follows bll = new xundh.BLL.CM_Rent_Follows();
            xundh.Model.CM_Rent_Follows m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.CM_Rent_Follows bll = new xundh.BLL.CM_Rent_Follows();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}