﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.Notice
{
    /// <summary>
    ///Notice_Views 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Notice_Views : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int ViewId, int NoticeId, int AddUserId, string AddUser, int AddDepartId, DateTime AddTime, string AddIp
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理
            AddUser = StringUtil.CutBadSqlInfo(AddUser);
            AddIp = StringUtil.CutBadSqlInfo(AddIp);

            xundh.BLL.Notice_Views bll = new xundh.BLL.Notice_Views();
            xundh.Model.Notice_Views m;
            if (ViewId > 0)
                m = bll.GetModel(ViewId);
            else
                m = new xundh.Model.Notice_Views();
            m.NoticeId = NoticeId;
            m.AddUserId = AddUserId;
            m.AddUser = AddUser;
            m.AddDepartId = AddDepartId;
            m.AddTime = AddTime;
            m.AddIp = AddIp;
            if (ViewId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.Notice_Views bll = new xundh.BLL.Notice_Views();
            using (DataSet ds = bll.GetList(int.MaxValue, "", "ViewId"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.Notice_Views Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Notice_Views bll = new xundh.BLL.Notice_Views();
            xundh.Model.Notice_Views m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Notice_Views bll = new xundh.BLL.Notice_Views();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}