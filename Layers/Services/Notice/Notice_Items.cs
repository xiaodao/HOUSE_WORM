﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.Notice
{
    /// <summary>
    ///Notice_Items 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Notice_Items : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int NoticeId, string FromUser, int AddDepartId, DateTime AddTime, int AddUserId, string AddUser, int State, DateTime AfterTime, string ModuleName, string NoticeTitle, string NoticeBody, string Url, string runJs, string arg0, int FromDepartId, int FromUserId
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理
            FromUser = StringUtil.CutBadSqlInfo(FromUser);
            AddUser = StringUtil.CutBadSqlInfo(AddUser);
            ModuleName = StringUtil.CutBadSqlInfo(ModuleName);
            NoticeTitle = StringUtil.CutBadSqlInfo(NoticeTitle);
            //NoticeBody = StringUtil.CutBadSqlInfo(NoticeBody);
            Url = StringUtil.CutBadSqlInfo(Url);
            runJs = StringUtil.CutBadSqlInfo(runJs);
            arg0 = StringUtil.CutBadSqlInfo(arg0);

            xundh.BLL.Notice_Items bll = new xundh.BLL.Notice_Items();
            xundh.Model.Notice_Items m;
            if (NoticeId > 0)
                m = bll.GetModel(NoticeId);
            else
                m = new xundh.Model.Notice_Items();
            m.FromUser = FromUser;
            m.AddDepartId = AddDepartId;
            m.AddTime = AddTime;
            m.AddUserId = AddUserId;
            m.AddUser = AddUser;
            m.State = State;
            m.AfterTime = AfterTime;
            m.ModuleName = ModuleName;
            m.NoticeTitle = NoticeTitle;
            m.NoticeBody = NoticeBody;
            m.Url = Url;
            m.runJs = runJs;
            m.arg0 = arg0;
            m.FromDepartId = FromDepartId;
            m.FromUserId = FromUserId;
            if (NoticeId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "Announcement_Items";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "AnnoucementId,LastTime,LastUserId,LastUser,LastIp,AnnoucementTitle,AnnoucementBody,AddDepartId,AddTime,AddUserId,AddUser,AddIp,LastDepartId";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.Notice_Items Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Notice_Items bll = new xundh.BLL.Notice_Items();
            xundh.Model.Notice_Items m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Notice_Items bll = new xundh.BLL.Notice_Items();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}