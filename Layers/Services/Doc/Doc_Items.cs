﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.Doc
{
    /// <summary>
    ///Doc_Items 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Doc_Items : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int DocId, int TypeId, string Title, string Body, string Obj, string Chaosong
            , string Luokuan, string Yinfa,string Chaobao)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Title = StringUtil.CutBadSqlInfo(Title);
            Obj = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(Obj));
            Chaosong = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(Chaosong));
            Luokuan = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(Luokuan));
            Yinfa = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(Yinfa));
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (TypeId > 0)
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "公文", "改")) return ServiceStateKeys.NoPermiss;
            }
            else
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "公文", "增")) return ServiceStateKeys.NoPermiss;
            }
            xundh.BLL.Doc_Items bll = new xundh.BLL.Doc_Items();
            xundh.Model.Doc_Items m;
            if (DocId > 0)
                m = bll.GetModel(DocId);
            else
            {
                m = new xundh.Model.Doc_Items();

                m.AddDepartId = U.DepartId;
                m.AddTime = NOW;
                m.AddUserId = U.UserId;
                m.AddUser = U.UserName;
                m.AddIp = IP;
            }
            m.LastDepartId = U.DepartId;
            m.LastTime = NOW;
            m.LastUserId = U.UserId;
            m.LastUser = U.UserName;
            m.LastIp = IP;
            m.TypeId = TypeId;
            m.Title = Title;
            m.Body = Body;
            m.Yinfa = Yinfa;
            m.Chaobao = Chaobao;
            m.Chaosong = Chaosong;
            m.Luokuan = Luokuan;
            m.Obj = Obj;

            if (DocId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "v_Doc_Items_Types";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "DocId,TypeId,Obj,Chaosong,Chaobao,Luokuan,Yinfa,TypeName,Title,Body,AddDepartId,AddTime,AddUser";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "DocId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet=true)]
        public xundh.Model.Doc_Items Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Doc_Items bll = new xundh.BLL.Doc_Items();
            xundh.Model.Doc_Items m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet=true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "公文", "删")) return ServiceStateKeys.NoPermiss;

            xundh.BLL.Doc_Items bll = new xundh.BLL.Doc_Items();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}