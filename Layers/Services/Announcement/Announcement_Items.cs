﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.Announcement
{
    /// <summary>
    ///Announcement_Items 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Announcement_Items : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int AnnoucementId, string AnnoucementTitle, string AnnoucementBody)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///变量
            ///参数处理
            AnnoucementTitle = StringUtil.TBCode(AnnoucementTitle);
            AnnoucementTitle = AnnoucementTitle.Replace("'", "").Replace("’", "");

            //AnnoucementBody = StringUtil.CutBadSqlInfo(AnnoucementBody);
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();

            xundh.BLL.Announcement_Items bll = new xundh.BLL.Announcement_Items();
            xundh.Model.Announcement_Items m;
            if (AnnoucementId > 0)
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "公告", "改")) return ServiceStateKeys.NoPermiss;
                m = bll.GetModel(AnnoucementId);
            }
            else
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "公告", "增")) return ServiceStateKeys.NoPermiss;
                m = new xundh.Model.Announcement_Items();
                m.AddDepartId = U.DepartId;
                m.AddTime = NOW;
                m.AddUserId = U.UserId;
                m.AddUser = U.UserName;
                m.AddIp = IP;
            }
            m.LastTime = NOW;
            m.LastUserId = U.UserId;
            m.LastUser = U.UserName;
            m.LastIp = IP;
            m.LastDepartId = U.DepartId;
            m.AnnoucementTitle = AnnoucementTitle;
            m.AnnoucementBody = AnnoucementBody;
            if (AnnoucementId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "Announcement_Items";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "AnnoucementId,LastTime,LastUserId,LastUser,LastIp,AnnoucementTitle,AnnoucementBody,AddDepartId,AddTime,AddUserId,AddUser,AddIp,LastDepartId";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "AnnoucementId DESC";

            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Announcement_Items Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Announcement_Items bll = new xundh.BLL.Announcement_Items();
            xundh.Model.Announcement_Items m = bll.GetModel(id);
            //查看记录
            API.Announcement.Announcement_Items_Views serviceItemView = new Announcement_Items_Views();
            serviceItemView.Add(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete(string id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Announcement_Items bll = new xundh.BLL.Announcement_Items();
            bll.DeleteList(id);
            return ServiceStateKeys.Success;
        }

    }
}