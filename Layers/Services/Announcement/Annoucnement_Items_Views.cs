﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
using System.Collections.Generic;
namespace xundh.API.Announcement
{
    /// <summary>
    ///Announcement_Items_Views 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Announcement_Items_Views : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int ItemId)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理

            xundh.BLL.Announcement_Items_Views bll = new xundh.BLL.Announcement_Items_Views();
            xundh.Model.Announcement_Items_Views m;
            //是否已经存在
            List<xundh.Model.Announcement_Items_Views> ms = bll.GetModelList("ItemId=" + ItemId + " AND AddUserId=" + U.UserId);
            if (ms.Count > 0)
            {
                return ServiceStateKeys.Exists;
            }
            else
            {
                m = new xundh.Model.Announcement_Items_Views();
                m.AddUserId = U.UserId;
                m.AddUser = U.UserName;
                m.AddTime = NOW;
                m.AddIp = IP;
                m.AddDepartId = U.DepartId;
            }
            m.ItemId = ItemId;

            bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int ItemId)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            where += " AND ItemId=" + ItemId;
            string table = "Announcement_Items_Views";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "ViewId,ItemId,AddUserId,AddUser,AddTime,AddIp,AddDepartId";
            datagrid.init(datagrid.StrWhere, 1, recordcount, int.MaxValue, "", "");
            if (datagrid.Order == "") datagrid.Order = "ViewId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
    }
}