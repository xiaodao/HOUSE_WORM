﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
using LitJson;
using System.Collections.Generic;
namespace xundh.API.WF
{
    /// <summary>
    ///WF_Templates 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class WF_Templates : BaseService
    {

        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int TemplateId, int TypeId, string Title, string GraphJson, string Form)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Title = StringUtil.CutBadSqlInfo(Title);
            // GraphJson = StringUtil.CutBadSqlInfo(GraphJson);
            Form = Server.UrlDecode(Form);
            xundh.BLL.WF_Templates bll = new xundh.BLL.WF_Templates();
            xundh.Model.WF_Templates m;
            if (TemplateId > 0)
            {
                m = bll.GetModel(TemplateId);
            }
            else
            {
                m = new xundh.Model.WF_Templates();
            }

            m.TypeId = TypeId;
            m.Title = Title;
            m.GraphJson = GraphJson;
            m.Form = Form;
            if (TemplateId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            string Title = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(REQUEST["Title"]));
            int TypeId = StringUtil.GetIntValue(REQUEST["TypeId"]);
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            if (Title != "")
                where += " AND Title LIKE '%" + Title + "%'";
            if (TypeId > 0) where += " AND TypeId=" + TypeId;
            string table = "WF_Templates";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "TemplateId,TypeId,Title,GraphJson";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "TemplateId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.WF_Templates Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.WF_Templates bll = new xundh.BLL.WF_Templates();
            xundh.Model.WF_Templates m = bll.GetModel(id);
           
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.WF_Templates QueryTitle(string Title)
        {
            if (U == null) return null;
            xundh.BLL.WF_Templates bll = new xundh.BLL.WF_Templates();
            List<xundh.Model.WF_Templates> ms = bll.GetModelList("Title='" + Title + "'");
            if (ms.Count > 0)
                return ms[0];
            else
                return null;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.WF_Templates bll = new xundh.BLL.WF_Templates();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }
    }
}