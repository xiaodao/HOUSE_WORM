﻿using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
using System.Collections.Generic;
namespace xundh.API.WF
{
    /// <summary>
    ///WF_WorkFlows 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class WF_WorkFlows : BaseService
    {
        [WebMethod(EnableSession = true)]
        public int Add(int FlowId, int ItemId, int NodeStep, string NodeName, string NodeType,
            string ToUserId, string ToUser, bool Verified)
        {
            ///Session
            if (U == null) return 0;
            ///参数处理
            ToUser = StringUtil.CutBadSqlInfo(ToUser);

            xundh.BLL.WF_WorkFlows bll = new xundh.BLL.WF_WorkFlows();
            xundh.Model.WF_WorkFlows m;
            if (FlowId > 0)
                m = bll.GetModel(FlowId);
            else
            {
                m = new xundh.Model.WF_WorkFlows();
                m.AddTime = NOW;
                m.State = (int)WFState.Need;
            }
            m.ItemId = ItemId;
            m.NodeStep = NodeStep;
            m.ToUserId = ToUserId;
            m.ToUser = ToUser;
            m.NodeName = NodeName;
            m.NodeType = NodeType;

            if (Verified)
            {
                m.VerifyTime = m.AddTime;
                m.VerifyUser = U.UserName;
                m.VerifyUserId = U.UserId;
                m.Suggest = "自动审批通过";
            }

            if (FlowId > 0)
            {
                bll.Update(m);
            }
            else
            {
                m.FlowId = bll.Add(m);
            }
            return m.FlowId;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows, int ItemId, string action)
        {
            if (U == null) return null;
            action = StringUtil.TBCode(action);
            int TypeId = StringUtil.GetIntValue(REQUEST["TypeId"]);
            int TemplateId = StringUtil.GetIntValue(REQUEST["Title"]);
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "v_WF_WorkFlow_Items";
            if (ItemId > 0) where += " AND ItemId=" + ItemId;
            if (TemplateId > 0) where += " AND TemplateId=" + TemplateId;
            if (TypeId > 0) where += " AND TypeId=" + TypeId;
            where += " AND ItemState<>" + (int)WFState.Delete;
            switch (action)
            {
                case "NeedMyVerify":
                    where += " AND State=" + (int)WFState.Need;
                    where += " AND ','+ToUserId+',' LIKE '%," + U.UserId + ",%'";
                    break;
                case "NeedMyVerifyAndNotPass":
                    where += " AND ((State=" + (int)WFState.Need;
                    where += " AND ','+ToUserId+',' LIKE '%," + U.UserId + ",%')";
                    where += " OR (State=" + (int)WFState.NotPass + " AND AddUserId=" + U.UserId + ")";
                    where += ")";
                    break;
                case "All":
                    //权限
                    //RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
                    //if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "流程管理", "查"))
                    //{
                    //    return null;
                    //}
                    //if (serviceOperation.CheckResouceOperationLimit(U.UserId, "流程管理", "查所有"))
                    //{

                    //}
                    //else if (serviceOperation.CheckResouceOperationLimit(U.UserId, "流程管理", "查部门"))
                    //{
                    //    where += " AND AddDepartId=" + U.DepartId;
                    //}

                    break;
                case "MyPost":
                    where += " AND AddUserId = " + U.UserId;
                    break;
                default:
                    return null;
                    break;
            }
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
            string cols = "ItemId,FlowAddTime,AddTime,AddUserId,AddDepartId,AddUser,Title,Name,State,TemplateId,LastTime,LastUser,NowToUsers,FlowId,NodeName,NodeType,Suggest,VerifyUser,VerifyTime";//Title:Template Name:WF_Type
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "FlowId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.WF_WorkFlows Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.WF_WorkFlows bll = new xundh.BLL.WF_WorkFlows();
            xundh.Model.WF_WorkFlows m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Verify(int WorkFlowId, string Suggest, int State)
        {
            xundh.BLL.WF_WorkFlows bll = new BLL.WF_WorkFlows();
            xundh.Model.WF_WorkFlows m = bll.GetModel(WorkFlowId);
            if (m == null) return ServiceStateKeys.NotExists;
            m.VerifyUserId = U.UserId;
            m.VerifyUser = U.UserName;
            m.VerifyTime = NOW;
            m.Suggest = Suggest;
            m.State = State;
            bll.Update(m);

            return ServiceStateKeys.Success;
        }
    }
}