﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.WF
{
    /// <summary>
    ///WF_Types 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class WF_Types : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int TypeId, string Name
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Name = StringUtil.CutBadSqlInfo(Name);

            xundh.BLL.WF_Types bll = new xundh.BLL.WF_Types();
            xundh.Model.WF_Types m;
            if (TypeId > 0)
                m = bll.GetModel(TypeId);
            else
                m = new xundh.Model.WF_Types();
            m.Name = Name;
            if (TypeId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "WF_Types";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "TypeId,Name";
            datagrid.init(datagrid.StrWhere, 1, recordcount, int.MaxValue, "", "");
            if (datagrid.Order == "") datagrid.Order = "TypeId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.WF_Types Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.WF_Types bll = new xundh.BLL.WF_Types();
            xundh.Model.WF_Types m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.WF_Types bll = new xundh.BLL.WF_Types();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}