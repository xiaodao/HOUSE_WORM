﻿using System.Web;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.House
{
    /// <summary>
    ///Update 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class House_SystemSet : BaseService
    {
        [System.ComponentModel.ToolboxItem(false)]
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Save()
        {           
            //用户权限  
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)HttpContext.Current.Session[EnumKeys.SessionKey.USER];

            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "高级设置", "改")) return ServiceStateKeys.NoPermiss;

            string ShowDongInList = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["DongFangInList"]));
            string IsViewCountLimit = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsViewCountLimit"]));
            int ViewCountLimit = StringUtil.GetIntValue(HttpContext.Current.Request["ViewCountLimit"]);
            string IsAllowDeleteSelf = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsAllowDeleteSelf"]));
            string IsAllowUserSetFollowViewLevel = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsAllowUserSetFollowViewLevel"]));
            string IsAllowListPrivate = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsAllowListPrivate"]));
            string IsAllowViewPrivateDongFang = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsAllowViewPrivateDongFang"]));
            string IsAllowDisplayPhoneNumber = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsAllowDisplayPhoneNumber"]));
            string IsUpdateHouseSellsList = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsUpdateHouseSellsList"]));
            string IsUpdateHouseRentsList = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsUpdateHouseRentsList"]));
            string IsAllowMemChangeHouseInfo = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsAllowMemChangeHouseInfo"]));
            string IsChangeLoginStyle = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["IsChangeLoginStyle"]));

            if (SystemSetBool("House", "DongFangInList", ShowDongInList == "true") == ServiceStateKeys.NotExists) {
                return ServiceStateKeys.NotExists;
            }
            SystemSetBool("House", "IsViewCountLimit", IsViewCountLimit == "true");
            SystemSetInt("House", "ViewCountLimit", ViewCountLimit);
            SystemSetBool("House", "IsAllowDeleteSelf", IsAllowDeleteSelf == "true");
            SystemSetBool("House", "IsAllowUserSetFollowViewLevel", IsAllowUserSetFollowViewLevel == "true");
            SystemSetBool("House", "IsAllowListPrivate", IsAllowListPrivate == "true");
            SystemSetBool("House", "IsAllowViewPrivateDongFang", IsAllowViewPrivateDongFang == "true");
            SystemSetBool("House", "IsAllowDisplayPhoneNumber", IsAllowDisplayPhoneNumber == "true");
            SystemSetBool("House", "IsUpdateHouseSellsList", IsUpdateHouseSellsList == "true");
            SystemSetBool("House", "IsUpdateHouseRentsList", IsUpdateHouseRentsList == "true");
            SystemSetBool("House", "IsAllowMemChangeHouseInfo", IsAllowMemChangeHouseInfo == "true");
            SystemSetBool("House", "IsChangeLoginStyle", IsChangeLoginStyle == "true");
            return ServiceStateKeys.Success;
        }
    }
}