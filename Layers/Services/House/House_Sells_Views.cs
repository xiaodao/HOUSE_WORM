﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.House
{
    /// <summary>
    ///House_Sells_Views 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class House_Sells_Views : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int SellId)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;

            xundh.BLL.House_Sells_Views bll = new xundh.BLL.House_Sells_Views();
            xundh.Model.House_Sells_Views m;
            m = new xundh.Model.House_Sells_Views();
            m.SellId = SellId;
            m.AddUserId = mLoginUser.UserId;
            m.AddUser = mLoginUser.UserName;
            m.AddTime = now;
            m.AddIp = ip;
            m.AddDepartId = mLoginUser.DepartId;
            bll.Add(m);
            return ServiceStateKeys.Success;
        }
        /// <summary>
        /// 某用户今天查看多少房源了
        /// </summary>
        /// <param name="SellId"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public int GetViewCount(int UserId)
        {
            DateTime now = DateTime.Now;
            string where = "AddUserId=" + UserId;
            where +=String.Format( " AND (AddTime BETWEEN '{0}' AND '{1}')",now.ToShortDateString(),now.ToShortDateString() + " 23:59:59");
            xundh.BLL.BasePager bll = new BLL.BasePager();
            return bll.GetAllCount(where,"House_Sells_Views");
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();

            string where = "1=1";// "HouseId=" + HouseId;
            string cols = "ViewLevel,SellId,SellRegionArea,SellFace,SellBuildingId,HouseArea,HouseFloor,HouseTotalFloor,UnitPrice,TotalPrice,Decoration,Shi,Ting,Chu,Wei,BuildingName,Dong,Fang,State,AddUser,AddTime,ViewId,AddDepartId";
            string table = "v_House_Sell_Views_Buildings";
            string txtUserName = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtUserName"]));
            string txtDeparts = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtDeparts"]));
            string AddTimeMin = StringUtil.GetNullToDate(HttpContext.Current.Request["AddTimeMin"]).ToString("yyyy-MM-dd");
            string AddTimeMax = StringUtil.GetNullToDate(HttpContext.Current.Request["AddTimeMax"]).ToString("yyyy-MM-dd");
            string today = DateTime.Now.ToString("yyyy-MM-dd");

            int txtSellId = StringUtil.Get_1Value(HttpContext.Current.Request["txtSellId"]);
            if (txtUserName != "")
            {
                if ((txtUserName.IndexOf(",") > -1) || StringUtil.IsInt(txtUserName))
                {
                    where += " AND AddUserId in(" + txtUserName + ")";
                }
                else
                {
                    string condition = "'%" + txtUserName + "%'";
                    where += " AND (AddUser LIKE " + condition + ")";
                }
            }
            if (txtDeparts != "")
            {
                where += " AND AddDepartId in(" + txtDeparts + ")";
            }
            if (txtSellId > -1)
            {
                where += " AND SellId=" + txtSellId;
            }
            if (today != AddTimeMax) where += " AND AddTime <='" + AddTimeMax + "'";
            if (today != AddTimeMin) where += " AND AddTime>='" + AddTimeMin + "'";

            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
            datagrid.init(where, page, recordcount, rows, "ViewId", "DESC");

            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                ///权限，列表是否显示楼栋号
                if (!SystemGetBool("House", "DongFangInList"))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dr["Dong"] = "";
                        dr["Fang"] = "";
                    }
                }

                JsonTable jt = new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
                return jt;
            }


        }
    }
}