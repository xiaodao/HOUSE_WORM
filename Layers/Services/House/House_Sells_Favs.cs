﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
using System.Collections.Generic;
using System.Web;
namespace xundh.API.House
{
    /// <summary>
    ///House_Sells_Favs 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class House_Sells_Favs : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int SellsId)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理

            xundh.BLL.House_Sells_Favs bll = new xundh.BLL.House_Sells_Favs();
            xundh.Model.House_Sells_Favs m;
            //避免重复
            List<xundh.Model.House_Sells_Favs> ms = bll.GetModelList("SellsId=" + SellsId + " AND AddUserId=" + U.UserId);
            if (ms.Count == 0)
            {
                m = new xundh.Model.House_Sells_Favs();

                m.AddUserId = U.UserId;
                m.AddDepartId = U.DepartId;
                m.AddTime = NOW;
                m.SellsId = SellsId;
                bll.Add(m);
                return ServiceStateKeys.Success;
            }
            else
                return ServiceStateKeys.Exists;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();

            string where = "1=1";// "HouseId=" + HouseId;
            string cols = "FavId,ViewLevel,SellId,SellRegionArea,SellFace,BuildingName,HouseArea,HouseFloor,HouseTotalFloor,UnitPrice,TotalPrice,Decoration,Dong,Fang,Shi,Ting,Chu,Wei,AddDepartId,AddUser,AddTime,State";
            string table = "v_House_Sell_Favs_Buildings";
            string txtUserName = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtUserName"]));
            string txtDeparts = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtDeparts"]));
            int txtSellId = StringUtil.Get_1Value(HttpContext.Current.Request["txtSellId"]);
            //int txtBuilding = StringUtil.GetIntValue(HttpContext.Current.Request["txtBuilding"]);


            if (txtUserName != "")
            {
                if (txtUserName.IndexOf(",") > -1 || StringUtil.IsInt(txtUserName))
                {
                    where += " AND UserId in(" + txtUserName + ")";
                }
                else
                {
                    string condition = "'%" + txtUserName + "%'";
                    where += " AND (UserName LIKE " + condition + " OR UserNameEn LIKE " + condition + " OR UserMobile LIKE " + condition + " OR UserEmail LIKE " + condition + ")";
                }
            }
            if (txtDeparts != "")
            {
                where += " AND AddDepartId in(" + txtDeparts + ")";
            }
            if (txtSellId > 0)
            {
                where += " AND SellBuildingId = " + txtSellId;
            }
            //if (txtBuilding > 0)
            //{
            //    where += " AND SellBuildingId=" + txtBuilding;
            //}
            //权限
            //where += " AND (";
            //where += "ViewLevel=" + (int)HouseViewLevel.All;
            //where += " OR (ViewLevel=" + (int)HouseViewLevel.Depart + " AND AddDepartId=" + U.DepartId + ")";
            //where += " OR (ViewLevel=" + (int)HouseViewLevel.Self + " AND AddUserId=" + U.UserId + ")";
            //where += ")";
            where += " AND AddUserId="+U.UserId;
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
            datagrid.init(where, page, recordcount, rows, "FavId", "DESC");

            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                ///权限，列表是否显示楼栋号
                if (!SystemGetBool("House", "DongFangInList"))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dr["Dong"] = "";
                        dr["Fang"] = "";
                    }
                }

                JsonTable jt = new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
                return jt;
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.House_Sells_Favs Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.House_Sells_Favs bll = new xundh.BLL.House_Sells_Favs();
            xundh.Model.House_Sells_Favs m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete()
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            int SellsId = StringUtil.GetIntValue(REQUEST["SellsId"]);
            int id = StringUtil.GetIntValue(REQUEST["id"]);
            xundh.BLL.House_Sells_Favs bll = new xundh.BLL.House_Sells_Favs();
            if (id > 0)
            {
                xundh.Model.House_Sells_Favs m = bll.GetModel(id);
                if (m.AddUserId == U.UserId)
                    bll.Delete(id);
                else return ServiceStateKeys.NoPermiss;
            }
            else
            {
                List<xundh.Model.House_Sells_Favs> list = bll.GetModelList("SellsId=" + SellsId);
                if (list.Count > 0)
                {
                    if (list[0].AddUserId == U.UserId)
                        bll.Delete(list[0].FavId);
                    else return ServiceStateKeys.NoPermiss;
                }
            }
            return ServiceStateKeys.Success;
        }
    }
}