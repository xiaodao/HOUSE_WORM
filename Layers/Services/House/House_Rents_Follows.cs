﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.House
{
    /// <summary>
    ///House_Rents_Follows 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class House_Rents_Follows : BaseService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="RentId"></param>
        /// <param name="Follow"></param>
        /// <param name="FollowHouseState">职位-1时，不修改</param>
        /// <param name="ViewLevel">跟进的可见性</param>
        /// <param name="ChgHouseViewLevel">房源的可见性</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int RentId, string Follow, int FollowHouseState, int ViewLevel, int ChgHouseViewLevel)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理
            Follow = StringUtil.CutBadSqlInfo(Follow);

            xundh.BLL.House_Rents_Follows bll = new xundh.BLL.House_Rents_Follows();
            xundh.Model.House_Rents_Follows m;

            m = new xundh.Model.House_Rents_Follows();
            m.RentId = RentId;
            m.Follow = Follow;
            m.FollowHouseState = FollowHouseState;
            m.AddUserId = U.UserId;
            m.AddUser = U.UserName;
            m.AddTime = now;
            m.AddIp = ip;
            m.AddDepartId = U.DepartId;
            ///是否允许设定可见性
            bool IsAllowUserSetFollowViewLevel = SystemGetBool("House", "IsAllowUserSetFollowViewLevel");
            if (!IsAllowUserSetFollowViewLevel)
                m.ViewLevel = (int)HouseViewLevel.All;
            else
                m.ViewLevel = ViewLevel;
            bll.Add(m);
            ///修改房源状态,权限在House_Rents里判断
            xundh.API.House.House_Rents serviceRent = new House_Rents();
            serviceRent.UpdateState(RentId, 0, (int)FollowHouseState, ChgHouseViewLevel);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows, int txtRentId)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();

            string where = "1=1";// "HouseId=" + HouseId;
            string cols = "FollowId,ViewLevel,RentId,RentRegionArea,RentFace,BuildingName,Follow,FollowHouseState,HouseArea,HouseFloor,HouseTotalFloor,TotalPrice,Decoration,Dong,Fang,Shi,Ting,Chu,Wei,AddDepartId,AddUser,AddUserId,AddTime,State";
            string table = "v_House_Rent_Follows_Buildings";
            string txtUserName = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["UserId"]));
            string txtDeparts = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtDeparts"]));

            if (txtUserName != "")
            {
                if (txtUserName.IndexOf(",") > -1 || StringUtil.IsInt(txtUserName))
                {
                    where += " AND UserId in(" + txtUserName + ")";
                }
                else
                {
                    string condition = "'%" + txtUserName + "%'";
                    where += " AND (UserName LIKE " + condition + " OR UserNameEn LIKE " + condition + " OR UserMobile LIKE " + condition + " OR UserEmail LIKE " + condition + ")";
                }
            }
            if ((txtDeparts != "") && (txtDeparts != "0"))
            {
                if (StringUtil.IsInt(txtDeparts))
                {
                    Org.Org_Departs serviceOrgDeparts = new Org.Org_Departs();
                    string departs = serviceOrgDeparts.GetStringsDepartsAndSons(int.Parse(txtDeparts));
                    departs = departs.ItemAdd(txtDeparts);
                    if (departs != "")
                        where += " AND AddDepartId in(" + departs + ")";
                }
                else
                    where += " AND AddDepartId in(" + txtDeparts + ")";

            }
            if (txtRentId > 0)
            {
                where += " AND RentId=" + txtRentId;
            }
            //权限
            where += " AND (";
            where += "ViewLevel=" + (int)HouseViewLevel.All;
            where += " OR (ViewLevel=" + (int)HouseViewLevel.Depart + " AND AddDepartId=" + U.DepartId + ")";
            where += " OR (ViewLevel=" + (int)HouseViewLevel.Self + " AND AddUserId=" + U.UserId + ")";
            where += ")";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
            datagrid.init(where, page, recordcount, rows, "FollowId", "DESC");

            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                ///权限，列表是否显示楼栋号
                if (!SystemGetBool("House", "DongFangInList"))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dr["Dong"] = "";
                        dr["Fang"] = "";
                    }
                }

                JsonTable jt = new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
                return jt;
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.House_Rents_Follows Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.House_Rents_Follows bll = new xundh.BLL.House_Rents_Follows();
            xundh.Model.House_Rents_Follows m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.House_Rents_Follows bll = new xundh.BLL.House_Rents_Follows();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}