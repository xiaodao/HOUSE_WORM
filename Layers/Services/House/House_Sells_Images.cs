﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
using System.IO;
using System.Web;
namespace xundh.API.House
{
    /// <summary>
    ///House_Sells_Images 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class House_Sells_Images : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int id, int HouseType, int HouseId, string ImgUrl,string ThumbImgUrl)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            //ImgUrl = StringUtil.CutBadSqlInfo(ImgUrl);
            xundh.BLL.HM_Sells bllHmSells = new BLL.HM_Sells();
            xundh.Model.House_Sells mHmSells = bllHmSells.GetModel(HouseId);
            //if (mHmSells.AddUserId != U.UserId) return ServiceStateKeys.NoPermiss;

            xundh.BLL.House_Sells_Images bll = new xundh.BLL.House_Sells_Images();
            xundh.Model.House_Sells_Images m;
            if (id > 0)
                m = bll.GetModel(id);
            else
                m = new xundh.Model.House_Sells_Images();
            m.HouseType = HouseType;
            m.HouseId = HouseId;
            if (ImgUrl == null) ImgUrl = "";
            m.ImgUrl = ImgUrl.Trim('\'');

            if (ThumbImgUrl == null) ThumbImgUrl = "";
            m.ThumbImageUrl = ThumbImgUrl.Trim('\'');
            m.AddTime = NOW;
            m.AddIp = IP;
            m.AddUserId = U.UserId;
            m.AddDepartId = U.DepartId;
            m.AddUser = U.UserName;
            if (id > 0)
                bll.Update(m);
            else
                bll.Add(m);

            mHmSells.HasImage = true;
            bllHmSells.Update(mHmSells);

            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            int HouseId = StringUtil.GetIntValue(REQUEST["HouseId"]);
            int HouseType=StringUtil.GetIntValue(REQUEST["HouseType"]);

            if (HouseId > 0) where += " AND HouseId=" + HouseId;

            where += " AND HouseType=" + HouseType;

            string table = "House_Sells_Images";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "id,HouseType,HouseId,ImgUrl,ThumbImageUrl,AddTime,AddIp,AddUserId,AddUser,AddDepartId";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "id DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.House_Sells_Images Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.House_Sells_Images bll = new xundh.BLL.House_Sells_Images();
            xundh.Model.House_Sells_Images m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.House_Sells_Images bll = new xundh.BLL.House_Sells_Images();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        public void Download(string path)
        {
            if (U == null) return;
            string[] pathArray = path.Split('/');
            string fileName = pathArray[pathArray.Length - 1];
            if (pathArray.Length < 2) return;
            if (pathArray[0] != "") return;
            if (pathArray[1].ToLower() != "attached") return;
            string[] fileNameArray = fileName.Split('.');
            string ext=fileNameArray[fileNameArray.Length-1].ToLower();
            if ((ext != "jpg")
                && (ext != "bmp")
                && (ext != "png")
                && (ext != "gif"))
                return;

            //string filePath = Maticsoft.Common.ConfigHelper.GetConfigString("defaultFileDirectory") + "\\" + path.Trim('/').Replace("/", "\\") + "\\" + fileName;
            //以字符流的形式下载文件 
            using (FileStream fs = new FileStream(Server.MapPath("~/")+ path, FileMode.Open))
            {
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                //通知浏览器下载文件而不是打开 
                // Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8));
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                int position = 0;
                while (position < fs.Length)
                {
                    long len = fs.Length - position;
                    if (len > 10 * 1024)
                    {
                        len = 10 * 1024;
                    }
                    fs.Seek(position, SeekOrigin.Begin);
                    byte[] bytes = new byte[(int)len];
                    fs.Read(bytes, 0, (int)len);

                    HttpContext.Current.Response.BinaryWrite(bytes);
                    position += (int)len;
                }
                fs.Close();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.End();
            }

        }
    }
}