﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.House
{
    /// <summary>
    ///House_Rents_Views 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class House_Rents_Views : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int RentId)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;

            xundh.BLL.House_Rents_Views bll = new xundh.BLL.House_Rents_Views();
            xundh.Model.House_Rents_Views m;
            m = new xundh.Model.House_Rents_Views();
            m.RentId = RentId;
            m.AddUserId = U.UserId;
            m.AddUser = U.UserName;
            m.AddTime = now;
            m.AddIp = ip;
            m.AddDepartId = U.DepartId;
            bll.Add(m);
            return ServiceStateKeys.Success;
        }
        /// <summary>
        /// 某用户今天查看多少房源了
        /// </summary>
        /// <param name="SellId"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public int GetViewCount(int UserId)
        {
            DateTime now = DateTime.Now;
            string where = "AddUserId=" + UserId;
            where += String.Format(" AND (AddTime BETWEEN '{0}' AND '{1}')", now.ToShortDateString(), now.ToShortDateString() + " 23:59:59");
            xundh.BLL.BasePager bll = new BLL.BasePager();
            return bll.GetAllCount(where, "House_Rents_Views");
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.House_Rents_Views bll = new xundh.BLL.House_Rents_Views();
            using (DataSet ds = bll.GetList(int.MaxValue, "", "ViewId"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.House_Rents_Views Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.House_Rents_Views bll = new xundh.BLL.House_Rents_Views();
            xundh.Model.House_Rents_Views m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.House_Rents_Views bll = new xundh.BLL.House_Rents_Views();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}