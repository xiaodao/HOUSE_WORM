﻿using EnumKeys;
using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
namespace xundh.API.House
{
    /// <summary>
    ///HM_Sells 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class House_Sells : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int SellId, int ViewLevel, int HouseFloor, int HouseTotalFloor, decimal UnitPrice, decimal TotalPrice,
            string GetPropertyDate, int Decoration, string CustomerName, string CustomerTel,
            int SellRegionArea, bool HasKey, int SellFace,
            int SellBuildingId, decimal Shi, int Ting, int Chu, int Wei, decimal HouseArea, string Dong, string Fang, string Mem
            )
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            CustomerName = StringUtil.CutBadSqlInfo(CustomerName.Trim());
            CustomerTel = StringUtil.CutBadSqlInfo(CustomerTel.Trim());
            Dong = StringUtil.CutBadSqlInfo(Dong.Trim());
            Fang = StringUtil.CutBadSqlInfo(Fang.Trim());
            Mem = StringUtil.CutBadSqlInfo(Mem);
            if (SellBuildingId <= 0) return ServiceStateKeys.NotExists;
            xundh.BLL.HM_Sells bll = new xundh.BLL.HM_Sells();
            xundh.Model.House_Sells m;
            //用户权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (SellId > 0)
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "出售房源", "改")) return ServiceStateKeys.NoPermiss;
                m = bll.GetModel(SellId);
            }
            else
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "出售房源", "增")) return ServiceStateKeys.NoPermiss;

                m = new xundh.Model.House_Sells();
                m.AddUser = U.UserName;
                m.AddUserId = U.UserId;
                m.AddTime = NOW;
                m.AddIp = IP;
                m.AddDepartId = U.DepartId;
                m.FollowCount = 0;
                m.ViewCount = 0;
                m.State = (int)StateKeys.Normal;
            }
            m.ViewLevel = ViewLevel;
            m.Dong = Dong;
            m.Fang = Fang;
            m.HouseFloor = HouseFloor;
            m.HouseTotalFloor = HouseTotalFloor;
            m.UnitPrice = UnitPrice;
            m.TotalPrice = TotalPrice;
            //toDo暂时这样写，有好的方法再改
            if (GetPropertyDate.ToString() == "未知")
            {
                m.GetPropertyDate = DateTime.Parse("1900-1-1");
            }
            else if (GetPropertyDate.ToString().IndexOf("以前") > -1)
            {
                m.GetPropertyDate = DateTime.Parse("2000-1-1");
            }
            else
            {
                string year = GetPropertyDate.Substring(0, GetPropertyDate.Length - 1);
                string date = year + "-1-1";
                m.GetPropertyDate = DateTime.Parse(date);
            }
            m.Decoration = Decoration;
            m.CustomerName = CustomerName;
            m.CustomerTel = CustomerTel;
            m.SellRegionArea = SellRegionArea;
            m.LastUserId = U.UserId;
            m.LastUser = U.UserName;
            m.LastTime = NOW;
            m.LastIp = IP;
            m.HasKey = HasKey;
            if (m.HasKey) m.KeyDepartId = U.DepartId;
            m.SellFace = SellFace;
            m.SellBuildingId = SellBuildingId;
            m.Shi = Shi;
            m.Ting = Ting;
            m.Chu = Chu;
            m.Wei = Wei;
            m.HouseArea = HouseArea;
            m.Mem = Mem;
            m.HasImage = false;
            if (SellId > 0)
                bll.Update(m);
            else
            {
                //不能重复登记
                string where = "SellBuildingId=" + SellBuildingId + " AND Dong='" + Dong + "' AND Fang='" + Fang + "' AND State=" + (int)StateKeys.Normal;
                int count = bll.GetRecordCount(where);
                if (count > 0)
                {
                    return ServiceStateKeys.Exists;
                }
                bll.Add(m);
            }
            Region.Region_Buildings serviceRegion = new Region.Region_Buildings();
            serviceRegion.Recalc("SellCount", SellBuildingId, GetCount(SellBuildingId));
            return ServiceStateKeys.Success;
        }
        /// <summary>
        /// 获取某小区的房源数量
        /// </summary>
        /// <param name="BuildingId"></param>
        /// <returns></returns>
        public int GetCount(int BuildingId)
        {
            xundh.BLL.HM_Sells bll = new BLL.HM_Sells();
            int count = bll.GetRecordCount("SellBuildingId=" + BuildingId);
            return count;
        }
        /// <summary>
        /// 改变钥匙房源状态
        /// </summary>
        /// <param name="HouseId"></param>
        /// <param name="HasKey"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public int ChangeKey(int HouseId)
        {
            if (U == null) return -1;

            xundh.BLL.HM_Sells bll = new BLL.HM_Sells();
            xundh.Model.House_Sells m = bll.GetModel(HouseId);

            //权限
            //bool hasKey = false;
            ////本人的房源可以变更
            //if (m.AddUserId == U.UserId) hasKey = true;

            ////用户权限 有修改出售房源角色可以改，但目前前面显示只判断是不是本人的房源
            //RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            //if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "出售房源", "改"))
            //{
            //    return -1;
            //}

            //if (!hasKey) return -1;
            m.HasKey = !m.HasKey;
            bll.Update(m);
            //写到日志里

            xundh.API.House.House_Sells_Follows bllFollow = new House_Sells_Follows();
            //FollowHouseState应该对应House_Sells表中的State，SuggestState应该对应House_Sells中Suggest字段
            bllFollow.Add(HouseId, ("修改钥匙房源状态为:" + (m.HasKey ? "有钥匙" : "无钥匙")), -1, (int)HouseViewLevel.All,
                 (int)HouseViewLevel.All, m.State ?? 0);
            return m.HasKey ? 1 : 0;
        }
        /// <summary>
        /// 获取某区域的房源数量
        /// </summary>
        /// <param name="BuildingId"></param>
        /// <returns></returns>
        public int GetRegionAreaCount(int RegionAreaId)
        {
            xundh.BLL.HM_Sells bll = new BLL.HM_Sells();
            int count = bll.GetRecordCount("SellRegionArea=" + RegionAreaId);
            return count;
        }
        /// <summary>
        /// 更新状态、单价、总价
        /// </summary>
        /// <param name="SellId"></param>
        /// <param name="UnitPrice">0代表不要修改</param>
        /// <param name="TotalPrice">0代表不要修改</param>
        /// <param name="State">-1代表不修改</param>
        /// <param name="ViewLevel">-1代表不修改</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys UpdateState(int SellId, decimal UnitPrice, decimal TotalPrice, int State, int ViewLevel, int SuggestState)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            if (SellId <= 0) return ServiceStateKeys.NotExists;

            xundh.BLL.HM_Sells bll = new xundh.BLL.HM_Sells();
            xundh.Model.House_Sells m;
            m = bll.GetModel(SellId);

            m.LastUserId = U.UserId;
            m.LastUser = U.UserName;
            if (State == (int)HouseState.Stop || State == (int)HouseState.Already || State == (int)HouseState.Delete)
            {
                API.Org.Org_Posts serviceOrgPost = new Org.Org_Posts();
                //信息员改状态，如果是删除、停售，不更新最后时间
                string PostName = serviceOrgPost.Query(U.PostId ?? 0).PostName;
                if (PostName != "信息员") m.LastTime = NOW;
            }
            else
                m.LastTime = NOW;
            m.LastIp = IP;
            if (ViewLevel != -1) m.ViewLevel = ViewLevel;

            //用户权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "出售房源", "改"))
            {
                ///没有房源修改权限的，如果自己房源，可以修改可见性
                if (U.UserId == m.AddUserId)
                {
                    bll.Update(m);
                    return ServiceStateKeys.Success;
                }
                else
                    return ServiceStateKeys.NoPermiss;
            }
            //有权限修改的
            if (UnitPrice != 0) m.UnitPrice = UnitPrice;
            if (TotalPrice != 0) m.TotalPrice = TotalPrice;
            if (State != -1) m.State = State;
            m.Suggest = SuggestState;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }

        /// <summary>
        /// 更新楼盘id
        /// </summary>
        /// <param name="SellId"></param>
        /// <param name="UnitPrice"></param>
        /// <param name="TotalPrice"></param>
        /// <param name="State">-1代表不修改</param>
        /// <param name="ViewLevel">-1代表不修改</param>
        /// <returns></returns>
        //[WebMethod(EnableSession = true)]
        public ServiceStateKeys UpdateBuilding(int fromBuildingId, int toBuildingId)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            //用户权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "楼盘字典", "改"))
            {
                return ServiceStateKeys.NoPermiss;
            }
            Region.Region_Buildings serviceRegionBuildings = new Region.Region_Buildings();
            xundh.Model.Region_Buildings mBuild = serviceRegionBuildings.Query(toBuildingId);
            string sql = "UPDATE House_Sells SET SellBuildingId=" + toBuildingId + ",SellRegionArea=" + mBuild.BuildingArea + " WHERE SellBuildingId=" + fromBuildingId;
            xundh.BLL.BasePager bllP = new BLL.BasePager();
            bllP.GetGPager(sql);

            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            //角色权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "出售房源", "查")) return null;

            xundh.BLL.BasePager bll = new BLL.BasePager();
            string SellRegionArea = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["SellRegionArea"]));
            int SellFace = StringUtil.Get_1Value(HttpContext.Current.Request["SellFace"]);
            int SellBuildingId = StringUtil.Get_1Value(HttpContext.Current.Request["SellBuildingId"]);
            string BuildingNames = "";
            if (SellBuildingId == -1)
            {
                //文字搜索
                Region.Region_Buildings serviceRegionBuilding = new Region.Region_Buildings();
                string name = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["SellBuildingId"]));
                if (name != "")
                {
                    BuildingNames = name;
                }
            }
            string Address = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["Address"]));
            string Dong = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["Dong"]));
            string Fang = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["Fang"]));
            string CustomerTel = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["CustomerTel"]));
            int Shi = StringUtil.GetIntValue(HttpContext.Current.Request["Shi"]);
            int Ting = StringUtil.GetIntValue(HttpContext.Current.Request["Ting"]);
            decimal TotalPriceMin = StringUtil.GetDecimal(HttpContext.Current.Request["TotalPriceMin"]);
            decimal TotalPriceMax = StringUtil.GetDecimal(HttpContext.Current.Request["TotalPriceMax"]);
            decimal UnitPriceMin = StringUtil.GetDecimal(HttpContext.Current.Request["UnitPriceMin"]);
            decimal UnitPriceMax = StringUtil.GetDecimal(HttpContext.Current.Request["UnitPriceMax"]);
            decimal HouseAreaMin = StringUtil.GetDecimal(HttpContext.Current.Request["HouseAreaMin"]);
            decimal HouseAreaMax = StringUtil.GetDecimal(HttpContext.Current.Request["HouseAreaMax"]);
            string GetPropertyDateMin = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["GetPropertyDateMin"]));
            string GetPropertyDateMax = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["GetPropertyDateMax"]));
            int AddDepartId = StringUtil.Get_1Value(HttpContext.Current.Request["AddDepartId"]);
            int UserId = StringUtil.Get_1Value(HttpContext.Current.Request["UserId"]);
            int HouseFloorMin = StringUtil.GetIntValue(HttpContext.Current.Request["HouseFloorMin"]);
            int HouseFloorMax = StringUtil.GetIntValue(HttpContext.Current.Request["HouseFloorMax"]);
            int State = StringUtil.Get_1Value(HttpContext.Current.Request["State"]);
            int HasKey = StringUtil.Get_1Value(HttpContext.Current.Request["HasKey"]);
            string AddTimeMin = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["AddTimeMin"]));
            string AddTimeMax = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["AddTimeMax"]));
            int Decoration = StringUtil.Get_1Value(HttpContext.Current.Request["Decoration"]);
            int SellId = StringUtil.Get_1Value(HttpContext.Current.Request["SellId"]);
            int Suggest = StringUtil.GetIntValue(REQUEST["Suggest"]);
            int ViewLevel = StringUtil.GetIntValue(REQUEST["ViewLevel"]);

            string where = " 1=1 ";
            string table = "v_House_Sell_Buildings";
            if (SellRegionArea != "") where += " AND SellRegionArea in(" + SellRegionArea + ")";
            if (SellFace > -1) where += " AND SellFace=" + SellFace;
            if (SellBuildingId > -1) where += " AND BuildingId=" + SellBuildingId;
            if (Address != "") where += " AND BuildingAddr LIKE '%" + Address + "%'";
            if (Dong != "") where += " AND Dong='" + Dong + "'";
            if (Fang != "") where += " AND Fang='" + Fang + "'";
            if (StringUtil.IsMobilePhone(CustomerTel))
            {
                where += " AND (CustomerTel LIKE '%" + CustomerTel + "%' OR MEM LIKE '%" + CustomerTel + "%'";
                //从跟进里也查询一次
                API.House.House_Sells_Follows serviceSellFollow = new House_Sells_Follows();
                DataTable dtFolllow = serviceSellFollow.GetLists(1, 20, -1, CustomerTel).Table;
                string ids = ",";
                foreach (DataRow dr in dtFolllow.Rows)
                {
                    string id = dr["SellId"].ToString();
                    if (ids.IndexOf("," + id + ",") < 0)
                        ids += id + ",";
                }
                if (ids.Trim(',') != "")
                    where += " OR SellId in(" + ids.Trim(',') + ")";

                where += ")";
            }
            else
            {
                where += " AND 1=0 ";
            }
            if (Shi > 0) where += " AND Shi=" + Shi;
            if (Ting > 0) where += " AND Ting=" + Ting;
            if (TotalPriceMin > 0) where += " AND TotalPrice>=" + TotalPriceMin;
            if (TotalPriceMax > 0) where += " AND TotalPrice<=" + TotalPriceMax;
            if (UnitPriceMin > 0) where += " AND UnitPrice>=" + UnitPriceMin;
            if (UnitPriceMax > 0) where += " AND UnitPrice<=" + UnitPriceMax;
            if (HouseAreaMin > 0) where += " AND HouseArea>=" + HouseAreaMin;
            if (HouseAreaMax > 0) where += " AND HouseArea<=" + HouseAreaMax;
            if (StringUtil.isDate(GetPropertyDateMin)) where += " AND GetPropertyDate>='" + GetPropertyDateMin + "'";
            if (StringUtil.isDate(GetPropertyDateMax)) where += " AND GetPropertyDate<='" + GetPropertyDateMax + " 23:59:59'";

            if (UserId > 0) { where += " AND AddUserId=" + UserId; }
            else
            {
                if (AddDepartId > 0)
                {
                    Org.Org_Departs serviceOrgDeparts = new Org.Org_Departs();
                    string departids = serviceOrgDeparts.GetStringsDepartsAndSons(AddDepartId);
                    departids = departids.ItemAdd(AddDepartId);
                    if (departids != "")
                        where += " AND AddDepartId in (" + departids + ")";
                }
            }
            if (HouseFloorMin > 0) where += " AND HouseFloor>=" + HouseFloorMin;
            if (HouseFloorMax > 0) where += " AND HouseFloor <=" + HouseFloorMax;
            if (State > -1) where += " AND State=" + State;
            else
                where += " AND State <>" + (int)HouseState.Delete;
            if (HasKey > -1) where += " AND HasKey=" + HasKey;
            if (StringUtil.isDate(AddTimeMin)) where += " AND AddTime>='" + AddTimeMin + "'";
            if (StringUtil.isDate(AddTimeMax)) where += " AND AddTime<='" + AddTimeMax + " 23:59:59'";
            if (Decoration > -1) where += " AND Decoration=" + Decoration;
            if (SellId > 0) where += " AND SellId=" + SellId;
            if (Suggest > 0) where += " AND Suggest>0";
            ///可见性权限，
            if (SystemGetBool("House", "IsAllowListPrivate"))
            {

            }
            else
            {
                //没有查所有的权限，就要受限
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "出售房源", "查所有"))
                {
                    //不允许列表查看私房
                    where += " AND (";
                    where += " ( ViewLevel=" + (int)HouseViewLevel.Self + " AND AddUserId=" + U.UserId + ")";
                    where += " OR ( ViewLevel=" + (int)HouseViewLevel.Depart + " AND AddDepartId=" + U.DepartId + ")";
                    where += " OR ( ViewLevel=" + (int)HouseViewLevel.All + ")";
                    where += " ) ";
                }
            }
            if (ViewLevel == 0) { };
            if (ViewLevel == 1) where += " AND ViewLevel=" + (int)HouseViewLevel.Self + " AND AddUserId=" + U.UserId;
            if (ViewLevel == 2) where += " AND ViewLevel=" + (int)HouseViewLevel.Depart + " AND AddDepartId=" + U.DepartId;
            if (ViewLevel == 3) where += " AND ViewLevel=" + (int)HouseViewLevel.All;

            if (BuildingNames != "") where += " AND BuildingName LIKE '%" + BuildingNames + "%'";
            JqGridHandler datagrid = new JqGridHandler(where);

            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
            string cols = "SellId,ViewLevel,BuildingId,SellRegionArea,SellFace,Dong,Fang,Shi,Ting,Wei,Chu,HouseArea,HouseFloor,HouseTotalFloor,UnitPrice,TotalPrice, BuildYear,GetPropertyDate,Decoration,CustomerName, AddUserId, AddUser, AddTime,AddDepartId,LastUser,LastTime,HasKey, KeyDepartId, FollowCount,ViewCount, State, Mem, BuildingArea, BuildingAddr, BuildingAlice, BuildingName, BuildingLongitude,  BuildingLatitude, BuildingNameEn,Suggest,HasImage ";
            if (SystemGetBool("House", "IsUpdateHouseSellsList"))
                datagrid.init(datagrid.StrWhere, page, recordcount, rows, "LastTime", "Desc");
            else datagrid.init(datagrid.StrWhere, page, recordcount, rows, "SellId", "Desc");
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                ///系统里列表是否显示楼栋号
                if (!SystemGetBool("House", "DongFangInList"))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dr["Dong"] = "";
                        dr["Fang"] = "";
                    }
                }
                else
                {
                    //如果不许查看私房栋号
                    if (!SystemGetBool("House", "IsAllowViewPrivateDongFang"))
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (int.Parse(dr["ViewLevel"].ToString()) == (int)HouseViewLevel.Self)
                            {
                                dr["Dong"] = "";
                                dr["Fang"] = "";
                            }
                        }
                    }

                }
                JsonTable jt = new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
                return jt;
            }

        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.House_Sells Query(int id)
        {
            if (U == null) return null;
            if (id <= 0) return null;

            //用户权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "出售房源", "查")) return null;

            House_Sells_Views serviceView = new House_Sells_Views();
            //如果限制每天查询数量，就限制
            if (SystemGetBool("House", "IsViewCountLimit"))
            {
                int limitcount = SystemGetInt("House", "ViewCountLimit");
                int todayalready = serviceView.GetViewCount(U.UserId);
                if (todayalready >= limitcount) return null;
            }
            xundh.BLL.HM_Sells bll = new xundh.BLL.HM_Sells();
            xundh.Model.House_Sells m = bll.GetModel(id);
            bool hasLimit = true;
            if (m.ViewLevel == (int)HouseViewLevel.All) { }
            else if (m.ViewLevel == (int)HouseViewLevel.Depart)
            {
                if (m.AddDepartId != U.DepartId)
                {
                    //没有权限
                    hasLimit = false;
                }
            }
            else if (m.ViewLevel == (int)HouseViewLevel.Self)
            {
                if (m.AddUserId != U.UserId) hasLimit = false;
            }
            //有查所有的权限，就不受限
            if (serviceOperation.CheckResouceOperationLimit(U.UserId, "出售房源", "查所有"))
            {
                hasLimit = true;
            }
            if (!hasLimit)
            {
                //私房限制，不允许查看私房栋号
                if (!SystemGetBool("House", "IsAllowViewPrivateDongFang"))
                {
                    m.Dong = ""; m.Fang = "";
                }
                m.CustomerTel = "";
            }
            ///生成一次查看记录
            serviceView.Add(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys DeleteBatch(string SellId)
        {
            string[] ids = SellId.Split(',');
            bool result = true;
            foreach (string id in ids)
            {
                result = result && Delete(StringUtil.GetIntValue(id)) == ServiceStateKeys.Success;
            }

            return result ? ServiceStateKeys.Success : ServiceStateKeys.Fail;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete(int SellId)
        {
            if (SellId == 0) return ServiceStateKeys.Fail;
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.HM_Sells bll = new xundh.BLL.HM_Sells();
            xundh.Model.House_Sells m = bll.GetModel(SellId);

            ///权限判段
            bool hasLimit = false;
            ///如果允许删除自己的
            if (SystemGetBool("House", "IsAllowDeleteSelf"))
            {
                if (m.AddUserId == U.UserId) hasLimit = true;
            }
            if (!hasLimit)
            {
                RBAC.RBAC_Users serviceUsers = new RBAC.RBAC_Users();
                hasLimit = serviceUsers.CheckResouceOperationLimit(U.UserId, "出售房源", "删");
            }
            if (!hasLimit) return ServiceStateKeys.NoPermiss;
            ///删除
            m.State = (int)HouseState.Delete;
            m.LastTime = DateTime.Now;
            m.LastIp = HttpContext.Current.Request.UserHostAddress;
            m.LastUser = U.UserName;
            m.LastUserId = U.UserId;
            m.LastDepartId = U.DepartId;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }
        public ServiceStateKeys UpdateStateForCM(int SellId, int? State)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.HM_Sells bll = new xundh.BLL.HM_Sells();
            xundh.Model.House_Sells m;
            m = bll.GetModel(SellId);
            m.State = State;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }

    }
}

public enum HouseState { Normal, Stop, Already, Delete, SelfCopSold } //Duplicate
public enum HouseDecorateKeys { BlankHouse, LiteHouse, CommonHouse, DeluxeHouse }
public enum HouseFaceKeys { Multiple, SmallUpper, Upper, Apartment, CommercialResidential, Shopfront, Store, Villa, Factory, Office, Garage, MultipleComplex, UpperComplex }
public enum HouseViewLevel { Self, Depart, All }
public enum HouseSuggestState { Normal, Baoxiao, Xianshi, Bisha }