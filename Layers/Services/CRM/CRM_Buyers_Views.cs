﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.CRM
{
    /// <summary>
    ///CRM_Customers_Views 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class CRM_Buyers_Views : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int ViewId, int CustomerId, int AddUserId, string AddUser, DateTime AddTime, string AddIp, int AddDepartId
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            AddUser = StringUtil.CutBadSqlInfo(AddUser);
            AddIp = StringUtil.CutBadSqlInfo(AddIp);

            xundh.BLL.CRM_Buyers_Views bll = new xundh.BLL.CRM_Buyers_Views();
            xundh.Model.CRM_Buyers_Views m;
            if (ViewId > 0)
                m = bll.GetModel(ViewId);
            else
                m = new xundh.Model.CRM_Buyers_Views();
            m.CustomerId = CustomerId;
            m.AddUserId = AddUserId;
            m.AddUser = AddUser;
            m.AddTime = AddTime;
            m.AddIp = AddIp;
            m.AddDepartId = AddDepartId;
            if (ViewId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.CRM_Buyers_Views bll = new xundh.BLL.CRM_Buyers_Views();
            using (DataSet ds = bll.GetList(int.MaxValue, "", "ViewId"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.CRM_Buyers_Views Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.CRM_Buyers_Views bll = new xundh.BLL.CRM_Buyers_Views();
            xundh.Model.CRM_Buyers_Views m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.CRM_Buyers_Views bll = new xundh.BLL.CRM_Buyers_Views();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}