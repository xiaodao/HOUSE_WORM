﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
using System.Web;
namespace xundh.API.CRM
{
    /// <summary>
    ///CRM_Customers_Follows 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class CRM_Buyers_Follows : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int CustomerId, string Follow, int FollowCustomerState)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            Follow = StringUtil.CutBadSqlInfo(Follow);

            xundh.BLL.CRM_Buyers_Follows bll = new xundh.BLL.CRM_Buyers_Follows();
            xundh.Model.CRM_Buyers_Follows m;
            m = new xundh.Model.CRM_Buyers_Follows();

            m.AddUserId = U.UserId;
            m.AddUser = U.UserName;
            m.AddTime = NOW;
            m.AddIp = IP;
            m.AddDepartId = U.DepartId;
            m.CustomerId = CustomerId;
            m.Follow = Follow;
            m.FollowCustomerState = FollowCustomerState;
            bll.Add(m);
            //有没有权限改变客户状态
            API.CRM.CRM_Buyers serviceCRMBuyers = new CRM_Buyers();
            return serviceCRMBuyers.UpdateState(CustomerId, FollowCustomerState);
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows, int CustomerId)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();

            string where = "1=1";
            string cols = "FollowId,CustomerId,CustomerName,State,WantRegionArea,WantBuildings,Follow,FollowCustomerState,AddUserId,AddUser,AddTime,AddIp,AddDepartId,ViewLevel";
            string table = "v_CRM_Buyer_Customers";
            string txtUserName = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtUserName"]));
            string txtDeparts = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtDeparts"]));

            if (txtUserName != "")
            {
                if (txtUserName.IndexOf(",") > -1 || StringUtil.IsInt(txtUserName))
                {
                    where += " AND AddUserId in(" + txtUserName + ")";
                }
                else
                {
                    string condition = "'%" + txtUserName + "%'";
                    where += " AND (CustomerName LIKE " + condition + " OR Tel LIKE " + condition + " OR Mem LIKE " + condition + ")";
                }
            }
            if (txtDeparts != "")
            {
                where += " AND AddDepartId in(" + txtDeparts + ")";
            }
            if (CustomerId > 0)
            {
                where += " AND CustomerId=" + CustomerId;
            }
            //角色权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (SystemGetBool("House", "IsAllowUserSetFollowViewLevel"))
            {
                //允许用户设置可见性权限
                where += " AND (";
                where += "ViewLevel=" + (int)HouseViewLevel.All;
                where += " OR (ViewLevel=" + (int)HouseViewLevel.Depart + " AND AddDepartId=" + U.DepartId + ")";
                where += " OR (ViewLevel=" + (int)HouseViewLevel.Self + " AND AddUserId=" + U.UserId + ")";
                where += ")";
            }
            else { 
                //使用系统权限
                if (serviceOperation.CheckResouceOperationLimit(U.UserId, "客源管理", "查所有"))
                {

                }
                else if (serviceOperation.CheckResouceOperationLimit(U.UserId, "客源管理", "查部门"))
                {
                    where += " AND ( AddDepartId=" + U.DepartId + ")";
                }
                else
                {
                    where += " AND (AddUserId=" + U.UserId + ")";
                }
            }
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
            datagrid.init(where, page, recordcount, rows, "FollowId", "DESC");

            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                JsonTable jt = new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
                return jt;
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.CRM_Buyers_Follows Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.CRM_Buyers_Follows bll = new xundh.BLL.CRM_Buyers_Follows();
            xundh.Model.CRM_Buyers_Follows m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.CRM_Buyers_Follows bll = new xundh.BLL.CRM_Buyers_Follows();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}