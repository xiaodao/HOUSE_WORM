﻿using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.Region
{
    /// <summary>
    ///I_Region_Area 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [System.Web.Script.Services.ScriptService]
    public class Region_Area : BaseService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Add(int id, string AreaName, int CityId)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            xundh.BLL.Region_Area bll = new xundh.BLL.Region_Area();

            //权限
            //RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();

            xundh.Model.Region_Area m;
            if (id > 0)
            {
                //if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "区域管理", "改")) return ServiceStateKeys.NoPermiss;
                m = bll.GetModel(id);
            }
            else
            {
                //if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "区域管理", "增")) return ServiceStateKeys.NoPermiss;
                m = new xundh.Model.Region_Area();
            }
            m.AreaName = AreaName;
            
            m.CityId = CityId;
            if (id > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.BLL.Region_Area bll = new xundh.BLL.Region_Area();
            string where = "1=1";

            using (DataSet ds = bll.GetList(where))
            {
                return new JsonTable( ds.Tables[0],bll.GetRecordCount(where));
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Region_Area Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Region_Area bll = new xundh.BLL.Region_Area();
            xundh.Model.Region_Area m = bll.GetModel(id);
            return m;
        }

    }
}