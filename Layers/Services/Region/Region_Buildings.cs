﻿using EnumKeys;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;

namespace xundh.API.Region
{
    /// <summary>
    ///I_Buildings 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]

    //若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。 
    [System.Web.Script.Services.ScriptService]
    public class Region_Buildings : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int BuildingId, int BuildingArea, string BuildingAlice, string BuildingName, string BuildingAddr,
            decimal BuildingLongitude, decimal BuildingLatitude
            )
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;

            xundh.BLL.Region_Buildings bll = new xundh.BLL.Region_Buildings();
            xundh.Model.Region_Buildings m;
            if (BuildingId > 0)
            {
                m = bll.GetModel(BuildingId);
                //是否已经存在
                List<xundh.Model.Region_Buildings> searchs = bll.GetModelList("BuildingName='" + BuildingName + "' AND IsDelete=0");
                if (searchs.Count > 0)
                {
                    if (searchs[0].BuildingId != BuildingId)
                        return ServiceStateKeys.Exists;
                }
            }
            else
            {
                m = new xundh.Model.Region_Buildings();
                //是否已经存在
                List<xundh.Model.Region_Buildings> searchs = bll.GetModelList("BuildingName='" + BuildingName + "' AND IsDelete=0");
                if (searchs.Count > 0)
                {
                    return ServiceStateKeys.Exists;
                }
                m.AddDepartId = mLoginUser.DepartId;
                m.AddIp = ip;
                m.AddTime = now;
                m.AddUser = mLoginUser.UserName;
                m.AddUserId = mLoginUser.UserId;
            }
            m.LastDepartId = mLoginUser.DepartId;
            m.LastIp = ip;
            m.LastTime = now;
            m.LastUser = mLoginUser.UserName;
            m.LastUserId = mLoginUser.UserId;

            m.BuildingArea = BuildingArea;
            m.BuildingAddr = BuildingAddr;
            m.BuildingAlice = BuildingAlice;
            m.BuildingAliceEn = ChineseForFirstCode.IndexCode(BuildingAlice);
            m.BuildingName = BuildingName;
            m.BuildingNameEn = ChineseForFirstCode.IndexCode(BuildingName);
            m.BuildingLongitude = BuildingLongitude;
            m.BuildingLatitude = BuildingLatitude;
            if (BuildingId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.Region_Buildings bll = new xundh.BLL.Region_Buildings();
            int AreaId = StringUtil.GetIntValue(HttpContext.Current.Request["selectArea"]);
            string BuildName = StringUtil.TBCode(StringUtil.GetNullToString(HttpContext.Current.Request["txtBuildName"]));
            return GetListsByName(page, rows, BuildName, AreaId);
        }

        [WebMethod(EnableSession = true)]
        public JsonTable GetListsByName(int page, int rows, string BuildName, int AreaId)
        {
            if (U == null) return null;
            xundh.BLL.Region_Buildings bll = new xundh.BLL.Region_Buildings();
            string where = " 1=1 ";
            if (AreaId > 0) where += " AND BuildingArea=" + AreaId;

            if (BuildName != "")
            {
                ///如果是传来的整数，则认为是id
                if (StringUtil.IsInt(BuildName))
                    where += " AND BuildingId=" + BuildName;
                else
                    where += " AND (BuildingName LIKE '%" + BuildName + "%' OR BuildingAlice LIKE '%" + BuildName + "%' OR BuildingAddr LIKE '%" + BuildName + "%' OR BuildingNameEn LIKE '%" + BuildName + "%')";
            }
            where += " AND IsDelete=0";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetRecordCount(datagrid.StrWhere);
            datagrid.init(where, page, recordcount, rows, "", "");
            using (DataSet ds = bll.GetListByPage(datagrid.StrWhere, datagrid.Order, datagrid.StartIndex, datagrid.EndIndex))
            {
                JsonTable jt = new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
                return jt;
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete(int BuildingId)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Region_Buildings bll = new xundh.BLL.Region_Buildings();
            Model.Region_Buildings m = bll.GetModel(BuildingId);
            m.IsDelete = true;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Region_Buildings Query(int BuildId)
        {
            if (U == null) return null;
            xundh.BLL.Region_Buildings bll = new xundh.BLL.Region_Buildings();
            return bll.GetModel(BuildId);
        }
        //[WebMethod(EnableSession = true)]
        //[ScriptMethod(UseHttpGet = true)]
        //public xundh.Model.Region_Buildings QueryBySellId(int HouseId, string type)
        //{
        //    if (U == null) return null;
        //    int BuildingId = 0;
        //    type = StringUtil.GetNullToString(type).Trim('\'');
        //    if (type == "sell")
        //    {
        //        API.House.House_Sells serviceHouseSells = new House.House_Sells();
        //        xundh.Model.House_Sells mSell = serviceHouseSells.Query(HouseId);
        //        if (mSell == null) return null;
        //        BuildingId = mSell.SellBuildingId??0;
        //    }
        //    else {
        //        API.House.House_Rents serviceHouseRents = new House.House_Rents();
        //        xundh.Model.House_Rents mSell = serviceHouseRents.Query(HouseId);
        //        if (mSell == null) return null;
        //        BuildingId = mSell.RentBuildingId ?? 0;
        //    }
        //    if (BuildingId > 0)
        //    {
        //        xundh.BLL.Region_Buildings bll = new xundh.BLL.Region_Buildings();
        //        return bll.GetModel(BuildingId);
        //    }
        //    return null;
        //}

        /// <summary>
        /// 从名称查询一个楼盘实体
        /// </summary>
        /// <param name="BuildId"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Region_Buildings QueryName(string BuildName)
        {
            if (U == null) return null;
            xundh.BLL.Region_Buildings bll = new xundh.BLL.Region_Buildings();
            string where = "1=1";
            where += " AND (BuildingName LIKE '%" + BuildName + "%' OR BuildingAlice LIKE '%" + BuildName + "%' OR BuildingAddr LIKE '%" + BuildName + "%' OR BuildingNameEn LIKE '%" + BuildName + "%')";
            List<xundh.Model.Region_Buildings> ms = bll.GetModelList(where);
            if (ms.Count > 0) return ms[0];
            else
                return null;
        }
        /// <summary>
        /// 更新楼盘的出售、出租房源数量
        /// </summary>
        /// <param name="type">SellCount RentCount</param>
        /// <param name="BuildingId"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public int Recalc(string type, int BuildingId, int count)
        {
            if (U == null) return 0;
            xundh.BLL.Region_Buildings bll = new BLL.Region_Buildings();
            xundh.Model.Region_Buildings m = bll.GetModel(BuildingId);
            if (type == "SellCount")
                m.SellCount = count;
            else if (type == "RentCount")
                m.RentCount = count;
            bll.Update(m);
            return count;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Merge(int fromBuildId, int toBuildId)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            //权限
            House.House_Sells serviceSells = new House.House_Sells();
            serviceSells.UpdateBuilding(fromBuildId, toBuildId);
            House.House_Rents serviceRents = new House.House_Rents();
            serviceRents.UpdateBuilding(fromBuildId, toBuildId);
            BLL.Region_Buildings bll = new BLL.Region_Buildings();
            Model.Region_Buildings m = bll.GetModel(fromBuildId);
            m.IsDelete = true;
            bll.Update(m);
            Recalc("SellCount", toBuildId, serviceSells.GetCount(toBuildId));
            Recalc("RentCount", toBuildId, serviceRents.GetCount(toBuildId));

            return ServiceStateKeys.Success;
        }
    }
}