﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.HR
{
    /// <summary>
    ///HR_Excitation 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class HR_Excitation : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int ExcitationId,
           int UserId, int wf_depart, DateTime wf_time,
            string wf_address, string wf_matter, decimal wf_amount, int wf_type)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理

            xundh.BLL.HR_Excitation bll = new xundh.BLL.HR_Excitation();
            xundh.Model.HR_Excitation m;
            if (ExcitationId > 0)
                m = bll.GetModel(ExcitationId);
            else
            {
                m = new xundh.Model.HR_Excitation();

                m.AddUserId = U.UserId;
                m.AddUser = U.UserName;
                m.AddDepartId = U.DepartId;

                m.VerifyUserId = 0;
                m.VerifyUser = "";
                m.VerifyDepartId = 0;
            }
            RBAC.RBAC_Users serviceRBACUsers = new RBAC.RBAC_Users();
            m.Amount = wf_amount;
            m.ObjectUserId = UserId;
            m.ObjectUser = serviceRBACUsers.Query(UserId).UserName;
            m.ObjectDepartId = wf_depart;
            m.State = (int)ExcitationState.Need;
            m.ObjectTime = wf_time;
            m.ObjectAddress = wf_address;
            m.Matter = wf_matter;
            m.Type = wf_type;
            if (ExcitationId > 0)
                bll.Update(m);
            else
                ExcitationId = bll.Add(m);
            //新建流程
            WF.WF_Items serviceWFItems = new WF.WF_Items();
            serviceWFItems.AddTemplateTitle(0, "激励审核流程", "", ExcitationId, "ExcitationId");
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            int DepartId = StringUtil.GetIntValue(REQUEST["txtDeparts"]);
            int UserId = StringUtil.GetIntValue(REQUEST["UserId"]);
            int wf_type = StringUtil.GetIntValue(REQUEST["wf_type"]);

            if (DepartId > 0) where += " AND ObjectDepartId=" + DepartId;
            if (UserId > 0) where += " AND ObjectUserId =" + UserId;
            if (wf_type > -1) where += " AND Type=" + wf_type;

            string table = "HR_Excitation";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "ExcitationId,ObjectAddress,State,VerifyUserId,VerifyUser,VerifyDepartId,Matter,Type,AddUserId,AddUser,AddDepartId,Amount,ObjectUserId,ObjectUser,ObjectDepartId,ObjectTime";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "ExcitationId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.HR_Excitation Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.HR_Excitation bll = new xundh.BLL.HR_Excitation();
            xundh.Model.HR_Excitation m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.HR_Excitation bll = new xundh.BLL.HR_Excitation();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}
public enum ExcitationType
{
    Zheng, Fu
}
public enum ExcitationState
{
    Need, Pass, NotPass
}