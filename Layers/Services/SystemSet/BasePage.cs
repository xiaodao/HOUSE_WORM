﻿using EnumKeys;
using System;

public partial class BasePage : System.Web.UI.Page
{
    public xundh.Model.RBAC_Users U;
    protected void Page_Init(object sender, EventArgs e)
    {
        if (Session[EnumKeys.SessionKey.USER] != null)
        {
            U = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
        }
        else
        {
            Response.Redirect("~/Error.aspx?go=/Login.htm&information=" + Server.UrlEncode("登陆超时，请重新登陆。"));
            return;
        }
    }
    public void CheckLimit() { 
    
    }
}
