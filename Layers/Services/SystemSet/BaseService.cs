﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Services;
using EnumKeys;
using Webapp;
using xundh.Common.Utils;
namespace xundh.API
{
    public partial class BaseService : System.Web.Services.WebService
    {
        #region 公有变量
        public xundh.Model.RBAC_Users U = null;
        public HttpRequest REQUEST = HttpContext.Current.Request;
        public DateTime NOW = DateTime.Now;
        public string IP = HttpContext.Current.Request.UserHostAddress;

        public BaseService()
        {
            Computer computer = Computer.Instance();
            string txt = "CpuID=" + computer.CpuID;
            txt += Environment.NewLine;
            txt += "DiskID=" + computer.DiskID;
            txt += Environment.NewLine;
            txt += "MacAddress=" + computer.MacAddress;
            txt += Environment.NewLine;
            string tempMd5 = StringUtil.EncryptMD5(txt + "xundh");

            string compareString = SystemGetString("SystemSet","SN");
            //if (tempMd5 != compareString)
            //{
            //}
            //else
            //{
            //    //todo:限制登录到期时间
            //    if (DateTime.Now > DateTime.Parse("2017-4-30"))
            //    {

            //    }
            //    else
            //    {
                    if (HttpContext.Current.Session[EnumKeys.SessionKey.USER] == null) return;
                    U = (xundh.Model.RBAC_Users)HttpContext.Current.Session[EnumKeys.SessionKey.USER];
            //    }
            //}
        }
        #endregion
        #region 读取配置
        private string file = HttpContext.Current.Server.MapPath("~/") + "ini\\SystemSet.config";
        [WebMethod(EnableSession = true)]
        public string SystemGetString(string module, string key)
        {
            if (module == "SystemSet" && key == "SN") { 
                //读取序列号不需要判断Session
            }
            else if (module == "House" && key == "IsChangeLoginStyle")
            {
            
            }
            else
            {
                if (Session[EnumKeys.SessionKey.USER] == null) return "";
            }
            iniFile ini = new iniFile();
            ini.IniFile(file);
            string result = ini.GetString(module, key, "false");
            return result;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys SystemSetString(string module, string key, string value)
        {
            if (Session[EnumKeys.SessionKey.USER] == null) return ServiceStateKeys.TimeOut;
            string ShowDongInList = StringUtil.TBCode(key);
            if (!File.Exists(file))
            {
                return ServiceStateKeys.NotExists;
            }
            iniFile ini = new iniFile();
            ini.IniFile(file);
            ini.WriteString(module, key, value);
            return ServiceStateKeys.Success;
        }
        [WebMethod(EnableSession = true)]
        public bool SystemGetBool(string module, string key)
        {
            if (module == "House" && key == "IsChangeLoginStyle")
            { 
                
            }
            else
            {
                if (Session[EnumKeys.SessionKey.USER] == null) return false;
            }
            iniFile ini = new iniFile();
            ini.IniFile(file);
            string result = ini.GetString(module, key, "false");
            return result == "true";
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys SystemSetBool(string module, string key, bool value)
        {
            if (Session[EnumKeys.SessionKey.USER] == null) return ServiceStateKeys.TimeOut;
            string ShowDongInList = StringUtil.TBCode(key);
            if (!File.Exists(file)) {
                return ServiceStateKeys.NotExists;
            }
            iniFile ini = new iniFile();
            ini.IniFile(file);
            ini.WriteString(module, key, value ? "true" : "false");
            return ServiceStateKeys.Success;
        }
        [WebMethod(EnableSession = true)]
        public int SystemGetInt(string module, string key)
        {
            if (Session[EnumKeys.SessionKey.USER] == null) return 0;
            iniFile ini = new iniFile();
            ini.IniFile(file);
            int result = ini.GetInt(module, key, 0);
            return result;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys SystemSetInt(string module, string key, int value)
        {
            if (Session[EnumKeys.SessionKey.USER] == null) return ServiceStateKeys.TimeOut;
            string ShowDongInList = StringUtil.TBCode(key);
            if (!File.Exists(file))
            {
                return ServiceStateKeys.NotExists;
            }
            iniFile ini = new iniFile();
            ini.IniFile(file);
            ini.WriteInt(module, key, value);
            return ServiceStateKeys.Success;
        }
        #endregion
    }
}