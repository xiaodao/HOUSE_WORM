﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
namespace xundh.API.SystemSet
{
    /// <summary>
    ///Update 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class SystemSetup : System.Web.Services.WebService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public void Install()
        {
            //string Modules = StringUtil.GetNullToString(HttpContext.Current.Request["Modules"]);
            //string[] ModuleArray = Modules.Split(',');
            //foreach (string module in ModuleArray) { 
            //    //删除对应模块
            //    string path = Server.MapPath("~/");
            //    path +=  module;
            //    System.IO.File.WriteAllText("C:\\c\\1.txt", path);
            //    try
            //    {
            //        if (Directory.Exists(path))
            //        {
            //            Directory.Delete(path);
            //        }
            //    }
            //    catch (Exception) { 
                
            //    }
            //}
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public bool SetupDatabase(String ip,String database,String account,String password) {
            if (ip == null || ip == "") return false;
            if (database == null || database == "") return false;
            if (account == null || account == "") return false;
            if (password == null || password == "") return false;
            //尝试连接数据库
            try
            {
                SqlConnection con = new SqlConnection();
                con.ConnectionString = String.Format("server={0};database={1};uid={2};pwd={3}", ip, database, account, password);
                con.Open();
                con.Close();
            }
            catch (Exception ) {
                return false;
            }
            //
            Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/");
            //config.AppSettings.Settings.Remove("ConnectionString");
            config.AppSettings.Settings["ConnectionString"].Value = String.Format("server={0};database={1};uid={2};pwd={3};Max Pool Size=8048;", ip, database, account, password);
            config.Save(ConfigurationSaveMode.Modified);   
            return true;
        }
    }
}