﻿using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
namespace xundh.API.SystemSet
{
    /// <summary>
    ///Update 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class SystemSet : BaseService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public static void ClearAllCache() {
            System.Web.Caching.Cache _cache = HttpRuntime.Cache;
            System.Collections.IDictionaryEnumerator CacheEnum = _cache.GetEnumerator();
            while (CacheEnum.MoveNext())
            {
                string _key = CacheEnum.Key.ToString();
                _cache.Remove(_key);
            }
        }
    }
}