﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.RBAC
{
    /// <summary>
    ///RBAC_Roles 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class RBAC_Roles : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int RoleId, string RoleName)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理
            RoleName = StringUtil.CutBadSqlInfo(RoleName);

            xundh.BLL.RBAC_Roles bll = new xundh.BLL.RBAC_Roles();
            xundh.Model.RBAC_Roles m;
            if (RoleId > 0)
                m = bll.GetModel(RoleId);
            else
                m = new xundh.Model.RBAC_Roles();
            m.RoleName = RoleName;
            if (RoleId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Roles bll = new xundh.BLL.RBAC_Roles();
            using (DataSet ds = bll.GetList(int.MaxValue, "", "RoleId"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        /// <summary>
        /// 某用户所有角色
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetListsByUserId(int UserId)
        {
            //if (U == null) return null; //登陆时需要调用
            string table = "v_RBAC_Users_Roles";
            string cols = "UserId,UserName,UserNameEn,UserPass,UserSex,UserState,UserMobile,UserEmail,UserAdmin,UserSessionId,UserOnline,DepartId,PostId,Mem,RoleId,RoleName,RoleDescription";
            string where = "UserId=" + UserId;
            xundh.BLL.BasePager bllPager = new BLL.BasePager();
            using (DataSet ds = bllPager.GetPagerDistinct(int.MaxValue, 1, where, "RoleId", table, cols))
            {
                return new JsonTable(ds.Tables[0], bllPager.GetAllCount(where,table));
            }
        }


        /// <summary>
        /// 某用户所有角色ByUserName
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetListsByUserName(string UserName)
        {
            //if (U == null) return null; //登陆时需要调用
            string table = "v_RBAC_Users_Roles";
            string cols = "UserId,UserName,UserNameEn,UserPass,UserSex,UserState,UserMobile,UserEmail,UserAdmin,UserSessionId,UserOnline,DepartId,PostId,Mem,RoleId,RoleName,RoleDescription";
            string where = "UserName=" + UserName;
            xundh.BLL.BasePager bllPager = new BLL.BasePager();
            using (DataSet ds = bllPager.GetPagerDistinct(int.MaxValue, 1, where, "RoleId", table, cols))
            {
                return new JsonTable(ds.Tables[0], bllPager.GetAllCount(where, table));
            }
        }

        /// <summary>
        /// 某用户所有角色,RoleId组合的字符串
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public string GetRoleIdsByUserId(int UserId)
        {
            //if (U == null) return null; 登陆前就要调用此函数
            JsonTable jt = GetListsByUserId(UserId);
            string str = "";
            if (jt.Table.Rows.Count > 0) {
                foreach (DataRow dr in jt.Table.Rows) {
                    str += dr["RoleId"].ToString() + ",";
                }
            }
            str = str.Trim(',');
            return str;
        }
        /// <summary>
        /// 某用户所有角色,RoleId组合的字符串
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public string GetRoleIdsByUserName(string UserName)
        {
            //if (U == null) return null; 登陆前就要调用此函数
            JsonTable jt = GetListsByUserName(UserName);
            string str = "";
            if (jt.Table.Rows.Count > 0)
            {
                foreach (DataRow dr in jt.Table.Rows)
                {
                    str += dr["RoleId"].ToString() + ",";
                }
            }
            str = str.Trim(',');
            return str;
        }

        /// <summary>
        /// 某用户所有角色，RoleName组合的字符串
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public string GetRoleNamesByUserId(int UserId)
        {
            if (U == null) return null;
            JsonTable jt = GetListsByUserId(UserId);
            string str = "";
            if (jt.Table.Rows.Count > 0)
            {
                foreach (DataRow dr in jt.Table.Rows)
                {
                    str += dr["RoleName"].ToString() + ",";
                }
            }
            str = str.Trim(',');
            return str;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.RBAC_Roles Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Roles bll = new xundh.BLL.RBAC_Roles();
            xundh.Model.RBAC_Roles m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.RBAC_Roles bll = new xundh.BLL.RBAC_Roles();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}