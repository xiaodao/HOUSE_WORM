﻿using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
using System.Collections.Generic;
namespace xundh.API.RBAC
{
    /// <summary>
    ///RBAC_Role_Resource_Operation 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class RBAC_Role_Resource_Operation : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Update(int RoleId, int ResourceId, int OperationId, bool Value
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///参数处理

            xundh.BLL.RBAC_Role_Resource_Operation bll = new xundh.BLL.RBAC_Role_Resource_Operation();
            xundh.Model.RBAC_Role_Resource_Operation m;
            List<xundh.Model.RBAC_Role_Resource_Operation> models = bll.GetModelList("RoleId=" + RoleId + " AND ResourceId=" + ResourceId + " AND OperationId=" + OperationId);
            if (models.Count > 0)
                m = models[0];
            else
                m = new xundh.Model.RBAC_Role_Resource_Operation();
            m.RoleId = RoleId;
            m.ResourceId = ResourceId;
            m.OperationId = OperationId;
            m.Value = Value;
            if (models.Count > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        /// <summary>
        /// 从角色获取所有资源及操作
        /// </summary>
        /// <param name="RoleIds"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(string RoleIds)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = "1=1";
            string table = "v_RBAC_Roles_Resouce_Operations";
            string cols = "RoleAuthorityId,Value,ResourceId,RoleId,ResourceName,ResourceDescription,OperationName,OperationDescription,ParentResourceId,ResourcePath,OperationId";
            if (RoleIds != "")
                RoleIds = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(RoleIds)).Trim();
            if (RoleIds != "")
                where += " AND RoleId in(" + RoleIds + ")";
            where += " AND value=1";
            using (DataSet ds = bll.GetPager(int.MaxValue, 1, where, "ResourceId", table, cols))
            {
                return new JsonTable(ds.Tables[0], bll.GetAllCount(where, table));
            }
        }
        /// <summary>
        /// 从用户获取所有资源及操作
        /// </summary>
        /// <param name="RoleIds"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetUserOperations(int UserId)
        {
            if (U == null) return null;
            xundh.API.RBAC.RBAC_Roles serviceRoles = new RBAC_Roles();
            string roleids = serviceRoles.GetRoleIdsByUserId(UserId);
            return GetLists(roleids);
        }
        /// <summary>
        /// 某模块 操作权限的 所有角色,返回id逗号隔开
        /// </summary>
        /// <param name="RoleIds"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public string GetRoleLists(string ResourceName, string OperationName)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = "1=1";
            string table = "v_RBAC_Roles_Resouce_Operations";
            string cols = "Value,ResourceId,RoleId,ResourceName,ResourceDescription,OperationName,OperationDescription,ParentResourceId,ResourcePath,OperationId";
            ResourceName = StringUtil.CutBadSqlInfo(ResourceName);
            OperationName = StringUtil.CutBadSqlInfo(OperationName);
            if (ResourceName != "") where += " AND ResourceName ='" + ResourceName + "'";
            if (OperationName != "") where += " AND OperationName='" + OperationName + "'";
            where += " AND Value=1";
            string result = "";
            using (DataSet ds = bll.GetPager(int.MaxValue, 1, where, "ResourceId", table, cols))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if ((result + ',').IndexOf(',' + dr["RoleId"].ToString() + ',') < 0)
                        result += "," + dr["RoleId"].ToString();
                }
                //return new JsonTable(ds.Tables[0], bll.GetAllCount(where, table));
            }
            return result.Trim(',');
        }
        /// <summary>
        /// 某角色有没有 某资源 某操作 权限
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="ResourceName"></param>
        /// <param name="OperationName"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public bool QueryResourceOperationByRoleId(int RoleId, string ResourceName, string OperationName)
        {
            if (U == null) return false;
            xundh.BLL.RBAC_Role_Resource_Operation bll = new xundh.BLL.RBAC_Role_Resource_Operation();
            string where = "1=1";
            where += " AND RoleId=" + RoleId;
            where += " AND ResourceName='" + ResourceName + "'";
            where += " AND OperationName='" + OperationName + "'";
            List<xundh.Model.RBAC_Role_Resource_Operation> ms = bll.GetModelList(where);
            if (ms.Count == 0) return false;

            return ms[0].Value;
        }
        /// <summary>
        /// 某些角色里有没有 某资源 某操作 权限
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="ResourceName"></param>
        /// <param name="OperationName"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public bool QueryResourceOperationByRoleIds(string RoleIds, string ResourceName, string OperationName)
        {
            if (U == null) return false;
            if (RoleIds == "") return false;
            xundh.BLL.BasePager bll = new xundh.BLL.BasePager();
            string table = "v_RBAC_Roles_Resouce_Operations";
            string where = "1=1";
            string cols = "Value,RoleId,ResourceName,OperationName";
            where += " AND RoleId in(" + RoleIds + ")";
            where += " AND ResourceName='" + ResourceName + "'";
            where += " AND OperationName='" + OperationName + "'";
            using (DataSet ds = bll.GetPager(int.MaxValue, 1, where, "RoleId", table, cols))
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    return bool.Parse(ds.Tables[0].Rows[0]["Value"].ToString());
                }
            }
            return false;
        }
        /// <summary>
        /// 某个用户里有没有 某资源 某操作 权限
        /// </summary>
        /// <param name="RoleId"></param>
        /// <param name="ResourceName"></param>
        /// <param name="OperationName"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public bool QueryResourceOperationByUserId(int UserId, string ResourceName, string OperationName)
        {
            if (U == null) return false;
            ///取得该用户所有角色
            xundh.API.RBAC.RBAC_Roles serviceRole = new RBAC_Roles();
            string RoleIds = serviceRole.GetRoleIdsByUserId(UserId);
            return QueryResourceOperationByRoleIds(RoleIds, ResourceName, OperationName);
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.RBAC_Role_Resource_Operation bll = new xundh.BLL.RBAC_Role_Resource_Operation();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}