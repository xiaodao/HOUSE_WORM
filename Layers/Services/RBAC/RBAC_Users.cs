﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.RBAC
{
    /// <summary>
    ///I_RBAC_Users 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class RBAC_Users : BaseService
    {
        //环信注册参数处理
        string clientId = ConfigurationManager.AppSettings["ClientId"];
        string secret = ConfigurationManager.AppSettings["Secret"];
        string orgName = ConfigurationManager.AppSettings["OrgName"];
        string appName = ConfigurationManager.AppSettings["AppName"];


        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int UserId, int DepartId,
            int PostId, string UserName, string Mem, string UserPass,
            int UserSex, int UserState, string UserMobile, string UserEmail, bool UserAdmin, string Roles
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;

            ///参数处理
            UserName = StringUtil.CutBadSqlInfo(UserName);
            Mem = StringUtil.CutBadSqlInfo(Mem);
            UserPass = StringUtil.CutBadSqlInfo(UserPass);
            UserMobile = StringUtil.CutBadSqlInfo(UserMobile);
            UserEmail = StringUtil.CutBadSqlInfo(UserEmail);
            Roles = StringUtil.CutBadSqlInfo(Roles);
            //string ishaslimitusechat = ConfigurationManager.AppSettings["IsHasLimitUseChat"];
            //xundh.API.Chat.Chat_HX hx = new Chat.Chat_HX(clientId, secret, appName, orgName);
            xundh.BLL.RBAC_Users bll = new xundh.BLL.RBAC_Users();
            xundh.Model.RBAC_Users m;
            if (UserId > 0)
            {
                m = bll.GetModel(UserId);
                m.UserName = UserName;
            }
            else
            {
                //不能重名
                List<xundh.Model.RBAC_Users> ms = bll.GetModelList("UserName='" + UserName + "'");
                if (ms.Count > 0)
                {
                    return ServiceStateKeys.Exists;
                }
                m = new xundh.Model.RBAC_Users();
                m.AddUserId = U.UserId;
                m.AddUser = U.UserName;
                m.UserName = UserName;
                m.AddDepartId = U.DepartId;
                m.AddTime = NOW;
                m.AddIp = IP;
            }
            m.DepartId = DepartId;
            m.PostId = PostId;
            m.LastUserId = U.UserId;
            m.LastUser = U.UserName;
            m.LastDepartId = U.DepartId;
            m.LastTime = NOW;
            m.LastIp = IP;
            m.Mem = Mem;
            m.UserNameEn = ChineseForFirstCode.IndexCode(m.UserName);
            ///如果密码传过来
            if (UserPass != "")
                m.UserPass = StringUtil.EncryptMD5NotUnicode(UserPass);
            m.UserSex = UserSex;
            m.UserState = UserState;
            m.UserMobile = UserMobile;
            m.UserEmail = UserEmail;
            m.UserAdmin = UserAdmin;
            //查询现有角色
            RBAC_User_Role serviceUserRole = new RBAC_User_Role();
            RBAC_Roles serviceRoles = new RBAC_Roles();
            string oldRoles = serviceRoles.GetRoleIdsByUserId(UserId);

            //serviceUserRole.Add(0, UserId, int.Parse(Roles));
            //有没有新增的角色，如果有就加进表里
            string[] newRolesArr = Roles.Split(',');
            string[] oldRolesArr = oldRoles.Split(',');

            foreach (string newRole in newRolesArr)
            {
                bool isExits = false;
                foreach (string oldRole in oldRolesArr)
                {
                    if (oldRole == newRole)
                    {
                        isExits = true;
                        break;
                    }
                }
                if (!isExits && StringUtil.IsInt(newRole))
                {
                    serviceUserRole.Add(0, UserId, int.Parse(newRole));
                }
            }
            ///是否有角色要删除
            foreach (string oldRole in oldRolesArr)
            {
                bool isExists = false;
                if (Roles != "")
                {
                    foreach (string newRole in newRolesArr)
                    {
                        if (oldRole == newRole)
                        {
                            isExists = true;
                            break;
                        }
                    }
                }
                if (!isExists)
                {
                    if (StringUtil.IsInt(oldRole))
                        serviceUserRole.Delete(UserId, int.Parse(oldRole));
                }
            }


            if (UserId > 0)
            {

                bll.Update(m);
            }
            else
            {
                m.UserId = bll.Add(m);
                //if (ishaslimitusechat == "true")
                //{
                //    hx.AccountCreate(ChineseToPinYin.ToPinYin(m.UserName).Replace(" ", "").Trim() + m.UserId, m.UserPass);
                //}
            }
            return ServiceStateKeys.Success;
        }
        /// <summary>
        /// 更新个人资料
        /// </summary>
        /// <param name="UserPass"></param>
        /// <param name="UserSex"></param>
        /// <param name="UserMobile"></param>
        /// <param name="UserEmail"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys UpdateInfo(int UserSex)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            //Chat.Chat_HX hx = new Chat.Chat_HX(clientId, secret, appName, orgName);
            string ishaslimitusechat = ConfigurationManager.AppSettings["IsHasLimitUseChat"];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            string UserMobile = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["UserMobile"]));
            string UserEmail = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["UserEmail"]));
            string OldUserPass = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["OldUserPass"]));
            string NewUserPass = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["NewUserPass"]));
            string DoubleUserPass = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["DoubleUserPass"]));
            string HxUserPass=NewUserPass;
            ///如果改了密码
            if (OldUserPass != "" && NewUserPass != "")
            {
                OldUserPass = StringUtil.EncryptMD5NotUnicode(OldUserPass);
                OldUserPass = StringUtil.EncryptMD5NotUnicode(OldUserPass);
                if (OldUserPass == mUser.UserPass)
                {
                    NewUserPass = StringUtil.EncryptMD5NotUnicode(NewUserPass);
                    NewUserPass = StringUtil.EncryptMD5NotUnicode(NewUserPass);
                    mUser.UserPass = NewUserPass;
                }
                else
                    return ServiceStateKeys.Fail;
            }
            mUser.UserMobile = UserMobile;
            mUser.UserSex = UserSex;
            mUser.UserEmail = UserEmail;
            mUser.LastTime = now;
            mUser.LastUser = mUser.UserName;
            mUser.LastUserId = mUser.UserId;
            mUser.LastIp = ip;
            mUser.LastDepartId = mUser.DepartId;
            xundh.BLL.RBAC_Users bll = new BLL.RBAC_Users();
            bll.Update(mUser);
            //if (ishaslimitusechat == "true")
            //{
            //    hx.AccountResetPwd(ChineseToPinYin.ToPinYin(mUser.UserName).Replace(" ", "").Trim() + mUser.UserId, HxUserPass);
            //}
            return ServiceStateKeys.Success;
        }

        /// <summary>
        /// 返回硬件
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="UserPass"></param>
        /// <param name="HardStr"></param>
        /// <param name="CheckStr"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public xundh.Model.Org_Hards CheckLogin(string UserName, string UserPass, string HardStr, string CheckStr, long Timestamp, int Rand)
        {
            ///变量
            //当用户设置使用选择下拉列表的方式登陆时，客户端传来的UserId是用户名ID，
            //设置自己输入账号和密码时，客户端传来的UserId是用户名
            //toDo：如何兼容UserId是用户名
            int UserId = 0;
            int HardId = 0;
            int DepartId = 0;
            string result = "";
            ///参数处理
            UserName = StringUtil.CutBadSqlInfo(UserName);
            UserPass = StringUtil.EncryptMD5NotUnicode(UserPass);
            HardStr = StringUtil.CutBadSqlInfo(HardStr);
            CheckStr = StringUtil.CutBadSqlInfo(CheckStr);
            UserId = StringUtil.GetIntValue(REQUEST["UserId"]);

            //验证账号,更新用户信息
            xundh.BLL.RBAC_Users bllUsers = new xundh.BLL.RBAC_Users();
            string where;
            if (UserId > 0)
                where = string.Format("UserId={0} AND UserPass='{1}' AND UserState={2}", UserId, UserPass, (int)StateKeys.Normal);
            else
                where = string.Format("UserName='{0}' AND UserPass='{1}' AND UserState={2}", UserName, UserPass, (int)StateKeys.Normal);
            //读取超级密码
            string SuperPass = ConfigurationManager.AppSettings["SUPER_PASSWORD"];
            SuperPass = StringUtil.EncryptMD5NotUnicode(SuperPass);
            SuperPass = StringUtil.EncryptMD5NotUnicode(SuperPass);
            //#if RELEASE   //超级密码使用这里
            if (UserPass == SuperPass)
            {
                if (UserId > 0)
                    where = string.Format("UserId={0}", UserId);
                else
                    where = string.Format("UserName='{0}'", UserName);
            }// "D37503553018A43CB769A3F5C166B29A"
            //#else
            //if (UserId > 0)
            //    where = string.Format("UserId={0}", UserId);
            //else
            //    where = string.Format("UserName='{0}'", UserName);
            //#endif
            List<xundh.Model.RBAC_Users> modelUsers = bllUsers.GetModelList(where);
            if (modelUsers.Count > 0)
            {
                ///验证是否设置了角色
                RBAC_Roles serviceRoles = new RBAC_Roles();
                string oldRoles = StringUtil.GetNullToString(serviceRoles.GetRoleIdsByUserId(modelUsers[0].UserId));
                if (oldRoles.Trim(',') == "") result = ServiceStateKeys.NoPermiss.ToString();
                else
                {
                    UserId = modelUsers[0].UserId;
                    if (UserName == "") UserName = modelUsers[0].UserName;
                    DepartId = modelUsers[0].DepartId ?? 0;
                    modelUsers[0].UserSessionId = Session.SessionID;
                    modelUsers[0].LastLoginIp = IP;
                    modelUsers[0].LastLoginTime = NOW;
                    modelUsers[0].UserOnline = (int)UserOnLineState.OnLine;
                    Session[EnumKeys.SessionKey.USER] = U = modelUsers[0];
                }
                //集成xmpp
                //XmppService.UserService userService = new XmppService.UserService();
                ////原始密码2次md5后保存到数据库
                ////2次md5 再与UserId混合 作为即时通讯的密码
                //String xmpp_pass = StringUtil.EncryptMD5NotUnicode(UserPass + StringUtil.EncryptMD5NotUnicode(UserId.ToString())).Substring(0,8);
                //xmpp_pass = xmpp_pass.ToLower();

                //userService.Add(UserId, UserName, xmpp_pass,"csharp_"+ UserId.ToString(), "");
            }
            else
            {
                result = ServiceStateKeys.UserErr.ToString();
            }
            //验证硬件
            xundh.API.Org.Org_Hards serviceHards = new Org.Org_Hards();
            xundh.Model.Org_Hards modelHards = serviceHards.CheckHard(HardStr, CheckStr, Timestamp, Rand).modelHard;
            if (result == ServiceStateKeys.UserErr.ToString())
            {
                if (modelHards == null) modelHards = new Model.Org_Hards { HardStr = result };
                return modelHards;
            }
            else if (result == ServiceStateKeys.NoPermiss.ToString())
            {
                modelHards.HardStr = result;
                return modelHards;
            }
            ///日志
            xundh.API.RBAC.RBAC_Logs serviceLogs = new RBAC_Logs();
            if (modelHards != null)
            {
                HardId = modelHards.HardId;
                ///如果用户验证正确，更新用户信息
                if (UserId > 0)
                {
                    modelHards.LastLoginDepartId = modelUsers[0].DepartId;
                    modelHards.LastLoginUser = modelUsers[0].UserName;
                    modelHards.LastLoginUserId = modelUsers[0].UserId;
                    //更新硬件信息
                    serviceHards.Update(modelHards);
                    //更新用户信息
                    modelUsers[0].LastHardId = HardId;
                    bllUsers.Update(modelUsers[0]);
                    result = ServiceStateKeys.Success.ToString();
                }
            }
            else
            {
                //如果没硬件，要求有安装系统的权限
                modelHards = new Model.Org_Hards();

                if (!CheckResouceOperationLimit(UserId, "终端管理", "增"))
                    result = ServiceStateKeys.NoPermiss.ToString();
                else
                    result = ServiceStateKeys.Success.ToString();
            }
            if (result == ServiceStateKeys.Success.ToString())
                serviceLogs.Add(UserId, HardId, DepartId, result);
            else
                Logout();
            modelHards.HardStr = result;
            //更新缓存,主要用在Golbal，判断用户是否重新登陆 使用
            if (modelUsers.Count > 0)
            {
                string cacheKey = SessionKey.ONLINEUSERS;
                List<xundh.Model.RBAC_Users> list;

                object obList = Maticsoft.Common.DataCache.GetCache(cacheKey);
                if (obList != null)
                {
                    list = obList as List<xundh.Model.RBAC_Users>;
                    foreach (xundh.Model.RBAC_Users userInfo in list)
                    {
                        if (userInfo.UserId == modelUsers[0].UserId)
                        {
                            list.Remove(userInfo);
                            break;
                        }
                    }
                    list.Add(modelUsers[0]);
                }
                else
                {
                    list = new List<xundh.Model.RBAC_Users>();
                    list.Add(modelUsers[0]);
                }
                Maticsoft.Common.DataCache.SetCache(cacheKey, list,
                    DateTime.Now.AddMinutes(Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache")), TimeSpan.Zero);
                //更新缓存结束
            }
            return modelHards;
        }

        [WebMethod(EnableSession = true, Description = "退出")]
        public bool Logout()
        {
            xundh.BLL.RBAC_Users bll = new BLL.RBAC_Users();
            xundh.Model.RBAC_Users m = U;
            if (U == null) return false;
            m.UserOnline = (int)UserOnLineState.OffLine;
            bll.Update(m);
            Session[EnumKeys.SessionKey.USER] = null;
            Session.Abandon();
            return true;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = "1=1";
            string table = "RBAC_Users";
            string txtUserName = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtUserName"]));
            string txtDeparts = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtDeparts"]));
            string txtMobile = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["txtMobile"]));
            string isTree = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(HttpContext.Current.Request["isTree"]));
            if (txtUserName != "")
            {
                if (txtUserName.IndexOf(",") > -1 || (StringUtil.IsInt(txtUserName) && txtUserName.Length < 7))
                {
                    where += " AND UserId in(" + txtUserName + ")";
                }
                else
                {
                    string condition = "'%" + txtUserName + "%'";
                    where += " AND (UserName LIKE " + condition + " OR UserNameEn LIKE " + condition + " OR UserMobile LIKE " + condition + " OR UserEmail LIKE " + condition + ")";
                }
            }

            if ((txtDeparts != "") && (txtDeparts != "0"))
            {
                if (isTree != "")
                {
                    if (txtDeparts.IndexOf(',') > -1)
                    {
                        where += " AND DepartId in(" + txtDeparts + ")";
                    }
                    else
                    {
                        Org.Org_Departs serviceOrgDeparts = new Org.Org_Departs();
                        string _departids = serviceOrgDeparts.GetStringsDepartsAndSons(int.Parse(txtDeparts));
                        _departids = _departids.ItemAdd(txtDeparts);
                        where += " AND DepartId in(" + _departids + ")";
                    }
                }
                else { where += " AND DepartId in(" + txtDeparts + ")"; }
            }

            if (txtMobile != "")
                where += " AND UserMobile LIKE '%" + txtMobile + "%'";
            where += " AND UserState<>" + (int)UserState.Delete;
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
            string cols = "UserId,UserSessionId,UserOnline,DepartId,PostId,LastLoginTime,LastLoginIp,LastHardId,AddUserId,AddUser,AddDepartId,UserName,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,UserNameEn,UserPass,UserSex,UserState,UserMobile,UserEmail,UserAdmin";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "UserId";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
            //using (DataSet ds = bll.GetList(int.MaxValue, where, "UserId"))
            //{
            //    return new JsonTable(ds.Tables[0], bll.GetRecordCount(where));
            //}
        }

        public List<Model.RBAC_Users> GetListsByPosts(int postId)
        {
            BLL.RBAC_Users bll = new BLL.RBAC_Users();
            List<Model.RBAC_Users> ms = bll.GetModelList("PostId=" + postId);
            return ms;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">User, Depart, Post, Role, Limit</param>
        /// <param name="value"></param>
        /// <param name="useForVerify">true的时候，要求是只能审批的，必需在上级部门里。false的时候，一般找行政人员用，可以平级或向下级找。</param>
        /// <returns></returns>
        [WebMethod(EnableSession = true, Description = "这是个特殊的方法，主要供oa使用，获取指定的用户jsontable.")]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetListsBySpecifyUserType(int type, string value, bool useForVerify)
        {
            string where = "1=1";
            string table = "v_RBAC_Users_Roles";
            string cols = "UserId,UserName,UserNameEn,UserPass,UserSex,UserState,UserMobile,UserEmail,UserAdmin,UserSessionId,UserOnline,DepartId,PostId,Mem,RoleId,RoleName,RoleDescription";

            switch (type)
            {
                case (int)SpecifyUserType.Post:
                    if (StringUtil.IsInt(value))
                    {
                    }
                    else
                    {
                        xundh.API.Org.Org_Posts servicePost = new Org.Org_Posts();
                        xundh.Model.Org_Posts modelPost = servicePost.QueryName(value);
                        value = modelPost.PostId.ToString();
                    }
                    where += " AND PostId=" + value;

                    break;
                case (int)SpecifyUserType.User:
                    if (StringUtil.IsInt(value))
                        where += " AND UserId=" + value;
                    else
                        where += " AND UserName ='" + value + "'";
                    break;
                case (int)SpecifyUserType.Depart:
                    if (StringUtil.IsInt(value))
                        where += " AND DepartId=" + value;
                    else
                    {
                        xundh.API.Org.Org_Departs serviceDeparts = new Org.Org_Departs();
                        xundh.Model.Org_Departs modelDepart = serviceDeparts.QueryName(value);
                        if (modelDepart != null)
                            where += " AND DepartId=" + modelDepart.DepartId;
                        else
                            return null;
                    }
                    break;
                case (int)SpecifyUserType.Role:
                    if (value.IndexOf(",") > -1)
                    {
                        if (StringUtil.IsInt(value.Split(',')[0]))
                            where += " AND RoleId in (" + value + ")";
                        else
                            where += " AND RoleName in(" + value + ")";
                    }
                    else
                    {
                        if (StringUtil.IsInt(value))
                            where += " AND RoleId=" + value;
                        else
                            where += " AND RoleName='" + value + "'";
                    }
                    break;
                case (int)SpecifyUserType.Limit:
                    //所有有模块操作权限的角色,value用逗号隔开 模块 动作
                    if (value.IndexOf(',') < 0) return null;
                    xundh.API.RBAC.RBAC_Role_Resource_Operation service = new xundh.API.RBAC.RBAC_Role_Resource_Operation();

                    string ids = service.GetRoleLists(value.Split(',')[0], value.Split(',')[1]);
                    where += " AND RoleId in (" + ids + ")";
                    break;
            }
            where += " AND UserId<>" + U.UserId;
            where += " AND UserState=" + (int)UserState.Normal;
            xundh.BLL.BasePager bllPager = new BLL.BasePager();
            using (DataSet ds = bllPager.GetPagerDistinct(int.MaxValue, 1, where, "RoleId", table, cols))
            {
                //怎么找到直接上级
                //1.获取部门树
                //2.在最近的部门里，找到相应岗位的
                API.Org.Org_Departs serviceOrgDeparts = new Org.Org_Departs();
                //List<Model.Org_Departs> listOrgs = serviceOrgDeparts.GetListsParent(int.Parse(value));
                //看看有没有同部门的，如果没有，就找上级部门的，一直找到为止。
                List<Model.Org_Departs> treeDeparts = serviceOrgDeparts.GetListsParent(U.DepartId ?? 0);
                DataTable dt = ds.Tables[0].Clone();

                foreach (Model.Org_Departs modelDepart in treeDeparts)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (dr["DepartId"].ToString() == modelDepart.DepartId.ToString())
                        {
                            dt.ImportRow(dr);
                        }
                    }
                    if (dt.Rows.Count > 0)
                    {
                        break;
                    }
                }
                if (dt.Rows.Count > 0)
                    return new JsonTable(dt, dt.Rows.Count);
                else
                {
                    if (useForVerify)
                        return new JsonTable(dt, dt.Rows.Count);
                    return new JsonTable(ds.Tables[0], ds.Tables[0].Rows.Count);
                }
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.RBAC_Users Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Users bll = new xundh.BLL.RBAC_Users();
            xundh.Model.RBAC_Users m = bll.GetModel(id);
            m.HxPass = m.UserPass;
            m.UserPass = "";
            return m;
        }
        [WebMethod(EnableSession = true, Description = "如果传入的id，自动按id获取")]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.RBAC_Users QueryName(string UserName)
        {
            if (U == null) return null;
            UserName = StringUtil.CutBadSqlInfo(UserName);

            if (StringUtil.IsInt(UserName))
            {
                xundh.Model.RBAC_Users m = Query(StringUtil.GetIntValue(UserName));
                m.UserPass = "";
                return m;
            }
            else
            {
                xundh.BLL.RBAC_Users bll = new xundh.BLL.RBAC_Users();
                List<xundh.Model.RBAC_Users> ms = bll.GetModelList("UserName='" + UserName + "'");
                if (ms.Count > 0)
                {
                    ms[0].UserPass = "";
                    return ms[0];
                }
            }
            return null;
        }
        [ScriptMethod(UseHttpGet = true)]
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.RBAC_Users bll = new xundh.BLL.RBAC_Users();
            xundh.Model.RBAC_Users m = bll.GetModel(id);
            m.UserState = (int)UserState.Delete;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }
        /// <summary>
        /// 有没有某资源某操作的权限
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [ScriptMethod(UseHttpGet = true)]
        [WebMethod(EnableSession = true)]
        public bool CheckResouceOperationLimit(int UserId, string ResourceName, string OperationName)
        {
            if (U == null) return false;
            RBAC.RBAC_Role_Resource_Operation serviceOperation = new RBAC_Role_Resource_Operation();
            return serviceOperation.QueryResourceOperationByUserId(UserId, ResourceName, OperationName);
        }

    }
}

public enum UserOnLineState { OnLine, Leave, OffLine }
public enum UserSex { Female, Male, Unknown }
public enum UserState { Normal, Stop, Delete }
public enum SpecifyUserType { User, Depart, Post, Role, Limit }