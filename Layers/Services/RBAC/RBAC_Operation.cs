﻿using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
namespace xundh.API.RBAC
{
    /// <summary>
    ///RBAC_Operation 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class RBAC_Operation : BaseService
    {
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Operation bll = new xundh.BLL.RBAC_Operation();
            using (DataSet ds = bll.GetList(int.MaxValue, "", "OperationId"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.RBAC_Operation Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Operation bll = new xundh.BLL.RBAC_Operation();
            xundh.Model.RBAC_Operation m = bll.GetModel(id);
            return m;
        }
    }
}