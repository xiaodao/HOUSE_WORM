﻿using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
using System;
namespace xundh.API.RBAC
{
    /// <summary>
    ///RBAC_Logs 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class RBAC_Logs : BaseService
    {
        public ServiceStateKeys Add(int UserId, int HardId, int DepartId,string Events)
        {
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理
            Events = StringUtil.CutBadSqlInfo(Events);

            xundh.BLL.RBAC_Logs bll = new xundh.BLL.RBAC_Logs();
            xundh.Model.RBAC_Logs m;
            m = new xundh.Model.RBAC_Logs();
            m.UserId = UserId;
            m.HardId = HardId;
            m.AddTime = now;
            m.AddIp = ip;
            m.Events = Events;
            m.DepartId = DepartId;
            bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            string where = " 1=1 ";
            string txtUserName = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(REQUEST["txtUserName"]));
            int txtDeparts = StringUtil.GetIntValue(REQUEST["txtDeparts"]);

            if (txtUserName != "")
                where += " AND UserName LIKE '%" + txtUserName + "%'";
            else if(txtDeparts>0) {
                Org.Org_Departs serviceOrgDeparts = new Org.Org_Departs();
                string departids = serviceOrgDeparts.GetStringsDepartsAndSons(txtDeparts);
                departids = departids.ItemAdd(txtDeparts);
                if (departids != "")
                    where += " AND DepartId in (" + departids + ")";
            }
            JqGridHandler datagrid = new JqGridHandler(where);
            xundh.BLL.BasePager bll = new xundh.BLL.BasePager();
            int recordcount = bll.GetAllCount(datagrid.StrWhere, "v_RBAC_Logs_Users_Hards");
            datagrid.init(where, page, recordcount, rows, "", "");

            if (datagrid.Order.Trim() == "") datagrid.Order = "LogId DESC";

            using (DataSet ds = bll.GetPager(datagrid.Pagesize, page, datagrid.StrWhere, datagrid.Order, "v_RBAC_Logs_Users_Hards", "LogId,DepartId,UserId,UserName,HardId,HardStr,Events,AddTime,AddIp"))
            {
                JsonTable jt = new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
                return jt;
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.RBAC_Logs Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Logs bll = new xundh.BLL.RBAC_Logs();
            xundh.Model.RBAC_Logs m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet=true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.RBAC_Logs bll = new xundh.BLL.RBAC_Logs();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}