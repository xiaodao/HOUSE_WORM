﻿using System;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.RBAC
{
    /// <summary>
    ///RBAC_User_Role 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class RBAC_User_Role : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int UserRoleID, int UserID, int RoleID
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理

            xundh.BLL.RBAC_User_Role bll = new xundh.BLL.RBAC_User_Role();
            xundh.Model.RBAC_User_Role m;
            if (UserRoleID > 0)
                m = bll.GetModel(UserRoleID);
            else
                m = new xundh.Model.RBAC_User_Role();
            m.UserID = UserID;
            m.RoleID = RoleID;
            if (UserRoleID > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.RBAC_User_Role bll = new xundh.BLL.RBAC_User_Role();
            using (DataSet ds = bll.GetList(int.MaxValue, "", "UserRoleID"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        [WebMethod(EnableSession = true,Description="Role可以是int或RoleName，返回某角色所有用户")]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetUserLists(string Role)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bllP = new BLL.BasePager();
            string table = "v_RBAC_Users_Roles"; 
            string cols = "UserId,UserName,UserNameEn,UserPass,UserSex,UserState,UserMobile,UserEmail,UserAdmin,UserSessionId,UserOnline,DepartId,PostId,Mem,RoleId,RoleName,RoleDescription";
            string where = "1=1";
            if (StringUtil.IsInt(Role)) {
                where += " AND RoleId=" + Role;
            }
            else {
                where += " AND RoleName='" + Role + "'";
            }
            xundh.BLL.BasePager bllPager = new BLL.BasePager();
            using (DataSet ds = bllPager.GetPagerDistinct(int.MaxValue, 1, where, "RoleId", table, cols))
            {
                return new JsonTable(ds.Tables[0], bllPager.GetAllCount(where, table));
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.RBAC_User_Role Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.RBAC_User_Role bll = new xundh.BLL.RBAC_User_Role();
            xundh.Model.RBAC_User_Role m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int UserId, int RoleId)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.RBAC_User_Role bll = new xundh.BLL.RBAC_User_Role();
            string where = "UserID=" + UserId;
            where += " AND RoleID=" + RoleId;
            using (DataSet ds = bll.GetList(where))
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (bll.Delete(int.Parse(ds.Tables[0].Rows[0]["UserRoleId"].ToString())))
                        return ServiceStateKeys.Success;
                    else
                        return ServiceStateKeys.Fail;
                }
            }
            return ServiceStateKeys.Success;
        }

    }
}