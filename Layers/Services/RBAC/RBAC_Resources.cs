﻿using System.Data;
using System.Web.Services;
using Webapp;
using System.Web.Script.Services;
using System.Collections.Generic;

namespace xundh.API.RBAC
{
    /// <summary>
    ///I_RBAC_Resources 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class RBAC_Resources : BaseService
    {
        /// <summary>
        /// 获取个人权限的菜单
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet=true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Resources bll = new xundh.BLL.RBAC_Resources();
            ///获取自己角色
            RBAC_Roles serviceRoles = new RBAC_Roles();
            JsonTable jt = serviceRoles.GetListsByUserId(U.UserId);
            string s = "";
            foreach (DataRow dr in jt.Table.Rows) {
                if (s != "")
                    s += ",";
                s += dr["RoleId"].ToString();
            }
            //如果没有角色
            if (s == "") {
                return null;
            }
            ///获取这些角色的所有权限
            xundh.BLL.BasePager bllPager = new BLL.BasePager();
            string where;
            string table = "v_RBAC_Roles_Resouce_Operations";
            string cols = "ResourceId,ResourceName,ParentResourceId,Js,ResourcePath,Sort,DefaultState,Icon";
            where = "RoleId in (" + s + ")";
            where += " AND Value=1";
            //where = ""; 这句调试用的，部署时勿必删除
            using (DataSet ds = bllPager.GetPagerDistinct(int.MaxValue, 1, where, "ResourceId", table, cols))
            {
                return new JsonTable(ds.Tables[0], bllPager.GetAllCount(where,table));
            }
        }
        /// <summary>
        /// 加载所有资源
        /// </summary>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetAllLists()
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Resources bll = new xundh.BLL.RBAC_Resources();
            ///获取自己所在所有组
            ///获取自己自己所在组的所有允许的权限
            using (DataSet ds = bll.GetList(int.MaxValue, "", "Sort"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }

        [WebMethod(EnableSession = true)]
        public xundh.Model.RBAC_Resources Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.RBAC_Resources bll = new xundh.BLL.RBAC_Resources();
            xundh.Model.RBAC_Resources m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public string Delete(int id)
        {
            if (U == null) return  EnumKeys.ServiceStateKeys.TimeOut.ToString();
            if (U.UserName != "管理员") return EnumKeys.ServiceStateKeys.NoPermiss.ToString();

            //删除一个资源
            //去掉角色与资源动作的关联
            //删除动作
            xundh.BLL.RBAC_Resources bll = new xundh.BLL.RBAC_Resources();
            xundh.BLL.RBAC_Role_Resource_Operation bllOperations = new BLL.RBAC_Role_Resource_Operation();
            List<xundh.Model.RBAC_Role_Resource_Operation> msOperations = bllOperations.GetModelList("ResourceId=" + id);
            string str = "";
            foreach (xundh.Model.RBAC_Role_Resource_Operation m in msOperations) {
                xundh.Model.RBAC_Resources mResouce = bll.GetModel(m.ResourceId ?? 0);
                str = str.ItemAdd(mResouce.ResourceName);
            }
            string sql = "DELETE FROM RBAC_Role_Resource_Operation WHERE ResourceId=" + id;
            xundh.BLL.BasePager bllP = new BLL.BasePager();
            bllP.GetGPager(sql);
            
            bll.Delete(id);
            return str;
        }
    }
}