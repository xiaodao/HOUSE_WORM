﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.Tools
{
    /// <summary>
    ///Tools_AgentMobiles 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Tools_AgentMobiles : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int AgentId, string AgentTel, string AgentName, string AgentUserName, int State
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理
            AgentTel = StringUtil.CutBadSqlInfo(AgentTel).Trim();
            AgentName = StringUtil.CutBadSqlInfo(AgentName);
            AgentUserName = StringUtil.CutBadSqlInfo(AgentUserName);

            xundh.BLL.Tools_AgentMobiles bll = new xundh.BLL.Tools_AgentMobiles();
            xundh.Model.Tools_AgentMobiles m;

            if (AgentId > 0)
            {
                m = bll.GetModel(AgentId);
                ///是否已经存在 了
                using (DataSet ds = bll.GetList("AgentTel='" + AgentTel + "'"))
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //已经存在了
                        if (ds.Tables[0].Rows[0]["AgentId"].ToString() != AgentId.ToString())
                        {
                            return ServiceStateKeys.Exists;
                        }
                    }
                }
            }
            else
            {
                m = new xundh.Model.Tools_AgentMobiles();
                ///是否已经存在 了
                using (DataSet ds = bll.GetList("AgentTel='" + AgentTel + "'"))
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        //已经存在了
                        return ServiceStateKeys.Exists;
                    }
                }
            }
            m.AddDepartId = mLoginUser.DepartId;
            m.AgentTel = AgentTel;
            m.AgentName = AgentName;
            m.AgentUserName = AgentUserName;
            m.State = State;
            m.AddUserId = mLoginUser.UserId;
            m.AddUser = mLoginUser.UserName;
            m.AddTime = now;
            m.AddIp = ip;
            if (AgentId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            string txtUserName = StringUtil.GetNullToString(HttpContext.Current.Request["txtUserName"]);
            string where = "1=1";
            if (txtUserName != "")
                where += " AND (AgentName LIKE '%" + txtUserName + "%' OR AgentTel LIKE '%" + txtUserName + "%' OR AgentUserName LIKE '%" + txtUserName + "%')";
            xundh.BLL.Tools_AgentMobiles bll = new xundh.BLL.Tools_AgentMobiles();
            using (DataSet ds = bll.GetList(int.MaxValue, where, "AgentId"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(where));
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Tools_AgentMobiles Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Tools_AgentMobiles bll = new xundh.BLL.Tools_AgentMobiles();
            xundh.Model.Tools_AgentMobiles m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Tools_AgentMobiles QueryTel(string tel)
        {
            if (U == null) return null;
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            xundh.BLL.Tools_AgentMobiles bll = new xundh.BLL.Tools_AgentMobiles();
            List<xundh.Model.Tools_AgentMobiles> ms = bll.GetModelList("AgentTel = '" + tel + "'");
            if (ms.Count > 0)
                return ms[0];
            else return null;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Tools_AgentMobiles bll = new xundh.BLL.Tools_AgentMobiles();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}