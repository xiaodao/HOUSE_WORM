﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.Tools
{
    /// <summary>
    ///Tools_DayClear 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.sybbs.com/", Description = "")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class Tools_DayClear : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int id, string day_close, int postId)
        {
            if (U == null) return ServiceStateKeys.TimeOut;

            ///参数处理

            xundh.BLL.Tools_DayClear bll = new xundh.BLL.Tools_DayClear();
            xundh.Model.Tools_DayClear m;
            if (id > 0)
            {
                m = bll.GetModel(id);
                ///是否已经存在
                using (DataSet ds = bll.GetList("postId=" + postId))
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (ds.Tables[0].Rows[0]["postId"].ToString() != postId.ToString())
                        {
                            return ServiceStateKeys.Exists;
                        }
                    }
                }
            }
            else
            {
                m = new xundh.Model.Tools_DayClear();
                using (DataSet ds = bll.GetList("postId=" + postId))
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ServiceStateKeys.Exists;
                    }
                    m.AddUserId = U.UserId;
                    m.AddUser = U.UserName;
                    m.AddDepartId = U.DepartId;
                    m.AddIp = IP;
                }
            }
            m.day_close = day_close;
            m.postId = postId;
            m.AddTime = NOW;
            if (id > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bllP = new xundh.BLL.BasePager();
            string table = "Tools_DayClear";
            string where = "1=1";

            JqGridHandler datagrid = new JqGridHandler(where);
            int totalRows = bllP.GetAllCount(where, table);
            string cols = "id,day_close,postId,AddUser,AddDepartId,AddTime";
            datagrid.init(datagrid.StrWhere, page, totalRows, rows, "", "");
            using (DataSet ds = bllP.GetPager(datagrid.Pagesize, datagrid.P, where, datagrid.Order, table, cols))
            {

                return new JsonTable(ds.Tables[0], totalRows, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Tools_DayClear Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Tools_DayClear bll = new xundh.BLL.Tools_DayClear();
            xundh.Model.Tools_DayClear m = bll.GetModel(id);
            return m;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Tools_DayClear QueryByPostId(int postId)
        {
            if (U == null) return null;
            postId = StringUtil.GetIntValue(postId);
            xundh.BLL.Tools_DayClear bll = new BLL.Tools_DayClear();
            if (postId!=0)
            {
                List<xundh.Model.Tools_DayClear> ms = bll.GetModelList(" postId=" + postId);
                if (ms.Count>0)
                {
                    return ms[0];
                }
            }
            return null;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete(string id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Tools_DayClear bll = new xundh.BLL.Tools_DayClear();
            bll.DeleteList(id);
            return ServiceStateKeys.Success;
        }

    }
}