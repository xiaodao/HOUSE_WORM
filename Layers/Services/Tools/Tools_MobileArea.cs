﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.Tools
{
    /// <summary>
    ///Tools_MobileArea 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class Tools_MobileArea : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int id, int num, string code, string city, string cardtype
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mLoginUser = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
            ///变量
            DateTime now = DateTime.Now;
            string ip = HttpContext.Current.Request.UserHostAddress;
            ///参数处理
            code = StringUtil.CutBadSqlInfo(code);
            city = StringUtil.CutBadSqlInfo(city);
            cardtype = StringUtil.CutBadSqlInfo(cardtype);

            xundh.BLL.Tools_MobileArea bll = new xundh.BLL.Tools_MobileArea();
            xundh.Model.Tools_MobileArea m;
            if (id > 0)
                m = bll.GetModel(id);
            else
                m = new xundh.Model.Tools_MobileArea();
            m.num = num;
            m.code = code;
            m.city = city;
            m.cardtype = cardtype;
            if (id > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        public xundh.Model.Tools_MobileArea Query(string number)
        {
            if (U == null) return null;
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (number.Length > 7)
            {
                number = number.Substring(0, 7);
            }
            else if (number.Length < 7)
                return null;
            string where;
            where = " num=" + number ;

            xundh.BLL.Tools_MobileArea bll = new xundh.BLL.Tools_MobileArea();
            List<xundh.Model.Tools_MobileArea> ms = bll.GetModelList(where);

            if (ms.Count > 0)
                return ms[0];
            else
            {
                //尝试从远程获取
                //PhoneLocation servicePhoneLocation = new PhoneLocation();
                //string result=servicePhoneLocation.Query(number);
                //if (result != "") {
                //    xundh.Model.Tools_MobileArea modelArea = new Model.Tools_MobileArea();
                //    modelArea.city = result;
                //    return modelArea;
                //}
                return null;
            }
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Tools_MobileArea bll = new xundh.BLL.Tools_MobileArea();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }
    }
}