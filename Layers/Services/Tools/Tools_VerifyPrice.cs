﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.Tools
{
    /// <summary>
    ///Tools_VerifyPrice 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Tools_VerifyPrice : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int VPId, int RegionArea, string BuildingName, string Addr, decimal Price, string SellFace
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            BuildingName = StringUtil.CutBadSqlInfo(BuildingName);
            Addr = StringUtil.CutBadSqlInfo(Addr);
            SellFace = StringUtil.CutBadSqlInfo(SellFace);

            xundh.BLL.Tools_VerifyPrice bll = new xundh.BLL.Tools_VerifyPrice();
            xundh.Model.Tools_VerifyPrice m;
            if (VPId > 0)
                m = bll.GetModel(VPId);
            else
                m = new xundh.Model.Tools_VerifyPrice();
            m.RegionArea = RegionArea;
            m.BuildingName = BuildingName;
            m.Addr = Addr;
            m.Price = Price;
            m.SellFace = SellFace;
            if (VPId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "Tools_VerifyPrice";
            string RegionArea = StringUtil.TBCode(StringUtil.GetNullToString(REQUEST["RegionArea"]));
            string Building = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(REQUEST["BuildingName"]));
            if (RegionArea != "")
                where += " AND RegionArea in(" + RegionArea + ")";
            if (Building != "")
                where += " AND BuildingName LIKE '%" + Building + "%'";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);
        
            string cols = "VPId,RegionArea,BuildingName,Addr,Price,SellFace";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "VPId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Tools_VerifyPrice Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Tools_VerifyPrice bll = new xundh.BLL.Tools_VerifyPrice();
            xundh.Model.Tools_VerifyPrice m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Tools_VerifyPrice bll = new xundh.BLL.Tools_VerifyPrice();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}