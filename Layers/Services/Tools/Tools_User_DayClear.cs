﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.Tools
{
    /// <summary>
    ///Tools_User_DayClear 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Tools_User_DayClear : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int DayId, string ApprovalContent, string DayContent
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            ApprovalContent = StringUtil.CutBadSqlInfo(ApprovalContent);

            xundh.BLL.Tools_User_DayClear bll = new xundh.BLL.Tools_User_DayClear();
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            xundh.Model.Tools_User_DayClear m;
            if (DayId > 0)
            {

                m = bll.GetModel(DayId);
            }
            else
            {
                m = new xundh.Model.Tools_User_DayClear();
                m.AddTime = NOW;
                m.AddDepartId = U.AddDepartId;
            }
            m.DayContent = DayContent;
            m.AddUserId = U.UserId;
            //
            
            m.ApprovalContent = ApprovalContent;
            //判断有无审批的权限，如果有，改变state，如果没有state还是默认值0
            if (serviceOperation.CheckResouceOperationLimit(U.UserId, "日清", "改"))
            {
                m.ApprovalUserId = U.UserId;
                m.State = (int)DCState.Approved;
                m.ApprovalTime = NOW;
                m.ApprovalDepartId = U.DepartId;
            }
            else
            {
                m.ApprovalUserId = 0;
                m.State = (int)DCState.Unapproved;
            }
            if (DayId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "Tools_User_DayClear";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "DayId,State,ApprovalContent,ApprovalDepartId,ApprovalTime,DayContent,AddUserId,AddTime,AddDepartId,ApprovalUserId";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "DayId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Tools_User_DayClear Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Tools_User_DayClear bll = new xundh.BLL.Tools_User_DayClear();
            xundh.Model.Tools_User_DayClear m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Tools_User_DayClear bll = new xundh.BLL.Tools_User_DayClear();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}

public enum DCState { Unapproved, Approved }