﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.FI
{
    /// <summary>
    ///FI_Vouchers 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class FI_Vouchers : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int VoucherId, string Cashier, string VoucherCode, decimal Amount,
            int SubjectId, int Direction, string Description, string AccountOfficer,int RelatedIndex
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            VoucherCode = StringUtil.CutBadSqlInfo(VoucherCode);
            Description = StringUtil.CutBadSqlInfo(Description);
            AccountOfficer = StringUtil.CutBadSqlInfo(AccountOfficer);

            xundh.BLL.FI_Vouchers bll = new xundh.BLL.FI_Vouchers();
            API.RBAC.RBAC_Users serviceUsers = new RBAC.RBAC_Users();
            API.FI.FI_Subjects serviceSubject = new FI_Subjects();

            xundh.Model.RBAC_Users modelCashier;
            xundh.Model.RBAC_Users modelOfficer;

            modelCashier = serviceUsers.QueryName(Cashier);
            modelOfficer = serviceUsers.QueryName(AccountOfficer);
            if ((modelCashier == null) ||
                (modelOfficer == null)) {
                    return ServiceStateKeys.NotExists;
            }
            xundh.Model.FI_Vouchers m;
            if (VoucherId > 0)
                m = bll.GetModel(VoucherId);
            else
            {
                m = new xundh.Model.FI_Vouchers();

                m.AddUserId = U.UserId;
                m.AddUser = U.UserName;
                m.AddTime = NOW;
                m.AddIp = IP;
            }
            m.CashierId = modelCashier.UserId;
            m.CashierUser = modelCashier.UserName;
            m.VoucherCode = VoucherCode;
            m.Amount = Amount;
            m.RootSubjectId = serviceSubject.GetRoot(SubjectId).SubjectId;
            m.SubjectId = SubjectId;
            m.Direction = Direction;
            m.Description = Description;
            m.AccountOfficer = modelOfficer.UserName;
            m.AccountOfficerId = modelOfficer.UserId;
            m.RelatedIndex = RelatedIndex;
            if (VoucherId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows, string SubjectIds, int RelatedIndex)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "FI_Vouchers";

            if (SubjectIds != "") where += " AND SubjectId in(" + SubjectIds + ")";
            if (RelatedIndex > 0) where += " AND RelatedIndex=" + RelatedIndex;
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "VoucherId,AddUserId,AddUser,AddTime,AddIp,CashierId,CashierUser,VoucherCode,Amount,RootSubjectId,SubjectId,Direction,Description,AccountOfficer,AccountOfficerId,RelatedIndex";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "VoucherId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }

        [WebMethod(EnableSession = true,Description="总金额，汇总")]
        public decimal QueryTotalAmount(string SubjectIds, int RelatedIndex)
        {
            if (U == null) return 0;
            xundh.BLL.FI_Vouchers bll = new xundh.BLL.FI_Vouchers();
            string where = " 1=1 ";
            if (SubjectIds != "") where += " AND SubjectId in(" + SubjectIds + ")";
            if (RelatedIndex > 0) where += " AND RelatedIndex=" + RelatedIndex;
            decimal result = 0;
            using (DataSet ds = bll.GetList(where))
            {
                foreach (DataRow dr in ds.Tables[0].Rows) {
                    result +=decimal.Parse( dr["Amount"].ToString());
                }
            }
            return result;
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.FI_Vouchers Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.FI_Vouchers bll = new xundh.BLL.FI_Vouchers();
            xundh.Model.FI_Vouchers m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.FI_Vouchers bll = new xundh.BLL.FI_Vouchers();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}