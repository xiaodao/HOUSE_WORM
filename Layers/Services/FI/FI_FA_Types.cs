﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.FI
{
    /// <summary>
    ///FI_FA_Types 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class FI_FA_Types : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int FATypeId, string FATypeTitle
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            FATypeTitle = StringUtil.CutBadSqlInfo(FATypeTitle);

            xundh.BLL.FI_FA_Types bll = new xundh.BLL.FI_FA_Types();
            xundh.Model.FI_FA_Types m;
            if (FATypeId > 0)
                m = bll.GetModel(FATypeId);
            else
                m = new xundh.Model.FI_FA_Types();
            m.FATypeTitle = FATypeTitle;
            if (FATypeId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "FI_FA_Types";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "FATypeId,FATypeTitle";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "FATypeId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.FI_FA_Types Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.FI_FA_Types bll = new xundh.BLL.FI_FA_Types();
            xundh.Model.FI_FA_Types m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.FI_FA_Types bll = new xundh.BLL.FI_FA_Types();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}