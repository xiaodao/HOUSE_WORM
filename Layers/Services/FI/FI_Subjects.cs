﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.FI
{
    /// <summary>
    ///FI_Subjects 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class FI_Subjects : BaseService
    {
        xundh.BLL.FI_Subjects bll = new xundh.BLL.FI_Subjects();
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int SubjectId, int ParentSubjectId, string SubjectTitle, string SubjectCode, int Types, int Direction, bool IsEnd
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            SubjectTitle = StringUtil.CutBadSqlInfo(SubjectTitle);
            SubjectCode = StringUtil.CutBadSqlInfo(SubjectCode);

            xundh.Model.FI_Subjects m;
            if (SubjectId > 0)
                m = bll.GetModel(SubjectId);
            else
                m = new xundh.Model.FI_Subjects();
            m.ParentSubjectId = ParentSubjectId;
            m.SubjectTitle = SubjectTitle;
            m.SubjectCode = SubjectCode;
            m.Types = Types;
            m.Direction = Direction;
            m.IsEnd = IsEnd;
            if (SubjectId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "FI_Subjects";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "SubjectId,ParentSubjectId,SubjectTitle,SubjectCode,Types,Direction,IsEnd";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "SubjectId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.FI_Subjects Query(int id)
        {
            if (U == null) return null;
            xundh.Model.FI_Subjects m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true,Description="获取一个会计科目的根节点")]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.FI_Subjects GetRoot(int id)
        {
            if (U == null) return null;
            return GetParent(id);
        }
        private xundh.Model.FI_Subjects GetParent(int id)
        {
            xundh.Model.FI_Subjects m = bll.GetModel(id);
            if (m.ParentSubjectId != 0)
                return GetParent(m.ParentSubjectId ?? 0);
            else
                return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.FI_Subjects bll = new xundh.BLL.FI_Subjects();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}