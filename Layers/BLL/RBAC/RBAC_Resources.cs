﻿using System;
using System.Data;
using System.Collections.Generic;
using xundh.Common;
using xundh.Model;
namespace xundh.BLL
{
	/// <summary>
	/// RBAC_Resources
	/// </summary>
	public partial class RBAC_Resources
	{
		private readonly xundh.DAL.RBAC_Resources dal=new xundh.DAL.RBAC_Resources();
		public RBAC_Resources()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int ResourceId)
		{
			return dal.Exists(ResourceId);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(xundh.Model.RBAC_Resources model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(xundh.Model.RBAC_Resources model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int ResourceId)
		{
			
			return dal.Delete(ResourceId);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string ResourceIdlist )
		{
			return dal.DeleteList(ResourceIdlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.RBAC_Resources GetModel(int ResourceId)
		{
			
			return dal.GetModel(ResourceId);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public xundh.Model.RBAC_Resources GetModelByCache(int ResourceId)
		{
			
			string CacheKey = "RBAC_ResourcesModel-" + ResourceId;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(ResourceId);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (xundh.Model.RBAC_Resources)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<xundh.Model.RBAC_Resources> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<xundh.Model.RBAC_Resources> DataTableToList(DataTable dt)
		{
			List<xundh.Model.RBAC_Resources> modelList = new List<xundh.Model.RBAC_Resources>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				xundh.Model.RBAC_Resources model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

