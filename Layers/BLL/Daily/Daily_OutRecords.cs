﻿/**  版本信息模板在安装目录下，可自行修改。
* Daily_OutRecords.cs
*
* 功 能： N/A
* 类 名： Daily_OutRecords
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  14/2/24 22:12:46   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using xundh.Model;
namespace xundh.BLL
{
	/// <summary>
	/// Daily_OutRecords
	/// </summary>
	public partial class Daily_OutRecords
	{
		private readonly xundh.DAL.Daily_OutRecords dal=new xundh.DAL.Daily_OutRecords();
		public Daily_OutRecords()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OutRecordId)
		{
			return dal.Exists(OutRecordId);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(xundh.Model.Daily_OutRecords model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(xundh.Model.Daily_OutRecords model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OutRecordId)
		{
			
			return dal.Delete(OutRecordId);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string OutRecordIdlist )
		{
			return dal.DeleteList(OutRecordIdlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.Daily_OutRecords GetModel(int OutRecordId)
		{
			
			return dal.GetModel(OutRecordId);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public xundh.Model.Daily_OutRecords GetModelByCache(int OutRecordId)
		{
			
			string CacheKey = "Daily_OutRecordsModel-" + OutRecordId;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(OutRecordId);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (xundh.Model.Daily_OutRecords)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<xundh.Model.Daily_OutRecords> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<xundh.Model.Daily_OutRecords> DataTableToList(DataTable dt)
		{
			List<xundh.Model.Daily_OutRecords> modelList = new List<xundh.Model.Daily_OutRecords>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				xundh.Model.Daily_OutRecords model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

