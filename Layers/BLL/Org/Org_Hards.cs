﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using xundh.Model;
namespace xundh.BLL
{
    /// <summary>
    /// Org_Hards
    /// </summary>
    public partial class Org_Hards
    {
        private readonly xundh.DAL.Org_Hards dal = new xundh.DAL.Org_Hards();
        public Org_Hards()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int HardId)
        {
            return dal.Exists(HardId);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(xundh.Model.Org_Hards model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Org_Hards model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int HardId)
        {

            return dal.Delete(HardId);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string HardIdlist)
        {
            return dal.DeleteList(HardIdlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Org_Hards GetModel(int HardId)
        {

            return dal.GetModel(HardId);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public xundh.Model.Org_Hards GetModelByCache(int HardId)
        {

            string CacheKey = "Org_HardsModel-" + HardId;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(HardId);
                    if (objModel != null)
                    {
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (xundh.Model.Org_Hards)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<xundh.Model.Org_Hards> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<xundh.Model.Org_Hards> DataTableToList(DataTable dt)
        {
            List<xundh.Model.Org_Hards> modelList = new List<xundh.Model.Org_Hards>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                xundh.Model.Org_Hards model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

