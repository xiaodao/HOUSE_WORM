﻿/**  版本信息模板在安装目录下，可自行修改。
* CM_Buy_Items.cs
*
* 功 能： N/A
* 类 名： CM_Buy_Items
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2014/3/8 16:09:46   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using xundh.Model;
namespace xundh.BLL
{
	/// <summary>
	/// CM_Buy_Items
	/// </summary>
	public partial class CM_Buy_Items
	{
		private readonly xundh.DAL.CM_Buy_Items dal=new xundh.DAL.CM_Buy_Items();
		public CM_Buy_Items()
		{}
		#region  BasicMethod

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int CMId)
		{
			return dal.Exists(CMId);
		}

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public int  Add(xundh.Model.CM_Buy_Items model)
		{
			return dal.Add(model);
		}

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(xundh.Model.CM_Buy_Items model)
		{
			return dal.Update(model);
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int CMId)
		{
			
			return dal.Delete(CMId);
		}
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string CMIdlist )
		{
			return dal.DeleteList(CMIdlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.CM_Buy_Items GetModel(int CMId)
		{
			
			return dal.GetModel(CMId);
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public xundh.Model.CM_Buy_Items GetModelByCache(int CMId)
		{
			
			string CacheKey = "CM_Buy_ItemsModel-" + CMId;
			object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(CMId);
					if (objModel != null)
					{
						int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
						Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
					}
				}
				catch{}
			}
			return (xundh.Model.CM_Buy_Items)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<xundh.Model.CM_Buy_Items> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}
		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<xundh.Model.CM_Buy_Items> DataTableToList(DataTable dt)
		{
			List<xundh.Model.CM_Buy_Items> modelList = new List<xundh.Model.CM_Buy_Items>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				xundh.Model.CM_Buy_Items model;
				for (int n = 0; n < rowsCount; n++)
				{
					model = dal.DataRowToModel(dt.Rows[n]);
					if (model != null)
					{
						modelList.Add(model);
					}
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		//public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		//{
			//return dal.GetList(PageSize,PageIndex,strWhere);
		//}

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

