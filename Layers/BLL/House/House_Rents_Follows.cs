﻿using System;
using System.Data;
using System.Collections.Generic;
using Maticsoft.Common;
using xundh.Model;
namespace xundh.BLL
{
    /// <summary>
    /// House_Rents_Follows
    /// </summary>
    public partial class House_Rents_Follows
    {
        private readonly xundh.DAL.House_Rents_Follows dal = new xundh.DAL.House_Rents_Follows();
        public House_Rents_Follows()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int FollowId)
        {
            return dal.Exists(FollowId);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(xundh.Model.House_Rents_Follows model)
        {
            return dal.Add(model);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(xundh.Model.House_Rents_Follows model)
        {
            return dal.Update(model);
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int FollowId)
        {

            return dal.Delete(FollowId);
        }
        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool DeleteList(string FollowIdlist)
        {
            return dal.DeleteList(FollowIdlist);
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.House_Rents_Follows GetModel(int FollowId)
        {

            return dal.GetModel(FollowId);
        }

        /// <summary>
        /// 得到一个对象实体，从缓存中
        /// </summary>
        public xundh.Model.House_Rents_Follows GetModelByCache(int FollowId)
        {

            string CacheKey = "House_Rents_FollowsModel-" + FollowId;
            object objModel = Maticsoft.Common.DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(FollowId);
                    if (objModel != null)
                    {
                        int ModelCache = Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache");
                        Maticsoft.Common.DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch { }
            }
            return (xundh.Model.House_Rents_Follows)objModel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }
        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<xundh.Model.House_Rents_Follows> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public List<xundh.Model.House_Rents_Follows> DataTableToList(DataTable dt)
        {
            List<xundh.Model.House_Rents_Follows> modelList = new List<xundh.Model.House_Rents_Follows>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                xundh.Model.House_Rents_Follows model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = dal.DataRowToModel(dt.Rows[n]);
                    if (model != null)
                    {
                        modelList.Add(model);
                    }
                }
            }
            return modelList;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            return dal.GetRecordCount(strWhere);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            return dal.GetListByPage(strWhere, orderby, startIndex, endIndex);
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        //public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        //{
        //return dal.GetList(PageSize,PageIndex,strWhere);
        //}

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

