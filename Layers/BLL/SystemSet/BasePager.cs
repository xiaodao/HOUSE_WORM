﻿
using System.Data;
namespace xundh.BLL
{
    public class BasePager
    {
        private readonly xundh.DAL.BasePager dal = new xundh.DAL.BasePager();
        public DataSet GetPager(int PageSize, int PageIndex, string strWhere, string orderby, string tablename, string cols)
        {
            return dal.GetPager(PageSize, PageIndex, strWhere, orderby, tablename, cols);
        }
        public DataSet GetPager(int PageSize, int PageIndex, string strWhere, string orderby, string tablename, string cols, string groupby)
        {
            return dal.GetPager(PageSize, PageIndex, strWhere, orderby, tablename, cols, groupby);
        }
        public DataSet GetPagerDistinct(int PageSize, int PageIndex, string strWhere, string orderby, string tablename, string cols)
        {
            return dal.GetPagerDistinct(PageSize, PageIndex, strWhere, orderby, tablename, cols);
        }
        public DataSet GetGPager(string sql)
        {
            return dal.GetGPager(sql);
        }
        public int GetAllCount(string strWhere, string tablename)
        {
            return dal.GetAllCount(strWhere, tablename);
        }
        public void Delete(string strWhere, string tablename)
        {
            dal.Delete(strWhere, tablename);
        }
    }
}
