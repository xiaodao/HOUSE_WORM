﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.Doc
{
    /// <summary>
    ///Doc_Types 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Doc_Types : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int TypeId, string TypeName
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            TypeName = StringUtil.CutBadSqlInfo(TypeName);
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (TypeId > 0)
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "公文", "改")) return ServiceStateKeys.NoPermiss;
            }
            else
            {
                if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "公文", "增")) return ServiceStateKeys.NoPermiss;
            }
            xundh.BLL.Doc_Types bll = new xundh.BLL.Doc_Types();
            xundh.Model.Doc_Types m;
            if (TypeId > 0)
                m = bll.GetModel(TypeId);
            else
                m = new xundh.Model.Doc_Types();
            m.TypeName = TypeName;
            if (TypeId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists()
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "Doc_Types";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "TypeId,TypeName";
            datagrid.init(datagrid.StrWhere, 1, recordcount, int.MaxValue, "", "");
            if (datagrid.Order == "") datagrid.Order = "TypeId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        public xundh.Model.Doc_Types Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Doc_Types bll = new xundh.BLL.Doc_Types();
            xundh.Model.Doc_Types m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            //权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "公文", "删")) return ServiceStateKeys.NoPermiss;

            xundh.BLL.Doc_Types bll = new xundh.BLL.Doc_Types();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}