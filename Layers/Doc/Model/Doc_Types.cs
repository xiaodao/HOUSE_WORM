﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Doc_Types:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Doc_Types
    {
        public Doc_Types()
        { }
        #region Model
        private int _typeid;
        private string _typename;
        /// <summary>
        /// 
        /// </summary>
        public int TypeId
        {
            set { _typeid = value; }
            get { return _typeid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string TypeName
        {
            set { _typename = value; }
            get { return _typename; }
        }
        #endregion Model

    }
}

