﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Doc_Items:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Doc_Items
    {
        public Doc_Items()
        { }
        #region Model
        private int _docid;
        private string _obj;
        private int? _typeid;
        private string _chaobao;
        private string _chaosong;
        private string _luokuan;
        private string _yinfa;
        private string _title;
        private string _body;
        private int? _adddepartid;
        private DateTime? _addtime;
        private int? _adduserid;
        private string _adduser;
        private string _addip;
        private int? _lastdepartid;
        private DateTime? _lasttime;
        private int? _lastuserid;
        private string _lastuser;
        private string _lastip;
        /// <summary>
        /// 
        /// </summary>
        public int DocId
        {
            set { _docid = value; }
            get { return _docid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Obj
        {
            set { _obj = value; }
            get { return _obj; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? TypeId
        {
            set { _typeid = value; }
            get { return _typeid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Chaobao
        {
            set { _chaobao = value; }
            get { return _chaobao; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Chaosong
        {
            set { _chaosong = value; }
            get { return _chaosong; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Luokuan
        {
            set { _luokuan = value; }
            get { return _luokuan; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Yinfa
        {
            set { _yinfa = value; }
            get { return _yinfa; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Body
        {
            set { _body = value; }
            get { return _body; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        #endregion Model

    }
}

