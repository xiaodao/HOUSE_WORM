﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Region_Buildings:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Region_Buildings
    {
        public Region_Buildings()
        { }
        #region Model
        private int _buildingid;
        private int? _buildingarea;
        private string _buildingaddr;
        private string _buildingalice;
        private string _buildingname;
        private decimal? _buildinglongitude;
        private decimal? _buildinglatitude;
        private string _buildingnameen;
        private string _buildingaliceen;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        private DateTime? _addtime;
        private string _addip;
        private int? _lastuserid;
        private string _lastuser;
        private int? _lastdepartid;
        private DateTime? _lasttime;
        private string _lastip;
        private string _mem;
        private int? _sellcount;
        private int? _rentcount;
        private bool _isdelete;
        /// <summary>
        /// 
        /// </summary>
        public int BuildingId
        {
            set { _buildingid = value; }
            get { return _buildingid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? BuildingArea
        {
            set { _buildingarea = value; }
            get { return _buildingarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildingAddr
        {
            set { _buildingaddr = value; }
            get { return _buildingaddr; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildingAlice
        {
            set { _buildingalice = value; }
            get { return _buildingalice; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildingName
        {
            set { _buildingname = value; }
            get { return _buildingname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BuildingLongitude
        {
            set { _buildinglongitude = value; }
            get { return _buildinglongitude; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? BuildingLatitude
        {
            set { _buildinglatitude = value; }
            get { return _buildinglatitude; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildingNameEn
        {
            set { _buildingnameen = value; }
            get { return _buildingnameen; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildingAliceEn
        {
            set { _buildingaliceen = value; }
            get { return _buildingaliceen; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SellCount
        {
            set { _sellcount = value; }
            get { return _sellcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RentCount
        {
            set { _rentcount = value; }
            get { return _rentcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }
        #endregion Model

    }
}

