﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Region_Area:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Region_Area
    {
        public Region_Area()
        { }
        #region Model
        private int _areaid;
        private string _areaname;
        private int? _cityid;
        private decimal? _regionlatitude;
        private decimal? _regionlongitude;
        /// <summary>
        /// 
        /// </summary>
        public int AreaId
        {
            set { _areaid = value; }
            get { return _areaid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AreaName
        {
            set { _areaname = value; }
            get { return _areaname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CityId
        {
            set { _cityid = value; }
            get { return _cityid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? RegionLatitude
        {
            set { _regionlatitude = value; }
            get { return _regionlatitude; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? RegionLongitude
        {
            set { _regionlongitude = value; }
            get { return _regionlongitude; }
        }
        #endregion Model

    }
}

