﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Region_Cities:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Region_Cities
    {
        public Region_Cities()
        { }
        #region Model
        private int _cityid;
        private string _cityname;
        private int? _provinceid;
        private decimal? _citylatitude;
        private decimal? _citylongitude;
        /// <summary>
        /// 
        /// </summary>
        public int CityId
        {
            set { _cityid = value; }
            get { return _cityid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CityName
        {
            set { _cityname = value; }
            get { return _cityname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ProvinceId
        {
            set { _provinceid = value; }
            get { return _provinceid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? CityLatitude
        {
            set { _citylatitude = value; }
            get { return _citylatitude; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? CityLongitude
        {
            set { _citylongitude = value; }
            get { return _citylongitude; }
        }
        #endregion Model

    }
}

