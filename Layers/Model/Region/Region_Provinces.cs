﻿using System;
namespace xundh.Model
{
	/// <summary>
	/// Region_Provinces:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Region_Provinces
	{
		public Region_Provinces()
		{}
		#region Model
		private int _provinceid;
		private string _provincename;
		/// <summary>
		/// 
		/// </summary>
		public int ProvinceId
		{
			set{ _provinceid=value;}
			get{return _provinceid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string ProvinceName
		{
			set{ _provincename=value;}
			get{return _provincename;}
		}
		#endregion Model

	}
}

