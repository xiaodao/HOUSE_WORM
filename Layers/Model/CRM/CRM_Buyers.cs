﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// CRM_Buyers:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CRM_Buyers
    {
        public CRM_Buyers()
        { }
        #region Model
        private int _customerid;
        private string _crmtype;
        private int? _informationcomefrom;
        private string _customername;
        private int? _viewlevel;
        private int? _sex;
        private string _tel;
        private string _mem;
        private int? _state;
        private string _wantregionarea;
        private decimal? _wanthouseareabegin;
        private decimal? _wanthouseareaend;
        private string _wanthuxing;
        private string _wantfloor;
        private decimal? _wantunitpricebegin;
        private decimal? _wantunitpriceend;
        private decimal? _wanttotalpricebegin;
        private decimal? _wanttotalpriceend;
        private string _wantdecoration;
        private string _wantbuildingids;
        private string _wantbuildings;
        private int? _wantshi;
        private int? _wantting;
        private bool _isfirstbuy;
        private string _residence;
        private string _informationsource;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _lasttime;
        private string _lastip;
        private int? _lastdepartid;
        private int? _lastuserid;
        private string _lastuser;
        private int? _manageuserid;
        private string _manageuser;
        private int? _managedepartid;
        private int? _distributeuserid;
        private string _distributeuser;
        private DateTime? _distributetime;
        private int? _distributedepartid;
        /// <summary>
        /// 
        /// </summary>
        public int CustomerId
        {
            set { _customerid = value; }
            get { return _customerid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CRMType
        {
            set { _crmtype = value; }
            get { return _crmtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? InformationComeFrom
        {
            set { _informationcomefrom = value; }
            get { return _informationcomefrom; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CustomerName
        {
            set { _customername = value; }
            get { return _customername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ViewLevel
        {
            set { _viewlevel = value; }
            get { return _viewlevel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Sex
        {
            set { _sex = value; }
            get { return _sex; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Tel
        {
            set { _tel = value; }
            get { return _tel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string WantRegionArea
        {
            set { _wantregionarea = value; }
            get { return _wantregionarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WantHouseAreaBegin
        {
            set { _wanthouseareabegin = value; }
            get { return _wanthouseareabegin; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WantHouseAreaEnd
        {
            set { _wanthouseareaend = value; }
            get { return _wanthouseareaend; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string WantHuxing
        {
            set { _wanthuxing = value; }
            get { return _wanthuxing; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string WantFloor
        {
            set { _wantfloor = value; }
            get { return _wantfloor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WantUnitPriceBegin
        {
            set { _wantunitpricebegin = value; }
            get { return _wantunitpricebegin; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WantUnitPriceEnd
        {
            set { _wantunitpriceend = value; }
            get { return _wantunitpriceend; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WantTotalPriceBegin
        {
            set { _wanttotalpricebegin = value; }
            get { return _wanttotalpricebegin; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? WantTotalPriceEnd
        {
            set { _wanttotalpriceend = value; }
            get { return _wanttotalpriceend; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string WantDecoration
        {
            set { _wantdecoration = value; }
            get { return _wantdecoration; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string WantBuildingIds
        {
            set { _wantbuildingids = value; }
            get { return _wantbuildingids; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string WantBuildings
        {
            set { _wantbuildings = value; }
            get { return _wantbuildings; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? WantShi
        {
            set { _wantshi = value; }
            get { return _wantshi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? WantTing
        {
            set { _wantting = value; }
            get { return _wantting; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsFirstBuy
        {
            set { _isfirstbuy = value; }
            get { return _isfirstbuy; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Residence
        {
            set { _residence = value; }
            get { return _residence; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string InformationSource
        {
            set { _informationsource = value; }
            get { return _informationsource; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ManageUserId
        {
            set { _manageuserid = value; }
            get { return _manageuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ManageUser
        {
            set { _manageuser = value; }
            get { return _manageuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ManageDepartId
        {
            set { _managedepartid = value; }
            get { return _managedepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? DistributeUserId
        {
            set { _distributeuserid = value; }
            get { return _distributeuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DistributeUser
        {
            set { _distributeuser = value; }
            get { return _distributeuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? DistributeTime
        {
            set { _distributetime = value; }
            get { return _distributetime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? DistributeDepartId
        {
            set { _distributedepartid = value; }
            get { return _distributedepartid; }
        }
        #endregion Model

    }
}

