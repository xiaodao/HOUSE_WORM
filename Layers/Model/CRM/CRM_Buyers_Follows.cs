﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// CRM_Customers_Follows:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CRM_Buyers_Follows
    {
        public CRM_Buyers_Follows()
        { }
        #region Model
        private int _followid;
        private int? _customerid;
        private string _follow;
        private int? _followcustomerstate;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        /// <summary>
        /// 
        /// </summary>
        public int FollowId
        {
            set { _followid = value; }
            get { return _followid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CustomerId
        {
            set { _customerid = value; }
            get { return _customerid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Follow
        {
            set { _follow = value; }
            get { return _follow; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FollowCustomerState
        {
            set { _followcustomerstate = value; }
            get { return _followcustomerstate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        #endregion Model

    }
}

