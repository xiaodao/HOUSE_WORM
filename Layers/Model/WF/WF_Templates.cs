﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// WF_Templates:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class WF_Templates
    {
        public WF_Templates()
        { }
        #region Model
        private int _templateid;
        private int? _typeid;
        private string _title;
        private string _form;
        private string _graphjson;
        /// <summary>
        /// 
        /// </summary>
        public int TemplateId
        {
            set { _templateid = value; }
            get { return _templateid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? TypeId
        {
            set { _typeid = value; }
            get { return _typeid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Form
        {
            set { _form = value; }
            get { return _form; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string GraphJson
        {
            set { _graphjson = value; }
            get { return _graphjson; }
        }
        #endregion Model

    }
}

