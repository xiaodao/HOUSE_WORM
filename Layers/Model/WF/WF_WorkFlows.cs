﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// WF_WorkFlows:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class WF_WorkFlows
    {
        public WF_WorkFlows()
        { }
        #region Model
        private int _flowid;
        private int? _itemid;
        private int? _nodestep;
        private string _nodename;
        private string _nodetype;
        private string _touserid;
        private string _touser;
        private DateTime? _addtime;
        private int? _state;
        private string _suggest;
        private int? _verifyuserid;
        private string _verifyuser;
        private DateTime? _verifytime;
        /// <summary>
        /// 
        /// </summary>
        public int FlowId
        {
            set { _flowid = value; }
            get { return _flowid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ItemId
        {
            set { _itemid = value; }
            get { return _itemid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? NodeStep
        {
            set { _nodestep = value; }
            get { return _nodestep; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NodeName
        {
            set { _nodename = value; }
            get { return _nodename; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NodeType
        {
            set { _nodetype = value; }
            get { return _nodetype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ToUserId
        {
            set { _touserid = value; }
            get { return _touserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ToUser
        {
            set { _touser = value; }
            get { return _touser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Suggest
        {
            set { _suggest = value; }
            get { return _suggest; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? VerifyUserId
        {
            set { _verifyuserid = value; }
            get { return _verifyuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string VerifyUser
        {
            set { _verifyuser = value; }
            get { return _verifyuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? VerifyTime
        {
            set { _verifytime = value; }
            get { return _verifytime; }
        }
        #endregion Model

    }
}

