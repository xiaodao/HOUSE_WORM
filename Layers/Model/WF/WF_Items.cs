﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// WF_Items:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class WF_Items
    {
        public WF_Items()
        { }
        #region Model
        private int _itemid;
        private int? _templateid;
        private string _params;
        private string _form;
        private DateTime? _addtime;
        private int? _adduserid;
        private string _adduser;
        private string _addip;
        private int? _adddepartid;
        private DateTime? _lasttime;
        private int? _lastuserid;
        private string _lastuser;
        private string _lastip;
        private int? _state;
        private int? _nowworkflowid;
        private string _nownodename;
        private string _nowtouserids;
        private string _nowtousers;
        private string _specifyids;
        private string _specifynames;
        private string _filepath;
        /// <summary>
        /// 
        /// </summary>
        public int ItemId
        {
            set { _itemid = value; }
            get { return _itemid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? TemplateId
        {
            set { _templateid = value; }
            get { return _templateid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Params
        {
            set { _params = value; }
            get { return _params; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Form
        {
            set { _form = value; }
            get { return _form; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? NowWorkFlowId
        {
            set { _nowworkflowid = value; }
            get { return _nowworkflowid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NowNodeName
        {
            set { _nownodename = value; }
            get { return _nownodename; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NowToUserIds
        {
            set { _nowtouserids = value; }
            get { return _nowtouserids; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NowToUsers
        {
            set { _nowtousers = value; }
            get { return _nowtousers; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SpecifyIds
        {
            set { _specifyids = value; }
            get { return _specifyids; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SpecifyNames
        {
            set { _specifynames = value; }
            get { return _specifynames; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FilePath
        {
            set { _filepath = value; }
            get { return _filepath; }
        }
        #endregion Model

    }
}

