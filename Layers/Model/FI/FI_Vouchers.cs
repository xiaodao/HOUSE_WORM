﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// FI_Vouchers:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class FI_Vouchers
    {
        public FI_Vouchers()
        { }
        #region Model
        private int _voucherid;
        private string _vouchercode;
        private decimal? _amount;
        private int? _rootsubjectid;
        private int? _subjectid;
        private int? _direction;
        private string _description;
        private string _accountofficer;
        private int? _accountofficerid;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _cashierid;
        private string _cashieruser;
        private int? _relatedindex;
        /// <summary>
        /// 
        /// </summary>
        public int VoucherId
        {
            set { _voucherid = value; }
            get { return _voucherid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string VoucherCode
        {
            set { _vouchercode = value; }
            get { return _vouchercode; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Amount
        {
            set { _amount = value; }
            get { return _amount; }
        }
        /// <summary>
        /// 总账科目
        /// </summary>
        public int? RootSubjectId
        {
            set { _rootsubjectid = value; }
            get { return _rootsubjectid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SubjectId
        {
            set { _subjectid = value; }
            get { return _subjectid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Direction
        {
            set { _direction = value; }
            get { return _direction; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        /// <summary>
        /// 会计主管
        /// </summary>
        public string AccountOfficer
        {
            set { _accountofficer = value; }
            get { return _accountofficer; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AccountOfficerId
        {
            set { _accountofficerid = value; }
            get { return _accountofficerid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CashierId
        {
            set { _cashierid = value; }
            get { return _cashierid; }
        }
        /// <summary>
        /// 出纳
        /// </summary>
        public string CashierUser
        {
            set { _cashieruser = value; }
            get { return _cashieruser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RelatedIndex
        {
            set { _relatedindex = value; }
            get { return _relatedindex; }
        }
        #endregion Model

    }
}

