﻿/**  版本信息模板在安装目录下，可自行修改。
* FI_FA_Items.cs
*
* 功 能： N/A
* 类 名： FI_FA_Items
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2014/3/9 11:46:55   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace xundh.Model
{
	/// <summary>
	/// FI_FA_Items:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class FI_FA_Items
	{
		public FI_FA_Items()
		{}
		#region Model
		private int _faid;
		private int? _subjectid;
		private string _facode;
		private int? _typeid;
		private int? _managedepartid;
		/// <summary>
		/// 
		/// </summary>
		public int FAId
		{
			set{ _faid=value;}
			get{return _faid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? SubjectId
		{
			set{ _subjectid=value;}
			get{return _subjectid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string FACode
		{
			set{ _facode=value;}
			get{return _facode;}
		}
		/// <summary>
		/// 固定资产类别
		/// </summary>
		public int? TypeId
		{
			set{ _typeid=value;}
			get{return _typeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ManageDepartId
		{
			set{ _managedepartid=value;}
			get{return _managedepartid;}
		}
		#endregion Model

	}
}

