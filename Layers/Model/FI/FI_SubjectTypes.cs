﻿/**  版本信息模板在安装目录下，可自行修改。
* FI_SubjectTypes.cs
*
* 功 能： N/A
* 类 名： FI_SubjectTypes
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2014/3/9 11:46:57   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace xundh.Model
{
	/// <summary>
	/// FI_SubjectTypes:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class FI_SubjectTypes
	{
		public FI_SubjectTypes()
		{}
		#region Model
		private int _typeid;
		private string _typetitle;
		/// <summary>
		/// 
		/// </summary>
		public int TypeId
		{
			set{ _typeid=value;}
			get{return _typeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string TypeTitle
		{
			set{ _typetitle=value;}
			get{return _typetitle;}
		}
		#endregion Model

	}
}

