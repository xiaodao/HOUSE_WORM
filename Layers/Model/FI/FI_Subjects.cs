﻿/**  版本信息模板在安装目录下，可自行修改。
* FI_Subjects.cs
*
* 功 能： N/A
* 类 名： FI_Subjects
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2014/3/9 11:46:56   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace xundh.Model
{
	/// <summary>
	/// FI_Subjects:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class FI_Subjects
	{
		public FI_Subjects()
		{}
		#region Model
		private int _subjectid;
		private int? _parentsubjectid;
		private string _subjecttitle;
		private string _subjectcode;
		private int? _types;
		private int? _direction;
		private bool _isend;
		/// <summary>
		/// 
		/// </summary>
		public int SubjectId
		{
			set{ _subjectid=value;}
			get{return _subjectid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? ParentSubjectId
		{
			set{ _parentsubjectid=value;}
			get{return _parentsubjectid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string SubjectTitle
		{
			set{ _subjecttitle=value;}
			get{return _subjecttitle;}
		}
		/// <summary>
		/// 科目代码
		/// </summary>
		public string SubjectCode
		{
			set{ _subjectcode=value;}
			get{return _subjectcode;}
		}
		/// <summary>
		/// 科目类别
		/// </summary>
		public int? Types
		{
			set{ _types=value;}
			get{return _types;}
		}
		/// <summary>
		/// 方向，1借方 0贷方
		/// </summary>
		public int? Direction
		{
			set{ _direction=value;}
			get{return _direction;}
		}
		/// <summary>
		/// 是否末级
		/// </summary>
		public bool IsEnd
		{
			set{ _isend=value;}
			get{return _isend;}
		}
		#endregion Model

	}
}

