﻿/**  版本信息模板在安装目录下，可自行修改。
* CM_Buy_FeeTypes.cs
*
* 功 能： N/A
* 类 名： CM_Buy_FeeTypes
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2014/3/8 16:09:45   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace xundh.Model
{
	/// <summary>
	/// CM_Buy_FeeTypes:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class CM_Buy_FeeTypes
	{
		public CM_Buy_FeeTypes()
		{}
		#region Model
		private int _feetypeid;
		private int? _cmtypeid;
		private string _title;
		private string _functions;
		/// <summary>
		/// 
		/// </summary>
		public int FeeTypeId
		{
			set{ _feetypeid=value;}
			get{return _feetypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? CMTypeId
		{
			set{ _cmtypeid=value;}
			get{return _cmtypeid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Title
		{
			set{ _title=value;}
			get{return _title;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Functions
		{
			set{ _functions=value;}
			get{return _functions;}
		}
		#endregion Model

	}
}

