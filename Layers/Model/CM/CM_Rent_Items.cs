﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// CM_Rent_Items:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CM_Rent_Items
    {
        public CM_Rent_Items()
        { }
        #region Model
        private int _cmid;
        private string _cmnumber;
        private int? _houseid;
        private decimal? _totalprice;
        private int? _houseregionarea;
        private decimal? _housearea;
        private string _housetel;
        private string _housecustomername;
        private string _housepersonid;
        private int? _customerid;
        private string _customername;
        private string _customertel;
        private string _customerpersonid;
        private int? _signuserid;
        private string _signuser;
        private int? _signdepartid;
        private int? _paytype;
        private decimal? _chargetotal;
        private int? _chargepaytype;
        private decimal? _chargehouse;
        private decimal? _chargecustomer;
        private int? _state;
        private string _mem;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        private DateTime? _addtime;
        private string _addip;
        private int? _lastuserid;
        private string _lastuser;
        private int? _lastdepartid;
        private DateTime? _lasttime;
        private string _lastip;
        /// <summary>
        /// 
        /// </summary>
        public int CMId
        {
            set { _cmid = value; }
            get { return _cmid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CMNumber
        {
            set { _cmnumber = value; }
            get { return _cmnumber; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseId
        {
            set { _houseid = value; }
            get { return _houseid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? TotalPrice
        {
            set { _totalprice = value; }
            get { return _totalprice; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseRegionArea
        {
            set { _houseregionarea = value; }
            get { return _houseregionarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? HouseArea
        {
            set { _housearea = value; }
            get { return _housearea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HouseTel
        {
            set { _housetel = value; }
            get { return _housetel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HouseCustomerName
        {
            set { _housecustomername = value; }
            get { return _housecustomername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HousePersonID
        {
            set { _housepersonid = value; }
            get { return _housepersonid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CustomerId
        {
            set { _customerid = value; }
            get { return _customerid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CustomerName
        {
            set { _customername = value; }
            get { return _customername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CustomerTel
        {
            set { _customertel = value; }
            get { return _customertel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CustomerPersonID
        {
            set { _customerpersonid = value; }
            get { return _customerpersonid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SignUserId
        {
            set { _signuserid = value; }
            get { return _signuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SignUser
        {
            set { _signuser = value; }
            get { return _signuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SignDepartId
        {
            set { _signdepartid = value; }
            get { return _signdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? PayType
        {
            set { _paytype = value; }
            get { return _paytype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ChargeTotal
        {
            set { _chargetotal = value; }
            get { return _chargetotal; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ChargePayType
        {
            set { _chargepaytype = value; }
            get { return _chargepaytype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ChargeHouse
        {
            set { _chargehouse = value; }
            get { return _chargehouse; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? ChargeCustomer
        {
            set { _chargecustomer = value; }
            get { return _chargecustomer; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        #endregion Model

    }
}

