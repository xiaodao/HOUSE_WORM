﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// CM_Buy_Doc_WorkFlow:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CM_Buy_Doc_WorkFlow
    {
        public CM_Buy_Doc_WorkFlow()
        { }
        #region Model
        private int _cmwfid;
        private int? _itemid;
        private string _senddocids;
        private int? _fromuserid;
        private string _fromuser;
        private int? _touserid;
        private string _touser;
        private string _receivedocids;
        private bool _isreceive;
        private DateTime? _sendtime;
        private DateTime? _receivetime;
        /// <summary>
        /// 
        /// </summary>
        public int CMWFId
        {
            set { _cmwfid = value; }
            get { return _cmwfid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ItemId
        {
            set { _itemid = value; }
            get { return _itemid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SendDocIds
        {
            set { _senddocids = value; }
            get { return _senddocids; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FromUserId
        {
            set { _fromuserid = value; }
            get { return _fromuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FromUser
        {
            set { _fromuser = value; }
            get { return _fromuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ToUserId
        {
            set { _touserid = value; }
            get { return _touserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ToUser
        {
            set { _touser = value; }
            get { return _touser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ReceiveDocIds
        {
            set { _receivedocids = value; }
            get { return _receivedocids; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsReceive
        {
            set { _isreceive = value; }
            get { return _isreceive; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? SendTime
        {
            set { _sendtime = value; }
            get { return _sendtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? ReceiveTime
        {
            set { _receivetime = value; }
            get { return _receivetime; }
        }
        #endregion Model

    }
}

