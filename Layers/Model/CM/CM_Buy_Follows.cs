﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// CM_Buy_Follows:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CM_Buy_Follows
    {
        public CM_Buy_Follows()
        { }
        #region Model
        private int _followid;
        private int? _cmid;
        private string _follow;
        private int? _followcmstate;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        /// <summary>
        /// 
        /// </summary>
        public int FollowId
        {
            set { _followid = value; }
            get { return _followid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CMId
        {
            set { _cmid = value; }
            get { return _cmid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Follow
        {
            set { _follow = value; }
            get { return _follow; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FollowCMState
        {
            set { _followcmstate = value; }
            get { return _followcmstate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        #endregion Model

    }
}

