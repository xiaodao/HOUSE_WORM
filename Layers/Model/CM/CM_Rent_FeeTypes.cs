﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// CM_Rent_FeeTypes:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CM_Rent_FeeTypes
    {
        public CM_Rent_FeeTypes()
        { }
        #region Model
        private int _feetypeid;
        private int? _cmtypeid;
        private string _title;
        private string _functions;
        private int? _fisubjectid;
        /// <summary>
        /// 
        /// </summary>
        public int FeeTypeId
        {
            set { _feetypeid = value; }
            get { return _feetypeid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CMTypeId
        {
            set { _cmtypeid = value; }
            get { return _cmtypeid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Functions
        {
            set { _functions = value; }
            get { return _functions; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FISubjectId
        {
            set { _fisubjectid = value; }
            get { return _fisubjectid; }
        }
        #endregion Model

    }
}

