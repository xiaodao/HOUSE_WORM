﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// CM_Buy_Docs:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CM_Buy_Docs
    {
        public CM_Buy_Docs()
        { }
        #region Model
        private int _docid;
        private string _doctitle;
        /// <summary>
        /// 
        /// </summary>
        public int DocId
        {
            set { _docid = value; }
            get { return _docid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DocTitle
        {
            set { _doctitle = value; }
            get { return _doctitle; }
        }
        #endregion Model

    }
}

