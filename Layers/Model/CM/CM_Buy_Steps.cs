﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// CM_Buy_Steps:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class CM_Buy_Steps
    {
        public CM_Buy_Steps()
        { }
        #region Model
        private int _stepid;
        private string _title;
        private string _description;
        /// <summary>
        /// 
        /// </summary>
        public int StepId
        {
            set { _stepid = value; }
            get { return _stepid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Title
        {
            set { _title = value; }
            get { return _title; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            set { _description = value; }
            get { return _description; }
        }
        #endregion Model

    }
}

