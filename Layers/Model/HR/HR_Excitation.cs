﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// HR_Excitation:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class HR_Excitation
    {
        public HR_Excitation()
        { }
        #region Model
        private int _excitationid;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        private decimal? _amount;
        private int? _objectuserid;
        private string _objectuser;
        private int? _objectdepartid;
        private DateTime? _objecttime;
        private string _objectaddress;
        private int? _state;
        private int? _verifyuserid;
        private string _verifyuser;
        private int? _verifydepartid;
        private string _matter;
        private int? _type;
        /// <summary>
        /// 
        /// </summary>
        public int ExcitationId
        {
            set { _excitationid = value; }
            get { return _excitationid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Amount
        {
            set { _amount = value; }
            get { return _amount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ObjectUserId
        {
            set { _objectuserid = value; }
            get { return _objectuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ObjectUser
        {
            set { _objectuser = value; }
            get { return _objectuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ObjectDepartId
        {
            set { _objectdepartid = value; }
            get { return _objectdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? ObjectTime
        {
            set { _objecttime = value; }
            get { return _objecttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ObjectAddress
        {
            set { _objectaddress = value; }
            get { return _objectaddress; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? VerifyUserId
        {
            set { _verifyuserid = value; }
            get { return _verifyuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string VerifyUser
        {
            set { _verifyuser = value; }
            get { return _verifyuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? VerifyDepartId
        {
            set { _verifydepartid = value; }
            get { return _verifydepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Matter
        {
            set { _matter = value; }
            get { return _matter; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Type
        {
            set { _type = value; }
            get { return _type; }
        }
        #endregion Model

    }
}

