﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// RBAC_Role_Resource_Operation:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class RBAC_Role_Resource_Operation
    {
        public RBAC_Role_Resource_Operation()
        { }
        #region Model
        private int _roleauthorityid;
        private int? _roleid;
        private int? _resourceid;
        private int? _operationid;
        private bool _value = false;
        /// <summary>
        /// 
        /// </summary>
        public int RoleAuthorityId
        {
            set { _roleauthorityid = value; }
            get { return _roleauthorityid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RoleId
        {
            set { _roleid = value; }
            get { return _roleid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ResourceId
        {
            set { _resourceid = value; }
            get { return _resourceid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? OperationId
        {
            set { _operationid = value; }
            get { return _operationid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool Value
        {
            set { _value = value; }
            get { return _value; }
        }
        #endregion Model

    }
}

