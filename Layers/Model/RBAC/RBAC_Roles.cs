﻿using System;
namespace xundh.Model
{
	/// <summary>
	/// RBAC_Roles:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class RBAC_Roles
	{
		public RBAC_Roles()
		{}
		#region Model
		private int _roleid;
		private string _rolename;
		private string _roledescription;
		/// <summary>
		/// 
		/// </summary>
		public int RoleId
		{
			set{ _roleid=value;}
			get{return _roleid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RoleName
		{
			set{ _rolename=value;}
			get{return _rolename;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string RoleDescription
		{
			set{ _roledescription=value;}
			get{return _roledescription;}
		}
		#endregion Model

	}
}

