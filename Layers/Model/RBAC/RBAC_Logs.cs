﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// RBAC_Logs:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class RBAC_Logs
    {
        public RBAC_Logs()
        { }
        #region Model
        private int _logid;
        private int? _userid;
        private int? _departid;
        private int? _hardid;
        private DateTime? _addtime;
        private string _addip;
        private string _events;
        /// <summary>
        /// 
        /// </summary>
        public int LogId
        {
            set { _logid = value; }
            get { return _logid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UserId
        {
            set { _userid = value; }
            get { return _userid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? DepartId
        {
            set { _departid = value; }
            get { return _departid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HardId
        {
            set { _hardid = value; }
            get { return _hardid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Events
        {
            set { _events = value; }
            get { return _events; }
        }
        #endregion Model

    }
}

