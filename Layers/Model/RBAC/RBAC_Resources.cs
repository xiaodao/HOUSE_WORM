﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// RBAC_Resources:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class RBAC_Resources
    {
        public RBAC_Resources()
        { }
        #region Model
        private int _resourceid;
        private string _resourcename;
        private string _resourcedescription;
        private int? _parentresourceid;
        private string _resourcepath;
        private int? _sort;
        private int? _defaultstate;
        private string _icon;
        private string _js;
        /// <summary>
        /// 
        /// </summary>
        public int ResourceId
        {
            set { _resourceid = value; }
            get { return _resourceid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ResourceName
        {
            set { _resourcename = value; }
            get { return _resourcename; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ResourceDescription
        {
            set { _resourcedescription = value; }
            get { return _resourcedescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ParentResourceId
        {
            set { _parentresourceid = value; }
            get { return _parentresourceid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ResourcePath
        {
            set { _resourcepath = value; }
            get { return _resourcepath; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Sort
        {
            set { _sort = value; }
            get { return _sort; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? DefaultState
        {
            set { _defaultstate = value; }
            get { return _defaultstate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Icon
        {
            set { _icon = value; }
            get { return _icon; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Js
        {
            set { _js = value; }
            get { return _js; }
        }
        #endregion Model

    }
}

