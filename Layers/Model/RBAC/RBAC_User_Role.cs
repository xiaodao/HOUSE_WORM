﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// RBAC_User_Role:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class RBAC_User_Role
    {
        public RBAC_User_Role()
        { }
        #region Model
        private int _userroleid;
        private int? _userid;
        private int? _roleid;
        /// <summary>
        /// 
        /// </summary>
        public int UserRoleID
        {
            set { _userroleid = value; }
            get { return _userroleid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UserID
        {
            set { _userid = value; }
            get { return _userid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RoleID
        {
            set { _roleid = value; }
            get { return _roleid; }
        }
        #endregion Model

    }
}

