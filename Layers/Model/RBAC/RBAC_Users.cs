﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// RBAC_Users:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class RBAC_Users
    {
        public RBAC_Users()
        { }
        #region Model
        private int _userid;
        private string _username;
        private string _usernameen;
        private string _userpass;
        private int? _usersex;
        private int? _userstate;
        private string _usermobile;
        private string _useremail;
        private bool _useradmin;
        private string _usersessionid;
        private int? _useronline;
        private int? _departid;
        private int? _postid;
        private DateTime? _lastlogintime;
        private string _lastloginip;
        private int? _lasthardid;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        private DateTime? _addtime;
        private string _addip;
        private int? _lastuserid;
        private string _lastuser;
        private int? _lastdepartid;
        private DateTime? _lasttime;
        private string _lastip;
        private string _mem;
        private DateTime _PreOpTime;
        private string _hxPass;

        public DateTime PreOpTime
        {
            get { return _PreOpTime; }
            set { _PreOpTime = value; }
        }
        public string HxPass
        {
            set { _hxPass = value; }
            get { return _hxPass; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int UserId
        {
            set { _userid = value; }
            get { return _userid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserName
        {
            set { _username = value; }
            get { return _username; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserNameEn
        {
            set { _usernameen = value; }
            get { return _usernameen; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserPass
        {
            set { _userpass = value; }
            get { return _userpass; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UserSex
        {
            set { _usersex = value; }
            get { return _usersex; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UserState
        {
            set { _userstate = value; }
            get { return _userstate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserMobile
        {
            set { _usermobile = value; }
            get { return _usermobile; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserEmail
        {
            set { _useremail = value; }
            get { return _useremail; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool UserAdmin
        {
            set { _useradmin = value; }
            get { return _useradmin; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserSessionId
        {
            set { _usersessionid = value; }
            get { return _usersessionid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UserOnline
        {
            set { _useronline = value; }
            get { return _useronline; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? DepartId
        {
            set { _departid = value; }
            get { return _departid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? PostId
        {
            set { _postid = value; }
            get { return _postid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastLoginTime
        {
            set { _lastlogintime = value; }
            get { return _lastlogintime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastLoginIp
        {
            set { _lastloginip = value; }
            get { return _lastloginip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastHardId
        {
            set { _lasthardid = value; }
            get { return _lasthardid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        #endregion Model

    }
}

