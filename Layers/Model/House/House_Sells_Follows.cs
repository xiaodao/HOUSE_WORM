﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// House_Sells_Follows:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class House_Sells_Follows
    {
        public House_Sells_Follows()
        { }
        #region Model
        private int _followid;
        private int? _houseid;
        private string _follow;
        private int? _followhousestate;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        private int? _viewlevel;
        private int? _suggeststate;
        /// <summary>
        /// 
        /// </summary>
        public int FollowId
        {
            set { _followid = value; }
            get { return _followid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseId
        {
            set { _houseid = value; }
            get { return _houseid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Follow
        {
            set { _follow = value; }
            get { return _follow; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FollowHouseState
        {
            set { _followhousestate = value; }
            get { return _followhousestate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ViewLevel
        {
            set { _viewlevel = value; }
            get { return _viewlevel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SuggestState
        {
            set { _suggeststate = value; }
            get { return _suggeststate; }
        }
        #endregion Model

    }
}

