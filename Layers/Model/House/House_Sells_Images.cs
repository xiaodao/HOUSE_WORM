﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// House_Sells_Images:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class House_Sells_Images
    {
        public House_Sells_Images()
        { }
        #region Model
        private int _id;
        private int? _housetype;
        private int? _houseid;
        private string _imgurl;
        private string _thumbimageurl;
        private DateTime? _addtime;
        private string _addip;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseType
        {
            set { _housetype = value; }
            get { return _housetype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseId
        {
            set { _houseid = value; }
            get { return _houseid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ImgUrl
        {
            set { _imgurl = value; }
            get { return _imgurl; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ThumbImageUrl
        {
            set { _thumbimageurl = value; }
            get { return _thumbimageurl; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        #endregion Model

    }
}

