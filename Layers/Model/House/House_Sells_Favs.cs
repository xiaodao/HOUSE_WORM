﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// House_Sells_Favs:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class House_Sells_Favs
    {
        public House_Sells_Favs()
        { }
        #region Model
        private int _favid;
        private int? _sellsid;
        private string _adduser;
        private int? _adduserid;
        private DateTime? _addtime;
        private int? _adddepartid;
        /// <summary>
        /// 
        /// </summary>
        public int FavId
        {
            set { _favid = value; }
            get { return _favid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SellsId
        {
            set { _sellsid = value; }
            get { return _sellsid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        #endregion Model

    }
}

