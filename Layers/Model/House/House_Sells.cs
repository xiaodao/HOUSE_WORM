﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// House_Sells:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class House_Sells
    {
        public House_Sells()
        { }
        #region Model
        private int _sellid;
        private int? _viewlevel;
        private int? _sellregionarea;
        private int? _sellface;
        private int? _sellbuildingid;
        private string _dong;
        private string _fang;
        private decimal? _shi;
        private int? _ting;
        private int? _chu;
        private int? _wei;
        private decimal? _housearea;
        private int? _housefloor;
        private int? _housetotalfloor;
        private decimal? _unitprice;
        private decimal? _totalprice;
        private int? _buildyear;
        private DateTime? _getpropertydate;
        private int? _decoration;
        private string _customername;
        private string _customertel;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        private int? _lastuserid;
        private string _lastuser;
        private DateTime? _lasttime;
        private string _lastip;
        private int? _lastdepartid;
        private bool _haskey;
        private int? _keydepartid;
        private int? _followcount;
        private int? _viewcount;
        private int? _state;
        private string _mem;
        private int? _suggest;
        private bool _hasimage;
        /// <summary>
        /// 
        /// </summary>
        public int SellId
        {
            set { _sellid = value; }
            get { return _sellid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ViewLevel
        {
            set { _viewlevel = value; }
            get { return _viewlevel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SellRegionArea
        {
            set { _sellregionarea = value; }
            get { return _sellregionarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SellFace
        {
            set { _sellface = value; }
            get { return _sellface; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SellBuildingId
        {
            set { _sellbuildingid = value; }
            get { return _sellbuildingid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Dong
        {
            set { _dong = value; }
            get { return _dong; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Fang
        {
            set { _fang = value; }
            get { return _fang; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Shi
        {
            set { _shi = value; }
            get { return _shi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Ting
        {
            set { _ting = value; }
            get { return _ting; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Chu
        {
            set { _chu = value; }
            get { return _chu; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Wei
        {
            set { _wei = value; }
            get { return _wei; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? HouseArea
        {
            set { _housearea = value; }
            get { return _housearea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseFloor
        {
            set { _housefloor = value; }
            get { return _housefloor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseTotalFloor
        {
            set { _housetotalfloor = value; }
            get { return _housetotalfloor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? UnitPrice
        {
            set { _unitprice = value; }
            get { return _unitprice; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? TotalPrice
        {
            set { _totalprice = value; }
            get { return _totalprice; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? BuildYear
        {
            set { _buildyear = value; }
            get { return _buildyear; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? GetPropertyDate
        {
            set { _getpropertydate = value; }
            get { return _getpropertydate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Decoration
        {
            set { _decoration = value; }
            get { return _decoration; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CustomerName
        {
            set { _customername = value; }
            get { return _customername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CustomerTel
        {
            set { _customertel = value; }
            get { return _customertel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool HasKey
        {
            set { _haskey = value; }
            get { return _haskey; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? KeyDepartId
        {
            set { _keydepartid = value; }
            get { return _keydepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FollowCount
        {
            set { _followcount = value; }
            get { return _followcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ViewCount
        {
            set { _viewcount = value; }
            get { return _viewcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Suggest
        {
            set { _suggest = value; }
            get { return _suggest; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool HasImage
        {
            set { _hasimage = value; }
            get { return _hasimage; }
        }
        #endregion Model

    }
}

