﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// House_Sells_Views:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class House_Sells_Views
    {
        public House_Sells_Views()
        { }
        #region Model
        private long _viewid;
        private int? _sellid;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        /// <summary>
        /// 
        /// </summary>
        public long ViewId
        {
            set { _viewid = value; }
            get { return _viewid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? SellId
        {
            set { _sellid = value; }
            get { return _sellid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        #endregion Model

    }
}

