﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// House_Rents:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class House_Rents
    {
        public House_Rents()
        { }
        #region Model
        private int _rentid;
        private int? _viewlevel;
        private int? _rentregionarea;
        private int? _rentface;
        private int? _rentbuildingid;
        private string _dong;
        private string _fang;
        private decimal? _shi;
        private int? _ting;
        private int? _chu;
        private int? _wei;
        private decimal? _housearea;
        private int? _housefloor;
        private int? _housetotalfloor;
        private decimal? _totalprice;
        private string _paytype;
        private int? _decoration;
        private string _customername;
        private string _customertel;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        private DateTime? _addtime;
        private string _addip;
        private int? _lastuserid;
        private string _lastuser;
        private int? _lastdepartid;
        private DateTime? _lasttime;
        private string _lastip;
        private string _mem;
        private bool _haskey;
        private int? _keydepartid;
        private int? _followcount;
        private int? _viewcount;
        private int? _state;
        /// <summary>
        /// 
        /// </summary>
        public int RentId
        {
            set { _rentid = value; }
            get { return _rentid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ViewLevel
        {
            set { _viewlevel = value; }
            get { return _viewlevel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RentRegionArea
        {
            set { _rentregionarea = value; }
            get { return _rentregionarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RentFace
        {
            set { _rentface = value; }
            get { return _rentface; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RentBuildingId
        {
            set { _rentbuildingid = value; }
            get { return _rentbuildingid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Dong
        {
            set { _dong = value; }
            get { return _dong; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Fang
        {
            set { _fang = value; }
            get { return _fang; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Shi
        {
            set { _shi = value; }
            get { return _shi; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Ting
        {
            set { _ting = value; }
            get { return _ting; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Chu
        {
            set { _chu = value; }
            get { return _chu; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Wei
        {
            set { _wei = value; }
            get { return _wei; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? HouseArea
        {
            set { _housearea = value; }
            get { return _housearea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseFloor
        {
            set { _housefloor = value; }
            get { return _housefloor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseTotalFloor
        {
            set { _housetotalfloor = value; }
            get { return _housetotalfloor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? TotalPrice
        {
            set { _totalprice = value; }
            get { return _totalprice; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string PayType
        {
            set { _paytype = value; }
            get { return _paytype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Decoration
        {
            set { _decoration = value; }
            get { return _decoration; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CustomerName
        {
            set { _customername = value; }
            get { return _customername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CustomerTel
        {
            set { _customertel = value; }
            get { return _customertel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool HasKey
        {
            set { _haskey = value; }
            get { return _haskey; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? KeyDepartId
        {
            set { _keydepartid = value; }
            get { return _keydepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FollowCount
        {
            set { _followcount = value; }
            get { return _followcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ViewCount
        {
            set { _viewcount = value; }
            get { return _viewcount; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        #endregion Model

    }
}

