﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Announcement_Items_Views:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Announcement_Items_Views
    {
        public Announcement_Items_Views()
        { }
        #region Model
        private int _viewid;
        private int? _itemid;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        /// <summary>
        /// 
        /// </summary>
        public int ViewId
        {
            set { _viewid = value; }
            get { return _viewid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ItemId
        {
            set { _itemid = value; }
            get { return _itemid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        #endregion Model

    }
}

