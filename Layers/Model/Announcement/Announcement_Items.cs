﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Announcement_Items:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Announcement_Items
    {
        public Announcement_Items()
        { }
        #region Model
        private int _annoucementid;
        private string _annoucementtitle;
        private string _annoucementbody;
        private int? _adddepartid;
        private DateTime? _addtime;
        private int? _adduserid;
        private string _adduser;
        private string _addip;
        private int? _lastdepartid;
        private DateTime? _lasttime;
        private int? _lastuserid;
        private string _lastuser;
        private string _lastip;
        /// <summary>
        /// 
        /// </summary>
        public int AnnoucementId
        {
            set { _annoucementid = value; }
            get { return _annoucementid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AnnoucementTitle
        {
            set { _annoucementtitle = value; }
            get { return _annoucementtitle; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AnnoucementBody
        {
            set { _annoucementbody = value; }
            get { return _annoucementbody; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        #endregion Model

    }
}

