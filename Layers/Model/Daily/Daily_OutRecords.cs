﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Daily_OutRecords:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Daily_OutRecords
    {
        public Daily_OutRecords()
        { }
        #region Model
        private int _outrecordid;
        private string _businesstype;
        private int? _housetype;
        private string _houseid;
        private string _housebuilds;
        private int? _crmtype;
        private string _crmids;
        private string _crmnames;
        private int? _agreementtype;
        private string _agreementids;
        private string _address;
        private string _dothins;
        private int? _state;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        private string _accompanyids;
        private string _accompanynames;
        private DateTime? _backtime;
        /// <summary>
        /// 
        /// </summary>
        public int OutRecordId
        {
            set { _outrecordid = value; }
            get { return _outrecordid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BusinessType
        {
            set { _businesstype = value; }
            get { return _businesstype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? HouseType
        {
            set { _housetype = value; }
            get { return _housetype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HouseId
        {
            set { _houseid = value; }
            get { return _houseid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HouseBuilds
        {
            set { _housebuilds = value; }
            get { return _housebuilds; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CRMType
        {
            set { _crmtype = value; }
            get { return _crmtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CRMIds
        {
            set { _crmids = value; }
            get { return _crmids; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CRMNames
        {
            set { _crmnames = value; }
            get { return _crmnames; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AgreementType
        {
            set { _agreementtype = value; }
            get { return _agreementtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AgreementIds
        {
            set { _agreementids = value; }
            get { return _agreementids; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Address
        {
            set { _address = value; }
            get { return _address; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DoThins
        {
            set { _dothins = value; }
            get { return _dothins; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AccompanyIds
        {
            set { _accompanyids = value; }
            get { return _accompanyids; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AccompanyNames
        {
            set { _accompanynames = value; }
            get { return _accompanynames; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? BackTime
        {
            set { _backtime = value; }
            get { return _backtime; }
        }
        #endregion Model

    }
}

