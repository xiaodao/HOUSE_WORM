﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Daily_RecordTemplate:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Daily_RecordTemplate
    {
        public Daily_RecordTemplate()
        { }
        #region Model
        private int _id;
        private string _templates;
        private int? _adduserid;
        private string _addusername;
        private DateTime? _addtime;
        private string _addip;
        private int? _postid;
        private int? _lastuserid;
        private string _lastusername;
        private DateTime? _lasttime;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Templates
        {
            set { _templates = value; }
            get { return _templates; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUserName
        {
            set { _addusername = value; }
            get { return _addusername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? PostId
        {
            set { _postid = value; }
            get { return _postid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUserName
        {
            set { _lastusername = value; }
            get { return _lastusername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        #endregion Model

    }
}

