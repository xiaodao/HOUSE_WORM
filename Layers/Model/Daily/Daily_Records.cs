﻿/**  版本信息模板在安装目录下，可自行修改。
* Daily_Records.cs
*
* 功 能： N/A
* 类 名： Daily_Records
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  14/2/22 14:29:35   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace xundh.Model
{
	/// <summary>
	/// Daily_Records:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Daily_Records
	{
		public Daily_Records()
		{}
		#region Model
		private int _id;
		private int? _organizatioinid;
		private string _type;
		private DateTime? _dailydate;
		private string _contents;
		private int? _adduserid;
		private string _addusername;
		private int? _adddepartid;
		private string _adddepart;
		private DateTime? _addtime;
		private string _addip;
		private int? _lastcommentuserid;
		private string _lastcommentusername;
		private string _lastcommentcontents;
		private DateTime? _lastcommenttime;
		private string _lastcommentip;
		private string _path;
		/// <summary>
		/// 
		/// </summary>
		public int id
		{
			set{ _id=value;}
			get{return _id;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? OrganizatioinId
		{
			set{ _organizatioinid=value;}
			get{return _organizatioinid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Type
		{
			set{ _type=value;}
			get{return _type;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? DailyDate
		{
			set{ _dailydate=value;}
			get{return _dailydate;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Contents
		{
			set{ _contents=value;}
			get{return _contents;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AddUserId
		{
			set{ _adduserid=value;}
			get{return _adduserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddUserName
		{
			set{ _addusername=value;}
			get{return _addusername;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? AddDepartId
		{
			set{ _adddepartid=value;}
			get{return _adddepartid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddDepart
		{
			set{ _adddepart=value;}
			get{return _adddepart;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? AddTime
		{
			set{ _addtime=value;}
			get{return _addtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string AddIp
		{
			set{ _addip=value;}
			get{return _addip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? LastCommentUserId
		{
			set{ _lastcommentuserid=value;}
			get{return _lastcommentuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LastCommentUserName
		{
			set{ _lastcommentusername=value;}
			get{return _lastcommentusername;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LastCommentContents
		{
			set{ _lastcommentcontents=value;}
			get{return _lastcommentcontents;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime? LastCommentTime
		{
			set{ _lastcommenttime=value;}
			get{return _lastcommenttime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string LastCommentIp
		{
			set{ _lastcommentip=value;}
			get{return _lastcommentip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string Path
		{
			set{ _path=value;}
			get{return _path;}
		}
		#endregion Model

	}
}

