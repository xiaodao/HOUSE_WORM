﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Tools_AgentMobiles:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Tools_AgentMobiles
    {
        public Tools_AgentMobiles()
        { }
        #region Model
        private int _agentid;
        private string _agenttel;
        private string _agentname;
        private string _agentusername;
        private int? _state;
        private int? _adduserid;
        private string _adduser;
        private DateTime? _addtime;
        private string _addip;
        private int? _adddepartid;
        /// <summary>
        /// 
        /// </summary>
        public int AgentId
        {
            set { _agentid = value; }
            get { return _agentid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AgentTel
        {
            set { _agenttel = value; }
            get { return _agenttel; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AgentName
        {
            set { _agentname = value; }
            get { return _agentname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AgentUserName
        {
            set { _agentusername = value; }
            get { return _agentusername; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        #endregion Model

    }
}

