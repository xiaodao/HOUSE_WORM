﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Tools_User_DayClear:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Tools_User_DayClear
    {
        public Tools_User_DayClear()
        { }
        #region Model
        private int _dayid;
        private string _daycontent;
        private int? _adduserid;
        private DateTime? _addtime;
        private int? _adddepartid;
        private int? _approvaluserid;
        private int? _state;
        private string _approvalcontent;
        private int? _approvaldepartid;
        private DateTime? _approvaltime;
        /// <summary>
        /// 
        /// </summary>
        public int DayId
        {
            set { _dayid = value; }
            get { return _dayid; }
        }
        /// <summary>
        /// 日清内容
        /// </summary>
        public string DayContent
        {
            set { _daycontent = value; }
            get { return _daycontent; }
        }
        /// <summary>
        /// 编写人员id
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 编写时间
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 编写人员部门id
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 审批者id
        /// </summary>
        public int? ApprovalUserId
        {
            set { _approvaluserid = value; }
            get { return _approvaluserid; }
        }
        /// <summary>
        /// 日清状态
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 审批评语
        /// </summary>
        public string ApprovalContent
        {
            set { _approvalcontent = value; }
            get { return _approvalcontent; }
        }
        /// <summary>
        /// 审批者部门id
        /// </summary>
        public int? ApprovalDepartId
        {
            set { _approvaldepartid = value; }
            get { return _approvaldepartid; }
        }
        /// <summary>
        /// 审批时间
        /// </summary>
        public DateTime? ApprovalTime
        {
            set { _approvaltime = value; }
            get { return _approvaltime; }
        }
        #endregion Model

    }
}

