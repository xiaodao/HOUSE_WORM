﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Tools_MobileArea:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Tools_MobileArea
    {
        public Tools_MobileArea()
        { }
        #region Model
        private int _id;
        private int? _num;
        private string _code;
        private string _city;
        private string _cardtype;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? num
        {
            set { _num = value; }
            get { return _num; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string code
        {
            set { _code = value; }
            get { return _code; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string city
        {
            set { _city = value; }
            get { return _city; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string cardtype
        {
            set { _cardtype = value; }
            get { return _cardtype; }
        }
        #endregion Model

    }
}

