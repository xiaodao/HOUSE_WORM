﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Tools_VerifyPrice:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Tools_VerifyPrice
    {
        public Tools_VerifyPrice()
        { }
        #region Model
        private int _vpid;
        private int? _regionarea;
        private string _buildingname;
        private string _addr;
        private decimal? _price;
        private string _sellface;
        private int? _cityid;
        /// <summary>
        /// 
        /// </summary>
        public int VPId
        {
            set { _vpid = value; }
            get { return _vpid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? RegionArea
        {
            set { _regionarea = value; }
            get { return _regionarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BuildingName
        {
            set { _buildingname = value; }
            get { return _buildingname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Addr
        {
            set { _addr = value; }
            get { return _addr; }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Price
        {
            set { _price = value; }
            get { return _price; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SellFace
        {
            set { _sellface = value; }
            get { return _sellface; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CityId
        {
            set { _cityid = value; }
            get { return _cityid; }
        }
        #endregion Model

    }
}

