﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Tools_DayClear:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Tools_DayClear
    {
        public Tools_DayClear()
        { }
        #region Model
        private int _id;
        private string _day_close;
        private int? _postid;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        private DateTime? _addtime;
        private string _addip;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string day_close
        {
            set { _day_close = value; }
            get { return _day_close; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? postId
        {
            set { _postid = value; }
            get { return _postid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        #endregion Model

    }
}

