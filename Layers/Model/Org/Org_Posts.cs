﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Org_Posts:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Org_Posts
    {
        public Org_Posts()
        { }
        #region Model
        private int _postid;
        private string _postname;
        private string _postnameen;
        private bool _isdelete;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        private DateTime? _addtime;
        private string _addip;
        private int? _lastuserid;
        private string _lastuser;
        private int? _lastdepartid;
        private DateTime? _lasttime;
        private string _lastip;
        private string _mem;
        /// <summary>
        /// 
        /// </summary>
        public int PostId
        {
            set { _postid = value; }
            get { return _postid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string PostName
        {
            set { _postname = value; }
            get { return _postname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string PostNameEn
        {
            set { _postnameen = value; }
            get { return _postnameen; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        #endregion Model

    }
}

