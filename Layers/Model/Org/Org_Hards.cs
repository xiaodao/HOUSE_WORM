﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Org_Hards:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Org_Hards
    {
        public Org_Hards()
        { }
        #region Model
        private int _hardid;
        private int? _departid;
        private string _mac;
        private string _hardstr;
        private bool _hardstate;
        private string _ua;
        private string _browser;
        private string _browserversion;
        private string _os;
        private int? _screenwidth;
        private int? _screenheight;
        private string _hardtype;
        private int? _lastloginuserid;
        private string _lastloginuser;
        private DateTime? _lastlogintime;
        private string _lastloginip;
        private int? _lastlogindepartid;
        private int? _adduserid;
        private string _adduser;
        private int? _adddepartid;
        private DateTime? _addtime;
        private string _addip;
        private int? _lastuserid;
        private string _lastuser;
        private int? _lastdepartid;
        private DateTime? _lasttime;
        private string _lastip;
        private string _mem;
        private bool _isdelete;
        /// <summary>
        /// 
        /// </summary>
        public int HardId
        {
            set { _hardid = value; }
            get { return _hardid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? DepartId
        {
            set { _departid = value; }
            get { return _departid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mac
        {
            set { _mac = value; }
            get { return _mac; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HardStr
        {
            set { _hardstr = value; }
            get { return _hardstr; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool HardState
        {
            set { _hardstate = value; }
            get { return _hardstate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Ua
        {
            set { _ua = value; }
            get { return _ua; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Browser
        {
            set { _browser = value; }
            get { return _browser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BrowserVersion
        {
            set { _browserversion = value; }
            get { return _browserversion; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Os
        {
            set { _os = value; }
            get { return _os; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ScreenWidth
        {
            set { _screenwidth = value; }
            get { return _screenwidth; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ScreenHeight
        {
            set { _screenheight = value; }
            get { return _screenheight; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HardType
        {
            set { _hardtype = value; }
            get { return _hardtype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastLoginUserId
        {
            set { _lastloginuserid = value; }
            get { return _lastloginuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastLoginUser
        {
            set { _lastloginuser = value; }
            get { return _lastloginuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastLoginTime
        {
            set { _lastlogintime = value; }
            get { return _lastlogintime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastLoginIp
        {
            set { _lastloginip = value; }
            get { return _lastloginip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastLoginDepartId
        {
            set { _lastlogindepartid = value; }
            get { return _lastlogindepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddIp
        {
            set { _addip = value; }
            get { return _addip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastDepartId
        {
            set { _lastdepartid = value; }
            get { return _lastdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastIp
        {
            set { _lastip = value; }
            get { return _lastip; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }
        #endregion Model

    }
}

