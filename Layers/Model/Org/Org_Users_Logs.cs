﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Org_Users_Logs:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Org_Users_Logs
    {
        public Org_Users_Logs()
        { }
        #region Model
        private int _logid;
        private int? _userid;
        private int? _postid;
        private int? _departid;
        /// <summary>
        /// 
        /// </summary>
        public int LogId
        {
            set { _logid = value; }
            get { return _logid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UserId
        {
            set { _userid = value; }
            get { return _userid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? PostId
        {
            set { _postid = value; }
            get { return _postid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? DepartId
        {
            set { _departid = value; }
            get { return _departid; }
        }
        #endregion Model

    }
}

