﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Org_Users:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Org_Users
    {
        public Org_Users()
        { }
        #region Model
        private int _userid;
        private string _school;
        private string _thumb;
        private DateTime? _birthday;
        private int? _education;
        private DateTime? _lasttime;
        private int? _lastuserid;
        private string _lastuser;
        private DateTime? _enterdeate;
        private DateTime? _leveldate;
        private int? _state;
        private int? _marry;
        private string _homeaddress;
        private string _mem;
        private string _userName;

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int UserId
        {
            set { _userid = value; }
            get { return _userid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string School
        {
            set { _school = value; }
            get { return _school; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Thumb
        {
            set { _thumb = value; }
            get { return _thumb; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? BirthDay
        {
            set { _birthday = value; }
            get { return _birthday; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Education
        {
            set { _education = value; }
            get { return _education; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastTime
        {
            set { _lasttime = value; }
            get { return _lasttime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? LastUserId
        {
            set { _lastuserid = value; }
            get { return _lastuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LastUser
        {
            set { _lastuser = value; }
            get { return _lastuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? EnterDeate
        {
            set { _enterdeate = value; }
            get { return _enterdeate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LevelDate
        {
            set { _leveldate = value; }
            get { return _leveldate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? Marry
        {
            set { _marry = value; }
            get { return _marry; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string HomeAddress
        {
            set { _homeaddress = value; }
            get { return _homeaddress; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Mem
        {
            set { _mem = value; }
            get { return _mem; }
        }
        #endregion Model

    }
}

