﻿using System;
namespace xundh.Model
{
	/// <summary>
	/// Org_Depart_Post_User:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
	public partial class Org_Depart_Post_User
	{
		public Org_Depart_Post_User()
		{}
		#region Model
		private int _orgdepartpostuserid;
		private int? _departsid;
		private int? _postid;
		private int? _userid;
		/// <summary>
		/// 
		/// </summary>
		public int OrgDepartPostUserId
		{
			set{ _orgdepartpostuserid=value;}
			get{return _orgdepartpostuserid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? DepartsId
		{
			set{ _departsid=value;}
			get{return _departsid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? PostId
		{
			set{ _postid=value;}
			get{return _postid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int? UserId
		{
			set{ _userid=value;}
			get{return _userid;}
		}
		#endregion Model

	}
}

