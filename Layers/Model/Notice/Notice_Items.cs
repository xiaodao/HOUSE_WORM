﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Notice_Items:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Notice_Items
    {
        public Notice_Items()
        { }
        #region Model
        private int _noticeid;
        private string _modulename;
        private string _noticetitle;
        private string _noticebody;
        private string _url;
        private string _runjs;
        private string _arg0;
        private int? _fromdepartid;
        private int? _fromuserid;
        private string _fromuser;
        private int? _adddepartid;
        private DateTime? _addtime;
        private int? _adduserid;
        private string _adduser;
        private int? _state;
        private DateTime? _aftertime;
        /// <summary>
        /// 
        /// </summary>
        public int NoticeId
        {
            set { _noticeid = value; }
            get { return _noticeid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ModuleName
        {
            set { _modulename = value; }
            get { return _modulename; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeTitle
        {
            set { _noticetitle = value; }
            get { return _noticetitle; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NoticeBody
        {
            set { _noticebody = value; }
            get { return _noticebody; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Url
        {
            set { _url = value; }
            get { return _url; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string runJs
        {
            set { _runjs = value; }
            get { return _runjs; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string arg0
        {
            set { _arg0 = value; }
            get { return _arg0; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FromDepartId
        {
            set { _fromdepartid = value; }
            get { return _fromdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? FromUserId
        {
            set { _fromuserid = value; }
            get { return _fromuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FromUser
        {
            set { _fromuser = value; }
            get { return _fromuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? State
        {
            set { _state = value; }
            get { return _state; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AfterTime
        {
            set { _aftertime = value; }
            get { return _aftertime; }
        }
        #endregion Model

    }
}

