﻿/**  版本信息模板在安装目录下，可自行修改。
* FI_Subjects.cs
*
* 功 能： N/A
* 类 名： FI_Subjects
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  2014/3/9 11:46:56   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
	/// <summary>
	/// 数据访问类:FI_Subjects
	/// </summary>
	public partial class FI_Subjects
	{
		public FI_Subjects()
		{}
		#region  Method

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
		return DbHelperSQL.GetMaxID("SubjectId", "FI_Subjects"); 
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int SubjectId)
		{
			int rowsAffected;
			SqlParameter[] parameters = {
					new SqlParameter("@SubjectId", SqlDbType.Int,4)
			};
			parameters[0].Value = SubjectId;

			int result= DbHelperSQL.RunProcedure("FI_Subjects_Exists",parameters,out rowsAffected);
			if(result==1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		///  增加一条数据
		/// </summary>
		public int Add(xundh.Model.FI_Subjects model)
		{
			int rowsAffected;
			SqlParameter[] parameters = {
					new SqlParameter("@SubjectId", SqlDbType.Int,4),
					new SqlParameter("@ParentSubjectId", SqlDbType.Int,4),
					new SqlParameter("@SubjectTitle", SqlDbType.NVarChar,64),
					new SqlParameter("@SubjectCode", SqlDbType.NVarChar,32),
					new SqlParameter("@Types", SqlDbType.Int,4),
					new SqlParameter("@Direction", SqlDbType.Int,4),
					new SqlParameter("@IsEnd", SqlDbType.Bit,1)};
			parameters[0].Direction = ParameterDirection.Output;
			parameters[1].Value = model.ParentSubjectId;
			parameters[2].Value = model.SubjectTitle;
			parameters[3].Value = model.SubjectCode;
			parameters[4].Value = model.Types;
			parameters[5].Value = model.Direction;
			parameters[6].Value = model.IsEnd;

			DbHelperSQL.RunProcedure("FI_Subjects_ADD",parameters,out rowsAffected);
			return (int)parameters[0].Value;
		}

		/// <summary>
		///  更新一条数据
		/// </summary>
		public bool Update(xundh.Model.FI_Subjects model)
		{
			int rowsAffected=0;
			SqlParameter[] parameters = {
					new SqlParameter("@SubjectId", SqlDbType.Int,4),
					new SqlParameter("@ParentSubjectId", SqlDbType.Int,4),
					new SqlParameter("@SubjectTitle", SqlDbType.NVarChar,64),
					new SqlParameter("@SubjectCode", SqlDbType.NVarChar,32),
					new SqlParameter("@Types", SqlDbType.Int,4),
					new SqlParameter("@Direction", SqlDbType.Int,4),
					new SqlParameter("@IsEnd", SqlDbType.Bit,1)};
			parameters[0].Value = model.SubjectId;
			parameters[1].Value = model.ParentSubjectId;
			parameters[2].Value = model.SubjectTitle;
			parameters[3].Value = model.SubjectCode;
			parameters[4].Value = model.Types;
			parameters[5].Value = model.Direction;
			parameters[6].Value = model.IsEnd;

			DbHelperSQL.RunProcedure("FI_Subjects_Update",parameters,out rowsAffected);
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int SubjectId)
		{
			int rowsAffected=0;
			SqlParameter[] parameters = {
					new SqlParameter("@SubjectId", SqlDbType.Int,4)
			};
			parameters[0].Value = SubjectId;

			DbHelperSQL.RunProcedure("FI_Subjects_Delete",parameters,out rowsAffected);
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string SubjectIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from FI_Subjects ");
			strSql.Append(" where SubjectId in ("+SubjectIdlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.FI_Subjects GetModel(int SubjectId)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@SubjectId", SqlDbType.Int,4)
			};
			parameters[0].Value = SubjectId;

			xundh.Model.FI_Subjects model=new xundh.Model.FI_Subjects();
			DataSet ds= DbHelperSQL.RunProcedure("FI_Subjects_GetModel",parameters,"ds");
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.FI_Subjects DataRowToModel(DataRow row)
		{
			xundh.Model.FI_Subjects model=new xundh.Model.FI_Subjects();
			if (row != null)
			{
				if(row["SubjectId"]!=null && row["SubjectId"].ToString()!="")
				{
					model.SubjectId=int.Parse(row["SubjectId"].ToString());
				}
				if(row["ParentSubjectId"]!=null && row["ParentSubjectId"].ToString()!="")
				{
					model.ParentSubjectId=int.Parse(row["ParentSubjectId"].ToString());
				}
				if(row["SubjectTitle"]!=null)
				{
					model.SubjectTitle=row["SubjectTitle"].ToString();
				}
				if(row["SubjectCode"]!=null)
				{
					model.SubjectCode=row["SubjectCode"].ToString();
				}
				if(row["Types"]!=null && row["Types"].ToString()!="")
				{
					model.Types=int.Parse(row["Types"].ToString());
				}
				if(row["Direction"]!=null && row["Direction"].ToString()!="")
				{
					model.Direction=int.Parse(row["Direction"].ToString());
				}
				if(row["IsEnd"]!=null && row["IsEnd"].ToString()!="")
				{
					if((row["IsEnd"].ToString()=="1")||(row["IsEnd"].ToString().ToLower()=="true"))
					{
						model.IsEnd=true;
					}
					else
					{
						model.IsEnd=false;
					}
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select SubjectId,ParentSubjectId,SubjectTitle,SubjectCode,Types,Direction,IsEnd ");
			strSql.Append(" FROM FI_Subjects ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" SubjectId,ParentSubjectId,SubjectTitle,SubjectCode,Types,Direction,IsEnd ");
			strSql.Append(" FROM FI_Subjects ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM FI_Subjects ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.SubjectId desc");
			}
			strSql.Append(")AS Row, T.*  from FI_Subjects T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "FI_Subjects";
			parameters[1].Value = "SubjectId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
		#region  MethodEx

		#endregion  MethodEx
	}
}

