﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:House_Rents
    /// </summary>
    public partial class House_Rents
    {
        public House_Rents()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int RentId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from House_Rents");
            strSql.Append(" where RentId=@RentId");
            SqlParameter[] parameters = {
					new SqlParameter("@RentId", SqlDbType.Int,4)
			};
            parameters[0].Value = RentId;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(xundh.Model.House_Rents model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into House_Rents(");
            strSql.Append("ViewLevel,RentRegionArea,RentFace,RentBuildingId,Dong,Fang,Shi,Ting,Chu,Wei,HouseArea,HouseFloor,HouseTotalFloor,TotalPrice,PayType,Decoration,CustomerName,CustomerTel,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,HasKey,KeyDepartId,FollowCount,ViewCount,State)");
            strSql.Append(" values (");
            strSql.Append("@ViewLevel,@RentRegionArea,@RentFace,@RentBuildingId,@Dong,@Fang,@Shi,@Ting,@Chu,@Wei,@HouseArea,@HouseFloor,@HouseTotalFloor,@TotalPrice,@PayType,@Decoration,@CustomerName,@CustomerTel,@AddUserId,@AddUser,@AddDepartId,@AddTime,@AddIp,@LastUserId,@LastUser,@LastDepartId,@LastTime,@LastIp,@Mem,@HasKey,@KeyDepartId,@FollowCount,@ViewCount,@State)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ViewLevel", SqlDbType.Int,4),
					new SqlParameter("@RentRegionArea", SqlDbType.Int,4),
					new SqlParameter("@RentFace", SqlDbType.Int,4),
					new SqlParameter("@RentBuildingId", SqlDbType.Int,4),
					new SqlParameter("@Dong", SqlDbType.NVarChar,16),
					new SqlParameter("@Fang", SqlDbType.NVarChar,8),
					new SqlParameter("@Shi", SqlDbType.Float,8),
					new SqlParameter("@Ting", SqlDbType.Int,4),
					new SqlParameter("@Chu", SqlDbType.Int,4),
					new SqlParameter("@Wei", SqlDbType.Int,4),
					new SqlParameter("@HouseArea", SqlDbType.Float,8),
					new SqlParameter("@HouseFloor", SqlDbType.Int,4),
					new SqlParameter("@HouseTotalFloor", SqlDbType.Int,4),
					new SqlParameter("@TotalPrice", SqlDbType.Money,8),
					new SqlParameter("@PayType", SqlDbType.NVarChar,16),
					new SqlParameter("@Decoration", SqlDbType.Int,4),
					new SqlParameter("@CustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerTel", SqlDbType.NVarChar,64),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@HasKey", SqlDbType.Bit,1),
					new SqlParameter("@KeyDepartId", SqlDbType.Int,4),
					new SqlParameter("@FollowCount", SqlDbType.Int,4),
					new SqlParameter("@ViewCount", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4)};
            parameters[0].Value = model.ViewLevel;
            parameters[1].Value = model.RentRegionArea;
            parameters[2].Value = model.RentFace;
            parameters[3].Value = model.RentBuildingId;
            parameters[4].Value = model.Dong;
            parameters[5].Value = model.Fang;
            parameters[6].Value = model.Shi;
            parameters[7].Value = model.Ting;
            parameters[8].Value = model.Chu;
            parameters[9].Value = model.Wei;
            parameters[10].Value = model.HouseArea;
            parameters[11].Value = model.HouseFloor;
            parameters[12].Value = model.HouseTotalFloor;
            parameters[13].Value = model.TotalPrice;
            parameters[14].Value = model.PayType;
            parameters[15].Value = model.Decoration;
            parameters[16].Value = model.CustomerName;
            parameters[17].Value = model.CustomerTel;
            parameters[18].Value = model.AddUserId;
            parameters[19].Value = model.AddUser;
            parameters[20].Value = model.AddDepartId;
            parameters[21].Value = model.AddTime;
            parameters[22].Value = model.AddIp;
            parameters[23].Value = model.LastUserId;
            parameters[24].Value = model.LastUser;
            parameters[25].Value = model.LastDepartId;
            parameters[26].Value = model.LastTime;
            parameters[27].Value = model.LastIp;
            parameters[28].Value = model.Mem;
            parameters[29].Value = model.HasKey;
            parameters[30].Value = model.KeyDepartId;
            parameters[31].Value = model.FollowCount;
            parameters[32].Value = model.ViewCount;
            parameters[33].Value = model.State;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(xundh.Model.House_Rents model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update House_Rents set ");
            strSql.Append("ViewLevel=@ViewLevel,");
            strSql.Append("RentRegionArea=@RentRegionArea,");
            strSql.Append("RentFace=@RentFace,");
            strSql.Append("RentBuildingId=@RentBuildingId,");
            strSql.Append("Dong=@Dong,");
            strSql.Append("Fang=@Fang,");
            strSql.Append("Shi=@Shi,");
            strSql.Append("Ting=@Ting,");
            strSql.Append("Chu=@Chu,");
            strSql.Append("Wei=@Wei,");
            strSql.Append("HouseArea=@HouseArea,");
            strSql.Append("HouseFloor=@HouseFloor,");
            strSql.Append("HouseTotalFloor=@HouseTotalFloor,");
            strSql.Append("TotalPrice=@TotalPrice,");
            strSql.Append("PayType=@PayType,");
            strSql.Append("Decoration=@Decoration,");
            strSql.Append("CustomerName=@CustomerName,");
            strSql.Append("CustomerTel=@CustomerTel,");
            strSql.Append("AddUserId=@AddUserId,");
            strSql.Append("AddUser=@AddUser,");
            strSql.Append("AddDepartId=@AddDepartId,");
            strSql.Append("AddTime=@AddTime,");
            strSql.Append("AddIp=@AddIp,");
            strSql.Append("LastUserId=@LastUserId,");
            strSql.Append("LastUser=@LastUser,");
            strSql.Append("LastDepartId=@LastDepartId,");
            strSql.Append("LastTime=@LastTime,");
            strSql.Append("LastIp=@LastIp,");
            strSql.Append("Mem=@Mem,");
            strSql.Append("HasKey=@HasKey,");
            strSql.Append("KeyDepartId=@KeyDepartId,");
            strSql.Append("FollowCount=@FollowCount,");
            strSql.Append("ViewCount=@ViewCount,");
            strSql.Append("State=@State");
            strSql.Append(" where RentId=@RentId");
            SqlParameter[] parameters = {
					new SqlParameter("@ViewLevel", SqlDbType.Int,4),
					new SqlParameter("@RentRegionArea", SqlDbType.Int,4),
					new SqlParameter("@RentFace", SqlDbType.Int,4),
					new SqlParameter("@RentBuildingId", SqlDbType.Int,4),
					new SqlParameter("@Dong", SqlDbType.NVarChar,16),
					new SqlParameter("@Fang", SqlDbType.NVarChar,8),
					new SqlParameter("@Shi", SqlDbType.Float,8),
					new SqlParameter("@Ting", SqlDbType.Int,4),
					new SqlParameter("@Chu", SqlDbType.Int,4),
					new SqlParameter("@Wei", SqlDbType.Int,4),
					new SqlParameter("@HouseArea", SqlDbType.Float,8),
					new SqlParameter("@HouseFloor", SqlDbType.Int,4),
					new SqlParameter("@HouseTotalFloor", SqlDbType.Int,4),
					new SqlParameter("@TotalPrice", SqlDbType.Money,8),
					new SqlParameter("@PayType", SqlDbType.NVarChar,16),
					new SqlParameter("@Decoration", SqlDbType.Int,4),
					new SqlParameter("@CustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerTel", SqlDbType.NVarChar,64),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@HasKey", SqlDbType.Bit,1),
					new SqlParameter("@KeyDepartId", SqlDbType.Int,4),
					new SqlParameter("@FollowCount", SqlDbType.Int,4),
					new SqlParameter("@ViewCount", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@RentId", SqlDbType.Int,4)};
            parameters[0].Value = model.ViewLevel;
            parameters[1].Value = model.RentRegionArea;
            parameters[2].Value = model.RentFace;
            parameters[3].Value = model.RentBuildingId;
            parameters[4].Value = model.Dong;
            parameters[5].Value = model.Fang;
            parameters[6].Value = model.Shi;
            parameters[7].Value = model.Ting;
            parameters[8].Value = model.Chu;
            parameters[9].Value = model.Wei;
            parameters[10].Value = model.HouseArea;
            parameters[11].Value = model.HouseFloor;
            parameters[12].Value = model.HouseTotalFloor;
            parameters[13].Value = model.TotalPrice;
            parameters[14].Value = model.PayType;
            parameters[15].Value = model.Decoration;
            parameters[16].Value = model.CustomerName;
            parameters[17].Value = model.CustomerTel;
            parameters[18].Value = model.AddUserId;
            parameters[19].Value = model.AddUser;
            parameters[20].Value = model.AddDepartId;
            parameters[21].Value = model.AddTime;
            parameters[22].Value = model.AddIp;
            parameters[23].Value = model.LastUserId;
            parameters[24].Value = model.LastUser;
            parameters[25].Value = model.LastDepartId;
            parameters[26].Value = model.LastTime;
            parameters[27].Value = model.LastIp;
            parameters[28].Value = model.Mem;
            parameters[29].Value = model.HasKey;
            parameters[30].Value = model.KeyDepartId;
            parameters[31].Value = model.FollowCount;
            parameters[32].Value = model.ViewCount;
            parameters[33].Value = model.State;
            parameters[34].Value = model.RentId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int RentId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from House_Rents ");
            strSql.Append(" where RentId=@RentId");
            SqlParameter[] parameters = {
					new SqlParameter("@RentId", SqlDbType.Int,4)
			};
            parameters[0].Value = RentId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string RentIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from House_Rents ");
            strSql.Append(" where RentId in (" + RentIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.House_Rents GetModel(int RentId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 RentId,ViewLevel,RentRegionArea,RentFace,RentBuildingId,Dong,Fang,Shi,Ting,Chu,Wei,HouseArea,HouseFloor,HouseTotalFloor,TotalPrice,PayType,Decoration,CustomerName,CustomerTel,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,HasKey,KeyDepartId,FollowCount,ViewCount,State from House_Rents ");
            strSql.Append(" where RentId=@RentId");
            SqlParameter[] parameters = {
					new SqlParameter("@RentId", SqlDbType.Int,4)
			};
            parameters[0].Value = RentId;

            xundh.Model.House_Rents model = new xundh.Model.House_Rents();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.House_Rents DataRowToModel(DataRow row)
        {
            xundh.Model.House_Rents model = new xundh.Model.House_Rents();
            if (row != null)
            {
                if (row["RentId"] != null && row["RentId"].ToString() != "")
                {
                    model.RentId = int.Parse(row["RentId"].ToString());
                }
                if (row["ViewLevel"] != null && row["ViewLevel"].ToString() != "")
                {
                    model.ViewLevel = int.Parse(row["ViewLevel"].ToString());
                }
                if (row["RentRegionArea"] != null && row["RentRegionArea"].ToString() != "")
                {
                    model.RentRegionArea = int.Parse(row["RentRegionArea"].ToString());
                }
                if (row["RentFace"] != null && row["RentFace"].ToString() != "")
                {
                    model.RentFace = int.Parse(row["RentFace"].ToString());
                }
                if (row["RentBuildingId"] != null && row["RentBuildingId"].ToString() != "")
                {
                    model.RentBuildingId = int.Parse(row["RentBuildingId"].ToString());
                }
                if (row["Dong"] != null)
                {
                    model.Dong = row["Dong"].ToString();
                }
                if (row["Fang"] != null)
                {
                    model.Fang = row["Fang"].ToString();
                }
                if (row["Shi"] != null && row["Shi"].ToString() != "")
                {
                    model.Shi = decimal.Parse(row["Shi"].ToString());
                }
                if (row["Ting"] != null && row["Ting"].ToString() != "")
                {
                    model.Ting = int.Parse(row["Ting"].ToString());
                }
                if (row["Chu"] != null && row["Chu"].ToString() != "")
                {
                    model.Chu = int.Parse(row["Chu"].ToString());
                }
                if (row["Wei"] != null && row["Wei"].ToString() != "")
                {
                    model.Wei = int.Parse(row["Wei"].ToString());
                }
                if (row["HouseArea"] != null && row["HouseArea"].ToString() != "")
                {
                    model.HouseArea = decimal.Parse(row["HouseArea"].ToString());
                }
                if (row["HouseFloor"] != null && row["HouseFloor"].ToString() != "")
                {
                    model.HouseFloor = int.Parse(row["HouseFloor"].ToString());
                }
                if (row["HouseTotalFloor"] != null && row["HouseTotalFloor"].ToString() != "")
                {
                    model.HouseTotalFloor = int.Parse(row["HouseTotalFloor"].ToString());
                }
                if (row["TotalPrice"] != null && row["TotalPrice"].ToString() != "")
                {
                    model.TotalPrice = decimal.Parse(row["TotalPrice"].ToString());
                }
                if (row["PayType"] != null)
                {
                    model.PayType = row["PayType"].ToString();
                }
                if (row["Decoration"] != null && row["Decoration"].ToString() != "")
                {
                    model.Decoration = int.Parse(row["Decoration"].ToString());
                }
                if (row["CustomerName"] != null)
                {
                    model.CustomerName = row["CustomerName"].ToString();
                }
                if (row["CustomerTel"] != null)
                {
                    model.CustomerTel = row["CustomerTel"].ToString();
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
                if (row["HasKey"] != null && row["HasKey"].ToString() != "")
                {
                    if ((row["HasKey"].ToString() == "1") || (row["HasKey"].ToString().ToLower() == "true"))
                    {
                        model.HasKey = true;
                    }
                    else
                    {
                        model.HasKey = false;
                    }
                }
                if (row["KeyDepartId"] != null && row["KeyDepartId"].ToString() != "")
                {
                    model.KeyDepartId = int.Parse(row["KeyDepartId"].ToString());
                }
                if (row["FollowCount"] != null && row["FollowCount"].ToString() != "")
                {
                    model.FollowCount = int.Parse(row["FollowCount"].ToString());
                }
                if (row["ViewCount"] != null && row["ViewCount"].ToString() != "")
                {
                    model.ViewCount = int.Parse(row["ViewCount"].ToString());
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select RentId,ViewLevel,RentRegionArea,RentFace,RentBuildingId,Dong,Fang,Shi,Ting,Chu,Wei,HouseArea,HouseFloor,HouseTotalFloor,TotalPrice,PayType,Decoration,CustomerName,CustomerTel,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,HasKey,KeyDepartId,FollowCount,ViewCount,State ");
            strSql.Append(" FROM House_Rents ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" RentId,ViewLevel,RentRegionArea,RentFace,RentBuildingId,Dong,Fang,Shi,Ting,Chu,Wei,HouseArea,HouseFloor,HouseTotalFloor,TotalPrice,PayType,Decoration,CustomerName,CustomerTel,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,HasKey,KeyDepartId,FollowCount,ViewCount,State ");
            strSql.Append(" FROM House_Rents ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM House_Rents ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.RentId desc");
            }
            strSql.Append(")AS Row, T.*  from House_Rents T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "House_Rents";
            parameters[1].Value = "RentId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

