﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:House_Rents_Views
    /// </summary>
    public partial class House_Rents_Views
    {
        public House_Rents_Views()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ViewId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from House_Rents_Views");
            strSql.Append(" where ViewId=@ViewId");
            SqlParameter[] parameters = {
					new SqlParameter("@ViewId", SqlDbType.Int,4)
			};
            parameters[0].Value = ViewId;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(xundh.Model.House_Rents_Views model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into House_Rents_Views(");
            strSql.Append("RentId,AddUserId,AddUser,AddTime,AddIp,AddDepartId)");
            strSql.Append(" values (");
            strSql.Append("@RentId,@AddUserId,@AddUser,@AddTime,@AddIp,@AddDepartId)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@RentId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4)};
            parameters[0].Value = model.RentId;
            parameters[1].Value = model.AddUserId;
            parameters[2].Value = model.AddUser;
            parameters[3].Value = model.AddTime;
            parameters[4].Value = model.AddIp;
            parameters[5].Value = model.AddDepartId;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(xundh.Model.House_Rents_Views model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update House_Rents_Views set ");
            strSql.Append("RentId=@RentId,");
            strSql.Append("AddUserId=@AddUserId,");
            strSql.Append("AddUser=@AddUser,");
            strSql.Append("AddTime=@AddTime,");
            strSql.Append("AddIp=@AddIp,");
            strSql.Append("AddDepartId=@AddDepartId");
            strSql.Append(" where ViewId=@ViewId");
            SqlParameter[] parameters = {
					new SqlParameter("@RentId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@ViewId", SqlDbType.Int,4)};
            parameters[0].Value = model.RentId;
            parameters[1].Value = model.AddUserId;
            parameters[2].Value = model.AddUser;
            parameters[3].Value = model.AddTime;
            parameters[4].Value = model.AddIp;
            parameters[5].Value = model.AddDepartId;
            parameters[6].Value = model.ViewId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ViewId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from House_Rents_Views ");
            strSql.Append(" where ViewId=@ViewId");
            SqlParameter[] parameters = {
					new SqlParameter("@ViewId", SqlDbType.Int,4)
			};
            parameters[0].Value = ViewId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string ViewIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from House_Rents_Views ");
            strSql.Append(" where ViewId in (" + ViewIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.House_Rents_Views GetModel(int ViewId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 ViewId,RentId,AddUserId,AddUser,AddTime,AddIp,AddDepartId from House_Rents_Views ");
            strSql.Append(" where ViewId=@ViewId");
            SqlParameter[] parameters = {
					new SqlParameter("@ViewId", SqlDbType.Int,4)
			};
            parameters[0].Value = ViewId;

            xundh.Model.House_Rents_Views model = new xundh.Model.House_Rents_Views();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.House_Rents_Views DataRowToModel(DataRow row)
        {
            xundh.Model.House_Rents_Views model = new xundh.Model.House_Rents_Views();
            if (row != null)
            {
                if (row["ViewId"] != null && row["ViewId"].ToString() != "")
                {
                    model.ViewId = int.Parse(row["ViewId"].ToString());
                }
                if (row["RentId"] != null && row["RentId"].ToString() != "")
                {
                    model.RentId = int.Parse(row["RentId"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ViewId,RentId,AddUserId,AddUser,AddTime,AddIp,AddDepartId ");
            strSql.Append(" FROM House_Rents_Views ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ViewId,RentId,AddUserId,AddUser,AddTime,AddIp,AddDepartId ");
            strSql.Append(" FROM House_Rents_Views ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM House_Rents_Views ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ViewId desc");
            }
            strSql.Append(")AS Row, T.*  from House_Rents_Views T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "House_Rents_Views";
            parameters[1].Value = "ViewId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

