﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:House_Sells
    /// </summary>
    public partial class House_Sells
    {
        public House_Sells()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int SellId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@SellId", SqlDbType.Int,4)
			};
            parameters[0].Value = SellId;

            int result = DbHelperSQL.RunProcedure("House_Sells_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.House_Sells model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@SellId", SqlDbType.Int,4),
					new SqlParameter("@ViewLevel", SqlDbType.Int,4),
					new SqlParameter("@SellRegionArea", SqlDbType.Int,4),
					new SqlParameter("@SellFace", SqlDbType.Int,4),
					new SqlParameter("@SellBuildingId", SqlDbType.Int,4),
					new SqlParameter("@Dong", SqlDbType.NVarChar,16),
					new SqlParameter("@Fang", SqlDbType.NVarChar,8),
					new SqlParameter("@Shi", SqlDbType.Float,8),
					new SqlParameter("@Ting", SqlDbType.Int,4),
					new SqlParameter("@Chu", SqlDbType.Int,4),
					new SqlParameter("@Wei", SqlDbType.Int,4),
					new SqlParameter("@HouseArea", SqlDbType.Float,8),
					new SqlParameter("@HouseFloor", SqlDbType.Int,4),
					new SqlParameter("@HouseTotalFloor", SqlDbType.Int,4),
					new SqlParameter("@UnitPrice", SqlDbType.Money,8),
					new SqlParameter("@TotalPrice", SqlDbType.Money,8),
					new SqlParameter("@BuildYear", SqlDbType.Int,4),
					new SqlParameter("@GetPropertyDate", SqlDbType.DateTime),
					new SqlParameter("@Decoration", SqlDbType.Int,4),
					new SqlParameter("@CustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerTel", SqlDbType.NVarChar,64),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@HasKey", SqlDbType.Bit,1),
					new SqlParameter("@KeyDepartId", SqlDbType.Int,4),
					new SqlParameter("@FollowCount", SqlDbType.Int,4),
					new SqlParameter("@ViewCount", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@Suggest", SqlDbType.Int,4),
					new SqlParameter("@HasImage", SqlDbType.Bit,1)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.ViewLevel;
            parameters[2].Value = model.SellRegionArea;
            parameters[3].Value = model.SellFace;
            parameters[4].Value = model.SellBuildingId;
            parameters[5].Value = model.Dong;
            parameters[6].Value = model.Fang;
            parameters[7].Value = model.Shi;
            parameters[8].Value = model.Ting;
            parameters[9].Value = model.Chu;
            parameters[10].Value = model.Wei;
            parameters[11].Value = model.HouseArea;
            parameters[12].Value = model.HouseFloor;
            parameters[13].Value = model.HouseTotalFloor;
            parameters[14].Value = model.UnitPrice;
            parameters[15].Value = model.TotalPrice;
            parameters[16].Value = model.BuildYear;
            parameters[17].Value = model.GetPropertyDate;
            parameters[18].Value = model.Decoration;
            parameters[19].Value = model.CustomerName;
            parameters[20].Value = model.CustomerTel;
            parameters[21].Value = model.AddUserId;
            parameters[22].Value = model.AddUser;
            parameters[23].Value = model.AddTime;
            parameters[24].Value = model.AddIp;
            parameters[25].Value = model.AddDepartId;
            parameters[26].Value = model.LastUserId;
            parameters[27].Value = model.LastUser;
            parameters[28].Value = model.LastTime;
            parameters[29].Value = model.LastIp;
            parameters[30].Value = model.LastDepartId;
            parameters[31].Value = model.HasKey;
            parameters[32].Value = model.KeyDepartId;
            parameters[33].Value = model.FollowCount;
            parameters[34].Value = model.ViewCount;
            parameters[35].Value = model.State;
            parameters[36].Value = model.Mem;
            parameters[37].Value = model.Suggest;
            parameters[38].Value = model.HasImage;

            DbHelperSQL.RunProcedure("House_Sells_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.House_Sells model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@SellId", SqlDbType.Int,4),
					new SqlParameter("@ViewLevel", SqlDbType.Int,4),
					new SqlParameter("@SellRegionArea", SqlDbType.Int,4),
					new SqlParameter("@SellFace", SqlDbType.Int,4),
					new SqlParameter("@SellBuildingId", SqlDbType.Int,4),
					new SqlParameter("@Dong", SqlDbType.NVarChar,16),
					new SqlParameter("@Fang", SqlDbType.NVarChar,8),
					new SqlParameter("@Shi", SqlDbType.Float,8),
					new SqlParameter("@Ting", SqlDbType.Int,4),
					new SqlParameter("@Chu", SqlDbType.Int,4),
					new SqlParameter("@Wei", SqlDbType.Int,4),
					new SqlParameter("@HouseArea", SqlDbType.Float,8),
					new SqlParameter("@HouseFloor", SqlDbType.Int,4),
					new SqlParameter("@HouseTotalFloor", SqlDbType.Int,4),
					new SqlParameter("@UnitPrice", SqlDbType.Money,8),
					new SqlParameter("@TotalPrice", SqlDbType.Money,8),
					new SqlParameter("@BuildYear", SqlDbType.Int,4),
					new SqlParameter("@GetPropertyDate", SqlDbType.DateTime),
					new SqlParameter("@Decoration", SqlDbType.Int,4),
					new SqlParameter("@CustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerTel", SqlDbType.NVarChar,64),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@HasKey", SqlDbType.Bit,1),
					new SqlParameter("@KeyDepartId", SqlDbType.Int,4),
					new SqlParameter("@FollowCount", SqlDbType.Int,4),
					new SqlParameter("@ViewCount", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@Suggest", SqlDbType.Int,4),
					new SqlParameter("@HasImage", SqlDbType.Bit,1)};
            parameters[0].Value = model.SellId;
            parameters[1].Value = model.ViewLevel;
            parameters[2].Value = model.SellRegionArea;
            parameters[3].Value = model.SellFace;
            parameters[4].Value = model.SellBuildingId;
            parameters[5].Value = model.Dong;
            parameters[6].Value = model.Fang;
            parameters[7].Value = model.Shi;
            parameters[8].Value = model.Ting;
            parameters[9].Value = model.Chu;
            parameters[10].Value = model.Wei;
            parameters[11].Value = model.HouseArea;
            parameters[12].Value = model.HouseFloor;
            parameters[13].Value = model.HouseTotalFloor;
            parameters[14].Value = model.UnitPrice;
            parameters[15].Value = model.TotalPrice;
            parameters[16].Value = model.BuildYear;
            parameters[17].Value = model.GetPropertyDate;
            parameters[18].Value = model.Decoration;
            parameters[19].Value = model.CustomerName;
            parameters[20].Value = model.CustomerTel;
            parameters[21].Value = model.AddUserId;
            parameters[22].Value = model.AddUser;
            parameters[23].Value = model.AddTime;
            parameters[24].Value = model.AddIp;
            parameters[25].Value = model.AddDepartId;
            parameters[26].Value = model.LastUserId;
            parameters[27].Value = model.LastUser;
            parameters[28].Value = model.LastTime;
            parameters[29].Value = model.LastIp;
            parameters[30].Value = model.LastDepartId;
            parameters[31].Value = model.HasKey;
            parameters[32].Value = model.KeyDepartId;
            parameters[33].Value = model.FollowCount;
            parameters[34].Value = model.ViewCount;
            parameters[35].Value = model.State;
            parameters[36].Value = model.Mem;
            parameters[37].Value = model.Suggest;
            parameters[38].Value = model.HasImage;

            DbHelperSQL.RunProcedure("House_Sells_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int SellId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@SellId", SqlDbType.Int,4)
			};
            parameters[0].Value = SellId;

            DbHelperSQL.RunProcedure("House_Sells_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string SellIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from House_Sells ");
            strSql.Append(" where SellId in (" + SellIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.House_Sells GetModel(int SellId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@SellId", SqlDbType.Int,4)
			};
            parameters[0].Value = SellId;

            xundh.Model.House_Sells model = new xundh.Model.House_Sells();
            DataSet ds = DbHelperSQL.RunProcedure("House_Sells_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.House_Sells DataRowToModel(DataRow row)
        {
            xundh.Model.House_Sells model = new xundh.Model.House_Sells();
            if (row != null)
            {
                if (row["SellId"] != null && row["SellId"].ToString() != "")
                {
                    model.SellId = int.Parse(row["SellId"].ToString());
                }
                if (row["ViewLevel"] != null && row["ViewLevel"].ToString() != "")
                {
                    model.ViewLevel = int.Parse(row["ViewLevel"].ToString());
                }
                if (row["SellRegionArea"] != null && row["SellRegionArea"].ToString() != "")
                {
                    model.SellRegionArea = int.Parse(row["SellRegionArea"].ToString());
                }
                if (row["SellFace"] != null && row["SellFace"].ToString() != "")
                {
                    model.SellFace = int.Parse(row["SellFace"].ToString());
                }
                if (row["SellBuildingId"] != null && row["SellBuildingId"].ToString() != "")
                {
                    model.SellBuildingId = int.Parse(row["SellBuildingId"].ToString());
                }
                if (row["Dong"] != null)
                {
                    model.Dong = row["Dong"].ToString();
                }
                if (row["Fang"] != null)
                {
                    model.Fang = row["Fang"].ToString();
                }
                if (row["Shi"] != null && row["Shi"].ToString() != "")
                {
                    model.Shi = decimal.Parse(row["Shi"].ToString());
                }
                if (row["Ting"] != null && row["Ting"].ToString() != "")
                {
                    model.Ting = int.Parse(row["Ting"].ToString());
                }
                if (row["Chu"] != null && row["Chu"].ToString() != "")
                {
                    model.Chu = int.Parse(row["Chu"].ToString());
                }
                if (row["Wei"] != null && row["Wei"].ToString() != "")
                {
                    model.Wei = int.Parse(row["Wei"].ToString());
                }
                if (row["HouseArea"] != null && row["HouseArea"].ToString() != "")
                {
                    model.HouseArea = decimal.Parse(row["HouseArea"].ToString());
                }
                if (row["HouseFloor"] != null && row["HouseFloor"].ToString() != "")
                {
                    model.HouseFloor = int.Parse(row["HouseFloor"].ToString());
                }
                if (row["HouseTotalFloor"] != null && row["HouseTotalFloor"].ToString() != "")
                {
                    model.HouseTotalFloor = int.Parse(row["HouseTotalFloor"].ToString());
                }
                if (row["UnitPrice"] != null && row["UnitPrice"].ToString() != "")
                {
                    model.UnitPrice = decimal.Parse(row["UnitPrice"].ToString());
                }
                if (row["TotalPrice"] != null && row["TotalPrice"].ToString() != "")
                {
                    model.TotalPrice = decimal.Parse(row["TotalPrice"].ToString());
                }
                if (row["BuildYear"] != null && row["BuildYear"].ToString() != "")
                {
                    model.BuildYear = int.Parse(row["BuildYear"].ToString());
                }
                if (row["GetPropertyDate"] != null && row["GetPropertyDate"].ToString() != "")
                {
                    model.GetPropertyDate = DateTime.Parse(row["GetPropertyDate"].ToString());
                }
                if (row["Decoration"] != null && row["Decoration"].ToString() != "")
                {
                    model.Decoration = int.Parse(row["Decoration"].ToString());
                }
                if (row["CustomerName"] != null)
                {
                    model.CustomerName = row["CustomerName"].ToString();
                }
                if (row["CustomerTel"] != null)
                {
                    model.CustomerTel = row["CustomerTel"].ToString();
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["HasKey"] != null && row["HasKey"].ToString() != "")
                {
                    if ((row["HasKey"].ToString() == "1") || (row["HasKey"].ToString().ToLower() == "true"))
                    {
                        model.HasKey = true;
                    }
                    else
                    {
                        model.HasKey = false;
                    }
                }
                if (row["KeyDepartId"] != null && row["KeyDepartId"].ToString() != "")
                {
                    model.KeyDepartId = int.Parse(row["KeyDepartId"].ToString());
                }
                if (row["FollowCount"] != null && row["FollowCount"].ToString() != "")
                {
                    model.FollowCount = int.Parse(row["FollowCount"].ToString());
                }
                if (row["ViewCount"] != null && row["ViewCount"].ToString() != "")
                {
                    model.ViewCount = int.Parse(row["ViewCount"].ToString());
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
                if (row["Suggest"] != null && row["Suggest"].ToString() != "")
                {
                    model.Suggest = int.Parse(row["Suggest"].ToString());
                }
                if (row["HasImage"] != null && row["HasImage"].ToString() != "")
                {
                    if ((row["HasImage"].ToString() == "1") || (row["HasImage"].ToString().ToLower() == "true"))
                    {
                        model.HasImage = true;
                    }
                    else
                    {
                        model.HasImage = false;
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select SellId,ViewLevel,SellRegionArea,SellFace,SellBuildingId,Dong,Fang,Shi,Ting,Chu,Wei,HouseArea,HouseFloor,HouseTotalFloor,UnitPrice,TotalPrice,BuildYear,GetPropertyDate,Decoration,CustomerName,CustomerTel,AddUserId,AddUser,AddTime,AddIp,AddDepartId,LastUserId,LastUser,LastTime,LastIp,LastDepartId,HasKey,KeyDepartId,FollowCount,ViewCount,State,Mem,Suggest,HasImage ");
            strSql.Append(" FROM House_Sells ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" SellId,ViewLevel,SellRegionArea,SellFace,SellBuildingId,Dong,Fang,Shi,Ting,Chu,Wei,HouseArea,HouseFloor,HouseTotalFloor,UnitPrice,TotalPrice,BuildYear,GetPropertyDate,Decoration,CustomerName,CustomerTel,AddUserId,AddUser,AddTime,AddIp,AddDepartId,LastUserId,LastUser,LastTime,LastIp,LastDepartId,HasKey,KeyDepartId,FollowCount,ViewCount,State,Mem,Suggest,HasImage ");
            strSql.Append(" FROM House_Sells ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM House_Sells ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.SellId desc");
            }
            strSql.Append(")AS Row, T.*  from House_Sells T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "House_Sells";
            parameters[1].Value = "SellId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

