﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Notice_Items
    /// </summary>
    public partial class Notice_Items
    {
        public Notice_Items()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int NoticeId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Notice_Items");
            strSql.Append(" where NoticeId=@NoticeId");
            SqlParameter[] parameters = {
					new SqlParameter("@NoticeId", SqlDbType.Int,4)
			};
            parameters[0].Value = NoticeId;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(xundh.Model.Notice_Items model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Notice_Items(");
            strSql.Append("ModuleName,NoticeTitle,NoticeBody,Url,runJs,arg0,FromDepartId,FromUserId,FromUser,AddDepartId,AddTime,AddUserId,AddUser,State,AfterTime)");
            strSql.Append(" values (");
            strSql.Append("@ModuleName,@NoticeTitle,@NoticeBody,@Url,@runJs,@arg0,@FromDepartId,@FromUserId,@FromUser,@AddDepartId,@AddTime,@AddUserId,@AddUser,@State,@AfterTime)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ModuleName", SqlDbType.NVarChar,64),
					new SqlParameter("@NoticeTitle", SqlDbType.NVarChar,2000),
					new SqlParameter("@NoticeBody", SqlDbType.NText),
					new SqlParameter("@Url", SqlDbType.NVarChar,1024),
					new SqlParameter("@runJs", SqlDbType.NText),
					new SqlParameter("@arg0", SqlDbType.NText),
					new SqlParameter("@FromDepartId", SqlDbType.Int,4),
					new SqlParameter("@FromUserId", SqlDbType.Int,4),
					new SqlParameter("@FromUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@AfterTime", SqlDbType.DateTime)};
            parameters[0].Value = model.ModuleName;
            parameters[1].Value = model.NoticeTitle;
            parameters[2].Value = model.NoticeBody;
            parameters[3].Value = model.Url;
            parameters[4].Value = model.runJs;
            parameters[5].Value = model.arg0;
            parameters[6].Value = model.FromDepartId;
            parameters[7].Value = model.FromUserId;
            parameters[8].Value = model.FromUser;
            parameters[9].Value = model.AddDepartId;
            parameters[10].Value = model.AddTime;
            parameters[11].Value = model.AddUserId;
            parameters[12].Value = model.AddUser;
            parameters[13].Value = model.State;
            parameters[14].Value = model.AfterTime;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Notice_Items model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Notice_Items set ");
            strSql.Append("ModuleName=@ModuleName,");
            strSql.Append("NoticeTitle=@NoticeTitle,");
            strSql.Append("NoticeBody=@NoticeBody,");
            strSql.Append("Url=@Url,");
            strSql.Append("runJs=@runJs,");
            strSql.Append("arg0=@arg0,");
            strSql.Append("FromDepartId=@FromDepartId,");
            strSql.Append("FromUserId=@FromUserId,");
            strSql.Append("FromUser=@FromUser,");
            strSql.Append("AddDepartId=@AddDepartId,");
            strSql.Append("AddTime=@AddTime,");
            strSql.Append("AddUserId=@AddUserId,");
            strSql.Append("AddUser=@AddUser,");
            strSql.Append("State=@State,");
            strSql.Append("AfterTime=@AfterTime");
            strSql.Append(" where NoticeId=@NoticeId");
            SqlParameter[] parameters = {
					new SqlParameter("@ModuleName", SqlDbType.NVarChar,64),
					new SqlParameter("@NoticeTitle", SqlDbType.NVarChar,2000),
					new SqlParameter("@NoticeBody", SqlDbType.NText),
					new SqlParameter("@Url", SqlDbType.NVarChar,1024),
					new SqlParameter("@runJs", SqlDbType.NText),
					new SqlParameter("@arg0", SqlDbType.NText),
					new SqlParameter("@FromDepartId", SqlDbType.Int,4),
					new SqlParameter("@FromUserId", SqlDbType.Int,4),
					new SqlParameter("@FromUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@AfterTime", SqlDbType.DateTime),
					new SqlParameter("@NoticeId", SqlDbType.Int,4)};
            parameters[0].Value = model.ModuleName;
            parameters[1].Value = model.NoticeTitle;
            parameters[2].Value = model.NoticeBody;
            parameters[3].Value = model.Url;
            parameters[4].Value = model.runJs;
            parameters[5].Value = model.arg0;
            parameters[6].Value = model.FromDepartId;
            parameters[7].Value = model.FromUserId;
            parameters[8].Value = model.FromUser;
            parameters[9].Value = model.AddDepartId;
            parameters[10].Value = model.AddTime;
            parameters[11].Value = model.AddUserId;
            parameters[12].Value = model.AddUser;
            parameters[13].Value = model.State;
            parameters[14].Value = model.AfterTime;
            parameters[15].Value = model.NoticeId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int NoticeId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Notice_Items ");
            strSql.Append(" where NoticeId=@NoticeId");
            SqlParameter[] parameters = {
					new SqlParameter("@NoticeId", SqlDbType.Int,4)
			};
            parameters[0].Value = NoticeId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string NoticeIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Notice_Items ");
            strSql.Append(" where NoticeId in (" + NoticeIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Notice_Items GetModel(int NoticeId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 NoticeId,ModuleName,NoticeTitle,NoticeBody,Url,runJs,arg0,FromDepartId,FromUserId,FromUser,AddDepartId,AddTime,AddUserId,AddUser,State,AfterTime from Notice_Items ");
            strSql.Append(" where NoticeId=@NoticeId");
            SqlParameter[] parameters = {
					new SqlParameter("@NoticeId", SqlDbType.Int,4)
			};
            parameters[0].Value = NoticeId;

            xundh.Model.Notice_Items model = new xundh.Model.Notice_Items();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Notice_Items DataRowToModel(DataRow row)
        {
            xundh.Model.Notice_Items model = new xundh.Model.Notice_Items();
            if (row != null)
            {
                if (row["NoticeId"] != null && row["NoticeId"].ToString() != "")
                {
                    model.NoticeId = int.Parse(row["NoticeId"].ToString());
                }
                if (row["ModuleName"] != null)
                {
                    model.ModuleName = row["ModuleName"].ToString();
                }
                if (row["NoticeTitle"] != null)
                {
                    model.NoticeTitle = row["NoticeTitle"].ToString();
                }
                if (row["NoticeBody"] != null)
                {
                    model.NoticeBody = row["NoticeBody"].ToString();
                }
                if (row["Url"] != null)
                {
                    model.Url = row["Url"].ToString();
                }
                if (row["runJs"] != null)
                {
                    model.runJs = row["runJs"].ToString();
                }
                if (row["arg0"] != null)
                {
                    model.arg0 = row["arg0"].ToString();
                }
                if (row["FromDepartId"] != null && row["FromDepartId"].ToString() != "")
                {
                    model.FromDepartId = int.Parse(row["FromDepartId"].ToString());
                }
                if (row["FromUserId"] != null && row["FromUserId"].ToString() != "")
                {
                    model.FromUserId = int.Parse(row["FromUserId"].ToString());
                }
                if (row["FromUser"] != null)
                {
                    model.FromUser = row["FromUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["AfterTime"] != null && row["AfterTime"].ToString() != "")
                {
                    model.AfterTime = DateTime.Parse(row["AfterTime"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select NoticeId,ModuleName,NoticeTitle,NoticeBody,Url,runJs,arg0,FromDepartId,FromUserId,FromUser,AddDepartId,AddTime,AddUserId,AddUser,State,AfterTime ");
            strSql.Append(" FROM Notice_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" NoticeId,ModuleName,NoticeTitle,NoticeBody,Url,runJs,arg0,FromDepartId,FromUserId,FromUser,AddDepartId,AddTime,AddUserId,AddUser,State,AfterTime ");
            strSql.Append(" FROM Notice_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Notice_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.NoticeId desc");
            }
            strSql.Append(")AS Row, T.*  from Notice_Items T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Notice_Items";
            parameters[1].Value = "NoticeId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

