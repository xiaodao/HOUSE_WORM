﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Tools_AgentMobiles
    /// </summary>
    public partial class Tools_AgentMobiles
    {
        public Tools_AgentMobiles()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int AgentId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@AgentId", SqlDbType.Int,4)
			};
            parameters[0].Value = AgentId;

            int result = DbHelperSQL.RunProcedure("Tools_AgentMobiles_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Tools_AgentMobiles model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@AgentId", SqlDbType.Int,4),
					new SqlParameter("@AgentTel", SqlDbType.NVarChar,64),
					new SqlParameter("@AgentName", SqlDbType.NVarChar,64),
					new SqlParameter("@AgentUserName", SqlDbType.NVarChar,32),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.AgentTel;
            parameters[2].Value = model.AgentName;
            parameters[3].Value = model.AgentUserName;
            parameters[4].Value = model.State;
            parameters[5].Value = model.AddUserId;
            parameters[6].Value = model.AddUser;
            parameters[7].Value = model.AddTime;
            parameters[8].Value = model.AddIp;
            parameters[9].Value = model.AddDepartId;

            DbHelperSQL.RunProcedure("Tools_AgentMobiles_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Tools_AgentMobiles model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@AgentId", SqlDbType.Int,4),
					new SqlParameter("@AgentTel", SqlDbType.NVarChar,64),
					new SqlParameter("@AgentName", SqlDbType.NVarChar,64),
					new SqlParameter("@AgentUserName", SqlDbType.NVarChar,32),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4)};
            parameters[0].Value = model.AgentId;
            parameters[1].Value = model.AgentTel;
            parameters[2].Value = model.AgentName;
            parameters[3].Value = model.AgentUserName;
            parameters[4].Value = model.State;
            parameters[5].Value = model.AddUserId;
            parameters[6].Value = model.AddUser;
            parameters[7].Value = model.AddTime;
            parameters[8].Value = model.AddIp;
            parameters[9].Value = model.AddDepartId;

            DbHelperSQL.RunProcedure("Tools_AgentMobiles_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int AgentId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@AgentId", SqlDbType.Int,4)
			};
            parameters[0].Value = AgentId;

            DbHelperSQL.RunProcedure("Tools_AgentMobiles_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string AgentIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Tools_AgentMobiles ");
            strSql.Append(" where AgentId in (" + AgentIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Tools_AgentMobiles GetModel(int AgentId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@AgentId", SqlDbType.Int,4)
			};
            parameters[0].Value = AgentId;

            xundh.Model.Tools_AgentMobiles model = new xundh.Model.Tools_AgentMobiles();
            DataSet ds = DbHelperSQL.RunProcedure("Tools_AgentMobiles_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Tools_AgentMobiles DataRowToModel(DataRow row)
        {
            xundh.Model.Tools_AgentMobiles model = new xundh.Model.Tools_AgentMobiles();
            if (row != null)
            {
                if (row["AgentId"] != null && row["AgentId"].ToString() != "")
                {
                    model.AgentId = int.Parse(row["AgentId"].ToString());
                }
                if (row["AgentTel"] != null)
                {
                    model.AgentTel = row["AgentTel"].ToString();
                }
                if (row["AgentName"] != null)
                {
                    model.AgentName = row["AgentName"].ToString();
                }
                if (row["AgentUserName"] != null)
                {
                    model.AgentUserName = row["AgentUserName"].ToString();
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select AgentId,AgentTel,AgentName,AgentUserName,State,AddUserId,AddUser,AddTime,AddIp,AddDepartId ");
            strSql.Append(" FROM Tools_AgentMobiles ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" AgentId,AgentTel,AgentName,AgentUserName,State,AddUserId,AddUser,AddTime,AddIp,AddDepartId ");
            strSql.Append(" FROM Tools_AgentMobiles ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Tools_AgentMobiles ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.AgentId desc");
            }
            strSql.Append(")AS Row, T.*  from Tools_AgentMobiles T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Tools_AgentMobiles";
            parameters[1].Value = "AgentId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

