﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Tools_User_DayClear
    /// </summary>
    public partial class Tools_User_DayClear
    {
        public Tools_User_DayClear()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int DayId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@DayId", SqlDbType.Int,4)
			};
            parameters[0].Value = DayId;

            int result = DbHelperSQL.RunProcedure("Tools_User_DayClear_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Tools_User_DayClear model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@DayId", SqlDbType.Int,4),
					new SqlParameter("@DayContent", SqlDbType.NText),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@ApprovalUserId", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@ApprovalContent", SqlDbType.NVarChar,4000),
					new SqlParameter("@ApprovalDepartId", SqlDbType.Int,4),
					new SqlParameter("@ApprovalTime", SqlDbType.DateTime)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.DayContent;
            parameters[2].Value = model.AddUserId;
            parameters[3].Value = model.AddTime;
            parameters[4].Value = model.AddDepartId;
            parameters[5].Value = model.ApprovalUserId;
            parameters[6].Value = model.State;
            parameters[7].Value = model.ApprovalContent;
            parameters[8].Value = model.ApprovalDepartId;
            parameters[9].Value = model.ApprovalTime;

            DbHelperSQL.RunProcedure("Tools_User_DayClear_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Tools_User_DayClear model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@DayId", SqlDbType.Int,4),
					new SqlParameter("@DayContent", SqlDbType.NText),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@ApprovalUserId", SqlDbType.Int,4),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@ApprovalContent", SqlDbType.NVarChar,4000),
					new SqlParameter("@ApprovalDepartId", SqlDbType.Int,4),
					new SqlParameter("@ApprovalTime", SqlDbType.DateTime)};
            parameters[0].Value = model.DayId;
            parameters[1].Value = model.DayContent;
            parameters[2].Value = model.AddUserId;
            parameters[3].Value = model.AddTime;
            parameters[4].Value = model.AddDepartId;
            parameters[5].Value = model.ApprovalUserId;
            parameters[6].Value = model.State;
            parameters[7].Value = model.ApprovalContent;
            parameters[8].Value = model.ApprovalDepartId;
            parameters[9].Value = model.ApprovalTime;

            DbHelperSQL.RunProcedure("Tools_User_DayClear_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int DayId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@DayId", SqlDbType.Int,4)
			};
            parameters[0].Value = DayId;

            DbHelperSQL.RunProcedure("Tools_User_DayClear_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string DayIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Tools_User_DayClear ");
            strSql.Append(" where DayId in (" + DayIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Tools_User_DayClear GetModel(int DayId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@DayId", SqlDbType.Int,4)
			};
            parameters[0].Value = DayId;

            xundh.Model.Tools_User_DayClear model = new xundh.Model.Tools_User_DayClear();
            DataSet ds = DbHelperSQL.RunProcedure("Tools_User_DayClear_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Tools_User_DayClear DataRowToModel(DataRow row)
        {
            xundh.Model.Tools_User_DayClear model = new xundh.Model.Tools_User_DayClear();
            if (row != null)
            {
                if (row["DayId"] != null && row["DayId"].ToString() != "")
                {
                    model.DayId = int.Parse(row["DayId"].ToString());
                }
                if (row["DayContent"] != null)
                {
                    model.DayContent = row["DayContent"].ToString();
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["ApprovalUserId"] != null && row["ApprovalUserId"].ToString() != "")
                {
                    model.ApprovalUserId = int.Parse(row["ApprovalUserId"].ToString());
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["ApprovalContent"] != null)
                {
                    model.ApprovalContent = row["ApprovalContent"].ToString();
                }
                if (row["ApprovalDepartId"] != null && row["ApprovalDepartId"].ToString() != "")
                {
                    model.ApprovalDepartId = int.Parse(row["ApprovalDepartId"].ToString());
                }
                if (row["ApprovalTime"] != null && row["ApprovalTime"].ToString() != "")
                {
                    model.ApprovalTime = DateTime.Parse(row["ApprovalTime"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select DayId,DayContent,AddUserId,AddTime,AddDepartId,ApprovalUserId,State,ApprovalContent,ApprovalDepartId,ApprovalTime ");
            strSql.Append(" FROM Tools_User_DayClear ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" DayId,DayContent,AddUserId,AddTime,AddDepartId,ApprovalUserId,State,ApprovalContent,ApprovalDepartId,ApprovalTime ");
            strSql.Append(" FROM Tools_User_DayClear ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Tools_User_DayClear ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.DayId desc");
            }
            strSql.Append(")AS Row, T.*  from Tools_User_DayClear T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Tools_User_DayClear";
            parameters[1].Value = "DayId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

