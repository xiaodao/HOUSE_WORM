﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Tools_VerifyPrice
    /// </summary>
    public partial class Tools_VerifyPrice
    {
        public Tools_VerifyPrice()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int VPId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@VPId", SqlDbType.Int,4)
			};
            parameters[0].Value = VPId;

            int result = DbHelperSQL.RunProcedure("Tools_VerifyPrice_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Tools_VerifyPrice model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@VPId", SqlDbType.Int,4),
					new SqlParameter("@RegionArea", SqlDbType.Int,4),
					new SqlParameter("@BuildingName", SqlDbType.NVarChar,256),
					new SqlParameter("@Addr", SqlDbType.NVarChar,256),
					new SqlParameter("@Price", SqlDbType.Money,8),
					new SqlParameter("@SellFace", SqlDbType.NVarChar,32),
					new SqlParameter("@CityId", SqlDbType.Int,4)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.RegionArea;
            parameters[2].Value = model.BuildingName;
            parameters[3].Value = model.Addr;
            parameters[4].Value = model.Price;
            parameters[5].Value = model.SellFace;
            parameters[6].Value = model.CityId;

            DbHelperSQL.RunProcedure("Tools_VerifyPrice_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Tools_VerifyPrice model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@VPId", SqlDbType.Int,4),
					new SqlParameter("@RegionArea", SqlDbType.Int,4),
					new SqlParameter("@BuildingName", SqlDbType.NVarChar,256),
					new SqlParameter("@Addr", SqlDbType.NVarChar,256),
					new SqlParameter("@Price", SqlDbType.Money,8),
					new SqlParameter("@SellFace", SqlDbType.NVarChar,32),
					new SqlParameter("@CityId", SqlDbType.Int,4)};
            parameters[0].Value = model.VPId;
            parameters[1].Value = model.RegionArea;
            parameters[2].Value = model.BuildingName;
            parameters[3].Value = model.Addr;
            parameters[4].Value = model.Price;
            parameters[5].Value = model.SellFace;
            parameters[6].Value = model.CityId;

            DbHelperSQL.RunProcedure("Tools_VerifyPrice_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int VPId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@VPId", SqlDbType.Int,4)
			};
            parameters[0].Value = VPId;

            DbHelperSQL.RunProcedure("Tools_VerifyPrice_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string VPIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Tools_VerifyPrice ");
            strSql.Append(" where VPId in (" + VPIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Tools_VerifyPrice GetModel(int VPId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@VPId", SqlDbType.Int,4)
			};
            parameters[0].Value = VPId;

            xundh.Model.Tools_VerifyPrice model = new xundh.Model.Tools_VerifyPrice();
            DataSet ds = DbHelperSQL.RunProcedure("Tools_VerifyPrice_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Tools_VerifyPrice DataRowToModel(DataRow row)
        {
            xundh.Model.Tools_VerifyPrice model = new xundh.Model.Tools_VerifyPrice();
            if (row != null)
            {
                if (row["VPId"] != null && row["VPId"].ToString() != "")
                {
                    model.VPId = int.Parse(row["VPId"].ToString());
                }
                if (row["RegionArea"] != null && row["RegionArea"].ToString() != "")
                {
                    model.RegionArea = int.Parse(row["RegionArea"].ToString());
                }
                if (row["BuildingName"] != null)
                {
                    model.BuildingName = row["BuildingName"].ToString();
                }
                if (row["Addr"] != null)
                {
                    model.Addr = row["Addr"].ToString();
                }
                if (row["Price"] != null && row["Price"].ToString() != "")
                {
                    model.Price = decimal.Parse(row["Price"].ToString());
                }
                if (row["SellFace"] != null)
                {
                    model.SellFace = row["SellFace"].ToString();
                }
                if (row["CityId"] != null && row["CityId"].ToString() != "")
                {
                    model.CityId = int.Parse(row["CityId"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select VPId,RegionArea,BuildingName,Addr,Price,SellFace,CityId ");
            strSql.Append(" FROM Tools_VerifyPrice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" VPId,RegionArea,BuildingName,Addr,Price,SellFace,CityId ");
            strSql.Append(" FROM Tools_VerifyPrice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Tools_VerifyPrice ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.VPId desc");
            }
            strSql.Append(")AS Row, T.*  from Tools_VerifyPrice T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Tools_VerifyPrice";
            parameters[1].Value = "VPId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

