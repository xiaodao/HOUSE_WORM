﻿using System;
using System.Data.SqlClient;
using System.Data;
using Maticsoft.DBUtility;
namespace xundh.DAL
{
    public class BasePager
    {
        #region  成员方法
        public DataSet GetPager(int PageSize, int PageIndex, string strWhere, string orderby, string tablename, string cols)
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                SqlParameter[] pa = new SqlParameter[7];
                pa[0] = new SqlParameter("@PageSize", PageSize);
                pa[1] = new SqlParameter("@PageIndex", PageIndex);
                pa[2] = new SqlParameter("@orderby", orderby);
                pa[3] = new SqlParameter("@strWhere", strWhere);
                pa[4] = new SqlParameter("@tablename", tablename);
                pa[5] = new SqlParameter("@cols", cols);
                pa[6] = new SqlParameter("@groupby", "");
                sda.SelectCommand = DbHelperSQL.GetCommand("T_GetPager", pa);
                using (DataSet ds = new DataSet())
                {
                    sda.Fill(ds);
                    return ds;
                }
            }
        }
        public DataSet GetPager(int PageSize, int PageIndex, string strWhere, string orderby, string tablename, string cols, string groupby)
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                SqlParameter[] pa = new SqlParameter[7];
                pa[0] = new SqlParameter("@PageSize", PageSize);
                pa[1] = new SqlParameter("@PageIndex", PageIndex);
                pa[2] = new SqlParameter("@orderby", orderby);
                pa[3] = new SqlParameter("@strWhere", strWhere);
                pa[4] = new SqlParameter("@tablename", tablename);
                pa[5] = new SqlParameter("@cols", cols);
                pa[6] = new SqlParameter("@groupby", groupby);

                sda.SelectCommand = DbHelperSQL.GetCommand("T_GetPager", pa);
                using (DataSet ds = new DataSet())
                {
                    sda.Fill(ds);
                    return ds;
                }
            }
        }

        public DataSet GetPagerDistinct(int PageSize, int PageIndex, string strWhere, string orderby, string tablename, string cols)
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                SqlParameter[] pa = new SqlParameter[6];
                pa[0] = new SqlParameter("@PageSize", PageSize);
                pa[1] = new SqlParameter("@PageIndex", PageIndex);
                pa[2] = new SqlParameter("@orderby", orderby);
                pa[3] = new SqlParameter("@strWhere", strWhere);
                pa[4] = new SqlParameter("@tablename", tablename);
                pa[5] = new SqlParameter("@cols", cols);
                sda.SelectCommand = DbHelperSQL.GetCommand("[T_GetPagerDistinct]", pa);
                using (DataSet ds = new DataSet())
                {
                    sda.Fill(ds);
                    return ds;
                }
            }
        }
        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetGPager(string sql)
        {
            return DbHelperSQL.Query(sql);
        }

        /// <summary>
        /// 获取所有符合条件的数量
        /// </summary>
        /// <param name="strWhere"></param>
        /// <returns></returns>
        public int GetAllCount(string strWhere, string tablename)
        {
            string sql = "T_GetCount";
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@strWhere",strWhere),
                new SqlParameter("@tablename",tablename)
            };
            return Convert.ToInt32(DbHelperSQL.ExecuteScalar(CommandType.StoredProcedure, sql, param));
        }
        public void Delete(string strWhere, string tablename)
        {
            string sql = "T_Delete";
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter("@strWhere",strWhere),
                new SqlParameter("@tablename",tablename)
            };
            DbHelperSQL.ExecuteScalar(CommandType.StoredProcedure, sql, param);
        }
    }
        #endregion  成员方法
}

