﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Doc_Items
    /// </summary>
    public partial class Doc_Items
    {
        public Doc_Items()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int DocId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@DocId", SqlDbType.Int,4)
			};
            parameters[0].Value = DocId;

            int result = DbHelperSQL.RunProcedure("Doc_Items_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Doc_Items model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@DocId", SqlDbType.Int,4),
					new SqlParameter("@Obj", SqlDbType.NVarChar,64),
					new SqlParameter("@TypeId", SqlDbType.Int,4),
					new SqlParameter("@Chaobao", SqlDbType.NVarChar,64),
					new SqlParameter("@Chaosong", SqlDbType.NVarChar,64),
					new SqlParameter("@Luokuan", SqlDbType.NVarChar,64),
					new SqlParameter("@Yinfa", SqlDbType.NVarChar,64),
					new SqlParameter("@Title", SqlDbType.NVarChar,256),
					new SqlParameter("@Body", SqlDbType.NText),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.Obj;
            parameters[2].Value = model.TypeId;
            parameters[3].Value = model.Chaobao;
            parameters[4].Value = model.Chaosong;
            parameters[5].Value = model.Luokuan;
            parameters[6].Value = model.Yinfa;
            parameters[7].Value = model.Title;
            parameters[8].Value = model.Body;
            parameters[9].Value = model.AddDepartId;
            parameters[10].Value = model.AddTime;
            parameters[11].Value = model.AddUserId;
            parameters[12].Value = model.AddUser;
            parameters[13].Value = model.AddIp;
            parameters[14].Value = model.LastDepartId;
            parameters[15].Value = model.LastTime;
            parameters[16].Value = model.LastUserId;
            parameters[17].Value = model.LastUser;
            parameters[18].Value = model.LastIp;

            DbHelperSQL.RunProcedure("Doc_Items_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Doc_Items model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@DocId", SqlDbType.Int,4),
					new SqlParameter("@Obj", SqlDbType.NVarChar,64),
					new SqlParameter("@TypeId", SqlDbType.Int,4),
					new SqlParameter("@Chaobao", SqlDbType.NVarChar,64),
					new SqlParameter("@Chaosong", SqlDbType.NVarChar,64),
					new SqlParameter("@Luokuan", SqlDbType.NVarChar,64),
					new SqlParameter("@Yinfa", SqlDbType.NVarChar,64),
					new SqlParameter("@Title", SqlDbType.NVarChar,256),
					new SqlParameter("@Body", SqlDbType.NText),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64)};
            parameters[0].Value = model.DocId;
            parameters[1].Value = model.Obj;
            parameters[2].Value = model.TypeId;
            parameters[3].Value = model.Chaobao;
            parameters[4].Value = model.Chaosong;
            parameters[5].Value = model.Luokuan;
            parameters[6].Value = model.Yinfa;
            parameters[7].Value = model.Title;
            parameters[8].Value = model.Body;
            parameters[9].Value = model.AddDepartId;
            parameters[10].Value = model.AddTime;
            parameters[11].Value = model.AddUserId;
            parameters[12].Value = model.AddUser;
            parameters[13].Value = model.AddIp;
            parameters[14].Value = model.LastDepartId;
            parameters[15].Value = model.LastTime;
            parameters[16].Value = model.LastUserId;
            parameters[17].Value = model.LastUser;
            parameters[18].Value = model.LastIp;

            DbHelperSQL.RunProcedure("Doc_Items_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int DocId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@DocId", SqlDbType.Int,4)
			};
            parameters[0].Value = DocId;

            DbHelperSQL.RunProcedure("Doc_Items_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string DocIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Doc_Items ");
            strSql.Append(" where DocId in (" + DocIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Doc_Items GetModel(int DocId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@DocId", SqlDbType.Int,4)
			};
            parameters[0].Value = DocId;

            xundh.Model.Doc_Items model = new xundh.Model.Doc_Items();
            DataSet ds = DbHelperSQL.RunProcedure("Doc_Items_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Doc_Items DataRowToModel(DataRow row)
        {
            xundh.Model.Doc_Items model = new xundh.Model.Doc_Items();
            if (row != null)
            {
                if (row["DocId"] != null && row["DocId"].ToString() != "")
                {
                    model.DocId = int.Parse(row["DocId"].ToString());
                }
                if (row["Obj"] != null)
                {
                    model.Obj = row["Obj"].ToString();
                }
                if (row["TypeId"] != null && row["TypeId"].ToString() != "")
                {
                    model.TypeId = int.Parse(row["TypeId"].ToString());
                }
                if (row["Chaobao"] != null)
                {
                    model.Chaobao = row["Chaobao"].ToString();
                }
                if (row["Chaosong"] != null)
                {
                    model.Chaosong = row["Chaosong"].ToString();
                }
                if (row["Luokuan"] != null)
                {
                    model.Luokuan = row["Luokuan"].ToString();
                }
                if (row["Yinfa"] != null)
                {
                    model.Yinfa = row["Yinfa"].ToString();
                }
                if (row["Title"] != null)
                {
                    model.Title = row["Title"].ToString();
                }
                if (row["Body"] != null)
                {
                    model.Body = row["Body"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select DocId,Obj,TypeId,Chaobao,Chaosong,Luokuan,Yinfa,Title,Body,AddDepartId,AddTime,AddUserId,AddUser,AddIp,LastDepartId,LastTime,LastUserId,LastUser,LastIp ");
            strSql.Append(" FROM Doc_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" DocId,Obj,TypeId,Chaobao,Chaosong,Luokuan,Yinfa,Title,Body,AddDepartId,AddTime,AddUserId,AddUser,AddIp,LastDepartId,LastTime,LastUserId,LastUser,LastIp ");
            strSql.Append(" FROM Doc_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Doc_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.DocId desc");
            }
            strSql.Append(")AS Row, T.*  from Doc_Items T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Doc_Items";
            parameters[1].Value = "DocId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

