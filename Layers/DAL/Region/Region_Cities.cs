﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Region_Cities
    /// </summary>
    public partial class Region_Cities
    {
        public Region_Cities()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int CityId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@CityId", SqlDbType.Int,4)
			};
            parameters[0].Value = CityId;

            int result = DbHelperSQL.RunProcedure("Region_Cities_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Region_Cities model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@CityId", SqlDbType.Int,4),
					new SqlParameter("@CityName", SqlDbType.NVarChar,128),
					new SqlParameter("@ProvinceId", SqlDbType.Int,4),
					new SqlParameter("@CityLatitude", SqlDbType.Float,8),
					new SqlParameter("@CityLongitude", SqlDbType.Float,8)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.CityName;
            parameters[2].Value = model.ProvinceId;
            parameters[3].Value = model.CityLatitude;
            parameters[4].Value = model.CityLongitude;

            DbHelperSQL.RunProcedure("Region_Cities_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Region_Cities model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@CityId", SqlDbType.Int,4),
					new SqlParameter("@CityName", SqlDbType.NVarChar,128),
					new SqlParameter("@ProvinceId", SqlDbType.Int,4),
					new SqlParameter("@CityLatitude", SqlDbType.Float,8),
					new SqlParameter("@CityLongitude", SqlDbType.Float,8)};
            parameters[0].Value = model.CityId;
            parameters[1].Value = model.CityName;
            parameters[2].Value = model.ProvinceId;
            parameters[3].Value = model.CityLatitude;
            parameters[4].Value = model.CityLongitude;

            DbHelperSQL.RunProcedure("Region_Cities_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CityId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@CityId", SqlDbType.Int,4)
			};
            parameters[0].Value = CityId;

            DbHelperSQL.RunProcedure("Region_Cities_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CityIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Region_Cities ");
            strSql.Append(" where CityId in (" + CityIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Region_Cities GetModel(int CityId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@CityId", SqlDbType.Int,4)
			};
            parameters[0].Value = CityId;

            xundh.Model.Region_Cities model = new xundh.Model.Region_Cities();
            DataSet ds = DbHelperSQL.RunProcedure("Region_Cities_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Region_Cities DataRowToModel(DataRow row)
        {
            xundh.Model.Region_Cities model = new xundh.Model.Region_Cities();
            if (row != null)
            {
                if (row["CityId"] != null && row["CityId"].ToString() != "")
                {
                    model.CityId = int.Parse(row["CityId"].ToString());
                }
                if (row["CityName"] != null)
                {
                    model.CityName = row["CityName"].ToString();
                }
                if (row["ProvinceId"] != null && row["ProvinceId"].ToString() != "")
                {
                    model.ProvinceId = int.Parse(row["ProvinceId"].ToString());
                }
                if (row["CityLatitude"] != null && row["CityLatitude"].ToString() != "")
                {
                    model.CityLatitude = decimal.Parse(row["CityLatitude"].ToString());
                }
                if (row["CityLongitude"] != null && row["CityLongitude"].ToString() != "")
                {
                    model.CityLongitude = decimal.Parse(row["CityLongitude"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CityId,CityName,ProvinceId,CityLatitude,CityLongitude ");
            strSql.Append(" FROM Region_Cities ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CityId,CityName,ProvinceId,CityLatitude,CityLongitude ");
            strSql.Append(" FROM Region_Cities ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Region_Cities ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CityId desc");
            }
            strSql.Append(")AS Row, T.*  from Region_Cities T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Region_Cities";
            parameters[1].Value = "CityId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

