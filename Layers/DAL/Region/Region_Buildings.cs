﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Region_Buildings
    /// </summary>
    public partial class Region_Buildings
    {
        public Region_Buildings()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int BuildingId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@BuildingId", SqlDbType.Int,4)
			};
            parameters[0].Value = BuildingId;

            int result = DbHelperSQL.RunProcedure("Region_Buildings_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Region_Buildings model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@BuildingId", SqlDbType.Int,4),
					new SqlParameter("@BuildingArea", SqlDbType.Int,4),
					new SqlParameter("@BuildingAddr", SqlDbType.NVarChar,256),
					new SqlParameter("@BuildingAlice", SqlDbType.NVarChar,256),
					new SqlParameter("@BuildingName", SqlDbType.NVarChar,256),
					new SqlParameter("@BuildingLongitude", SqlDbType.Float,8),
					new SqlParameter("@BuildingLatitude", SqlDbType.Float,8),
					new SqlParameter("@BuildingNameEn", SqlDbType.NVarChar,32),
					new SqlParameter("@BuildingAliceEn", SqlDbType.NVarChar,32),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@SellCount", SqlDbType.Int,4),
					new SqlParameter("@RentCount", SqlDbType.Int,4),
					new SqlParameter("@IsDelete", SqlDbType.Bit,1)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.BuildingArea;
            parameters[2].Value = model.BuildingAddr;
            parameters[3].Value = model.BuildingAlice;
            parameters[4].Value = model.BuildingName;
            parameters[5].Value = model.BuildingLongitude;
            parameters[6].Value = model.BuildingLatitude;
            parameters[7].Value = model.BuildingNameEn;
            parameters[8].Value = model.BuildingAliceEn;
            parameters[9].Value = model.AddUserId;
            parameters[10].Value = model.AddUser;
            parameters[11].Value = model.AddDepartId;
            parameters[12].Value = model.AddTime;
            parameters[13].Value = model.AddIp;
            parameters[14].Value = model.LastUserId;
            parameters[15].Value = model.LastUser;
            parameters[16].Value = model.LastDepartId;
            parameters[17].Value = model.LastTime;
            parameters[18].Value = model.LastIp;
            parameters[19].Value = model.Mem;
            parameters[20].Value = model.SellCount;
            parameters[21].Value = model.RentCount;
            parameters[22].Value = model.IsDelete;

            DbHelperSQL.RunProcedure("Region_Buildings_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Region_Buildings model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@BuildingId", SqlDbType.Int,4),
					new SqlParameter("@BuildingArea", SqlDbType.Int,4),
					new SqlParameter("@BuildingAddr", SqlDbType.NVarChar,256),
					new SqlParameter("@BuildingAlice", SqlDbType.NVarChar,256),
					new SqlParameter("@BuildingName", SqlDbType.NVarChar,256),
					new SqlParameter("@BuildingLongitude", SqlDbType.Float,8),
					new SqlParameter("@BuildingLatitude", SqlDbType.Float,8),
					new SqlParameter("@BuildingNameEn", SqlDbType.NVarChar,32),
					new SqlParameter("@BuildingAliceEn", SqlDbType.NVarChar,32),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@SellCount", SqlDbType.Int,4),
					new SqlParameter("@RentCount", SqlDbType.Int,4),
					new SqlParameter("@IsDelete", SqlDbType.Bit,1)};
            parameters[0].Value = model.BuildingId;
            parameters[1].Value = model.BuildingArea;
            parameters[2].Value = model.BuildingAddr;
            parameters[3].Value = model.BuildingAlice;
            parameters[4].Value = model.BuildingName;
            parameters[5].Value = model.BuildingLongitude;
            parameters[6].Value = model.BuildingLatitude;
            parameters[7].Value = model.BuildingNameEn;
            parameters[8].Value = model.BuildingAliceEn;
            parameters[9].Value = model.AddUserId;
            parameters[10].Value = model.AddUser;
            parameters[11].Value = model.AddDepartId;
            parameters[12].Value = model.AddTime;
            parameters[13].Value = model.AddIp;
            parameters[14].Value = model.LastUserId;
            parameters[15].Value = model.LastUser;
            parameters[16].Value = model.LastDepartId;
            parameters[17].Value = model.LastTime;
            parameters[18].Value = model.LastIp;
            parameters[19].Value = model.Mem;
            parameters[20].Value = model.SellCount;
            parameters[21].Value = model.RentCount;
            parameters[22].Value = model.IsDelete;

            DbHelperSQL.RunProcedure("Region_Buildings_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int BuildingId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@BuildingId", SqlDbType.Int,4)
			};
            parameters[0].Value = BuildingId;

            DbHelperSQL.RunProcedure("Region_Buildings_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string BuildingIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Region_Buildings ");
            strSql.Append(" where BuildingId in (" + BuildingIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Region_Buildings GetModel(int BuildingId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@BuildingId", SqlDbType.Int,4)
			};
            parameters[0].Value = BuildingId;

            xundh.Model.Region_Buildings model = new xundh.Model.Region_Buildings();
            DataSet ds = DbHelperSQL.RunProcedure("Region_Buildings_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Region_Buildings DataRowToModel(DataRow row)
        {
            xundh.Model.Region_Buildings model = new xundh.Model.Region_Buildings();
            if (row != null)
            {
                if (row["BuildingId"] != null && row["BuildingId"].ToString() != "")
                {
                    model.BuildingId = int.Parse(row["BuildingId"].ToString());
                }
                if (row["BuildingArea"] != null && row["BuildingArea"].ToString() != "")
                {
                    model.BuildingArea = int.Parse(row["BuildingArea"].ToString());
                }
                if (row["BuildingAddr"] != null)
                {
                    model.BuildingAddr = row["BuildingAddr"].ToString();
                }
                if (row["BuildingAlice"] != null)
                {
                    model.BuildingAlice = row["BuildingAlice"].ToString();
                }
                if (row["BuildingName"] != null)
                {
                    model.BuildingName = row["BuildingName"].ToString();
                }
                if (row["BuildingLongitude"] != null && row["BuildingLongitude"].ToString() != "")
                {
                    model.BuildingLongitude = decimal.Parse(row["BuildingLongitude"].ToString());
                }
                if (row["BuildingLatitude"] != null && row["BuildingLatitude"].ToString() != "")
                {
                    model.BuildingLatitude = decimal.Parse(row["BuildingLatitude"].ToString());
                }
                if (row["BuildingNameEn"] != null)
                {
                    model.BuildingNameEn = row["BuildingNameEn"].ToString();
                }
                if (row["BuildingAliceEn"] != null)
                {
                    model.BuildingAliceEn = row["BuildingAliceEn"].ToString();
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
                if (row["SellCount"] != null && row["SellCount"].ToString() != "")
                {
                    model.SellCount = int.Parse(row["SellCount"].ToString());
                }
                if (row["RentCount"] != null && row["RentCount"].ToString() != "")
                {
                    model.RentCount = int.Parse(row["RentCount"].ToString());
                }
                if (row["IsDelete"] != null && row["IsDelete"].ToString() != "")
                {
                    if ((row["IsDelete"].ToString() == "1") || (row["IsDelete"].ToString().ToLower() == "true"))
                    {
                        model.IsDelete = true;
                    }
                    else
                    {
                        model.IsDelete = false;
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select BuildingId,BuildingArea,BuildingAddr,BuildingAlice,BuildingName,BuildingLongitude,BuildingLatitude,BuildingNameEn,BuildingAliceEn,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,SellCount,RentCount,IsDelete ");
            strSql.Append(" FROM Region_Buildings ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" BuildingId,BuildingArea,BuildingAddr,BuildingAlice,BuildingName,BuildingLongitude,BuildingLatitude,BuildingNameEn,BuildingAliceEn,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,SellCount,RentCount,IsDelete ");
            strSql.Append(" FROM Region_Buildings ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Region_Buildings ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.BuildingId desc");
            }
            strSql.Append(")AS Row, T.*  from Region_Buildings T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Region_Buildings";
            parameters[1].Value = "BuildingId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

