﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Region_Area
    /// </summary>
    public partial class Region_Area
    {
        public Region_Area()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int AreaId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@AreaId", SqlDbType.Int,4)
			};
            parameters[0].Value = AreaId;

            int result = DbHelperSQL.RunProcedure("Region_Area_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Region_Area model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@AreaId", SqlDbType.Int,4),
					new SqlParameter("@AreaName", SqlDbType.NVarChar,128),
					new SqlParameter("@CityId", SqlDbType.Int,4),
					new SqlParameter("@RegionLatitude", SqlDbType.Float,8),
					new SqlParameter("@RegionLongitude", SqlDbType.Float,8)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.AreaName;
            parameters[2].Value = model.CityId;
            parameters[3].Value = model.RegionLatitude;
            parameters[4].Value = model.RegionLongitude;

            DbHelperSQL.RunProcedure("Region_Area_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Region_Area model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@AreaId", SqlDbType.Int,4),
					new SqlParameter("@AreaName", SqlDbType.NVarChar,128),
					new SqlParameter("@CityId", SqlDbType.Int,4),
					new SqlParameter("@RegionLatitude", SqlDbType.Float,8),
					new SqlParameter("@RegionLongitude", SqlDbType.Float,8)};
            parameters[0].Value = model.AreaId;
            parameters[1].Value = model.AreaName;
            parameters[2].Value = model.CityId;
            parameters[3].Value = model.RegionLatitude;
            parameters[4].Value = model.RegionLongitude;

            DbHelperSQL.RunProcedure("Region_Area_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int AreaId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@AreaId", SqlDbType.Int,4)
			};
            parameters[0].Value = AreaId;

            DbHelperSQL.RunProcedure("Region_Area_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string AreaIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Region_Area ");
            strSql.Append(" where AreaId in (" + AreaIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Region_Area GetModel(int AreaId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@AreaId", SqlDbType.Int,4)
			};
            parameters[0].Value = AreaId;

            xundh.Model.Region_Area model = new xundh.Model.Region_Area();
            DataSet ds = DbHelperSQL.RunProcedure("Region_Area_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Region_Area DataRowToModel(DataRow row)
        {
            xundh.Model.Region_Area model = new xundh.Model.Region_Area();
            if (row != null)
            {
                if (row["AreaId"] != null && row["AreaId"].ToString() != "")
                {
                    model.AreaId = int.Parse(row["AreaId"].ToString());
                }
                if (row["AreaName"] != null)
                {
                    model.AreaName = row["AreaName"].ToString();
                }
                if (row["CityId"] != null && row["CityId"].ToString() != "")
                {
                    model.CityId = int.Parse(row["CityId"].ToString());
                }
                if (row["RegionLatitude"] != null && row["RegionLatitude"].ToString() != "")
                {
                    model.RegionLatitude = decimal.Parse(row["RegionLatitude"].ToString());
                }
                if (row["RegionLongitude"] != null && row["RegionLongitude"].ToString() != "")
                {
                    model.RegionLongitude = decimal.Parse(row["RegionLongitude"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select AreaId,AreaName,CityId,RegionLatitude,RegionLongitude ");
            strSql.Append(" FROM Region_Area ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" AreaId,AreaName,CityId,RegionLatitude,RegionLongitude ");
            strSql.Append(" FROM Region_Area ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Region_Area ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.AreaId desc");
            }
            strSql.Append(")AS Row, T.*  from Region_Area T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Region_Area";
            parameters[1].Value = "AreaId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

