﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:CRM_Buyers
    /// </summary>
    public partial class CRM_Buyers
    {
        public CRM_Buyers()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int CustomerId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@CustomerId", SqlDbType.Int,4)
			};
            parameters[0].Value = CustomerId;

            int result = DbHelperSQL.RunProcedure("CRM_Buyers_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.CRM_Buyers model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@CustomerId", SqlDbType.Int,4),
					new SqlParameter("@CRMType", SqlDbType.NVarChar,64),
					new SqlParameter("@InformationComeFrom", SqlDbType.Int,4),
					new SqlParameter("@CustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@ViewLevel", SqlDbType.Int,4),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@Tel", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@WantRegionArea", SqlDbType.NVarChar,100),
					new SqlParameter("@WantHouseAreaBegin", SqlDbType.Float,8),
					new SqlParameter("@WantHouseAreaEnd", SqlDbType.Float,8),
					new SqlParameter("@WantHuxing", SqlDbType.NVarChar,64),
					new SqlParameter("@WantFloor", SqlDbType.NVarChar,100),
					new SqlParameter("@WantUnitPriceBegin", SqlDbType.Money,8),
					new SqlParameter("@WantUnitPriceEnd", SqlDbType.Money,8),
					new SqlParameter("@WantTotalPriceBegin", SqlDbType.Money,8),
					new SqlParameter("@WantTotalPriceEnd", SqlDbType.Money,8),
					new SqlParameter("@WantDecoration", SqlDbType.NVarChar,100),
					new SqlParameter("@WantBuildingIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@WantBuildings", SqlDbType.NVarChar,4000),
					new SqlParameter("@WantShi", SqlDbType.Int,4),
					new SqlParameter("@WantTing", SqlDbType.Int,4),
					new SqlParameter("@IsFirstBuy", SqlDbType.Bit,1),
					new SqlParameter("@Residence", SqlDbType.NVarChar,64),
					new SqlParameter("@InformationSource", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ManageUserId", SqlDbType.Int,4),
					new SqlParameter("@ManageUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ManageDepartId", SqlDbType.Int,4),
					new SqlParameter("@DistributeUserId", SqlDbType.Int,4),
					new SqlParameter("@DistributeUser", SqlDbType.NVarChar,64),
					new SqlParameter("@DistributeTime", SqlDbType.DateTime),
					new SqlParameter("@DistributeDepartId", SqlDbType.Int,4)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.CRMType;
            parameters[2].Value = model.InformationComeFrom;
            parameters[3].Value = model.CustomerName;
            parameters[4].Value = model.ViewLevel;
            parameters[5].Value = model.Sex;
            parameters[6].Value = model.Tel;
            parameters[7].Value = model.Mem;
            parameters[8].Value = model.State;
            parameters[9].Value = model.WantRegionArea;
            parameters[10].Value = model.WantHouseAreaBegin;
            parameters[11].Value = model.WantHouseAreaEnd;
            parameters[12].Value = model.WantHuxing;
            parameters[13].Value = model.WantFloor;
            parameters[14].Value = model.WantUnitPriceBegin;
            parameters[15].Value = model.WantUnitPriceEnd;
            parameters[16].Value = model.WantTotalPriceBegin;
            parameters[17].Value = model.WantTotalPriceEnd;
            parameters[18].Value = model.WantDecoration;
            parameters[19].Value = model.WantBuildingIds;
            parameters[20].Value = model.WantBuildings;
            parameters[21].Value = model.WantShi;
            parameters[22].Value = model.WantTing;
            parameters[23].Value = model.IsFirstBuy;
            parameters[24].Value = model.Residence;
            parameters[25].Value = model.InformationSource;
            parameters[26].Value = model.AddTime;
            parameters[27].Value = model.AddIp;
            parameters[28].Value = model.AddDepartId;
            parameters[29].Value = model.AddUserId;
            parameters[30].Value = model.AddUser;
            parameters[31].Value = model.LastTime;
            parameters[32].Value = model.LastIp;
            parameters[33].Value = model.LastDepartId;
            parameters[34].Value = model.LastUserId;
            parameters[35].Value = model.LastUser;
            parameters[36].Value = model.ManageUserId;
            parameters[37].Value = model.ManageUser;
            parameters[38].Value = model.ManageDepartId;
            parameters[39].Value = model.DistributeUserId;
            parameters[40].Value = model.DistributeUser;
            parameters[41].Value = model.DistributeTime;
            parameters[42].Value = model.DistributeDepartId;

            DbHelperSQL.RunProcedure("CRM_Buyers_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.CRM_Buyers model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@CustomerId", SqlDbType.Int,4),
					new SqlParameter("@CRMType", SqlDbType.NVarChar,64),
					new SqlParameter("@InformationComeFrom", SqlDbType.Int,4),
					new SqlParameter("@CustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@ViewLevel", SqlDbType.Int,4),
					new SqlParameter("@Sex", SqlDbType.Int,4),
					new SqlParameter("@Tel", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@WantRegionArea", SqlDbType.NVarChar,100),
					new SqlParameter("@WantHouseAreaBegin", SqlDbType.Float,8),
					new SqlParameter("@WantHouseAreaEnd", SqlDbType.Float,8),
					new SqlParameter("@WantHuxing", SqlDbType.NVarChar,64),
					new SqlParameter("@WantFloor", SqlDbType.NVarChar,100),
					new SqlParameter("@WantUnitPriceBegin", SqlDbType.Money,8),
					new SqlParameter("@WantUnitPriceEnd", SqlDbType.Money,8),
					new SqlParameter("@WantTotalPriceBegin", SqlDbType.Money,8),
					new SqlParameter("@WantTotalPriceEnd", SqlDbType.Money,8),
					new SqlParameter("@WantDecoration", SqlDbType.NVarChar,100),
					new SqlParameter("@WantBuildingIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@WantBuildings", SqlDbType.NVarChar,4000),
					new SqlParameter("@WantShi", SqlDbType.Int,4),
					new SqlParameter("@WantTing", SqlDbType.Int,4),
					new SqlParameter("@IsFirstBuy", SqlDbType.Bit,1),
					new SqlParameter("@Residence", SqlDbType.NVarChar,64),
					new SqlParameter("@InformationSource", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ManageUserId", SqlDbType.Int,4),
					new SqlParameter("@ManageUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ManageDepartId", SqlDbType.Int,4),
					new SqlParameter("@DistributeUserId", SqlDbType.Int,4),
					new SqlParameter("@DistributeUser", SqlDbType.NVarChar,64),
					new SqlParameter("@DistributeTime", SqlDbType.DateTime),
					new SqlParameter("@DistributeDepartId", SqlDbType.Int,4)};
            parameters[0].Value = model.CustomerId;
            parameters[1].Value = model.CRMType;
            parameters[2].Value = model.InformationComeFrom;
            parameters[3].Value = model.CustomerName;
            parameters[4].Value = model.ViewLevel;
            parameters[5].Value = model.Sex;
            parameters[6].Value = model.Tel;
            parameters[7].Value = model.Mem;
            parameters[8].Value = model.State;
            parameters[9].Value = model.WantRegionArea;
            parameters[10].Value = model.WantHouseAreaBegin;
            parameters[11].Value = model.WantHouseAreaEnd;
            parameters[12].Value = model.WantHuxing;
            parameters[13].Value = model.WantFloor;
            parameters[14].Value = model.WantUnitPriceBegin;
            parameters[15].Value = model.WantUnitPriceEnd;
            parameters[16].Value = model.WantTotalPriceBegin;
            parameters[17].Value = model.WantTotalPriceEnd;
            parameters[18].Value = model.WantDecoration;
            parameters[19].Value = model.WantBuildingIds;
            parameters[20].Value = model.WantBuildings;
            parameters[21].Value = model.WantShi;
            parameters[22].Value = model.WantTing;
            parameters[23].Value = model.IsFirstBuy;
            parameters[24].Value = model.Residence;
            parameters[25].Value = model.InformationSource;
            parameters[26].Value = model.AddTime;
            parameters[27].Value = model.AddIp;
            parameters[28].Value = model.AddDepartId;
            parameters[29].Value = model.AddUserId;
            parameters[30].Value = model.AddUser;
            parameters[31].Value = model.LastTime;
            parameters[32].Value = model.LastIp;
            parameters[33].Value = model.LastDepartId;
            parameters[34].Value = model.LastUserId;
            parameters[35].Value = model.LastUser;
            parameters[36].Value = model.ManageUserId;
            parameters[37].Value = model.ManageUser;
            parameters[38].Value = model.ManageDepartId;
            parameters[39].Value = model.DistributeUserId;
            parameters[40].Value = model.DistributeUser;
            parameters[41].Value = model.DistributeTime;
            parameters[42].Value = model.DistributeDepartId;

            DbHelperSQL.RunProcedure("CRM_Buyers_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CustomerId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@CustomerId", SqlDbType.Int,4)
			};
            parameters[0].Value = CustomerId;

            DbHelperSQL.RunProcedure("CRM_Buyers_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CustomerIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CRM_Buyers ");
            strSql.Append(" where CustomerId in (" + CustomerIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.CRM_Buyers GetModel(int CustomerId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@CustomerId", SqlDbType.Int,4)
			};
            parameters[0].Value = CustomerId;

            xundh.Model.CRM_Buyers model = new xundh.Model.CRM_Buyers();
            DataSet ds = DbHelperSQL.RunProcedure("CRM_Buyers_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.CRM_Buyers DataRowToModel(DataRow row)
        {
            xundh.Model.CRM_Buyers model = new xundh.Model.CRM_Buyers();
            if (row != null)
            {
                if (row["CustomerId"] != null && row["CustomerId"].ToString() != "")
                {
                    model.CustomerId = int.Parse(row["CustomerId"].ToString());
                }
                if (row["CRMType"] != null)
                {
                    model.CRMType = row["CRMType"].ToString();
                }
                if (row["InformationComeFrom"] != null && row["InformationComeFrom"].ToString() != "")
                {
                    model.InformationComeFrom = int.Parse(row["InformationComeFrom"].ToString());
                }
                if (row["CustomerName"] != null)
                {
                    model.CustomerName = row["CustomerName"].ToString();
                }
                if (row["ViewLevel"] != null && row["ViewLevel"].ToString() != "")
                {
                    model.ViewLevel = int.Parse(row["ViewLevel"].ToString());
                }
                if (row["Sex"] != null && row["Sex"].ToString() != "")
                {
                    model.Sex = int.Parse(row["Sex"].ToString());
                }
                if (row["Tel"] != null)
                {
                    model.Tel = row["Tel"].ToString();
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["WantRegionArea"] != null)
                {
                    model.WantRegionArea = row["WantRegionArea"].ToString();
                }
                if (row["WantHouseAreaBegin"] != null && row["WantHouseAreaBegin"].ToString() != "")
                {
                    model.WantHouseAreaBegin = decimal.Parse(row["WantHouseAreaBegin"].ToString());
                }
                if (row["WantHouseAreaEnd"] != null && row["WantHouseAreaEnd"].ToString() != "")
                {
                    model.WantHouseAreaEnd = decimal.Parse(row["WantHouseAreaEnd"].ToString());
                }
                if (row["WantHuxing"] != null)
                {
                    model.WantHuxing = row["WantHuxing"].ToString();
                }
                if (row["WantFloor"] != null)
                {
                    model.WantFloor = row["WantFloor"].ToString();
                }
                if (row["WantUnitPriceBegin"] != null && row["WantUnitPriceBegin"].ToString() != "")
                {
                    model.WantUnitPriceBegin = decimal.Parse(row["WantUnitPriceBegin"].ToString());
                }
                if (row["WantUnitPriceEnd"] != null && row["WantUnitPriceEnd"].ToString() != "")
                {
                    model.WantUnitPriceEnd = decimal.Parse(row["WantUnitPriceEnd"].ToString());
                }
                if (row["WantTotalPriceBegin"] != null && row["WantTotalPriceBegin"].ToString() != "")
                {
                    model.WantTotalPriceBegin = decimal.Parse(row["WantTotalPriceBegin"].ToString());
                }
                if (row["WantTotalPriceEnd"] != null && row["WantTotalPriceEnd"].ToString() != "")
                {
                    model.WantTotalPriceEnd = decimal.Parse(row["WantTotalPriceEnd"].ToString());
                }
                if (row["WantDecoration"] != null)
                {
                    model.WantDecoration = row["WantDecoration"].ToString();
                }
                if (row["WantBuildingIds"] != null)
                {
                    model.WantBuildingIds = row["WantBuildingIds"].ToString();
                }
                if (row["WantBuildings"] != null)
                {
                    model.WantBuildings = row["WantBuildings"].ToString();
                }
                if (row["WantShi"] != null && row["WantShi"].ToString() != "")
                {
                    model.WantShi = int.Parse(row["WantShi"].ToString());
                }
                if (row["WantTing"] != null && row["WantTing"].ToString() != "")
                {
                    model.WantTing = int.Parse(row["WantTing"].ToString());
                }
                if (row["IsFirstBuy"] != null && row["IsFirstBuy"].ToString() != "")
                {
                    if ((row["IsFirstBuy"].ToString() == "1") || (row["IsFirstBuy"].ToString().ToLower() == "true"))
                    {
                        model.IsFirstBuy = true;
                    }
                    else
                    {
                        model.IsFirstBuy = false;
                    }
                }
                if (row["Residence"] != null)
                {
                    model.Residence = row["Residence"].ToString();
                }
                if (row["InformationSource"] != null)
                {
                    model.InformationSource = row["InformationSource"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["ManageUserId"] != null && row["ManageUserId"].ToString() != "")
                {
                    model.ManageUserId = int.Parse(row["ManageUserId"].ToString());
                }
                if (row["ManageUser"] != null)
                {
                    model.ManageUser = row["ManageUser"].ToString();
                }
                if (row["ManageDepartId"] != null && row["ManageDepartId"].ToString() != "")
                {
                    model.ManageDepartId = int.Parse(row["ManageDepartId"].ToString());
                }
                if (row["DistributeUserId"] != null && row["DistributeUserId"].ToString() != "")
                {
                    model.DistributeUserId = int.Parse(row["DistributeUserId"].ToString());
                }
                if (row["DistributeUser"] != null)
                {
                    model.DistributeUser = row["DistributeUser"].ToString();
                }
                if (row["DistributeTime"] != null && row["DistributeTime"].ToString() != "")
                {
                    model.DistributeTime = DateTime.Parse(row["DistributeTime"].ToString());
                }
                if (row["DistributeDepartId"] != null && row["DistributeDepartId"].ToString() != "")
                {
                    model.DistributeDepartId = int.Parse(row["DistributeDepartId"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CustomerId,CRMType,InformationComeFrom,CustomerName,ViewLevel,Sex,Tel,Mem,State,WantRegionArea,WantHouseAreaBegin,WantHouseAreaEnd,WantHuxing,WantFloor,WantUnitPriceBegin,WantUnitPriceEnd,WantTotalPriceBegin,WantTotalPriceEnd,WantDecoration,WantBuildingIds,WantBuildings,WantShi,WantTing,IsFirstBuy,Residence,InformationSource,AddTime,AddIp,AddDepartId,AddUserId,AddUser,LastTime,LastIp,LastDepartId,LastUserId,LastUser,ManageUserId,ManageUser,ManageDepartId,DistributeUserId,DistributeUser,DistributeTime,DistributeDepartId ");
            strSql.Append(" FROM CRM_Buyers ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CustomerId,CRMType,InformationComeFrom,CustomerName,ViewLevel,Sex,Tel,Mem,State,WantRegionArea,WantHouseAreaBegin,WantHouseAreaEnd,WantHuxing,WantFloor,WantUnitPriceBegin,WantUnitPriceEnd,WantTotalPriceBegin,WantTotalPriceEnd,WantDecoration,WantBuildingIds,WantBuildings,WantShi,WantTing,IsFirstBuy,Residence,InformationSource,AddTime,AddIp,AddDepartId,AddUserId,AddUser,LastTime,LastIp,LastDepartId,LastUserId,LastUser,ManageUserId,ManageUser,ManageDepartId,DistributeUserId,DistributeUser,DistributeTime,DistributeDepartId ");
            strSql.Append(" FROM CRM_Buyers ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM CRM_Buyers ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CustomerId desc");
            }
            strSql.Append(")AS Row, T.*  from CRM_Buyers T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "CRM_Buyers";
            parameters[1].Value = "CustomerId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

