﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:HR_Excitation
    /// </summary>
    public partial class HR_Excitation
    {
        public HR_Excitation()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ExcitationId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@ExcitationId", SqlDbType.Int,4)
			};
            parameters[0].Value = ExcitationId;

            int result = DbHelperSQL.RunProcedure("HR_Excitation_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.HR_Excitation model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@ExcitationId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@Amount", SqlDbType.Money,8),
					new SqlParameter("@ObjectUserId", SqlDbType.Int,4),
					new SqlParameter("@ObjectUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ObjectDepartId", SqlDbType.Int,4),
					new SqlParameter("@ObjectTime", SqlDbType.DateTime),
					new SqlParameter("@ObjectAddress", SqlDbType.NVarChar,4000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@VerifyUserId", SqlDbType.Int,4),
					new SqlParameter("@VerifyUser", SqlDbType.NVarChar,64),
					new SqlParameter("@VerifyDepartId", SqlDbType.Int,4),
					new SqlParameter("@Matter", SqlDbType.NVarChar,4000),
					new SqlParameter("@Type", SqlDbType.Int,4)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.AddUserId;
            parameters[2].Value = model.AddUser;
            parameters[3].Value = model.AddDepartId;
            parameters[4].Value = model.Amount;
            parameters[5].Value = model.ObjectUserId;
            parameters[6].Value = model.ObjectUser;
            parameters[7].Value = model.ObjectDepartId;
            parameters[8].Value = model.ObjectTime;
            parameters[9].Value = model.ObjectAddress;
            parameters[10].Value = model.State;
            parameters[11].Value = model.VerifyUserId;
            parameters[12].Value = model.VerifyUser;
            parameters[13].Value = model.VerifyDepartId;
            parameters[14].Value = model.Matter;
            parameters[15].Value = model.Type;

            DbHelperSQL.RunProcedure("HR_Excitation_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.HR_Excitation model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@ExcitationId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@Amount", SqlDbType.Money,8),
					new SqlParameter("@ObjectUserId", SqlDbType.Int,4),
					new SqlParameter("@ObjectUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ObjectDepartId", SqlDbType.Int,4),
					new SqlParameter("@ObjectTime", SqlDbType.DateTime),
					new SqlParameter("@ObjectAddress", SqlDbType.NVarChar,4000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@VerifyUserId", SqlDbType.Int,4),
					new SqlParameter("@VerifyUser", SqlDbType.NVarChar,64),
					new SqlParameter("@VerifyDepartId", SqlDbType.Int,4),
					new SqlParameter("@Matter", SqlDbType.NVarChar,4000),
					new SqlParameter("@Type", SqlDbType.Int,4)};
            parameters[0].Value = model.ExcitationId;
            parameters[1].Value = model.AddUserId;
            parameters[2].Value = model.AddUser;
            parameters[3].Value = model.AddDepartId;
            parameters[4].Value = model.Amount;
            parameters[5].Value = model.ObjectUserId;
            parameters[6].Value = model.ObjectUser;
            parameters[7].Value = model.ObjectDepartId;
            parameters[8].Value = model.ObjectTime;
            parameters[9].Value = model.ObjectAddress;
            parameters[10].Value = model.State;
            parameters[11].Value = model.VerifyUserId;
            parameters[12].Value = model.VerifyUser;
            parameters[13].Value = model.VerifyDepartId;
            parameters[14].Value = model.Matter;
            parameters[15].Value = model.Type;

            DbHelperSQL.RunProcedure("HR_Excitation_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ExcitationId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@ExcitationId", SqlDbType.Int,4)
			};
            parameters[0].Value = ExcitationId;

            DbHelperSQL.RunProcedure("HR_Excitation_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string ExcitationIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from HR_Excitation ");
            strSql.Append(" where ExcitationId in (" + ExcitationIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.HR_Excitation GetModel(int ExcitationId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@ExcitationId", SqlDbType.Int,4)
			};
            parameters[0].Value = ExcitationId;

            xundh.Model.HR_Excitation model = new xundh.Model.HR_Excitation();
            DataSet ds = DbHelperSQL.RunProcedure("HR_Excitation_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.HR_Excitation DataRowToModel(DataRow row)
        {
            xundh.Model.HR_Excitation model = new xundh.Model.HR_Excitation();
            if (row != null)
            {
                if (row["ExcitationId"] != null && row["ExcitationId"].ToString() != "")
                {
                    model.ExcitationId = int.Parse(row["ExcitationId"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["Amount"] != null && row["Amount"].ToString() != "")
                {
                    model.Amount = decimal.Parse(row["Amount"].ToString());
                }
                if (row["ObjectUserId"] != null && row["ObjectUserId"].ToString() != "")
                {
                    model.ObjectUserId = int.Parse(row["ObjectUserId"].ToString());
                }
                if (row["ObjectUser"] != null)
                {
                    model.ObjectUser = row["ObjectUser"].ToString();
                }
                if (row["ObjectDepartId"] != null && row["ObjectDepartId"].ToString() != "")
                {
                    model.ObjectDepartId = int.Parse(row["ObjectDepartId"].ToString());
                }
                if (row["ObjectTime"] != null && row["ObjectTime"].ToString() != "")
                {
                    model.ObjectTime = DateTime.Parse(row["ObjectTime"].ToString());
                }
                if (row["ObjectAddress"] != null)
                {
                    model.ObjectAddress = row["ObjectAddress"].ToString();
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["VerifyUserId"] != null && row["VerifyUserId"].ToString() != "")
                {
                    model.VerifyUserId = int.Parse(row["VerifyUserId"].ToString());
                }
                if (row["VerifyUser"] != null)
                {
                    model.VerifyUser = row["VerifyUser"].ToString();
                }
                if (row["VerifyDepartId"] != null && row["VerifyDepartId"].ToString() != "")
                {
                    model.VerifyDepartId = int.Parse(row["VerifyDepartId"].ToString());
                }
                if (row["Matter"] != null)
                {
                    model.Matter = row["Matter"].ToString();
                }
                if (row["Type"] != null && row["Type"].ToString() != "")
                {
                    model.Type = int.Parse(row["Type"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ExcitationId,AddUserId,AddUser,AddDepartId,Amount,ObjectUserId,ObjectUser,ObjectDepartId,ObjectTime,ObjectAddress,State,VerifyUserId,VerifyUser,VerifyDepartId,Matter,Type ");
            strSql.Append(" FROM HR_Excitation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ExcitationId,AddUserId,AddUser,AddDepartId,Amount,ObjectUserId,ObjectUser,ObjectDepartId,ObjectTime,ObjectAddress,State,VerifyUserId,VerifyUser,VerifyDepartId,Matter,Type ");
            strSql.Append(" FROM HR_Excitation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM HR_Excitation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ExcitationId desc");
            }
            strSql.Append(")AS Row, T.*  from HR_Excitation T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "HR_Excitation";
            parameters[1].Value = "ExcitationId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

