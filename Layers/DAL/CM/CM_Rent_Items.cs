﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:CM_Rent_Items
    /// </summary>
    public partial class CM_Rent_Items
    {
        public CM_Rent_Items()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int CMId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@CMId", SqlDbType.Int,4)
			};
            parameters[0].Value = CMId;

            int result = DbHelperSQL.RunProcedure("CM_Rent_Items_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.CM_Rent_Items model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@CMId", SqlDbType.Int,4),
					new SqlParameter("@CMNumber", SqlDbType.NVarChar,32),
					new SqlParameter("@HouseId", SqlDbType.Int,4),
					new SqlParameter("@TotalPrice", SqlDbType.Money,8),
					new SqlParameter("@HouseRegionArea", SqlDbType.Int,4),
					new SqlParameter("@HouseArea", SqlDbType.Float,8),
					new SqlParameter("@HouseTel", SqlDbType.NVarChar,64),
					new SqlParameter("@HouseCustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@HousePersonID", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerId", SqlDbType.Int,4),
					new SqlParameter("@CustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerTel", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerPersonID", SqlDbType.NVarChar,64),
					new SqlParameter("@SignUserId", SqlDbType.Int,4),
					new SqlParameter("@SignUser", SqlDbType.NVarChar,64),
					new SqlParameter("@SignDepartId", SqlDbType.Int,4),
					new SqlParameter("@PayType", SqlDbType.Int,4),
					new SqlParameter("@ChargeTotal", SqlDbType.Money,8),
					new SqlParameter("@ChargePayType", SqlDbType.Int,4),
					new SqlParameter("@ChargeHouse", SqlDbType.Money,8),
					new SqlParameter("@ChargeCustomer", SqlDbType.Money,8),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@Mem", SqlDbType.NVarChar,4000),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.CMNumber;
            parameters[2].Value = model.HouseId;
            parameters[3].Value = model.TotalPrice;
            parameters[4].Value = model.HouseRegionArea;
            parameters[5].Value = model.HouseArea;
            parameters[6].Value = model.HouseTel;
            parameters[7].Value = model.HouseCustomerName;
            parameters[8].Value = model.HousePersonID;
            parameters[9].Value = model.CustomerId;
            parameters[10].Value = model.CustomerName;
            parameters[11].Value = model.CustomerTel;
            parameters[12].Value = model.CustomerPersonID;
            parameters[13].Value = model.SignUserId;
            parameters[14].Value = model.SignUser;
            parameters[15].Value = model.SignDepartId;
            parameters[16].Value = model.PayType;
            parameters[17].Value = model.ChargeTotal;
            parameters[18].Value = model.ChargePayType;
            parameters[19].Value = model.ChargeHouse;
            parameters[20].Value = model.ChargeCustomer;
            parameters[21].Value = model.State;
            parameters[22].Value = model.Mem;
            parameters[23].Value = model.AddUserId;
            parameters[24].Value = model.AddUser;
            parameters[25].Value = model.AddDepartId;
            parameters[26].Value = model.AddTime;
            parameters[27].Value = model.AddIp;
            parameters[28].Value = model.LastUserId;
            parameters[29].Value = model.LastUser;
            parameters[30].Value = model.LastDepartId;
            parameters[31].Value = model.LastTime;
            parameters[32].Value = model.LastIp;

            DbHelperSQL.RunProcedure("CM_Rent_Items_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.CM_Rent_Items model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@CMId", SqlDbType.Int,4),
					new SqlParameter("@CMNumber", SqlDbType.NVarChar,32),
					new SqlParameter("@HouseId", SqlDbType.Int,4),
					new SqlParameter("@TotalPrice", SqlDbType.Money,8),
					new SqlParameter("@HouseRegionArea", SqlDbType.Int,4),
					new SqlParameter("@HouseArea", SqlDbType.Float,8),
					new SqlParameter("@HouseTel", SqlDbType.NVarChar,64),
					new SqlParameter("@HouseCustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@HousePersonID", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerId", SqlDbType.Int,4),
					new SqlParameter("@CustomerName", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerTel", SqlDbType.NVarChar,64),
					new SqlParameter("@CustomerPersonID", SqlDbType.NVarChar,64),
					new SqlParameter("@SignUserId", SqlDbType.Int,4),
					new SqlParameter("@SignUser", SqlDbType.NVarChar,64),
					new SqlParameter("@SignDepartId", SqlDbType.Int,4),
					new SqlParameter("@PayType", SqlDbType.Int,4),
					new SqlParameter("@ChargeTotal", SqlDbType.Money,8),
					new SqlParameter("@ChargePayType", SqlDbType.Int,4),
					new SqlParameter("@ChargeHouse", SqlDbType.Money,8),
					new SqlParameter("@ChargeCustomer", SqlDbType.Money,8),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@Mem", SqlDbType.NVarChar,4000),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64)};
            parameters[0].Value = model.CMId;
            parameters[1].Value = model.CMNumber;
            parameters[2].Value = model.HouseId;
            parameters[3].Value = model.TotalPrice;
            parameters[4].Value = model.HouseRegionArea;
            parameters[5].Value = model.HouseArea;
            parameters[6].Value = model.HouseTel;
            parameters[7].Value = model.HouseCustomerName;
            parameters[8].Value = model.HousePersonID;
            parameters[9].Value = model.CustomerId;
            parameters[10].Value = model.CustomerName;
            parameters[11].Value = model.CustomerTel;
            parameters[12].Value = model.CustomerPersonID;
            parameters[13].Value = model.SignUserId;
            parameters[14].Value = model.SignUser;
            parameters[15].Value = model.SignDepartId;
            parameters[16].Value = model.PayType;
            parameters[17].Value = model.ChargeTotal;
            parameters[18].Value = model.ChargePayType;
            parameters[19].Value = model.ChargeHouse;
            parameters[20].Value = model.ChargeCustomer;
            parameters[21].Value = model.State;
            parameters[22].Value = model.Mem;
            parameters[23].Value = model.AddUserId;
            parameters[24].Value = model.AddUser;
            parameters[25].Value = model.AddDepartId;
            parameters[26].Value = model.AddTime;
            parameters[27].Value = model.AddIp;
            parameters[28].Value = model.LastUserId;
            parameters[29].Value = model.LastUser;
            parameters[30].Value = model.LastDepartId;
            parameters[31].Value = model.LastTime;
            parameters[32].Value = model.LastIp;

            DbHelperSQL.RunProcedure("CM_Rent_Items_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CMId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@CMId", SqlDbType.Int,4)
			};
            parameters[0].Value = CMId;

            DbHelperSQL.RunProcedure("CM_Rent_Items_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CMIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CM_Rent_Items ");
            strSql.Append(" where CMId in (" + CMIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.CM_Rent_Items GetModel(int CMId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@CMId", SqlDbType.Int,4)
			};
            parameters[0].Value = CMId;

            xundh.Model.CM_Rent_Items model = new xundh.Model.CM_Rent_Items();
            DataSet ds = DbHelperSQL.RunProcedure("CM_Rent_Items_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.CM_Rent_Items DataRowToModel(DataRow row)
        {
            xundh.Model.CM_Rent_Items model = new xundh.Model.CM_Rent_Items();
            if (row != null)
            {
                if (row["CMId"] != null && row["CMId"].ToString() != "")
                {
                    model.CMId = int.Parse(row["CMId"].ToString());
                }
                if (row["CMNumber"] != null)
                {
                    model.CMNumber = row["CMNumber"].ToString();
                }
                if (row["HouseId"] != null && row["HouseId"].ToString() != "")
                {
                    model.HouseId = int.Parse(row["HouseId"].ToString());
                }
                if (row["TotalPrice"] != null && row["TotalPrice"].ToString() != "")
                {
                    model.TotalPrice = decimal.Parse(row["TotalPrice"].ToString());
                }
                if (row["HouseRegionArea"] != null && row["HouseRegionArea"].ToString() != "")
                {
                    model.HouseRegionArea = int.Parse(row["HouseRegionArea"].ToString());
                }
                if (row["HouseArea"] != null && row["HouseArea"].ToString() != "")
                {
                    model.HouseArea = decimal.Parse(row["HouseArea"].ToString());
                }
                if (row["HouseTel"] != null)
                {
                    model.HouseTel = row["HouseTel"].ToString();
                }
                if (row["HouseCustomerName"] != null)
                {
                    model.HouseCustomerName = row["HouseCustomerName"].ToString();
                }
                if (row["HousePersonID"] != null)
                {
                    model.HousePersonID = row["HousePersonID"].ToString();
                }
                if (row["CustomerId"] != null && row["CustomerId"].ToString() != "")
                {
                    model.CustomerId = int.Parse(row["CustomerId"].ToString());
                }
                if (row["CustomerName"] != null)
                {
                    model.CustomerName = row["CustomerName"].ToString();
                }
                if (row["CustomerTel"] != null)
                {
                    model.CustomerTel = row["CustomerTel"].ToString();
                }
                if (row["CustomerPersonID"] != null)
                {
                    model.CustomerPersonID = row["CustomerPersonID"].ToString();
                }
                if (row["SignUserId"] != null && row["SignUserId"].ToString() != "")
                {
                    model.SignUserId = int.Parse(row["SignUserId"].ToString());
                }
                if (row["SignUser"] != null)
                {
                    model.SignUser = row["SignUser"].ToString();
                }
                if (row["SignDepartId"] != null && row["SignDepartId"].ToString() != "")
                {
                    model.SignDepartId = int.Parse(row["SignDepartId"].ToString());
                }
                if (row["PayType"] != null && row["PayType"].ToString() != "")
                {
                    model.PayType = int.Parse(row["PayType"].ToString());
                }
                if (row["ChargeTotal"] != null && row["ChargeTotal"].ToString() != "")
                {
                    model.ChargeTotal = decimal.Parse(row["ChargeTotal"].ToString());
                }
                if (row["ChargePayType"] != null && row["ChargePayType"].ToString() != "")
                {
                    model.ChargePayType = int.Parse(row["ChargePayType"].ToString());
                }
                if (row["ChargeHouse"] != null && row["ChargeHouse"].ToString() != "")
                {
                    model.ChargeHouse = decimal.Parse(row["ChargeHouse"].ToString());
                }
                if (row["ChargeCustomer"] != null && row["ChargeCustomer"].ToString() != "")
                {
                    model.ChargeCustomer = decimal.Parse(row["ChargeCustomer"].ToString());
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CMId,CMNumber,HouseId,TotalPrice,HouseRegionArea,HouseArea,HouseTel,HouseCustomerName,HousePersonID,CustomerId,CustomerName,CustomerTel,CustomerPersonID,SignUserId,SignUser,SignDepartId,PayType,ChargeTotal,ChargePayType,ChargeHouse,ChargeCustomer,State,Mem,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp ");
            strSql.Append(" FROM CM_Rent_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CMId,CMNumber,HouseId,TotalPrice,HouseRegionArea,HouseArea,HouseTel,HouseCustomerName,HousePersonID,CustomerId,CustomerName,CustomerTel,CustomerPersonID,SignUserId,SignUser,SignDepartId,PayType,ChargeTotal,ChargePayType,ChargeHouse,ChargeCustomer,State,Mem,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp ");
            strSql.Append(" FROM CM_Rent_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM CM_Rent_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CMId desc");
            }
            strSql.Append(")AS Row, T.*  from CM_Rent_Items T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "CM_Rent_Items";
            parameters[1].Value = "CMId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

