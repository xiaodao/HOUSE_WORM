﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:CM_Buy_Doc_WorkFlow
    /// </summary>
    public partial class CM_Buy_Doc_WorkFlow
    {
        public CM_Buy_Doc_WorkFlow()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int CMWFId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@CMWFId", SqlDbType.Int,4)
			};
            parameters[0].Value = CMWFId;

            int result = DbHelperSQL.RunProcedure("CM_Buy_Doc_WorkFlow_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.CM_Buy_Doc_WorkFlow model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@CMWFId", SqlDbType.Int,4),
					new SqlParameter("@ItemId", SqlDbType.Int,4),
					new SqlParameter("@SendDocIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@FromUserId", SqlDbType.Int,4),
					new SqlParameter("@FromUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ToUserId", SqlDbType.Int,4),
					new SqlParameter("@ToUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ReceiveDocIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@IsReceive", SqlDbType.Bit,1),
					new SqlParameter("@SendTime", SqlDbType.DateTime),
					new SqlParameter("@ReceiveTime", SqlDbType.DateTime)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.ItemId;
            parameters[2].Value = model.SendDocIds;
            parameters[3].Value = model.FromUserId;
            parameters[4].Value = model.FromUser;
            parameters[5].Value = model.ToUserId;
            parameters[6].Value = model.ToUser;
            parameters[7].Value = model.ReceiveDocIds;
            parameters[8].Value = model.IsReceive;
            parameters[9].Value = model.SendTime;
            parameters[10].Value = model.ReceiveTime;

            DbHelperSQL.RunProcedure("CM_Buy_Doc_WorkFlow_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.CM_Buy_Doc_WorkFlow model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@CMWFId", SqlDbType.Int,4),
					new SqlParameter("@ItemId", SqlDbType.Int,4),
					new SqlParameter("@SendDocIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@FromUserId", SqlDbType.Int,4),
					new SqlParameter("@FromUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ToUserId", SqlDbType.Int,4),
					new SqlParameter("@ToUser", SqlDbType.NVarChar,64),
					new SqlParameter("@ReceiveDocIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@IsReceive", SqlDbType.Bit,1),
					new SqlParameter("@SendTime", SqlDbType.DateTime),
					new SqlParameter("@ReceiveTime", SqlDbType.DateTime)};
            parameters[0].Value = model.CMWFId;
            parameters[1].Value = model.ItemId;
            parameters[2].Value = model.SendDocIds;
            parameters[3].Value = model.FromUserId;
            parameters[4].Value = model.FromUser;
            parameters[5].Value = model.ToUserId;
            parameters[6].Value = model.ToUser;
            parameters[7].Value = model.ReceiveDocIds;
            parameters[8].Value = model.IsReceive;
            parameters[9].Value = model.SendTime;
            parameters[10].Value = model.ReceiveTime;

            DbHelperSQL.RunProcedure("CM_Buy_Doc_WorkFlow_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int CMWFId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@CMWFId", SqlDbType.Int,4)
			};
            parameters[0].Value = CMWFId;

            DbHelperSQL.RunProcedure("CM_Buy_Doc_WorkFlow_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string CMWFIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CM_Buy_Doc_WorkFlow ");
            strSql.Append(" where CMWFId in (" + CMWFIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.CM_Buy_Doc_WorkFlow GetModel(int CMWFId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@CMWFId", SqlDbType.Int,4)
			};
            parameters[0].Value = CMWFId;

            xundh.Model.CM_Buy_Doc_WorkFlow model = new xundh.Model.CM_Buy_Doc_WorkFlow();
            DataSet ds = DbHelperSQL.RunProcedure("CM_Buy_Doc_WorkFlow_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.CM_Buy_Doc_WorkFlow DataRowToModel(DataRow row)
        {
            xundh.Model.CM_Buy_Doc_WorkFlow model = new xundh.Model.CM_Buy_Doc_WorkFlow();
            if (row != null)
            {
                if (row["CMWFId"] != null && row["CMWFId"].ToString() != "")
                {
                    model.CMWFId = int.Parse(row["CMWFId"].ToString());
                }
                if (row["ItemId"] != null && row["ItemId"].ToString() != "")
                {
                    model.ItemId = int.Parse(row["ItemId"].ToString());
                }
                if (row["SendDocIds"] != null)
                {
                    model.SendDocIds = row["SendDocIds"].ToString();
                }
                if (row["FromUserId"] != null && row["FromUserId"].ToString() != "")
                {
                    model.FromUserId = int.Parse(row["FromUserId"].ToString());
                }
                if (row["FromUser"] != null)
                {
                    model.FromUser = row["FromUser"].ToString();
                }
                if (row["ToUserId"] != null && row["ToUserId"].ToString() != "")
                {
                    model.ToUserId = int.Parse(row["ToUserId"].ToString());
                }
                if (row["ToUser"] != null)
                {
                    model.ToUser = row["ToUser"].ToString();
                }
                if (row["ReceiveDocIds"] != null)
                {
                    model.ReceiveDocIds = row["ReceiveDocIds"].ToString();
                }
                if (row["IsReceive"] != null && row["IsReceive"].ToString() != "")
                {
                    if ((row["IsReceive"].ToString() == "1") || (row["IsReceive"].ToString().ToLower() == "true"))
                    {
                        model.IsReceive = true;
                    }
                    else
                    {
                        model.IsReceive = false;
                    }
                }
                if (row["SendTime"] != null && row["SendTime"].ToString() != "")
                {
                    model.SendTime = DateTime.Parse(row["SendTime"].ToString());
                }
                if (row["ReceiveTime"] != null && row["ReceiveTime"].ToString() != "")
                {
                    model.ReceiveTime = DateTime.Parse(row["ReceiveTime"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select CMWFId,ItemId,SendDocIds,FromUserId,FromUser,ToUserId,ToUser,ReceiveDocIds,IsReceive,SendTime,ReceiveTime ");
            strSql.Append(" FROM CM_Buy_Doc_WorkFlow ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" CMWFId,ItemId,SendDocIds,FromUserId,FromUser,ToUserId,ToUser,ReceiveDocIds,IsReceive,SendTime,ReceiveTime ");
            strSql.Append(" FROM CM_Buy_Doc_WorkFlow ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM CM_Buy_Doc_WorkFlow ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.CMWFId desc");
            }
            strSql.Append(")AS Row, T.*  from CM_Buy_Doc_WorkFlow T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "CM_Buy_Doc_WorkFlow";
            parameters[1].Value = "CMWFId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

