﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:CM_Buy_Follows
    /// </summary>
    public partial class CM_Buy_Follows
    {
        public CM_Buy_Follows()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int FollowId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@FollowId", SqlDbType.Int,4)
			};
            parameters[0].Value = FollowId;

            int result = DbHelperSQL.RunProcedure("CM_Buy_Follows_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.CM_Buy_Follows model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@FollowId", SqlDbType.Int,4),
					new SqlParameter("@CMId", SqlDbType.Int,4),
					new SqlParameter("@Follow", SqlDbType.NVarChar,4000),
					new SqlParameter("@FollowCMState", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.CMId;
            parameters[2].Value = model.Follow;
            parameters[3].Value = model.FollowCMState;
            parameters[4].Value = model.AddUserId;
            parameters[5].Value = model.AddUser;
            parameters[6].Value = model.AddTime;
            parameters[7].Value = model.AddIp;
            parameters[8].Value = model.AddDepartId;

            DbHelperSQL.RunProcedure("CM_Buy_Follows_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.CM_Buy_Follows model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@FollowId", SqlDbType.Int,4),
					new SqlParameter("@CMId", SqlDbType.Int,4),
					new SqlParameter("@Follow", SqlDbType.NVarChar,4000),
					new SqlParameter("@FollowCMState", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4)};
            parameters[0].Value = model.FollowId;
            parameters[1].Value = model.CMId;
            parameters[2].Value = model.Follow;
            parameters[3].Value = model.FollowCMState;
            parameters[4].Value = model.AddUserId;
            parameters[5].Value = model.AddUser;
            parameters[6].Value = model.AddTime;
            parameters[7].Value = model.AddIp;
            parameters[8].Value = model.AddDepartId;

            DbHelperSQL.RunProcedure("CM_Buy_Follows_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int FollowId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@FollowId", SqlDbType.Int,4)
			};
            parameters[0].Value = FollowId;

            DbHelperSQL.RunProcedure("CM_Buy_Follows_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string FollowIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from CM_Buy_Follows ");
            strSql.Append(" where FollowId in (" + FollowIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.CM_Buy_Follows GetModel(int FollowId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@FollowId", SqlDbType.Int,4)
			};
            parameters[0].Value = FollowId;

            xundh.Model.CM_Buy_Follows model = new xundh.Model.CM_Buy_Follows();
            DataSet ds = DbHelperSQL.RunProcedure("CM_Buy_Follows_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.CM_Buy_Follows DataRowToModel(DataRow row)
        {
            xundh.Model.CM_Buy_Follows model = new xundh.Model.CM_Buy_Follows();
            if (row != null)
            {
                if (row["FollowId"] != null && row["FollowId"].ToString() != "")
                {
                    model.FollowId = int.Parse(row["FollowId"].ToString());
                }
                if (row["CMId"] != null && row["CMId"].ToString() != "")
                {
                    model.CMId = int.Parse(row["CMId"].ToString());
                }
                if (row["Follow"] != null)
                {
                    model.Follow = row["Follow"].ToString();
                }
                if (row["FollowCMState"] != null && row["FollowCMState"].ToString() != "")
                {
                    model.FollowCMState = int.Parse(row["FollowCMState"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select FollowId,CMId,Follow,FollowCMState,AddUserId,AddUser,AddTime,AddIp,AddDepartId ");
            strSql.Append(" FROM CM_Buy_Follows ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" FollowId,CMId,Follow,FollowCMState,AddUserId,AddUser,AddTime,AddIp,AddDepartId ");
            strSql.Append(" FROM CM_Buy_Follows ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM CM_Buy_Follows ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.FollowId desc");
            }
            strSql.Append(")AS Row, T.*  from CM_Buy_Follows T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "CM_Buy_Follows";
            parameters[1].Value = "FollowId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

