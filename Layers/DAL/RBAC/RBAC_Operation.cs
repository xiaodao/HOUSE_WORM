﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:RBAC_Operation
    /// </summary>
    public partial class RBAC_Operation
    {
        public RBAC_Operation()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OperationId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@OperationId", SqlDbType.Int,4)
			};
            parameters[0].Value = OperationId;

            int result = DbHelperSQL.RunProcedure("RBAC_Operation_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.RBAC_Operation model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@OperationId", SqlDbType.Int,4),
					new SqlParameter("@OperationName", SqlDbType.NVarChar,64),
					new SqlParameter("@OperationDescription", SqlDbType.NVarChar,256),
					new SqlParameter("@OperationEn", SqlDbType.NVarChar,16),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.OperationName;
            parameters[2].Value = model.OperationDescription;
            parameters[3].Value = model.OperationEn;
            parameters[4].Value = model.AddUserId;
            parameters[5].Value = model.AddUser;
            parameters[6].Value = model.AddDepartId;
            parameters[7].Value = model.AddTime;
            parameters[8].Value = model.AddIp;
            parameters[9].Value = model.LastUserId;
            parameters[10].Value = model.LastUser;
            parameters[11].Value = model.LastDepartId;
            parameters[12].Value = model.LastTime;
            parameters[13].Value = model.LastIp;
            parameters[14].Value = model.Mem;

            DbHelperSQL.RunProcedure("RBAC_Operation_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.RBAC_Operation model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@OperationId", SqlDbType.Int,4),
					new SqlParameter("@OperationName", SqlDbType.NVarChar,64),
					new SqlParameter("@OperationDescription", SqlDbType.NVarChar,256),
					new SqlParameter("@OperationEn", SqlDbType.NVarChar,16),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000)};
            parameters[0].Value = model.OperationId;
            parameters[1].Value = model.OperationName;
            parameters[2].Value = model.OperationDescription;
            parameters[3].Value = model.OperationEn;
            parameters[4].Value = model.AddUserId;
            parameters[5].Value = model.AddUser;
            parameters[6].Value = model.AddDepartId;
            parameters[7].Value = model.AddTime;
            parameters[8].Value = model.AddIp;
            parameters[9].Value = model.LastUserId;
            parameters[10].Value = model.LastUser;
            parameters[11].Value = model.LastDepartId;
            parameters[12].Value = model.LastTime;
            parameters[13].Value = model.LastIp;
            parameters[14].Value = model.Mem;

            DbHelperSQL.RunProcedure("RBAC_Operation_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OperationId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@OperationId", SqlDbType.Int,4)
			};
            parameters[0].Value = OperationId;

            DbHelperSQL.RunProcedure("RBAC_Operation_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OperationIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from RBAC_Operation ");
            strSql.Append(" where OperationId in (" + OperationIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.RBAC_Operation GetModel(int OperationId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@OperationId", SqlDbType.Int,4)
			};
            parameters[0].Value = OperationId;

            xundh.Model.RBAC_Operation model = new xundh.Model.RBAC_Operation();
            DataSet ds = DbHelperSQL.RunProcedure("RBAC_Operation_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.RBAC_Operation DataRowToModel(DataRow row)
        {
            xundh.Model.RBAC_Operation model = new xundh.Model.RBAC_Operation();
            if (row != null)
            {
                if (row["OperationId"] != null && row["OperationId"].ToString() != "")
                {
                    model.OperationId = int.Parse(row["OperationId"].ToString());
                }
                if (row["OperationName"] != null)
                {
                    model.OperationName = row["OperationName"].ToString();
                }
                if (row["OperationDescription"] != null)
                {
                    model.OperationDescription = row["OperationDescription"].ToString();
                }
                if (row["OperationEn"] != null)
                {
                    model.OperationEn = row["OperationEn"].ToString();
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OperationId,OperationName,OperationDescription,OperationEn,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem ");
            strSql.Append(" FROM RBAC_Operation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OperationId,OperationName,OperationDescription,OperationEn,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem ");
            strSql.Append(" FROM RBAC_Operation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM RBAC_Operation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OperationId desc");
            }
            strSql.Append(")AS Row, T.*  from RBAC_Operation T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "RBAC_Operation";
            parameters[1].Value = "OperationId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

