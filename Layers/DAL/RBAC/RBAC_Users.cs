﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:RBAC_Users
    /// </summary>
    public partial class RBAC_Users
    {
        public RBAC_Users()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int UserId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4)
			};
            parameters[0].Value = UserId;

            int result = DbHelperSQL.RunProcedure("RBAC_Users_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.RBAC_Users model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4),
					new SqlParameter("@UserName", SqlDbType.NVarChar,64),
					new SqlParameter("@UserNameEn", SqlDbType.NVarChar,32),
					new SqlParameter("@UserPass", SqlDbType.NVarChar,64),
					new SqlParameter("@UserSex", SqlDbType.Int,4),
					new SqlParameter("@UserState", SqlDbType.Int,4),
					new SqlParameter("@UserMobile", SqlDbType.NVarChar,64),
					new SqlParameter("@UserEmail", SqlDbType.NVarChar,64),
					new SqlParameter("@UserAdmin", SqlDbType.Bit,1),
					new SqlParameter("@UserSessionId", SqlDbType.NVarChar,64),
					new SqlParameter("@UserOnline", SqlDbType.Int,4),
					new SqlParameter("@DepartId", SqlDbType.Int,4),
					new SqlParameter("@PostId", SqlDbType.Int,4),
					new SqlParameter("@LastLoginTime", SqlDbType.DateTime),
					new SqlParameter("@LastLoginIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastHardId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.UserName;
            parameters[2].Value = model.UserNameEn;
            parameters[3].Value = model.UserPass;
            parameters[4].Value = model.UserSex;
            parameters[5].Value = model.UserState;
            parameters[6].Value = model.UserMobile;
            parameters[7].Value = model.UserEmail;
            parameters[8].Value = model.UserAdmin;
            parameters[9].Value = model.UserSessionId;
            parameters[10].Value = model.UserOnline;
            parameters[11].Value = model.DepartId;
            parameters[12].Value = model.PostId;
            parameters[13].Value = model.LastLoginTime;
            parameters[14].Value = model.LastLoginIp;
            parameters[15].Value = model.LastHardId;
            parameters[16].Value = model.AddUserId;
            parameters[17].Value = model.AddUser;
            parameters[18].Value = model.AddDepartId;
            parameters[19].Value = model.AddTime;
            parameters[20].Value = model.AddIp;
            parameters[21].Value = model.LastUserId;
            parameters[22].Value = model.LastUser;
            parameters[23].Value = model.LastDepartId;
            parameters[24].Value = model.LastTime;
            parameters[25].Value = model.LastIp;
            parameters[26].Value = model.Mem;

            DbHelperSQL.RunProcedure("RBAC_Users_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.RBAC_Users model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4),
					new SqlParameter("@UserName", SqlDbType.NVarChar,64),
					new SqlParameter("@UserNameEn", SqlDbType.NVarChar,32),
					new SqlParameter("@UserPass", SqlDbType.NVarChar,64),
					new SqlParameter("@UserSex", SqlDbType.Int,4),
					new SqlParameter("@UserState", SqlDbType.Int,4),
					new SqlParameter("@UserMobile", SqlDbType.NVarChar,64),
					new SqlParameter("@UserEmail", SqlDbType.NVarChar,64),
					new SqlParameter("@UserAdmin", SqlDbType.Bit,1),
					new SqlParameter("@UserSessionId", SqlDbType.NVarChar,64),
					new SqlParameter("@UserOnline", SqlDbType.Int,4),
					new SqlParameter("@DepartId", SqlDbType.Int,4),
					new SqlParameter("@PostId", SqlDbType.Int,4),
					new SqlParameter("@LastLoginTime", SqlDbType.DateTime),
					new SqlParameter("@LastLoginIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastHardId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000)};
            parameters[0].Value = model.UserId;
            parameters[1].Value = model.UserName;
            parameters[2].Value = model.UserNameEn;
            parameters[3].Value = model.UserPass;
            parameters[4].Value = model.UserSex;
            parameters[5].Value = model.UserState;
            parameters[6].Value = model.UserMobile;
            parameters[7].Value = model.UserEmail;
            parameters[8].Value = model.UserAdmin;
            parameters[9].Value = model.UserSessionId;
            parameters[10].Value = model.UserOnline;
            parameters[11].Value = model.DepartId;
            parameters[12].Value = model.PostId;
            parameters[13].Value = model.LastLoginTime;
            parameters[14].Value = model.LastLoginIp;
            parameters[15].Value = model.LastHardId;
            parameters[16].Value = model.AddUserId;
            parameters[17].Value = model.AddUser;
            parameters[18].Value = model.AddDepartId;
            parameters[19].Value = model.AddTime;
            parameters[20].Value = model.AddIp;
            parameters[21].Value = model.LastUserId;
            parameters[22].Value = model.LastUser;
            parameters[23].Value = model.LastDepartId;
            parameters[24].Value = model.LastTime;
            parameters[25].Value = model.LastIp;
            parameters[26].Value = model.Mem;

            DbHelperSQL.RunProcedure("RBAC_Users_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int UserId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4)
			};
            parameters[0].Value = UserId;

            DbHelperSQL.RunProcedure("RBAC_Users_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string UserIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from RBAC_Users ");
            strSql.Append(" where UserId in (" + UserIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.RBAC_Users GetModel(int UserId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4)
			};
            parameters[0].Value = UserId;

            xundh.Model.RBAC_Users model = new xundh.Model.RBAC_Users();
            DataSet ds = DbHelperSQL.RunProcedure("RBAC_Users_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.RBAC_Users DataRowToModel(DataRow row)
        {
            xundh.Model.RBAC_Users model = new xundh.Model.RBAC_Users();
            if (row != null)
            {
                if (row["UserId"] != null && row["UserId"].ToString() != "")
                {
                    model.UserId = int.Parse(row["UserId"].ToString());
                }
                if (row["UserName"] != null)
                {
                    model.UserName = row["UserName"].ToString();
                }
                if (row["UserNameEn"] != null)
                {
                    model.UserNameEn = row["UserNameEn"].ToString();
                }
                if (row["UserPass"] != null)
                {
                    model.UserPass = row["UserPass"].ToString();
                }
                if (row["UserSex"] != null && row["UserSex"].ToString() != "")
                {
                    model.UserSex = int.Parse(row["UserSex"].ToString());
                }
                if (row["UserState"] != null && row["UserState"].ToString() != "")
                {
                    model.UserState = int.Parse(row["UserState"].ToString());
                }
                if (row["UserMobile"] != null)
                {
                    model.UserMobile = row["UserMobile"].ToString();
                }
                if (row["UserEmail"] != null)
                {
                    model.UserEmail = row["UserEmail"].ToString();
                }
                if (row["UserAdmin"] != null && row["UserAdmin"].ToString() != "")
                {
                    if ((row["UserAdmin"].ToString() == "1") || (row["UserAdmin"].ToString().ToLower() == "true"))
                    {
                        model.UserAdmin = true;
                    }
                    else
                    {
                        model.UserAdmin = false;
                    }
                }
                if (row["UserSessionId"] != null)
                {
                    model.UserSessionId = row["UserSessionId"].ToString();
                }
                if (row["UserOnline"] != null && row["UserOnline"].ToString() != "")
                {
                    model.UserOnline = int.Parse(row["UserOnline"].ToString());
                }
                if (row["DepartId"] != null && row["DepartId"].ToString() != "")
                {
                    model.DepartId = int.Parse(row["DepartId"].ToString());
                }
                if (row["PostId"] != null && row["PostId"].ToString() != "")
                {
                    model.PostId = int.Parse(row["PostId"].ToString());
                }
                if (row["LastLoginTime"] != null && row["LastLoginTime"].ToString() != "")
                {
                    model.LastLoginTime = DateTime.Parse(row["LastLoginTime"].ToString());
                }
                if (row["LastLoginIp"] != null)
                {
                    model.LastLoginIp = row["LastLoginIp"].ToString();
                }
                if (row["LastHardId"] != null && row["LastHardId"].ToString() != "")
                {
                    model.LastHardId = int.Parse(row["LastHardId"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select UserId,UserName,UserNameEn,UserPass,UserSex,UserState,UserMobile,UserEmail,UserAdmin,UserSessionId,UserOnline,DepartId,PostId,LastLoginTime,LastLoginIp,LastHardId,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem ");
            strSql.Append(" FROM RBAC_Users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" UserId,UserName,UserNameEn,UserPass,UserSex,UserState,UserMobile,UserEmail,UserAdmin,UserSessionId,UserOnline,DepartId,PostId,LastLoginTime,LastLoginIp,LastHardId,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem ");
            strSql.Append(" FROM RBAC_Users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM RBAC_Users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.UserId desc");
            }
            strSql.Append(")AS Row, T.*  from RBAC_Users T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "RBAC_Users";
            parameters[1].Value = "UserId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

