﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:RBAC_Role_Resource_Operation
    /// </summary>
    public partial class RBAC_Role_Resource_Operation
    {
        public RBAC_Role_Resource_Operation()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int RoleAuthorityId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@RoleAuthorityId", SqlDbType.Int,4)
			};
            parameters[0].Value = RoleAuthorityId;

            int result = DbHelperSQL.RunProcedure("RBAC_Role_Resource_Operation_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.RBAC_Role_Resource_Operation model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@RoleAuthorityId", SqlDbType.Int,4),
					new SqlParameter("@RoleId", SqlDbType.Int,4),
					new SqlParameter("@ResourceId", SqlDbType.Int,4),
					new SqlParameter("@OperationId", SqlDbType.Int,4),
					new SqlParameter("@Value", SqlDbType.Bit,1)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.RoleId;
            parameters[2].Value = model.ResourceId;
            parameters[3].Value = model.OperationId;
            parameters[4].Value = model.Value;

            DbHelperSQL.RunProcedure("RBAC_Role_Resource_Operation_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.RBAC_Role_Resource_Operation model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@RoleAuthorityId", SqlDbType.Int,4),
					new SqlParameter("@RoleId", SqlDbType.Int,4),
					new SqlParameter("@ResourceId", SqlDbType.Int,4),
					new SqlParameter("@OperationId", SqlDbType.Int,4),
					new SqlParameter("@Value", SqlDbType.Bit,1)};
            parameters[0].Value = model.RoleAuthorityId;
            parameters[1].Value = model.RoleId;
            parameters[2].Value = model.ResourceId;
            parameters[3].Value = model.OperationId;
            parameters[4].Value = model.Value;

            DbHelperSQL.RunProcedure("RBAC_Role_Resource_Operation_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int RoleAuthorityId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@RoleAuthorityId", SqlDbType.Int,4)
			};
            parameters[0].Value = RoleAuthorityId;

            DbHelperSQL.RunProcedure("RBAC_Role_Resource_Operation_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string RoleAuthorityIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from RBAC_Role_Resource_Operation ");
            strSql.Append(" where RoleAuthorityId in (" + RoleAuthorityIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.RBAC_Role_Resource_Operation GetModel(int RoleAuthorityId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@RoleAuthorityId", SqlDbType.Int,4)
			};
            parameters[0].Value = RoleAuthorityId;

            xundh.Model.RBAC_Role_Resource_Operation model = new xundh.Model.RBAC_Role_Resource_Operation();
            DataSet ds = DbHelperSQL.RunProcedure("RBAC_Role_Resource_Operation_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.RBAC_Role_Resource_Operation DataRowToModel(DataRow row)
        {
            xundh.Model.RBAC_Role_Resource_Operation model = new xundh.Model.RBAC_Role_Resource_Operation();
            if (row != null)
            {
                if (row["RoleAuthorityId"] != null && row["RoleAuthorityId"].ToString() != "")
                {
                    model.RoleAuthorityId = int.Parse(row["RoleAuthorityId"].ToString());
                }
                if (row["RoleId"] != null && row["RoleId"].ToString() != "")
                {
                    model.RoleId = int.Parse(row["RoleId"].ToString());
                }
                if (row["ResourceId"] != null && row["ResourceId"].ToString() != "")
                {
                    model.ResourceId = int.Parse(row["ResourceId"].ToString());
                }
                if (row["OperationId"] != null && row["OperationId"].ToString() != "")
                {
                    model.OperationId = int.Parse(row["OperationId"].ToString());
                }
                if (row["Value"] != null && row["Value"].ToString() != "")
                {
                    if ((row["Value"].ToString() == "1") || (row["Value"].ToString().ToLower() == "true"))
                    {
                        model.Value = true;
                    }
                    else
                    {
                        model.Value = false;
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select RoleAuthorityId,RoleId,ResourceId,OperationId,Value ");
            strSql.Append(" FROM RBAC_Role_Resource_Operation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" RoleAuthorityId,RoleId,ResourceId,OperationId,Value ");
            strSql.Append(" FROM RBAC_Role_Resource_Operation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM RBAC_Role_Resource_Operation ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.RoleAuthorityId desc");
            }
            strSql.Append(")AS Row, T.*  from RBAC_Role_Resource_Operation T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "RBAC_Role_Resource_Operation";
            parameters[1].Value = "RoleAuthorityId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

