﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:RBAC_Resources
    /// </summary>
    public partial class RBAC_Resources
    {
        public RBAC_Resources()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ResourceId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@ResourceId", SqlDbType.Int,4)
			};
            parameters[0].Value = ResourceId;

            int result = DbHelperSQL.RunProcedure("RBAC_Resources_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.RBAC_Resources model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@ResourceId", SqlDbType.Int,4),
					new SqlParameter("@ResourceName", SqlDbType.NVarChar,64),
					new SqlParameter("@ResourceDescription", SqlDbType.NVarChar,256),
					new SqlParameter("@ParentResourceId", SqlDbType.Int,4),
					new SqlParameter("@ResourcePath", SqlDbType.NVarChar,256),
					new SqlParameter("@Sort", SqlDbType.Int,4),
					new SqlParameter("@DefaultState", SqlDbType.Int,4),
					new SqlParameter("@Icon", SqlDbType.NVarChar,256),
					new SqlParameter("@Js", SqlDbType.NVarChar,4000)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.ResourceName;
            parameters[2].Value = model.ResourceDescription;
            parameters[3].Value = model.ParentResourceId;
            parameters[4].Value = model.ResourcePath;
            parameters[5].Value = model.Sort;
            parameters[6].Value = model.DefaultState;
            parameters[7].Value = model.Icon;
            parameters[8].Value = model.Js;

            DbHelperSQL.RunProcedure("RBAC_Resources_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.RBAC_Resources model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@ResourceId", SqlDbType.Int,4),
					new SqlParameter("@ResourceName", SqlDbType.NVarChar,64),
					new SqlParameter("@ResourceDescription", SqlDbType.NVarChar,256),
					new SqlParameter("@ParentResourceId", SqlDbType.Int,4),
					new SqlParameter("@ResourcePath", SqlDbType.NVarChar,256),
					new SqlParameter("@Sort", SqlDbType.Int,4),
					new SqlParameter("@DefaultState", SqlDbType.Int,4),
					new SqlParameter("@Icon", SqlDbType.NVarChar,256),
					new SqlParameter("@Js", SqlDbType.NVarChar,4000)};
            parameters[0].Value = model.ResourceId;
            parameters[1].Value = model.ResourceName;
            parameters[2].Value = model.ResourceDescription;
            parameters[3].Value = model.ParentResourceId;
            parameters[4].Value = model.ResourcePath;
            parameters[5].Value = model.Sort;
            parameters[6].Value = model.DefaultState;
            parameters[7].Value = model.Icon;
            parameters[8].Value = model.Js;

            DbHelperSQL.RunProcedure("RBAC_Resources_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ResourceId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@ResourceId", SqlDbType.Int,4)
			};
            parameters[0].Value = ResourceId;

            DbHelperSQL.RunProcedure("RBAC_Resources_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string ResourceIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from RBAC_Resources ");
            strSql.Append(" where ResourceId in (" + ResourceIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.RBAC_Resources GetModel(int ResourceId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@ResourceId", SqlDbType.Int,4)
			};
            parameters[0].Value = ResourceId;

            xundh.Model.RBAC_Resources model = new xundh.Model.RBAC_Resources();
            DataSet ds = DbHelperSQL.RunProcedure("RBAC_Resources_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.RBAC_Resources DataRowToModel(DataRow row)
        {
            xundh.Model.RBAC_Resources model = new xundh.Model.RBAC_Resources();
            if (row != null)
            {
                if (row["ResourceId"] != null && row["ResourceId"].ToString() != "")
                {
                    model.ResourceId = int.Parse(row["ResourceId"].ToString());
                }
                if (row["ResourceName"] != null)
                {
                    model.ResourceName = row["ResourceName"].ToString();
                }
                if (row["ResourceDescription"] != null)
                {
                    model.ResourceDescription = row["ResourceDescription"].ToString();
                }
                if (row["ParentResourceId"] != null && row["ParentResourceId"].ToString() != "")
                {
                    model.ParentResourceId = int.Parse(row["ParentResourceId"].ToString());
                }
                if (row["ResourcePath"] != null)
                {
                    model.ResourcePath = row["ResourcePath"].ToString();
                }
                if (row["Sort"] != null && row["Sort"].ToString() != "")
                {
                    model.Sort = int.Parse(row["Sort"].ToString());
                }
                if (row["DefaultState"] != null && row["DefaultState"].ToString() != "")
                {
                    model.DefaultState = int.Parse(row["DefaultState"].ToString());
                }
                if (row["Icon"] != null)
                {
                    model.Icon = row["Icon"].ToString();
                }
                if (row["Js"] != null)
                {
                    model.Js = row["Js"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ResourceId,ResourceName,ResourceDescription,ParentResourceId,ResourcePath,Sort,DefaultState,Icon,Js ");
            strSql.Append(" FROM RBAC_Resources ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ResourceId,ResourceName,ResourceDescription,ParentResourceId,ResourcePath,Sort,DefaultState,Icon,Js ");
            strSql.Append(" FROM RBAC_Resources ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM RBAC_Resources ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ResourceId desc");
            }
            strSql.Append(")AS Row, T.*  from RBAC_Resources T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "RBAC_Resources";
            parameters[1].Value = "ResourceId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

