﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:WF_WorkFlows
    /// </summary>
    public partial class WF_WorkFlows
    {
        public WF_WorkFlows()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int FlowId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@FlowId", SqlDbType.Int,4)
			};
            parameters[0].Value = FlowId;

            int result = DbHelperSQL.RunProcedure("WF_WorkFlows_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.WF_WorkFlows model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@FlowId", SqlDbType.Int,4),
					new SqlParameter("@ItemId", SqlDbType.Int,4),
					new SqlParameter("@NodeStep", SqlDbType.Int,4),
					new SqlParameter("@NodeName", SqlDbType.NVarChar,64),
					new SqlParameter("@NodeType", SqlDbType.NVarChar,64),
					new SqlParameter("@ToUserId", SqlDbType.NVarChar,4000),
					new SqlParameter("@ToUser", SqlDbType.NVarChar,4000),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@Suggest", SqlDbType.NVarChar,4000),
					new SqlParameter("@VerifyUserId", SqlDbType.Int,4),
					new SqlParameter("@VerifyUser", SqlDbType.NVarChar,64),
					new SqlParameter("@VerifyTime", SqlDbType.DateTime)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.ItemId;
            parameters[2].Value = model.NodeStep;
            parameters[3].Value = model.NodeName;
            parameters[4].Value = model.NodeType;
            parameters[5].Value = model.ToUserId;
            parameters[6].Value = model.ToUser;
            parameters[7].Value = model.AddTime;
            parameters[8].Value = model.State;
            parameters[9].Value = model.Suggest;
            parameters[10].Value = model.VerifyUserId;
            parameters[11].Value = model.VerifyUser;
            parameters[12].Value = model.VerifyTime;

            DbHelperSQL.RunProcedure("WF_WorkFlows_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.WF_WorkFlows model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@FlowId", SqlDbType.Int,4),
					new SqlParameter("@ItemId", SqlDbType.Int,4),
					new SqlParameter("@NodeStep", SqlDbType.Int,4),
					new SqlParameter("@NodeName", SqlDbType.NVarChar,64),
					new SqlParameter("@NodeType", SqlDbType.NVarChar,64),
					new SqlParameter("@ToUserId", SqlDbType.NVarChar,4000),
					new SqlParameter("@ToUser", SqlDbType.NVarChar,4000),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@Suggest", SqlDbType.NVarChar,4000),
					new SqlParameter("@VerifyUserId", SqlDbType.Int,4),
					new SqlParameter("@VerifyUser", SqlDbType.NVarChar,64),
					new SqlParameter("@VerifyTime", SqlDbType.DateTime)};
            parameters[0].Value = model.FlowId;
            parameters[1].Value = model.ItemId;
            parameters[2].Value = model.NodeStep;
            parameters[3].Value = model.NodeName;
            parameters[4].Value = model.NodeType;
            parameters[5].Value = model.ToUserId;
            parameters[6].Value = model.ToUser;
            parameters[7].Value = model.AddTime;
            parameters[8].Value = model.State;
            parameters[9].Value = model.Suggest;
            parameters[10].Value = model.VerifyUserId;
            parameters[11].Value = model.VerifyUser;
            parameters[12].Value = model.VerifyTime;

            DbHelperSQL.RunProcedure("WF_WorkFlows_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int FlowId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@FlowId", SqlDbType.Int,4)
			};
            parameters[0].Value = FlowId;

            DbHelperSQL.RunProcedure("WF_WorkFlows_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string FlowIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from WF_WorkFlows ");
            strSql.Append(" where FlowId in (" + FlowIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.WF_WorkFlows GetModel(int FlowId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@FlowId", SqlDbType.Int,4)
			};
            parameters[0].Value = FlowId;

            xundh.Model.WF_WorkFlows model = new xundh.Model.WF_WorkFlows();
            DataSet ds = DbHelperSQL.RunProcedure("WF_WorkFlows_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.WF_WorkFlows DataRowToModel(DataRow row)
        {
            xundh.Model.WF_WorkFlows model = new xundh.Model.WF_WorkFlows();
            if (row != null)
            {
                if (row["FlowId"] != null && row["FlowId"].ToString() != "")
                {
                    model.FlowId = int.Parse(row["FlowId"].ToString());
                }
                if (row["ItemId"] != null && row["ItemId"].ToString() != "")
                {
                    model.ItemId = int.Parse(row["ItemId"].ToString());
                }
                if (row["NodeStep"] != null && row["NodeStep"].ToString() != "")
                {
                    model.NodeStep = int.Parse(row["NodeStep"].ToString());
                }
                if (row["NodeName"] != null)
                {
                    model.NodeName = row["NodeName"].ToString();
                }
                if (row["NodeType"] != null)
                {
                    model.NodeType = row["NodeType"].ToString();
                }
                if (row["ToUserId"] != null)
                {
                    model.ToUserId = row["ToUserId"].ToString();
                }
                if (row["ToUser"] != null)
                {
                    model.ToUser = row["ToUser"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["Suggest"] != null)
                {
                    model.Suggest = row["Suggest"].ToString();
                }
                if (row["VerifyUserId"] != null && row["VerifyUserId"].ToString() != "")
                {
                    model.VerifyUserId = int.Parse(row["VerifyUserId"].ToString());
                }
                if (row["VerifyUser"] != null)
                {
                    model.VerifyUser = row["VerifyUser"].ToString();
                }
                if (row["VerifyTime"] != null && row["VerifyTime"].ToString() != "")
                {
                    model.VerifyTime = DateTime.Parse(row["VerifyTime"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select FlowId,ItemId,NodeStep,NodeName,NodeType,ToUserId,ToUser,AddTime,State,Suggest,VerifyUserId,VerifyUser,VerifyTime ");
            strSql.Append(" FROM WF_WorkFlows ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" FlowId,ItemId,NodeStep,NodeName,NodeType,ToUserId,ToUser,AddTime,State,Suggest,VerifyUserId,VerifyUser,VerifyTime ");
            strSql.Append(" FROM WF_WorkFlows ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM WF_WorkFlows ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.FlowId desc");
            }
            strSql.Append(")AS Row, T.*  from WF_WorkFlows T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "WF_WorkFlows";
            parameters[1].Value = "FlowId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

