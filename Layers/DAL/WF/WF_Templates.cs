﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:WF_Templates
    /// </summary>
    public partial class WF_Templates
    {
        public WF_Templates()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int TemplateId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@TemplateId", SqlDbType.Int,4)
			};
            parameters[0].Value = TemplateId;

            int result = DbHelperSQL.RunProcedure("WF_Templates_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.WF_Templates model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@TemplateId", SqlDbType.Int,4),
					new SqlParameter("@TypeId", SqlDbType.Int,4),
					new SqlParameter("@Title", SqlDbType.NVarChar,64),
					new SqlParameter("@Form", SqlDbType.NText),
					new SqlParameter("@GraphJson", SqlDbType.NText)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.TypeId;
            parameters[2].Value = model.Title;
            parameters[3].Value = model.Form;
            parameters[4].Value = model.GraphJson;

            DbHelperSQL.RunProcedure("WF_Templates_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.WF_Templates model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@TemplateId", SqlDbType.Int,4),
					new SqlParameter("@TypeId", SqlDbType.Int,4),
					new SqlParameter("@Title", SqlDbType.NVarChar,64),
					new SqlParameter("@Form", SqlDbType.NText),
					new SqlParameter("@GraphJson", SqlDbType.NText)};
            parameters[0].Value = model.TemplateId;
            parameters[1].Value = model.TypeId;
            parameters[2].Value = model.Title;
            parameters[3].Value = model.Form;
            parameters[4].Value = model.GraphJson;

            DbHelperSQL.RunProcedure("WF_Templates_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int TemplateId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@TemplateId", SqlDbType.Int,4)
			};
            parameters[0].Value = TemplateId;

            DbHelperSQL.RunProcedure("WF_Templates_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string TemplateIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from WF_Templates ");
            strSql.Append(" where TemplateId in (" + TemplateIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.WF_Templates GetModel(int TemplateId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@TemplateId", SqlDbType.Int,4)
			};
            parameters[0].Value = TemplateId;

            xundh.Model.WF_Templates model = new xundh.Model.WF_Templates();
            DataSet ds = DbHelperSQL.RunProcedure("WF_Templates_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.WF_Templates DataRowToModel(DataRow row)
        {
            xundh.Model.WF_Templates model = new xundh.Model.WF_Templates();
            if (row != null)
            {
                if (row["TemplateId"] != null && row["TemplateId"].ToString() != "")
                {
                    model.TemplateId = int.Parse(row["TemplateId"].ToString());
                }
                if (row["TypeId"] != null && row["TypeId"].ToString() != "")
                {
                    model.TypeId = int.Parse(row["TypeId"].ToString());
                }
                if (row["Title"] != null)
                {
                    model.Title = row["Title"].ToString();
                }
                if (row["Form"] != null)
                {
                    model.Form = row["Form"].ToString();
                }
                if (row["GraphJson"] != null)
                {
                    model.GraphJson = row["GraphJson"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select TemplateId,TypeId,Title,Form,GraphJson ");
            strSql.Append(" FROM WF_Templates ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" TemplateId,TypeId,Title,Form,GraphJson ");
            strSql.Append(" FROM WF_Templates ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM WF_Templates ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.TemplateId desc");
            }
            strSql.Append(")AS Row, T.*  from WF_Templates T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "WF_Templates";
            parameters[1].Value = "TemplateId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

