﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:WF_Items
    /// </summary>
    public partial class WF_Items
    {
        public WF_Items()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int ItemId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@ItemId", SqlDbType.Int,4)
			};
            parameters[0].Value = ItemId;

            int result = DbHelperSQL.RunProcedure("WF_Items_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.WF_Items model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@ItemId", SqlDbType.Int,4),
					new SqlParameter("@TemplateId", SqlDbType.Int,4),
					new SqlParameter("@Params", SqlDbType.NText),
					new SqlParameter("@Form", SqlDbType.NText),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@NowWorkFlowId", SqlDbType.Int,4),
					new SqlParameter("@NowNodeName", SqlDbType.NVarChar,64),
					new SqlParameter("@NowToUserIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@NowToUsers", SqlDbType.NVarChar,4000),
					new SqlParameter("@SpecifyIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@SpecifyNames", SqlDbType.NVarChar,4000),
					new SqlParameter("@FilePath", SqlDbType.NVarChar,256)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.TemplateId;
            parameters[2].Value = model.Params;
            parameters[3].Value = model.Form;
            parameters[4].Value = model.AddTime;
            parameters[5].Value = model.AddUserId;
            parameters[6].Value = model.AddUser;
            parameters[7].Value = model.AddIp;
            parameters[8].Value = model.AddDepartId;
            parameters[9].Value = model.LastTime;
            parameters[10].Value = model.LastUserId;
            parameters[11].Value = model.LastUser;
            parameters[12].Value = model.LastIp;
            parameters[13].Value = model.State;
            parameters[14].Value = model.NowWorkFlowId;
            parameters[15].Value = model.NowNodeName;
            parameters[16].Value = model.NowToUserIds;
            parameters[17].Value = model.NowToUsers;
            parameters[18].Value = model.SpecifyIds;
            parameters[19].Value = model.SpecifyNames;
            parameters[20].Value = model.FilePath;

            DbHelperSQL.RunProcedure("WF_Items_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.WF_Items model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@ItemId", SqlDbType.Int,4),
					new SqlParameter("@TemplateId", SqlDbType.Int,4),
					new SqlParameter("@Params", SqlDbType.NText),
					new SqlParameter("@Form", SqlDbType.NText),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@NowWorkFlowId", SqlDbType.Int,4),
					new SqlParameter("@NowNodeName", SqlDbType.NVarChar,64),
					new SqlParameter("@NowToUserIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@NowToUsers", SqlDbType.NVarChar,4000),
					new SqlParameter("@SpecifyIds", SqlDbType.NVarChar,4000),
					new SqlParameter("@SpecifyNames", SqlDbType.NVarChar,4000),
					new SqlParameter("@FilePath", SqlDbType.NVarChar,256)};
            parameters[0].Value = model.ItemId;
            parameters[1].Value = model.TemplateId;
            parameters[2].Value = model.Params;
            parameters[3].Value = model.Form;
            parameters[4].Value = model.AddTime;
            parameters[5].Value = model.AddUserId;
            parameters[6].Value = model.AddUser;
            parameters[7].Value = model.AddIp;
            parameters[8].Value = model.AddDepartId;
            parameters[9].Value = model.LastTime;
            parameters[10].Value = model.LastUserId;
            parameters[11].Value = model.LastUser;
            parameters[12].Value = model.LastIp;
            parameters[13].Value = model.State;
            parameters[14].Value = model.NowWorkFlowId;
            parameters[15].Value = model.NowNodeName;
            parameters[16].Value = model.NowToUserIds;
            parameters[17].Value = model.NowToUsers;
            parameters[18].Value = model.SpecifyIds;
            parameters[19].Value = model.SpecifyNames;
            parameters[20].Value = model.FilePath;

            DbHelperSQL.RunProcedure("WF_Items_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int ItemId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@ItemId", SqlDbType.Int,4)
			};
            parameters[0].Value = ItemId;

            DbHelperSQL.RunProcedure("WF_Items_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string ItemIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from WF_Items ");
            strSql.Append(" where ItemId in (" + ItemIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.WF_Items GetModel(int ItemId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@ItemId", SqlDbType.Int,4)
			};
            parameters[0].Value = ItemId;

            xundh.Model.WF_Items model = new xundh.Model.WF_Items();
            DataSet ds = DbHelperSQL.RunProcedure("WF_Items_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.WF_Items DataRowToModel(DataRow row)
        {
            xundh.Model.WF_Items model = new xundh.Model.WF_Items();
            if (row != null)
            {
                if (row["ItemId"] != null && row["ItemId"].ToString() != "")
                {
                    model.ItemId = int.Parse(row["ItemId"].ToString());
                }
                if (row["TemplateId"] != null && row["TemplateId"].ToString() != "")
                {
                    model.TemplateId = int.Parse(row["TemplateId"].ToString());
                }
                if (row["Params"] != null)
                {
                    model.Params = row["Params"].ToString();
                }
                if (row["Form"] != null)
                {
                    model.Form = row["Form"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["NowWorkFlowId"] != null && row["NowWorkFlowId"].ToString() != "")
                {
                    model.NowWorkFlowId = int.Parse(row["NowWorkFlowId"].ToString());
                }
                if (row["NowNodeName"] != null)
                {
                    model.NowNodeName = row["NowNodeName"].ToString();
                }
                if (row["NowToUserIds"] != null)
                {
                    model.NowToUserIds = row["NowToUserIds"].ToString();
                }
                if (row["NowToUsers"] != null)
                {
                    model.NowToUsers = row["NowToUsers"].ToString();
                }
                if (row["SpecifyIds"] != null)
                {
                    model.SpecifyIds = row["SpecifyIds"].ToString();
                }
                if (row["SpecifyNames"] != null)
                {
                    model.SpecifyNames = row["SpecifyNames"].ToString();
                }
                if (row["FilePath"] != null)
                {
                    model.FilePath = row["FilePath"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ItemId,TemplateId,Params,Form,AddTime,AddUserId,AddUser,AddIp,AddDepartId,LastTime,LastUserId,LastUser,LastIp,State,NowWorkFlowId,NowNodeName,NowToUserIds,NowToUsers,SpecifyIds,SpecifyNames,FilePath ");
            strSql.Append(" FROM WF_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" ItemId,TemplateId,Params,Form,AddTime,AddUserId,AddUser,AddIp,AddDepartId,LastTime,LastUserId,LastUser,LastIp,State,NowWorkFlowId,NowNodeName,NowToUserIds,NowToUsers,SpecifyIds,SpecifyNames,FilePath ");
            strSql.Append(" FROM WF_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM WF_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.ItemId desc");
            }
            strSql.Append(")AS Row, T.*  from WF_Items T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "WF_Items";
            parameters[1].Value = "ItemId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

