﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Announcement_Items
    /// </summary>
    public partial class Announcement_Items
    {
        public Announcement_Items()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int AnnoucementId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Announcement_Items");
            strSql.Append(" where AnnoucementId=@AnnoucementId");
            SqlParameter[] parameters = {
					new SqlParameter("@AnnoucementId", SqlDbType.Int,4)
			};
            parameters[0].Value = AnnoucementId;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(xundh.Model.Announcement_Items model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Announcement_Items(");
            strSql.Append("AnnoucementTitle,AnnoucementBody,AddDepartId,AddTime,AddUserId,AddUser,AddIp,LastDepartId,LastTime,LastUserId,LastUser,LastIp)");
            strSql.Append(" values (");
            strSql.Append("@AnnoucementTitle,@AnnoucementBody,@AddDepartId,@AddTime,@AddUserId,@AddUser,@AddIp,@LastDepartId,@LastTime,@LastUserId,@LastUser,@LastIp)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@AnnoucementTitle", SqlDbType.NVarChar,2000),
					new SqlParameter("@AnnoucementBody", SqlDbType.NText),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64)};
            parameters[0].Value = model.AnnoucementTitle;
            parameters[1].Value = model.AnnoucementBody;
            parameters[2].Value = model.AddDepartId;
            parameters[3].Value = model.AddTime;
            parameters[4].Value = model.AddUserId;
            parameters[5].Value = model.AddUser;
            parameters[6].Value = model.AddIp;
            parameters[7].Value = model.LastDepartId;
            parameters[8].Value = model.LastTime;
            parameters[9].Value = model.LastUserId;
            parameters[10].Value = model.LastUser;
            parameters[11].Value = model.LastIp;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Announcement_Items model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Announcement_Items set ");
            strSql.Append("AnnoucementTitle=@AnnoucementTitle,");
            strSql.Append("AnnoucementBody=@AnnoucementBody,");
            strSql.Append("AddDepartId=@AddDepartId,");
            strSql.Append("AddTime=@AddTime,");
            strSql.Append("AddUserId=@AddUserId,");
            strSql.Append("AddUser=@AddUser,");
            strSql.Append("AddIp=@AddIp,");
            strSql.Append("LastDepartId=@LastDepartId,");
            strSql.Append("LastTime=@LastTime,");
            strSql.Append("LastUserId=@LastUserId,");
            strSql.Append("LastUser=@LastUser,");
            strSql.Append("LastIp=@LastIp");
            strSql.Append(" where AnnoucementId=@AnnoucementId");
            SqlParameter[] parameters = {
					new SqlParameter("@AnnoucementTitle", SqlDbType.NVarChar,2000),
					new SqlParameter("@AnnoucementBody", SqlDbType.NText),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AnnoucementId", SqlDbType.Int,4)};
            parameters[0].Value = model.AnnoucementTitle;
            parameters[1].Value = model.AnnoucementBody;
            parameters[2].Value = model.AddDepartId;
            parameters[3].Value = model.AddTime;
            parameters[4].Value = model.AddUserId;
            parameters[5].Value = model.AddUser;
            parameters[6].Value = model.AddIp;
            parameters[7].Value = model.LastDepartId;
            parameters[8].Value = model.LastTime;
            parameters[9].Value = model.LastUserId;
            parameters[10].Value = model.LastUser;
            parameters[11].Value = model.LastIp;
            parameters[12].Value = model.AnnoucementId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int AnnoucementId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Announcement_Items ");
            strSql.Append(" where AnnoucementId=@AnnoucementId");
            SqlParameter[] parameters = {
					new SqlParameter("@AnnoucementId", SqlDbType.Int,4)
			};
            parameters[0].Value = AnnoucementId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string AnnoucementIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Announcement_Items ");
            strSql.Append(" where AnnoucementId in (" + AnnoucementIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Announcement_Items GetModel(int AnnoucementId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 AnnoucementId,AnnoucementTitle,AnnoucementBody,AddDepartId,AddTime,AddUserId,AddUser,AddIp,LastDepartId,LastTime,LastUserId,LastUser,LastIp from Announcement_Items ");
            strSql.Append(" where AnnoucementId=@AnnoucementId");
            SqlParameter[] parameters = {
					new SqlParameter("@AnnoucementId", SqlDbType.Int,4)
			};
            parameters[0].Value = AnnoucementId;

            xundh.Model.Announcement_Items model = new xundh.Model.Announcement_Items();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Announcement_Items DataRowToModel(DataRow row)
        {
            xundh.Model.Announcement_Items model = new xundh.Model.Announcement_Items();
            if (row != null)
            {
                if (row["AnnoucementId"] != null && row["AnnoucementId"].ToString() != "")
                {
                    model.AnnoucementId = int.Parse(row["AnnoucementId"].ToString());
                }
                if (row["AnnoucementTitle"] != null)
                {
                    model.AnnoucementTitle = row["AnnoucementTitle"].ToString();
                }
                if (row["AnnoucementBody"] != null)
                {
                    model.AnnoucementBody = row["AnnoucementBody"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select AnnoucementId,AnnoucementTitle,AnnoucementBody,AddDepartId,AddTime,AddUserId,AddUser,AddIp,LastDepartId,LastTime,LastUserId,LastUser,LastIp ");
            strSql.Append(" FROM Announcement_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" AnnoucementId,AnnoucementTitle,AnnoucementBody,AddDepartId,AddTime,AddUserId,AddUser,AddIp,LastDepartId,LastTime,LastUserId,LastUser,LastIp ");
            strSql.Append(" FROM Announcement_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Announcement_Items ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.AnnoucementId desc");
            }
            strSql.Append(")AS Row, T.*  from Announcement_Items T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Announcement_Items";
            parameters[1].Value = "AnnoucementId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

