﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Org_Hards
    /// </summary>
    public partial class Org_Hards
    {
        public Org_Hards()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int HardId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Org_Hards");
            strSql.Append(" where HardId=@HardId");
            SqlParameter[] parameters = {
					new SqlParameter("@HardId", SqlDbType.Int,4)
			};
            parameters[0].Value = HardId;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(xundh.Model.Org_Hards model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Org_Hards(");
            strSql.Append("DepartId,Mac,HardStr,HardState,Ua,Browser,BrowserVersion,Os,ScreenWidth,ScreenHeight,HardType,LastLoginUserId,LastLoginUser,LastLoginTime,LastLoginIp,LastLoginDepartId,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,IsDelete)");
            strSql.Append(" values (");
            strSql.Append("@DepartId,@Mac,@HardStr,@HardState,@Ua,@Browser,@BrowserVersion,@Os,@ScreenWidth,@ScreenHeight,@HardType,@LastLoginUserId,@LastLoginUser,@LastLoginTime,@LastLoginIp,@LastLoginDepartId,@AddUserId,@AddUser,@AddDepartId,@AddTime,@AddIp,@LastUserId,@LastUser,@LastDepartId,@LastTime,@LastIp,@Mem,@IsDelete)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@DepartId", SqlDbType.Int,4),
					new SqlParameter("@Mac", SqlDbType.NVarChar,32),
					new SqlParameter("@HardStr", SqlDbType.NVarChar,64),
					new SqlParameter("@HardState", SqlDbType.Bit,1),
					new SqlParameter("@Ua", SqlDbType.NVarChar,256),
					new SqlParameter("@Browser", SqlDbType.NVarChar,256),
					new SqlParameter("@BrowserVersion", SqlDbType.NVarChar,16),
					new SqlParameter("@Os", SqlDbType.NVarChar,256),
					new SqlParameter("@ScreenWidth", SqlDbType.Int,4),
					new SqlParameter("@ScreenHeight", SqlDbType.Int,4),
					new SqlParameter("@HardType", SqlDbType.NVarChar,32),
					new SqlParameter("@LastLoginUserId", SqlDbType.Int,4),
					new SqlParameter("@LastLoginUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastLoginTime", SqlDbType.DateTime),
					new SqlParameter("@LastLoginIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastLoginDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@IsDelete", SqlDbType.Bit,1)};
            parameters[0].Value = model.DepartId;
            parameters[1].Value = model.Mac;
            parameters[2].Value = model.HardStr;
            parameters[3].Value = model.HardState;
            parameters[4].Value = model.Ua;
            parameters[5].Value = model.Browser;
            parameters[6].Value = model.BrowserVersion;
            parameters[7].Value = model.Os;
            parameters[8].Value = model.ScreenWidth;
            parameters[9].Value = model.ScreenHeight;
            parameters[10].Value = model.HardType;
            parameters[11].Value = model.LastLoginUserId;
            parameters[12].Value = model.LastLoginUser;
            parameters[13].Value = model.LastLoginTime;
            parameters[14].Value = model.LastLoginIp;
            parameters[15].Value = model.LastLoginDepartId;
            parameters[16].Value = model.AddUserId;
            parameters[17].Value = model.AddUser;
            parameters[18].Value = model.AddDepartId;
            parameters[19].Value = model.AddTime;
            parameters[20].Value = model.AddIp;
            parameters[21].Value = model.LastUserId;
            parameters[22].Value = model.LastUser;
            parameters[23].Value = model.LastDepartId;
            parameters[24].Value = model.LastTime;
            parameters[25].Value = model.LastIp;
            parameters[26].Value = model.Mem;
            parameters[27].Value = model.IsDelete;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Org_Hards model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Org_Hards set ");
            strSql.Append("DepartId=@DepartId,");
            strSql.Append("Mac=@Mac,");
            strSql.Append("HardStr=@HardStr,");
            strSql.Append("HardState=@HardState,");
            strSql.Append("Ua=@Ua,");
            strSql.Append("Browser=@Browser,");
            strSql.Append("BrowserVersion=@BrowserVersion,");
            strSql.Append("Os=@Os,");
            strSql.Append("ScreenWidth=@ScreenWidth,");
            strSql.Append("ScreenHeight=@ScreenHeight,");
            strSql.Append("HardType=@HardType,");
            strSql.Append("LastLoginUserId=@LastLoginUserId,");
            strSql.Append("LastLoginUser=@LastLoginUser,");
            strSql.Append("LastLoginTime=@LastLoginTime,");
            strSql.Append("LastLoginIp=@LastLoginIp,");
            strSql.Append("LastLoginDepartId=@LastLoginDepartId,");
            strSql.Append("AddUserId=@AddUserId,");
            strSql.Append("AddUser=@AddUser,");
            strSql.Append("AddDepartId=@AddDepartId,");
            strSql.Append("AddTime=@AddTime,");
            strSql.Append("AddIp=@AddIp,");
            strSql.Append("LastUserId=@LastUserId,");
            strSql.Append("LastUser=@LastUser,");
            strSql.Append("LastDepartId=@LastDepartId,");
            strSql.Append("LastTime=@LastTime,");
            strSql.Append("LastIp=@LastIp,");
            strSql.Append("Mem=@Mem,");
            strSql.Append("IsDelete=@IsDelete");
            strSql.Append(" where HardId=@HardId");
            SqlParameter[] parameters = {
					new SqlParameter("@DepartId", SqlDbType.Int,4),
					new SqlParameter("@Mac", SqlDbType.NVarChar,32),
					new SqlParameter("@HardStr", SqlDbType.NVarChar,64),
					new SqlParameter("@HardState", SqlDbType.Bit,1),
					new SqlParameter("@Ua", SqlDbType.NVarChar,256),
					new SqlParameter("@Browser", SqlDbType.NVarChar,256),
					new SqlParameter("@BrowserVersion", SqlDbType.NVarChar,16),
					new SqlParameter("@Os", SqlDbType.NVarChar,256),
					new SqlParameter("@ScreenWidth", SqlDbType.Int,4),
					new SqlParameter("@ScreenHeight", SqlDbType.Int,4),
					new SqlParameter("@HardType", SqlDbType.NVarChar,32),
					new SqlParameter("@LastLoginUserId", SqlDbType.Int,4),
					new SqlParameter("@LastLoginUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastLoginTime", SqlDbType.DateTime),
					new SqlParameter("@LastLoginIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastLoginDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@IsDelete", SqlDbType.Bit,1),
					new SqlParameter("@HardId", SqlDbType.Int,4)};
            parameters[0].Value = model.DepartId;
            parameters[1].Value = model.Mac;
            parameters[2].Value = model.HardStr;
            parameters[3].Value = model.HardState;
            parameters[4].Value = model.Ua;
            parameters[5].Value = model.Browser;
            parameters[6].Value = model.BrowserVersion;
            parameters[7].Value = model.Os;
            parameters[8].Value = model.ScreenWidth;
            parameters[9].Value = model.ScreenHeight;
            parameters[10].Value = model.HardType;
            parameters[11].Value = model.LastLoginUserId;
            parameters[12].Value = model.LastLoginUser;
            parameters[13].Value = model.LastLoginTime;
            parameters[14].Value = model.LastLoginIp;
            parameters[15].Value = model.LastLoginDepartId;
            parameters[16].Value = model.AddUserId;
            parameters[17].Value = model.AddUser;
            parameters[18].Value = model.AddDepartId;
            parameters[19].Value = model.AddTime;
            parameters[20].Value = model.AddIp;
            parameters[21].Value = model.LastUserId;
            parameters[22].Value = model.LastUser;
            parameters[23].Value = model.LastDepartId;
            parameters[24].Value = model.LastTime;
            parameters[25].Value = model.LastIp;
            parameters[26].Value = model.Mem;
            parameters[27].Value = model.IsDelete;
            parameters[28].Value = model.HardId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int HardId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Org_Hards ");
            strSql.Append(" where HardId=@HardId");
            SqlParameter[] parameters = {
					new SqlParameter("@HardId", SqlDbType.Int,4)
			};
            parameters[0].Value = HardId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string HardIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Org_Hards ");
            strSql.Append(" where HardId in (" + HardIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Org_Hards GetModel(int HardId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 HardId,DepartId,Mac,HardStr,HardState,Ua,Browser,BrowserVersion,Os,ScreenWidth,ScreenHeight,HardType,LastLoginUserId,LastLoginUser,LastLoginTime,LastLoginIp,LastLoginDepartId,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,IsDelete from Org_Hards ");
            strSql.Append(" where HardId=@HardId");
            SqlParameter[] parameters = {
					new SqlParameter("@HardId", SqlDbType.Int,4)
			};
            parameters[0].Value = HardId;

            xundh.Model.Org_Hards model = new xundh.Model.Org_Hards();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Org_Hards DataRowToModel(DataRow row)
        {
            xundh.Model.Org_Hards model = new xundh.Model.Org_Hards();
            if (row != null)
            {
                if (row["HardId"] != null && row["HardId"].ToString() != "")
                {
                    model.HardId = int.Parse(row["HardId"].ToString());
                }
                if (row["DepartId"] != null && row["DepartId"].ToString() != "")
                {
                    model.DepartId = int.Parse(row["DepartId"].ToString());
                }
                if (row["Mac"] != null)
                {
                    model.Mac = row["Mac"].ToString();
                }
                if (row["HardStr"] != null)
                {
                    model.HardStr = row["HardStr"].ToString();
                }
                if (row["HardState"] != null && row["HardState"].ToString() != "")
                {
                    if ((row["HardState"].ToString() == "1") || (row["HardState"].ToString().ToLower() == "true"))
                    {
                        model.HardState = true;
                    }
                    else
                    {
                        model.HardState = false;
                    }
                }
                if (row["Ua"] != null)
                {
                    model.Ua = row["Ua"].ToString();
                }
                if (row["Browser"] != null)
                {
                    model.Browser = row["Browser"].ToString();
                }
                if (row["BrowserVersion"] != null)
                {
                    model.BrowserVersion = row["BrowserVersion"].ToString();
                }
                if (row["Os"] != null)
                {
                    model.Os = row["Os"].ToString();
                }
                if (row["ScreenWidth"] != null && row["ScreenWidth"].ToString() != "")
                {
                    model.ScreenWidth = int.Parse(row["ScreenWidth"].ToString());
                }
                if (row["ScreenHeight"] != null && row["ScreenHeight"].ToString() != "")
                {
                    model.ScreenHeight = int.Parse(row["ScreenHeight"].ToString());
                }
                if (row["HardType"] != null)
                {
                    model.HardType = row["HardType"].ToString();
                }
                if (row["LastLoginUserId"] != null && row["LastLoginUserId"].ToString() != "")
                {
                    model.LastLoginUserId = int.Parse(row["LastLoginUserId"].ToString());
                }
                if (row["LastLoginUser"] != null)
                {
                    model.LastLoginUser = row["LastLoginUser"].ToString();
                }
                if (row["LastLoginTime"] != null && row["LastLoginTime"].ToString() != "")
                {
                    model.LastLoginTime = DateTime.Parse(row["LastLoginTime"].ToString());
                }
                if (row["LastLoginIp"] != null)
                {
                    model.LastLoginIp = row["LastLoginIp"].ToString();
                }
                if (row["LastLoginDepartId"] != null && row["LastLoginDepartId"].ToString() != "")
                {
                    model.LastLoginDepartId = int.Parse(row["LastLoginDepartId"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
                if (row["IsDelete"] != null && row["IsDelete"].ToString() != "")
                {
                    if ((row["IsDelete"].ToString() == "1") || (row["IsDelete"].ToString().ToLower() == "true"))
                    {
                        model.IsDelete = true;
                    }
                    else
                    {
                        model.IsDelete = false;
                    }
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select HardId,DepartId,Mac,HardStr,HardState,Ua,Browser,BrowserVersion,Os,ScreenWidth,ScreenHeight,HardType,LastLoginUserId,LastLoginUser,LastLoginTime,LastLoginIp,LastLoginDepartId,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,IsDelete ");
            strSql.Append(" FROM Org_Hards ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" HardId,DepartId,Mac,HardStr,HardState,Ua,Browser,BrowserVersion,Os,ScreenWidth,ScreenHeight,HardType,LastLoginUserId,LastLoginUser,LastLoginTime,LastLoginIp,LastLoginDepartId,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,IsDelete ");
            strSql.Append(" FROM Org_Hards ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Org_Hards ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.HardId desc");
            }
            strSql.Append(")AS Row, T.*  from Org_Hards T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Org_Hards";
            parameters[1].Value = "HardId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

