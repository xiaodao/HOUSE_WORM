﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Org_Departs
    /// </summary>
    public partial class Org_Departs
    {
        public Org_Departs()
        { }
        #region  BasicMethod
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int DepartId)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from Org_Departs");
            strSql.Append(" where DepartId=@DepartId");
            SqlParameter[] parameters = {
					new SqlParameter("@DepartId", SqlDbType.Int,4)
			};
            parameters[0].Value = DepartId;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(xundh.Model.Org_Departs model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into Org_Departs(");
            strSql.Append("ParentId,DepartName,DepartNameEn,IsDelete,IsVirtual,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,Telphone)");
            strSql.Append(" values (");
            strSql.Append("@ParentId,@DepartName,@DepartNameEn,@IsDelete,@IsVirtual,@AddUserId,@AddUser,@AddDepartId,@AddTime,@AddIp,@LastUserId,@LastUser,@LastDepartId,@LastTime,@LastIp,@Mem,@Telphone)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@ParentId", SqlDbType.Int,4),
					new SqlParameter("@DepartName", SqlDbType.NVarChar,256),
					new SqlParameter("@DepartNameEn", SqlDbType.NVarChar,32),
					new SqlParameter("@IsDelete", SqlDbType.Bit,1),
					new SqlParameter("@IsVirtual", SqlDbType.Bit,1),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@Telphone", SqlDbType.NVarChar,32)};
            parameters[0].Value = model.ParentId;
            parameters[1].Value = model.DepartName;
            parameters[2].Value = model.DepartNameEn;
            parameters[3].Value = model.IsDelete;
            parameters[4].Value = model.IsVirtual;
            parameters[5].Value = model.AddUserId;
            parameters[6].Value = model.AddUser;
            parameters[7].Value = model.AddDepartId;
            parameters[8].Value = model.AddTime;
            parameters[9].Value = model.AddIp;
            parameters[10].Value = model.LastUserId;
            parameters[11].Value = model.LastUser;
            parameters[12].Value = model.LastDepartId;
            parameters[13].Value = model.LastTime;
            parameters[14].Value = model.LastIp;
            parameters[15].Value = model.Mem;
            parameters[16].Value = model.Telphone;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Org_Departs model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update Org_Departs set ");
            strSql.Append("ParentId=@ParentId,");
            strSql.Append("DepartName=@DepartName,");
            strSql.Append("DepartNameEn=@DepartNameEn,");
            strSql.Append("IsDelete=@IsDelete,");
            strSql.Append("IsVirtual=@IsVirtual,");
            strSql.Append("AddUserId=@AddUserId,");
            strSql.Append("AddUser=@AddUser,");
            strSql.Append("AddDepartId=@AddDepartId,");
            strSql.Append("AddTime=@AddTime,");
            strSql.Append("AddIp=@AddIp,");
            strSql.Append("LastUserId=@LastUserId,");
            strSql.Append("LastUser=@LastUser,");
            strSql.Append("LastDepartId=@LastDepartId,");
            strSql.Append("LastTime=@LastTime,");
            strSql.Append("LastIp=@LastIp,");
            strSql.Append("Mem=@Mem,");
            strSql.Append("Telphone=@Telphone");
            strSql.Append(" where DepartId=@DepartId");
            SqlParameter[] parameters = {
					new SqlParameter("@ParentId", SqlDbType.Int,4),
					new SqlParameter("@DepartName", SqlDbType.NVarChar,256),
					new SqlParameter("@DepartNameEn", SqlDbType.NVarChar,32),
					new SqlParameter("@IsDelete", SqlDbType.Bit,1),
					new SqlParameter("@IsVirtual", SqlDbType.Bit,1),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@LastDepartId", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Mem", SqlDbType.NVarChar,1000),
					new SqlParameter("@Telphone", SqlDbType.NVarChar,32),
					new SqlParameter("@DepartId", SqlDbType.Int,4)};
            parameters[0].Value = model.ParentId;
            parameters[1].Value = model.DepartName;
            parameters[2].Value = model.DepartNameEn;
            parameters[3].Value = model.IsDelete;
            parameters[4].Value = model.IsVirtual;
            parameters[5].Value = model.AddUserId;
            parameters[6].Value = model.AddUser;
            parameters[7].Value = model.AddDepartId;
            parameters[8].Value = model.AddTime;
            parameters[9].Value = model.AddIp;
            parameters[10].Value = model.LastUserId;
            parameters[11].Value = model.LastUser;
            parameters[12].Value = model.LastDepartId;
            parameters[13].Value = model.LastTime;
            parameters[14].Value = model.LastIp;
            parameters[15].Value = model.Mem;
            parameters[16].Value = model.Telphone;
            parameters[17].Value = model.DepartId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int DepartId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Org_Departs ");
            strSql.Append(" where DepartId=@DepartId");
            SqlParameter[] parameters = {
					new SqlParameter("@DepartId", SqlDbType.Int,4)
			};
            parameters[0].Value = DepartId;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string DepartIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Org_Departs ");
            strSql.Append(" where DepartId in (" + DepartIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Org_Departs GetModel(int DepartId)
        {

            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 DepartId,ParentId,DepartName,DepartNameEn,IsDelete,IsVirtual,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,Telphone from Org_Departs ");
            strSql.Append(" where DepartId=@DepartId");
            SqlParameter[] parameters = {
					new SqlParameter("@DepartId", SqlDbType.Int,4)
			};
            parameters[0].Value = DepartId;

            xundh.Model.Org_Departs model = new xundh.Model.Org_Departs();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Org_Departs DataRowToModel(DataRow row)
        {
            xundh.Model.Org_Departs model = new xundh.Model.Org_Departs();
            if (row != null)
            {
                if (row["DepartId"] != null && row["DepartId"].ToString() != "")
                {
                    model.DepartId = int.Parse(row["DepartId"].ToString());
                }
                if (row["ParentId"] != null && row["ParentId"].ToString() != "")
                {
                    model.ParentId = int.Parse(row["ParentId"].ToString());
                }
                if (row["DepartName"] != null)
                {
                    model.DepartName = row["DepartName"].ToString();
                }
                if (row["DepartNameEn"] != null)
                {
                    model.DepartNameEn = row["DepartNameEn"].ToString();
                }
                if (row["IsDelete"] != null && row["IsDelete"].ToString() != "")
                {
                    if ((row["IsDelete"].ToString() == "1") || (row["IsDelete"].ToString().ToLower() == "true"))
                    {
                        model.IsDelete = true;
                    }
                    else
                    {
                        model.IsDelete = false;
                    }
                }
                if (row["IsVirtual"] != null && row["IsVirtual"].ToString() != "")
                {
                    if ((row["IsVirtual"].ToString() == "1") || (row["IsVirtual"].ToString().ToLower() == "true"))
                    {
                        model.IsVirtual = true;
                    }
                    else
                    {
                        model.IsVirtual = false;
                    }
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["LastDepartId"] != null && row["LastDepartId"].ToString() != "")
                {
                    model.LastDepartId = int.Parse(row["LastDepartId"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastIp"] != null)
                {
                    model.LastIp = row["LastIp"].ToString();
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
                if (row["Telphone"] != null)
                {
                    model.Telphone = row["Telphone"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select DepartId,ParentId,DepartName,DepartNameEn,IsDelete,IsVirtual,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,Telphone ");
            strSql.Append(" FROM Org_Departs ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" DepartId,ParentId,DepartName,DepartNameEn,IsDelete,IsVirtual,AddUserId,AddUser,AddDepartId,AddTime,AddIp,LastUserId,LastUser,LastDepartId,LastTime,LastIp,Mem,Telphone ");
            strSql.Append(" FROM Org_Departs ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Org_Departs ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.DepartId desc");
            }
            strSql.Append(")AS Row, T.*  from Org_Departs T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Org_Departs";
            parameters[1].Value = "DepartId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  BasicMethod
        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}

