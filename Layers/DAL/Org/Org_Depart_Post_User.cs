﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;
namespace xundh.DAL
{
	/// <summary>
	/// 数据访问类:Org_Depart_Post_User
	/// </summary>
	public partial class Org_Depart_Post_User
	{
		public Org_Depart_Post_User()
		{}
		#region  Method


		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int OrgDepartPostUserId)
		{
			int rowsAffected;
			SqlParameter[] parameters = {
					new SqlParameter("@OrgDepartPostUserId", SqlDbType.Int,4)
			};
			parameters[0].Value = OrgDepartPostUserId;

			int result= DbHelperSQL.RunProcedure("Org_Depart_Post_User_Exists",parameters,out rowsAffected);
			if(result==1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		///  增加一条数据
		/// </summary>
		public int Add(xundh.Model.Org_Depart_Post_User model)
		{
			int rowsAffected;
			SqlParameter[] parameters = {
					new SqlParameter("@OrgDepartPostUserId", SqlDbType.Int,4),
					new SqlParameter("@DepartsId", SqlDbType.Int,4),
					new SqlParameter("@PostId", SqlDbType.Int,4),
					new SqlParameter("@UserId", SqlDbType.Int,4)};
			parameters[0].Direction = ParameterDirection.Output;
			parameters[1].Value = model.DepartsId;
			parameters[2].Value = model.PostId;
			parameters[3].Value = model.UserId;

			DbHelperSQL.RunProcedure("Org_Depart_Post_User_ADD",parameters,out rowsAffected);
			return (int)parameters[0].Value;
		}

		/// <summary>
		///  更新一条数据
		/// </summary>
		public bool Update(xundh.Model.Org_Depart_Post_User model)
		{
			int rowsAffected=0;
			SqlParameter[] parameters = {
					new SqlParameter("@OrgDepartPostUserId", SqlDbType.Int,4),
					new SqlParameter("@DepartsId", SqlDbType.Int,4),
					new SqlParameter("@PostId", SqlDbType.Int,4),
					new SqlParameter("@UserId", SqlDbType.Int,4)};
			parameters[0].Value = model.OrgDepartPostUserId;
			parameters[1].Value = model.DepartsId;
			parameters[2].Value = model.PostId;
			parameters[3].Value = model.UserId;

			DbHelperSQL.RunProcedure("Org_Depart_Post_User_Update",parameters,out rowsAffected);
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int OrgDepartPostUserId)
		{
			int rowsAffected=0;
			SqlParameter[] parameters = {
					new SqlParameter("@OrgDepartPostUserId", SqlDbType.Int,4)
			};
			parameters[0].Value = OrgDepartPostUserId;

			DbHelperSQL.RunProcedure("Org_Depart_Post_User_Delete",parameters,out rowsAffected);
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string OrgDepartPostUserIdlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Org_Depart_Post_User ");
			strSql.Append(" where OrgDepartPostUserId in ("+OrgDepartPostUserIdlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.Org_Depart_Post_User GetModel(int OrgDepartPostUserId)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@OrgDepartPostUserId", SqlDbType.Int,4)
			};
			parameters[0].Value = OrgDepartPostUserId;

			xundh.Model.Org_Depart_Post_User model=new xundh.Model.Org_Depart_Post_User();
			DataSet ds= DbHelperSQL.RunProcedure("Org_Depart_Post_User_GetModel",parameters,"ds");
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.Org_Depart_Post_User DataRowToModel(DataRow row)
		{
			xundh.Model.Org_Depart_Post_User model=new xundh.Model.Org_Depart_Post_User();
			if (row != null)
			{
				if(row["OrgDepartPostUserId"]!=null && row["OrgDepartPostUserId"].ToString()!="")
				{
					model.OrgDepartPostUserId=int.Parse(row["OrgDepartPostUserId"].ToString());
				}
				if(row["DepartsId"]!=null && row["DepartsId"].ToString()!="")
				{
					model.DepartsId=int.Parse(row["DepartsId"].ToString());
				}
				if(row["PostId"]!=null && row["PostId"].ToString()!="")
				{
					model.PostId=int.Parse(row["PostId"].ToString());
				}
				if(row["UserId"]!=null && row["UserId"].ToString()!="")
				{
					model.UserId=int.Parse(row["UserId"].ToString());
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select OrgDepartPostUserId,DepartsId,PostId,UserId ");
			strSql.Append(" FROM Org_Depart_Post_User ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" OrgDepartPostUserId,DepartsId,PostId,UserId ");
			strSql.Append(" FROM Org_Depart_Post_User ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Org_Depart_Post_User ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.OrgDepartPostUserId desc");
			}
			strSql.Append(")AS Row, T.*  from Org_Depart_Post_User T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Org_Depart_Post_User";
			parameters[1].Value = "OrgDepartPostUserId";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
		#region  MethodEx

		#endregion  MethodEx
	}
}

