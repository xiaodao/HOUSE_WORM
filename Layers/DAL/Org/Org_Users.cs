﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Org_Users
    /// </summary>
    public partial class Org_Users
    {
        public Org_Users()
        { }
        #region  Method

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("UserId", "Org_Users");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int UserId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4)			};
            parameters[0].Value = UserId;

            int result = DbHelperSQL.RunProcedure("Org_Users_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public bool Add(xundh.Model.Org_Users model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4),
					new SqlParameter("@School", SqlDbType.NVarChar,512),
					new SqlParameter("@Thumb", SqlDbType.NVarChar,512),
					new SqlParameter("@BirthDay", SqlDbType.DateTime),
					new SqlParameter("@Education", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@EnterDeate", SqlDbType.DateTime),
					new SqlParameter("@LevelDate", SqlDbType.DateTime),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@Marry", SqlDbType.Int,4),
					new SqlParameter("@HomeAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@Mem", SqlDbType.NVarChar,4000)};
            parameters[0].Value = model.UserId;
            parameters[1].Value = model.School;
            parameters[2].Value = model.Thumb;
            parameters[3].Value = model.BirthDay;
            parameters[4].Value = model.Education;
            parameters[5].Value = model.LastTime;
            parameters[6].Value = model.LastUserId;
            parameters[7].Value = model.LastUser;
            parameters[8].Value = model.EnterDeate;
            parameters[9].Value = model.LevelDate;
            parameters[10].Value = model.State;
            parameters[11].Value = model.Marry;
            parameters[12].Value = model.HomeAddress;
            parameters[13].Value = model.Mem;

          return  DbHelperSQL.RunProcedure("Org_Users_ADD", parameters, out rowsAffected)>0;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Org_Users model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4),
					new SqlParameter("@School", SqlDbType.NVarChar,512),
					new SqlParameter("@Thumb", SqlDbType.NVarChar,512),
					new SqlParameter("@BirthDay", SqlDbType.DateTime),
					new SqlParameter("@Education", SqlDbType.Int,4),
					new SqlParameter("@LastTime", SqlDbType.DateTime),
					new SqlParameter("@LastUserId", SqlDbType.Int,4),
					new SqlParameter("@LastUser", SqlDbType.NVarChar,64),
					new SqlParameter("@EnterDeate", SqlDbType.DateTime),
					new SqlParameter("@LevelDate", SqlDbType.DateTime),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@Marry", SqlDbType.Int,4),
					new SqlParameter("@HomeAddress", SqlDbType.NVarChar,512),
					new SqlParameter("@Mem", SqlDbType.NVarChar,4000)};
            parameters[0].Value = model.UserId;
            parameters[1].Value = model.School;
            parameters[2].Value = model.Thumb;
            parameters[3].Value = model.BirthDay;
            parameters[4].Value = model.Education;
            parameters[5].Value = model.LastTime;
            parameters[6].Value = model.LastUserId;
            parameters[7].Value = model.LastUser;
            parameters[8].Value = model.EnterDeate;
            parameters[9].Value = model.LevelDate;
            parameters[10].Value = model.State;
            parameters[11].Value = model.Marry;
            parameters[12].Value = model.HomeAddress;
            parameters[13].Value = model.Mem;

            DbHelperSQL.RunProcedure("Org_Users_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int UserId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4)			};
            parameters[0].Value = UserId;

            DbHelperSQL.RunProcedure("Org_Users_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string UserIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Org_Users ");
            strSql.Append(" where UserId in (" + UserIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Org_Users GetModel(int UserId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@UserId", SqlDbType.Int,4)			};
            parameters[0].Value = UserId;

            xundh.Model.Org_Users model = new xundh.Model.Org_Users();
            DataSet ds = DbHelperSQL.RunProcedure("Org_Users_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Org_Users DataRowToModel(DataRow row)
        {
            xundh.Model.Org_Users model = new xundh.Model.Org_Users();
            if (row != null)
            {
                if (row["UserId"] != null && row["UserId"].ToString() != "")
                {
                    model.UserId = int.Parse(row["UserId"].ToString());
                }
                if (row["School"] != null)
                {
                    model.School = row["School"].ToString();
                }
                if (row["Thumb"] != null)
                {
                    model.Thumb = row["Thumb"].ToString();
                }
                if (row["BirthDay"] != null && row["BirthDay"].ToString() != "")
                {
                    model.BirthDay = DateTime.Parse(row["BirthDay"].ToString());
                }
                if (row["Education"] != null && row["Education"].ToString() != "")
                {
                    model.Education = int.Parse(row["Education"].ToString());
                }
                if (row["LastTime"] != null && row["LastTime"].ToString() != "")
                {
                    model.LastTime = DateTime.Parse(row["LastTime"].ToString());
                }
                if (row["LastUserId"] != null && row["LastUserId"].ToString() != "")
                {
                    model.LastUserId = int.Parse(row["LastUserId"].ToString());
                }
                if (row["LastUser"] != null)
                {
                    model.LastUser = row["LastUser"].ToString();
                }
                if (row["EnterDeate"] != null && row["EnterDeate"].ToString() != "")
                {
                    model.EnterDeate = DateTime.Parse(row["EnterDeate"].ToString());
                }
                if (row["LevelDate"] != null && row["LevelDate"].ToString() != "")
                {
                    model.LevelDate = DateTime.Parse(row["LevelDate"].ToString());
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["Marry"] != null && row["Marry"].ToString() != "")
                {
                    model.Marry = int.Parse(row["Marry"].ToString());
                }
                if (row["HomeAddress"] != null)
                {
                    model.HomeAddress = row["HomeAddress"].ToString();
                }
                if (row["Mem"] != null)
                {
                    model.Mem = row["Mem"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select UserId,School,Thumb,BirthDay,Education,LastTime,LastUserId,LastUser,EnterDeate,LevelDate,State,Marry,HomeAddress,Mem ");
            strSql.Append(" FROM Org_Users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" UserId,School,Thumb,BirthDay,Education,LastTime,LastUserId,LastUser,EnterDeate,LevelDate,State,Marry,HomeAddress,Mem ");
            strSql.Append(" FROM Org_Users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Org_Users ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.UserId desc");
            }
            strSql.Append(")AS Row, T.*  from Org_Users T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Org_Users";
            parameters[1].Value = "UserId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

