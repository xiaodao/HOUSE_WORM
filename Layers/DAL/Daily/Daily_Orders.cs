﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Daily_Orders
    /// </summary>
    public partial class Daily_Orders
    {
        public Daily_Orders()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OrderId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.Int,4)
			};
            parameters[0].Value = OrderId;

            int result = DbHelperSQL.RunProcedure("Daily_Orders_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Daily_Orders model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@OrderBeginTime", SqlDbType.DateTime),
					new SqlParameter("@OrderEndTime", SqlDbType.DateTime),
					new SqlParameter("@OrderUserId", SqlDbType.Int,4),
					new SqlParameter("@OrderUser", SqlDbType.NVarChar,64),
					new SqlParameter("@OrderDepartId", SqlDbType.Int,4),
					new SqlParameter("@OrderArea", SqlDbType.Int,4),
					new SqlParameter("@Address", SqlDbType.NVarChar,4000),
					new SqlParameter("@States", SqlDbType.Int,4),
					new SqlParameter("@Matter", SqlDbType.NVarChar,4000),
					new SqlParameter("@Needs", SqlDbType.NVarChar,4000),
					new SqlParameter("@BusinessType", SqlDbType.VarChar,32),
					new SqlParameter("@CMId", SqlDbType.Int,4),
					new SqlParameter("@ContractType", SqlDbType.Int,4),
					new SqlParameter("@CMNumber", SqlDbType.NVarChar,32)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.AddUserId;
            parameters[2].Value = model.AddDepartId;
            parameters[3].Value = model.AddUser;
            parameters[4].Value = model.AddTime;
            parameters[5].Value = model.OrderBeginTime;
            parameters[6].Value = model.OrderEndTime;
            parameters[7].Value = model.OrderUserId;
            parameters[8].Value = model.OrderUser;
            parameters[9].Value = model.OrderDepartId;
            parameters[10].Value = model.OrderArea;
            parameters[11].Value = model.Address;
            parameters[12].Value = model.States;
            parameters[13].Value = model.Matter;
            parameters[14].Value = model.Needs;
            parameters[15].Value = model.BusinessType;
            parameters[16].Value = model.CMId;
            parameters[17].Value = model.ContractType;
            parameters[18].Value = model.CMNumber;

            DbHelperSQL.RunProcedure("Daily_Orders_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Daily_Orders model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@OrderBeginTime", SqlDbType.DateTime),
					new SqlParameter("@OrderEndTime", SqlDbType.DateTime),
					new SqlParameter("@OrderUserId", SqlDbType.Int,4),
					new SqlParameter("@OrderUser", SqlDbType.NVarChar,64),
					new SqlParameter("@OrderDepartId", SqlDbType.Int,4),
					new SqlParameter("@OrderArea", SqlDbType.Int,4),
					new SqlParameter("@Address", SqlDbType.NVarChar,4000),
					new SqlParameter("@States", SqlDbType.Int,4),
					new SqlParameter("@Matter", SqlDbType.NVarChar,4000),
					new SqlParameter("@Needs", SqlDbType.NVarChar,4000),
					new SqlParameter("@BusinessType", SqlDbType.VarChar,32),
					new SqlParameter("@CMId", SqlDbType.Int,4),
					new SqlParameter("@ContractType", SqlDbType.Int,4),
					new SqlParameter("@CMNumber", SqlDbType.NVarChar,32)};
            parameters[0].Value = model.OrderId;
            parameters[1].Value = model.AddUserId;
            parameters[2].Value = model.AddDepartId;
            parameters[3].Value = model.AddUser;
            parameters[4].Value = model.AddTime;
            parameters[5].Value = model.OrderBeginTime;
            parameters[6].Value = model.OrderEndTime;
            parameters[7].Value = model.OrderUserId;
            parameters[8].Value = model.OrderUser;
            parameters[9].Value = model.OrderDepartId;
            parameters[10].Value = model.OrderArea;
            parameters[11].Value = model.Address;
            parameters[12].Value = model.States;
            parameters[13].Value = model.Matter;
            parameters[14].Value = model.Needs;
            parameters[15].Value = model.BusinessType;
            parameters[16].Value = model.CMId;
            parameters[17].Value = model.ContractType;
            parameters[18].Value = model.CMNumber;

            DbHelperSQL.RunProcedure("Daily_Orders_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OrderId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.Int,4)
			};
            parameters[0].Value = OrderId;

            DbHelperSQL.RunProcedure("Daily_Orders_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OrderIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Daily_Orders ");
            strSql.Append(" where OrderId in (" + OrderIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Daily_Orders GetModel(int OrderId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@OrderId", SqlDbType.Int,4)
			};
            parameters[0].Value = OrderId;

            xundh.Model.Daily_Orders model = new xundh.Model.Daily_Orders();
            DataSet ds = DbHelperSQL.RunProcedure("Daily_Orders_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Daily_Orders DataRowToModel(DataRow row)
        {
            xundh.Model.Daily_Orders model = new xundh.Model.Daily_Orders();
            if (row != null)
            {
                if (row["OrderId"] != null && row["OrderId"].ToString() != "")
                {
                    model.OrderId = int.Parse(row["OrderId"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["OrderBeginTime"] != null && row["OrderBeginTime"].ToString() != "")
                {
                    model.OrderBeginTime = DateTime.Parse(row["OrderBeginTime"].ToString());
                }
                if (row["OrderEndTime"] != null && row["OrderEndTime"].ToString() != "")
                {
                    model.OrderEndTime = DateTime.Parse(row["OrderEndTime"].ToString());
                }
                if (row["OrderUserId"] != null && row["OrderUserId"].ToString() != "")
                {
                    model.OrderUserId = int.Parse(row["OrderUserId"].ToString());
                }
                if (row["OrderUser"] != null)
                {
                    model.OrderUser = row["OrderUser"].ToString();
                }
                if (row["OrderDepartId"] != null && row["OrderDepartId"].ToString() != "")
                {
                    model.OrderDepartId = int.Parse(row["OrderDepartId"].ToString());
                }
                if (row["OrderArea"] != null && row["OrderArea"].ToString() != "")
                {
                    model.OrderArea = int.Parse(row["OrderArea"].ToString());
                }
                if (row["Address"] != null)
                {
                    model.Address = row["Address"].ToString();
                }
                if (row["States"] != null && row["States"].ToString() != "")
                {
                    model.States = int.Parse(row["States"].ToString());
                }
                if (row["Matter"] != null)
                {
                    model.Matter = row["Matter"].ToString();
                }
                if (row["Needs"] != null)
                {
                    model.Needs = row["Needs"].ToString();
                }
                if (row["BusinessType"] != null)
                {
                    model.BusinessType = row["BusinessType"].ToString();
                }
                if (row["CMId"] != null && row["CMId"].ToString() != "")
                {
                    model.CMId = int.Parse(row["CMId"].ToString());
                }
                if (row["ContractType"] != null && row["ContractType"].ToString() != "")
                {
                    model.ContractType = int.Parse(row["ContractType"].ToString());
                }
                if (row["CMNumber"] != null)
                {
                    model.CMNumber = row["CMNumber"].ToString();
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OrderId,AddUserId,AddDepartId,AddUser,AddTime,OrderBeginTime,OrderEndTime,OrderUserId,OrderUser,OrderDepartId,OrderArea,Address,States,Matter,Needs,BusinessType,CMId,ContractType,CMNumber ");
            strSql.Append(" FROM Daily_Orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OrderId,AddUserId,AddDepartId,AddUser,AddTime,OrderBeginTime,OrderEndTime,OrderUserId,OrderUser,OrderDepartId,OrderArea,Address,States,Matter,Needs,BusinessType,CMId,ContractType,CMNumber ");
            strSql.Append(" FROM Daily_Orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Daily_Orders ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OrderId desc");
            }
            strSql.Append(")AS Row, T.*  from Daily_Orders T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Daily_Orders";
            parameters[1].Value = "OrderId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

