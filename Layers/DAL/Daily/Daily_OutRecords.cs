﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
    /// <summary>
    /// 数据访问类:Daily_OutRecords
    /// </summary>
    public partial class Daily_OutRecords
    {
        public Daily_OutRecords()
        { }
        #region  Method
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int OutRecordId)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@OutRecordId", SqlDbType.Int,4)
			};
            parameters[0].Value = OutRecordId;

            int result = DbHelperSQL.RunProcedure("Daily_OutRecords_Exists", parameters, out rowsAffected);
            if (result == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///  增加一条数据
        /// </summary>
        public int Add(xundh.Model.Daily_OutRecords model)
        {
            int rowsAffected;
            SqlParameter[] parameters = {
					new SqlParameter("@OutRecordId", SqlDbType.Int,4),
					new SqlParameter("@BusinessType", SqlDbType.NVarChar,32),
					new SqlParameter("@HouseType", SqlDbType.Int,4),
					new SqlParameter("@HouseId", SqlDbType.NVarChar,2000),
					new SqlParameter("@HouseBuilds", SqlDbType.NVarChar,4000),
					new SqlParameter("@CRMType", SqlDbType.Int,4),
					new SqlParameter("@CRMIds", SqlDbType.NVarChar,2000),
					new SqlParameter("@CRMNames", SqlDbType.NVarChar,4000),
					new SqlParameter("@AgreementType", SqlDbType.Int,4),
					new SqlParameter("@AgreementIds", SqlDbType.NVarChar,2000),
					new SqlParameter("@Address", SqlDbType.NVarChar,4000),
					new SqlParameter("@DoThins", SqlDbType.NVarChar,4000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AccompanyIds", SqlDbType.NVarChar,2000),
					new SqlParameter("@AccompanyNames", SqlDbType.NVarChar,4000),
					new SqlParameter("@BackTime", SqlDbType.DateTime)};
            parameters[0].Direction = ParameterDirection.Output;
            parameters[1].Value = model.BusinessType;
            parameters[2].Value = model.HouseType;
            parameters[3].Value = model.HouseId;
            parameters[4].Value = model.HouseBuilds;
            parameters[5].Value = model.CRMType;
            parameters[6].Value = model.CRMIds;
            parameters[7].Value = model.CRMNames;
            parameters[8].Value = model.AgreementType;
            parameters[9].Value = model.AgreementIds;
            parameters[10].Value = model.Address;
            parameters[11].Value = model.DoThins;
            parameters[12].Value = model.State;
            parameters[13].Value = model.AddUserId;
            parameters[14].Value = model.AddUser;
            parameters[15].Value = model.AddTime;
            parameters[16].Value = model.AddIp;
            parameters[17].Value = model.AddDepartId;
            parameters[18].Value = model.AccompanyIds;
            parameters[19].Value = model.AccompanyNames;
            parameters[20].Value = model.BackTime;

            DbHelperSQL.RunProcedure("Daily_OutRecords_ADD", parameters, out rowsAffected);
            return (int)parameters[0].Value;
        }

        /// <summary>
        ///  更新一条数据
        /// </summary>
        public bool Update(xundh.Model.Daily_OutRecords model)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@OutRecordId", SqlDbType.Int,4),
					new SqlParameter("@BusinessType", SqlDbType.NVarChar,32),
					new SqlParameter("@HouseType", SqlDbType.Int,4),
					new SqlParameter("@HouseId", SqlDbType.NVarChar,2000),
					new SqlParameter("@HouseBuilds", SqlDbType.NVarChar,4000),
					new SqlParameter("@CRMType", SqlDbType.Int,4),
					new SqlParameter("@CRMIds", SqlDbType.NVarChar,2000),
					new SqlParameter("@CRMNames", SqlDbType.NVarChar,4000),
					new SqlParameter("@AgreementType", SqlDbType.Int,4),
					new SqlParameter("@AgreementIds", SqlDbType.NVarChar,2000),
					new SqlParameter("@Address", SqlDbType.NVarChar,4000),
					new SqlParameter("@DoThins", SqlDbType.NVarChar,4000),
					new SqlParameter("@State", SqlDbType.Int,4),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUser", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AccompanyIds", SqlDbType.NVarChar,2000),
					new SqlParameter("@AccompanyNames", SqlDbType.NVarChar,4000),
					new SqlParameter("@BackTime", SqlDbType.DateTime)};
            parameters[0].Value = model.OutRecordId;
            parameters[1].Value = model.BusinessType;
            parameters[2].Value = model.HouseType;
            parameters[3].Value = model.HouseId;
            parameters[4].Value = model.HouseBuilds;
            parameters[5].Value = model.CRMType;
            parameters[6].Value = model.CRMIds;
            parameters[7].Value = model.CRMNames;
            parameters[8].Value = model.AgreementType;
            parameters[9].Value = model.AgreementIds;
            parameters[10].Value = model.Address;
            parameters[11].Value = model.DoThins;
            parameters[12].Value = model.State;
            parameters[13].Value = model.AddUserId;
            parameters[14].Value = model.AddUser;
            parameters[15].Value = model.AddTime;
            parameters[16].Value = model.AddIp;
            parameters[17].Value = model.AddDepartId;
            parameters[18].Value = model.AccompanyIds;
            parameters[19].Value = model.AccompanyNames;
            parameters[20].Value = model.BackTime;

            DbHelperSQL.RunProcedure("Daily_OutRecords_Update", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int OutRecordId)
        {
            int rowsAffected = 0;
            SqlParameter[] parameters = {
					new SqlParameter("@OutRecordId", SqlDbType.Int,4)
			};
            parameters[0].Value = OutRecordId;

            DbHelperSQL.RunProcedure("Daily_OutRecords_Delete", parameters, out rowsAffected);
            if (rowsAffected > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string OutRecordIdlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from Daily_OutRecords ");
            strSql.Append(" where OutRecordId in (" + OutRecordIdlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Daily_OutRecords GetModel(int OutRecordId)
        {
            SqlParameter[] parameters = {
					new SqlParameter("@OutRecordId", SqlDbType.Int,4)
			};
            parameters[0].Value = OutRecordId;

            xundh.Model.Daily_OutRecords model = new xundh.Model.Daily_OutRecords();
            DataSet ds = DbHelperSQL.RunProcedure("Daily_OutRecords_GetModel", parameters, "ds");
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public xundh.Model.Daily_OutRecords DataRowToModel(DataRow row)
        {
            xundh.Model.Daily_OutRecords model = new xundh.Model.Daily_OutRecords();
            if (row != null)
            {
                if (row["OutRecordId"] != null && row["OutRecordId"].ToString() != "")
                {
                    model.OutRecordId = int.Parse(row["OutRecordId"].ToString());
                }
                if (row["BusinessType"] != null)
                {
                    model.BusinessType = row["BusinessType"].ToString();
                }
                if (row["HouseType"] != null && row["HouseType"].ToString() != "")
                {
                    model.HouseType = int.Parse(row["HouseType"].ToString());
                }
                if (row["HouseId"] != null)
                {
                    model.HouseId = row["HouseId"].ToString();
                }
                if (row["HouseBuilds"] != null)
                {
                    model.HouseBuilds = row["HouseBuilds"].ToString();
                }
                if (row["CRMType"] != null && row["CRMType"].ToString() != "")
                {
                    model.CRMType = int.Parse(row["CRMType"].ToString());
                }
                if (row["CRMIds"] != null)
                {
                    model.CRMIds = row["CRMIds"].ToString();
                }
                if (row["CRMNames"] != null)
                {
                    model.CRMNames = row["CRMNames"].ToString();
                }
                if (row["AgreementType"] != null && row["AgreementType"].ToString() != "")
                {
                    model.AgreementType = int.Parse(row["AgreementType"].ToString());
                }
                if (row["AgreementIds"] != null)
                {
                    model.AgreementIds = row["AgreementIds"].ToString();
                }
                if (row["Address"] != null)
                {
                    model.Address = row["Address"].ToString();
                }
                if (row["DoThins"] != null)
                {
                    model.DoThins = row["DoThins"].ToString();
                }
                if (row["State"] != null && row["State"].ToString() != "")
                {
                    model.State = int.Parse(row["State"].ToString());
                }
                if (row["AddUserId"] != null && row["AddUserId"].ToString() != "")
                {
                    model.AddUserId = int.Parse(row["AddUserId"].ToString());
                }
                if (row["AddUser"] != null)
                {
                    model.AddUser = row["AddUser"].ToString();
                }
                if (row["AddTime"] != null && row["AddTime"].ToString() != "")
                {
                    model.AddTime = DateTime.Parse(row["AddTime"].ToString());
                }
                if (row["AddIp"] != null)
                {
                    model.AddIp = row["AddIp"].ToString();
                }
                if (row["AddDepartId"] != null && row["AddDepartId"].ToString() != "")
                {
                    model.AddDepartId = int.Parse(row["AddDepartId"].ToString());
                }
                if (row["AccompanyIds"] != null)
                {
                    model.AccompanyIds = row["AccompanyIds"].ToString();
                }
                if (row["AccompanyNames"] != null)
                {
                    model.AccompanyNames = row["AccompanyNames"].ToString();
                }
                if (row["BackTime"] != null && row["BackTime"].ToString() != "")
                {
                    model.BackTime = DateTime.Parse(row["BackTime"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select OutRecordId,BusinessType,HouseType,HouseId,HouseBuilds,CRMType,CRMIds,CRMNames,AgreementType,AgreementIds,Address,DoThins,State,AddUserId,AddUser,AddTime,AddIp,AddDepartId,AccompanyIds,AccompanyNames,BackTime ");
            strSql.Append(" FROM Daily_OutRecords ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" OutRecordId,BusinessType,HouseType,HouseId,HouseBuilds,CRMType,CRMIds,CRMNames,AgreementType,AgreementIds,Address,DoThins,State,AddUserId,AddUser,AddTime,AddIp,AddDepartId,AccompanyIds,AccompanyNames,BackTime ");
            strSql.Append(" FROM Daily_OutRecords ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM Daily_OutRecords ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.OutRecordId desc");
            }
            strSql.Append(")AS Row, T.*  from Daily_OutRecords T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /*
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize,int PageIndex,string strWhere)
        {
            SqlParameter[] parameters = {
                    new SqlParameter("@tblName", SqlDbType.VarChar, 255),
                    new SqlParameter("@fldName", SqlDbType.VarChar, 255),
                    new SqlParameter("@PageSize", SqlDbType.Int),
                    new SqlParameter("@PageIndex", SqlDbType.Int),
                    new SqlParameter("@IsReCount", SqlDbType.Bit),
                    new SqlParameter("@OrderType", SqlDbType.Bit),
                    new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
                    };
            parameters[0].Value = "Daily_OutRecords";
            parameters[1].Value = "OutRecordId";
            parameters[2].Value = PageSize;
            parameters[3].Value = PageIndex;
            parameters[4].Value = 0;
            parameters[5].Value = 0;
            parameters[6].Value = strWhere;	
            return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
        }*/

        #endregion  Method
        #region  MethodEx

        #endregion  MethodEx
    }
}

