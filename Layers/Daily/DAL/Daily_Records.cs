﻿/**  版本信息模板在安装目录下，可自行修改。
* Daily_Records.cs
*
* 功 能： N/A
* 类 名： Daily_Records
*
* Ver    变更日期             负责人  变更内容
* ───────────────────────────────────
* V0.01  14/2/22 14:29:35   N/A    初版
*
* Copyright (c) 2012 Maticsoft Corporation. All rights reserved.
*┌──────────────────────────────────┐
*│　此技术信息为本公司机密信息，未经本公司书面同意禁止向第三方披露．　│
*│　版权所有：动软卓越（北京）科技有限公司　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using Maticsoft.DBUtility;//Please add references
namespace xundh.DAL
{
	/// <summary>
	/// 数据访问类:Daily_Records
	/// </summary>
	public partial class Daily_Records
	{
		public Daily_Records()
		{}
		#region  Method

		
		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int id)
		{
			int rowsAffected;
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			int result= DbHelperSQL.RunProcedure("Daily_Records_Exists",parameters,out rowsAffected);
			if(result==1)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		///  增加一条数据
		/// </summary>
		public int Add(xundh.Model.Daily_Records model)
		{
			int rowsAffected;
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4),
					new SqlParameter("@OrganizatioinId", SqlDbType.Int,4),
					new SqlParameter("@Type", SqlDbType.NVarChar,32),
					new SqlParameter("@DailyDate", SqlDbType.DateTime),
					new SqlParameter("@Contents", SqlDbType.NText),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUserName", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddDepart", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastCommentUserId", SqlDbType.Int,4),
					new SqlParameter("@LastCommentUserName", SqlDbType.NVarChar,64),
					new SqlParameter("@LastCommentContents", SqlDbType.NVarChar,4000),
					new SqlParameter("@LastCommentTime", SqlDbType.DateTime),
					new SqlParameter("@LastCommentIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Path", SqlDbType.NVarChar,256)};
			parameters[0].Direction = ParameterDirection.Output;
			parameters[1].Value = model.OrganizatioinId;
			parameters[2].Value = model.Type;
			parameters[3].Value = model.DailyDate;
			parameters[4].Value = model.Contents;
			parameters[5].Value = model.AddUserId;
			parameters[6].Value = model.AddUserName;
			parameters[7].Value = model.AddDepartId;
			parameters[8].Value = model.AddDepart;
			parameters[9].Value = model.AddTime;
			parameters[10].Value = model.AddIp;
			parameters[11].Value = model.LastCommentUserId;
			parameters[12].Value = model.LastCommentUserName;
			parameters[13].Value = model.LastCommentContents;
			parameters[14].Value = model.LastCommentTime;
			parameters[15].Value = model.LastCommentIp;
			parameters[16].Value = model.Path;

			DbHelperSQL.RunProcedure("Daily_Records_ADD",parameters,out rowsAffected);
			return (int)parameters[0].Value;
		}

		/// <summary>
		///  更新一条数据
		/// </summary>
		public bool Update(xundh.Model.Daily_Records model)
		{
			int rowsAffected=0;
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4),
					new SqlParameter("@OrganizatioinId", SqlDbType.Int,4),
					new SqlParameter("@Type", SqlDbType.NVarChar,32),
					new SqlParameter("@DailyDate", SqlDbType.DateTime),
					new SqlParameter("@Contents", SqlDbType.NText),
					new SqlParameter("@AddUserId", SqlDbType.Int,4),
					new SqlParameter("@AddUserName", SqlDbType.NVarChar,64),
					new SqlParameter("@AddDepartId", SqlDbType.Int,4),
					new SqlParameter("@AddDepart", SqlDbType.NVarChar,64),
					new SqlParameter("@AddTime", SqlDbType.DateTime),
					new SqlParameter("@AddIp", SqlDbType.NVarChar,64),
					new SqlParameter("@LastCommentUserId", SqlDbType.Int,4),
					new SqlParameter("@LastCommentUserName", SqlDbType.NVarChar,64),
					new SqlParameter("@LastCommentContents", SqlDbType.NVarChar,4000),
					new SqlParameter("@LastCommentTime", SqlDbType.DateTime),
					new SqlParameter("@LastCommentIp", SqlDbType.NVarChar,64),
					new SqlParameter("@Path", SqlDbType.NVarChar,256)};
			parameters[0].Value = model.id;
			parameters[1].Value = model.OrganizatioinId;
			parameters[2].Value = model.Type;
			parameters[3].Value = model.DailyDate;
			parameters[4].Value = model.Contents;
			parameters[5].Value = model.AddUserId;
			parameters[6].Value = model.AddUserName;
			parameters[7].Value = model.AddDepartId;
			parameters[8].Value = model.AddDepart;
			parameters[9].Value = model.AddTime;
			parameters[10].Value = model.AddIp;
			parameters[11].Value = model.LastCommentUserId;
			parameters[12].Value = model.LastCommentUserName;
			parameters[13].Value = model.LastCommentContents;
			parameters[14].Value = model.LastCommentTime;
			parameters[15].Value = model.LastCommentIp;
			parameters[16].Value = model.Path;

			DbHelperSQL.RunProcedure("Daily_Records_Update",parameters,out rowsAffected);
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int id)
		{
			int rowsAffected=0;
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			DbHelperSQL.RunProcedure("Daily_Records_Delete",parameters,out rowsAffected);
			if (rowsAffected > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		/// <summary>
		/// 批量删除数据
		/// </summary>
		public bool DeleteList(string idlist )
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("delete from Daily_Records ");
			strSql.Append(" where id in ("+idlist + ")  ");
			int rows=DbHelperSQL.ExecuteSql(strSql.ToString());
			if (rows > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.Daily_Records GetModel(int id)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@id", SqlDbType.Int,4)
			};
			parameters[0].Value = id;

			xundh.Model.Daily_Records model=new xundh.Model.Daily_Records();
			DataSet ds= DbHelperSQL.RunProcedure("Daily_Records_GetModel",parameters,"ds");
			if(ds.Tables[0].Rows.Count>0)
			{
				return DataRowToModel(ds.Tables[0].Rows[0]);
			}
			else
			{
				return null;
			}
		}


		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public xundh.Model.Daily_Records DataRowToModel(DataRow row)
		{
			xundh.Model.Daily_Records model=new xundh.Model.Daily_Records();
			if (row != null)
			{
				if(row["id"]!=null && row["id"].ToString()!="")
				{
					model.id=int.Parse(row["id"].ToString());
				}
				if(row["OrganizatioinId"]!=null && row["OrganizatioinId"].ToString()!="")
				{
					model.OrganizatioinId=int.Parse(row["OrganizatioinId"].ToString());
				}
				if(row["Type"]!=null)
				{
					model.Type=row["Type"].ToString();
				}
				if(row["DailyDate"]!=null && row["DailyDate"].ToString()!="")
				{
					model.DailyDate=DateTime.Parse(row["DailyDate"].ToString());
				}
				if(row["Contents"]!=null)
				{
					model.Contents=row["Contents"].ToString();
				}
				if(row["AddUserId"]!=null && row["AddUserId"].ToString()!="")
				{
					model.AddUserId=int.Parse(row["AddUserId"].ToString());
				}
				if(row["AddUserName"]!=null)
				{
					model.AddUserName=row["AddUserName"].ToString();
				}
				if(row["AddDepartId"]!=null && row["AddDepartId"].ToString()!="")
				{
					model.AddDepartId=int.Parse(row["AddDepartId"].ToString());
				}
				if(row["AddDepart"]!=null)
				{
					model.AddDepart=row["AddDepart"].ToString();
				}
				if(row["AddTime"]!=null && row["AddTime"].ToString()!="")
				{
					model.AddTime=DateTime.Parse(row["AddTime"].ToString());
				}
				if(row["AddIp"]!=null)
				{
					model.AddIp=row["AddIp"].ToString();
				}
				if(row["LastCommentUserId"]!=null && row["LastCommentUserId"].ToString()!="")
				{
					model.LastCommentUserId=int.Parse(row["LastCommentUserId"].ToString());
				}
				if(row["LastCommentUserName"]!=null)
				{
					model.LastCommentUserName=row["LastCommentUserName"].ToString();
				}
				if(row["LastCommentContents"]!=null)
				{
					model.LastCommentContents=row["LastCommentContents"].ToString();
				}
				if(row["LastCommentTime"]!=null && row["LastCommentTime"].ToString()!="")
				{
					model.LastCommentTime=DateTime.Parse(row["LastCommentTime"].ToString());
				}
				if(row["LastCommentIp"]!=null)
				{
					model.LastCommentIp=row["LastCommentIp"].ToString();
				}
				if(row["Path"]!=null)
				{
					model.Path=row["Path"].ToString();
				}
			}
			return model;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select id,OrganizatioinId,Type,DailyDate,Contents,AddUserId,AddUserName,AddDepartId,AddDepart,AddTime,AddIp,LastCommentUserId,LastCommentUserName,LastCommentContents,LastCommentTime,LastCommentIp,Path ");
			strSql.Append(" FROM Daily_Records ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select ");
			if(Top>0)
			{
				strSql.Append(" top "+Top.ToString());
			}
			strSql.Append(" id,OrganizatioinId,Type,DailyDate,Contents,AddUserId,AddUserName,AddDepartId,AddDepart,AddTime,AddIp,LastCommentUserId,LastCommentUserName,LastCommentContents,LastCommentTime,LastCommentIp,Path ");
			strSql.Append(" FROM Daily_Records ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			strSql.Append(" order by " + filedOrder);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/// <summary>
		/// 获取记录总数
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("select count(1) FROM Daily_Records ");
			if(strWhere.Trim()!="")
			{
				strSql.Append(" where "+strWhere);
			}
			object obj = DbHelperSQL.GetSingle(strSql.ToString());
			if (obj == null)
			{
				return 0;
			}
			else
			{
				return Convert.ToInt32(obj);
			}
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			StringBuilder strSql=new StringBuilder();
			strSql.Append("SELECT * FROM ( ");
			strSql.Append(" SELECT ROW_NUMBER() OVER (");
			if (!string.IsNullOrEmpty(orderby.Trim()))
			{
				strSql.Append("order by T." + orderby );
			}
			else
			{
				strSql.Append("order by T.id desc");
			}
			strSql.Append(")AS Row, T.*  from Daily_Records T ");
			if (!string.IsNullOrEmpty(strWhere.Trim()))
			{
				strSql.Append(" WHERE " + strWhere);
			}
			strSql.Append(" ) TT");
			strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
			return DbHelperSQL.Query(strSql.ToString());
		}

		/*
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetList(int PageSize,int PageIndex,string strWhere)
		{
			SqlParameter[] parameters = {
					new SqlParameter("@tblName", SqlDbType.VarChar, 255),
					new SqlParameter("@fldName", SqlDbType.VarChar, 255),
					new SqlParameter("@PageSize", SqlDbType.Int),
					new SqlParameter("@PageIndex", SqlDbType.Int),
					new SqlParameter("@IsReCount", SqlDbType.Bit),
					new SqlParameter("@OrderType", SqlDbType.Bit),
					new SqlParameter("@strWhere", SqlDbType.VarChar,1000),
					};
			parameters[0].Value = "Daily_Records";
			parameters[1].Value = "id";
			parameters[2].Value = PageSize;
			parameters[3].Value = PageIndex;
			parameters[4].Value = 0;
			parameters[5].Value = 0;
			parameters[6].Value = strWhere;	
			return DbHelperSQL.RunProcedure("UP_GetRecordByPage",parameters,"ds");
		}*/

		#endregion  Method
		#region  MethodEx

		#endregion  MethodEx
	}
}

