﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
using System.Collections.Generic;
namespace xundh.API.Daily
{
    /// <summary>
    ///Daily_RecordTemplate 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Daily_RecordTemplate : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int id, string Templates, int PostId)
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            //Templates = StringUtil.CutBadSqlInfo(Templates);

            xundh.BLL.Daily_RecordTemplate bll = new xundh.BLL.Daily_RecordTemplate();
            xundh.Model.Daily_RecordTemplate m=null;
            if (id > 0) m = bll.GetModel(id);
            else
            {
                List<xundh.Model.Daily_RecordTemplate> ms = bll.GetModelList("PostId=" + PostId);

                if (ms.Count > 0)
                {
                    m = ms[0];
                    id = m.id;
                }
            }
            if (m == null)
            {
                m = new xundh.Model.Daily_RecordTemplate();
                m.AddUserId = U.UserId;
                m.AddUserName = U.UserName;
                m.AddTime = NOW;
                m.AddIp = IP;
            }
            m.Templates = Templates;
            m.PostId = PostId;
            m.LastTime = NOW;
            m.LastUserId = U.UserId;
            m.LastUserName = U.UserName;
            if (id>0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.Daily_RecordTemplate bll = new xundh.BLL.Daily_RecordTemplate();
            using (DataSet ds = bll.GetList(int.MaxValue, "", "id"))
            {
                return new JsonTable(ds.Tables[0], bll.GetRecordCount(""));
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Daily_RecordTemplate Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Daily_RecordTemplate bll = new xundh.BLL.Daily_RecordTemplate();
            List<xundh.Model.Daily_RecordTemplate> ms = bll.GetModelList("id=" +id);
            return bll.GetModel(id);
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Daily_RecordTemplate QueryPost(int PostId)
        {
            if (U == null) return null;
            xundh.BLL.Daily_RecordTemplate bll = new xundh.BLL.Daily_RecordTemplate();
            //int? PostId = U.PostId;
            //if (UserId > 0)
            //{
            //    API.RBAC.RBAC_Users serviceRBACUsers = new RBAC.RBAC_Users();
            //    xundh.Model.RBAC_Users rbacUsers = serviceRBACUsers.Query(UserId);
            //    if (rbacUsers != null) PostId = rbacUsers.PostId;
            //}
            List<xundh.Model.Daily_RecordTemplate> ms = bll.GetModelList("PostId=" + PostId);
            if (ms.Count > 0) return ms[0];
            else
                return null;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Daily_RecordTemplate bll = new xundh.BLL.Daily_RecordTemplate();
            bll.Delete(id);
            return ServiceStateKeys.Success;
        }

    }
}