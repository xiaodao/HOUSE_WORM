﻿using EnumKeys;
using System.Web;
using System.Web.Services;
using Webapp;
namespace xundh.API.Daily
{
    /// <summary>
    ///Update 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]
    public class Daily_SystemSet : BaseService
    {
        [System.ComponentModel.ToolboxItem(false)]
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Save()
        {
            //用户权限  
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.Model.RBAC_Users mUser = (xundh.Model.RBAC_Users)HttpContext.Current.Session[EnumKeys.SessionKey.USER];

            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(mUser.UserId, "高级设置", "改")) return ServiceStateKeys.NoPermiss;

            int DailyDaysBeforeCountAllow = StringUtil.GetIntValue(HttpContext.Current.Request["DailyDaysBeforeCountAllow"]);

            SystemSetInt("Daily", "DailyDaysBeforeCountAllow", DailyDaysBeforeCountAllow);
            return ServiceStateKeys.Success;

        }
    }
}