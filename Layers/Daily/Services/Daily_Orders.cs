﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using EnumKeys;
using Webapp;
namespace xundh.API.Daily
{
    /// <summary>
    ///Daily_Orders 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Daily_Orders : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int OrderId, int OrderDepartId, int OrderArea, string Address, 
            string Needs,  string Matter, string BusinessType,
            DateTime OrderBeginTime, DateTime OrderEndTime, int UserId, string CMNumber
       )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            if (OrderBeginTime.ToShortDateString() != OrderEndTime.ToShortDateString())
                return ServiceStateKeys.HardErr;
            xundh.BLL.Daily_Orders bll = new xundh.BLL.Daily_Orders();
            xundh.Model.Daily_Orders m;
            if (OrderId > 0)
                m = bll.GetModel(OrderId);
            else
            {
                m = new xundh.Model.Daily_Orders();
                m.AddUserId = U.UserId;
                m.AddDepartId = U.DepartId;
                m.AddUser = U.UserName;
                m.AddTime = NOW;
                //有效性验证
                string where = "OrderUserId=" + UserId ;
                where += " AND ((OrderBeginTime BETWEEN '" + OrderBeginTime + "' AND '" + OrderEndTime + "')";
                where += " OR (OrderEndTime BETWEEN '" + OrderBeginTime + "' AND '" + OrderEndTime + "')";
                where += ") AND States<2";
                if (bll.GetRecordCount(where) > 0)
                    return ServiceStateKeys.Exists;
            }
            m.OrderDepartId = OrderDepartId;
            m.OrderArea = OrderArea;
            m.Address = Address;
            m.States = (int)DailyOrderStates.Appliance;
            m.Matter = Matter;
            m.Needs = Needs;
            m.BusinessType = BusinessType;
            m.OrderBeginTime = OrderBeginTime;
            m.OrderEndTime = OrderEndTime;
            m.OrderUserId = UserId;
            RBAC.RBAC_Users serviceRBACUsers = new RBAC.RBAC_Users();
            m.OrderUser = serviceRBACUsers.Query(UserId).UserName;
            m.CMId = 0;
            m.CMNumber = CMNumber;
            if (OrderId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            xundh.BLL.BasePager bll = new BLL.BasePager();
            string where = " 1=1 ";
            string table = "Daily_Orders";
            JqGridHandler datagrid = new JqGridHandler(where);
            int recordcount = bll.GetAllCount(datagrid.StrWhere, table);

            string cols = "OrderId,OrderDepartId,OrderArea,Address,States,Needs,Matter,ContractType,CMId,BusinessType,AddUserId,AddDepartId,AddUser,AddTime,OrderBeginTime,OrderEndTime,OrderUserId,OrderUser";
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "", "");
            if (datagrid.Order == "") datagrid.Order = "OrderId DESC";
            using (DataSet ds = bll.GetPager(datagrid.Pagesize, datagrid.P, datagrid.StrWhere, datagrid.Order, table, cols))
            {
                return new JsonTable(ds.Tables[0], recordcount, datagrid.P, datagrid.Recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Daily_Orders Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Daily_Orders bll = new xundh.BLL.Daily_Orders();
            xundh.Model.Daily_Orders m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys DoAction(int id,int State)
        {
            if (U == null) return  ServiceStateKeys.TimeOut;
            xundh.BLL.Daily_Orders bll = new xundh.BLL.Daily_Orders();
            xundh.Model.Daily_Orders m = bll.GetModel(id);
            m.States = State;
            //权限
            switch (State)
            {
                case (int)DailyOrderStates.Appliance:
                    break;
                case (int)DailyOrderStates.Accept:
                    if (m.OrderUserId != U.UserId) return ServiceStateKeys.NoPermiss;
                    break;
                case (int)DailyOrderStates.Cancel:
                    if (m.AddUserId != U.UserId) return ServiceStateKeys.NoPermiss;
                    break;
                case (int)DailyOrderStates.Reject:
                    if (m.OrderUserId != U.UserId) return ServiceStateKeys.NoPermiss;
                    break;
            }
            bll.Update(m);
            return ServiceStateKeys.Success;
        }
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Delete(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Daily_Orders bll = new xundh.BLL.Daily_Orders();
            xundh.Model.Daily_Orders m = bll.GetModel(id);
            m.States = (int)DailyOrderStates.Cancel;
            if (m.AddUserId != U.UserId) return ServiceStateKeys.NoPermiss;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }

    }
}
public enum DailyOrderStates
{
    Appliance, Accept, Cancel, Reject
}
public enum DailyBusinessType { 
    Guohu,Daikuan,Jiaojie
}
public enum DailyContractType
{
    Buy, Rent
}