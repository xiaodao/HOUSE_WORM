﻿using System;
using System.Data;
using System.Web.Script.Services;
using System.Web.Services;
using Webapp;
using EnumKeys;
namespace xundh.API.Daily
{
    /// <summary>
    ///Daily_OutRecords 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://www.lansezhongjie.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.Web.Script.Services.ScriptService]

    public class Daily_OutRecords : BaseService
    {
        [WebMethod(EnableSession = true)]
        public ServiceStateKeys Add(int OutRecordId, string AgreementIds, string Address, string DoThins,
            string AccompanyIds, string BusinessType, string AccompanyNames, int HouseType, string HouseId,
            string HouseBuilds, int CRMType, string CRMIds, string CRMNames, int AgreementType
            )
        {
            ///Session
            if (U == null) return ServiceStateKeys.TimeOut;
            ///参数处理
            AgreementIds = StringUtil.CutBadSqlInfo(AgreementIds);
            Address = StringUtil.CutBadSqlInfo(Address);
            DoThins = StringUtil.CutBadSqlInfo(DoThins);
            AccompanyIds = StringUtil.CutBadSqlInfo(AccompanyIds);
            BusinessType = StringUtil.CutBadSqlInfo(BusinessType);
            AccompanyNames = StringUtil.CutBadSqlInfo(AccompanyNames);
            HouseId = StringUtil.CutBadSqlInfo(HouseId);
            HouseBuilds = StringUtil.CutBadSqlInfo(HouseBuilds);
            CRMIds = StringUtil.CutBadSqlInfo(CRMIds);
            CRMNames = StringUtil.CutBadSqlInfo(CRMNames);

            xundh.BLL.Daily_OutRecords bll = new xundh.BLL.Daily_OutRecords();
            xundh.Model.Daily_OutRecords m;
            if (OutRecordId > 0)
                m = bll.GetModel(OutRecordId);
            else
            {
                m = new xundh.Model.Daily_OutRecords();

                m.AddUserId = U.UserId;
                m.AddUser = U.UserName;
                m.AddTime = NOW;
                m.AddIp = IP;
                m.AddDepartId = U.DepartId;
            }
            m.AgreementIds = AgreementIds;
            m.Address = Address;
            m.DoThins = DoThins;
            m.State = (int)OutRecordState.Out;
            m.AccompanyIds = AccompanyIds;
            m.BusinessType = BusinessType;
            m.AccompanyNames = AccompanyNames;
            m.HouseType = HouseType;
            m.HouseId = HouseId;
            m.HouseBuilds = HouseBuilds;
            m.CRMType = CRMType;
            m.CRMIds = CRMIds;
            m.CRMNames = CRMNames;
            m.AgreementType = AgreementType;
            if (OutRecordId > 0)
                bll.Update(m);
            else
                bll.Add(m);
            return ServiceStateKeys.Success;
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public JsonTable GetLists(int page, int rows)
        {
            if (U == null) return null;
            //角色权限
            RBAC.RBAC_Users serviceOperation = new RBAC.RBAC_Users();
            if (!serviceOperation.CheckResouceOperationLimit(U.UserId, "外出记录", "查")) return null;


            xundh.BLL.Daily_OutRecords bll = new xundh.BLL.Daily_OutRecords();
            string strWhere = "1=1";
            string UserId = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(REQUEST["UserId"]));
            int DepartId = StringUtil.GetIntValue(REQUEST["txtDeparts"]);
            int HouseId = StringUtil.GetIntValue(REQUEST["HouseId"]);
            string BusinessType = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(REQUEST["BusinessType"]));
            string BeginDate = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(REQUEST["BeginDate"]));
            string EndDate = StringUtil.CutBadSqlInfo(StringUtil.GetNullToString(REQUEST["EndDate"]));

            if (StringUtil.IsInt(UserId))
                strWhere += " AND AddUserId=" + UserId;
            else if (UserId != "")
                strWhere += " AND AddUser='" + UserId + "'";
            if (DepartId > 1)
                strWhere += " AND AddDepartId=" + DepartId;
            if (HouseId > 0)
                strWhere += " AND HouseId=" + HouseId;
            if (BusinessType != "")
            {
                switch (BusinessType)
                {
                    case "自看":
                        strWhere += " AND BusinessType=0 AND CRMIds=''";
                        break;
                    case "带看":
                        strWhere += " AND BusinessType=0 AND CRMIds<>''";
                        break;
                    default:
                        strWhere += " AND BusinessType=" + BusinessType;
                        break;
                }
            }
            if (StringUtil.isDate(BeginDate))
            {
                strWhere += " AND AddTime >='" + BeginDate + "'";
            }
            if (StringUtil.isDate(EndDate))
            {
                strWhere += " AND AddTime <='" + EndDate + " 23:59:59'";
            }
            ///权限
            //没有查所有的权限，就要受限
            if (serviceOperation.CheckResouceOperationLimit(U.UserId, "外出记录", "查所有"))
            {
            
            }
            else if (serviceOperation.CheckResouceOperationLimit(U.UserId, "外出记录", "查部门"))
            {
                Org.Org_Departs serviceOrgDeparts = new Org.Org_Departs();
                string ids = serviceOrgDeparts.GetStringsDepartsAndSons(U.DepartId??0);
                ids = ids.ItemAdd(U.DepartId ?? 0);
                strWhere += " AND AddDepartId in(" + ids + ")";
            }
            else if (serviceOperation.CheckResouceOperationLimit(U.UserId, "外出记录", "查"))
            {
                strWhere += " AND AddUserId=" + U.UserId ;
            }

            JqGridHandler datagrid = new JqGridHandler(strWhere);
            int recordcount = bll.GetRecordCount(datagrid.StrWhere);
            datagrid.init(datagrid.StrWhere, page, recordcount, rows, "OutRecordId", "desc");
            using (DataSet ds = bll.GetListByPage( strWhere, datagrid.Order,datagrid.StartIndex,datagrid.EndIndex))
            {
                return new JsonTable(ds.Tables[0], recordcount);
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(UseHttpGet = true)]
        public xundh.Model.Daily_OutRecords Query(int id)
        {
            if (U == null) return null;
            xundh.BLL.Daily_OutRecords bll = new xundh.BLL.Daily_OutRecords();
            xundh.Model.Daily_OutRecords m = bll.GetModel(id);
            return m;
        }
        [WebMethod(EnableSession = true, Description = "取消")]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Cancel(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Daily_OutRecords bll = new xundh.BLL.Daily_OutRecords();
            xundh.Model.Daily_OutRecords m = bll.GetModel(id);
            if (m.AddUserId != U.UserId) return ServiceStateKeys.Fail;
            m.State = (int)OutRecordState.Cancel;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }
        [WebMethod(EnableSession = true, Description = "外出返回操作")]
        [ScriptMethod(UseHttpGet = true)]
        public ServiceStateKeys Back(int id)
        {
            if (U == null) return ServiceStateKeys.TimeOut;
            xundh.BLL.Daily_OutRecords bll = new xundh.BLL.Daily_OutRecords();
            xundh.Model.Daily_OutRecords m = bll.GetModel(id);
            if (m.AddUserId != U.UserId) return ServiceStateKeys.Fail;
            m.State = (int)OutRecordState.Back;
            m.BackTime = NOW;
            bll.Update(m);
            return ServiceStateKeys.Success;
        }
    }
}
public enum OutRecordState { Out, Back, Cancel } //Duplicate