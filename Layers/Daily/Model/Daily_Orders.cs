﻿using System;
namespace xundh.Model
{
    /// <summary>
    /// Daily_Orders:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Daily_Orders
    {
        public Daily_Orders()
        { }
        #region Model
        private int _orderid;
        private int? _adduserid;
        private int? _adddepartid;
        private string _adduser;
        private DateTime? _addtime;
        private DateTime? _orderbegintime;
        private DateTime? _orderendtime;
        private int? _orderuserid;
        private string _orderuser;
        private int? _orderdepartid;
        private int? _orderarea;
        private string _address;
        private int? _states;
        private string _matter;
        private string _needs;
        private string _businesstype;
        private int? _cmid;
        private int? _contracttype;
        private string _cmnumber;
        /// <summary>
        /// 
        /// </summary>
        public int OrderId
        {
            set { _orderid = value; }
            get { return _orderid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddUserId
        {
            set { _adduserid = value; }
            get { return _adduserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? AddDepartId
        {
            set { _adddepartid = value; }
            get { return _adddepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AddUser
        {
            set { _adduser = value; }
            get { return _adduser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? AddTime
        {
            set { _addtime = value; }
            get { return _addtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? OrderBeginTime
        {
            set { _orderbegintime = value; }
            get { return _orderbegintime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? OrderEndTime
        {
            set { _orderendtime = value; }
            get { return _orderendtime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? OrderUserId
        {
            set { _orderuserid = value; }
            get { return _orderuserid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string OrderUser
        {
            set { _orderuser = value; }
            get { return _orderuser; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? OrderDepartId
        {
            set { _orderdepartid = value; }
            get { return _orderdepartid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? OrderArea
        {
            set { _orderarea = value; }
            get { return _orderarea; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Address
        {
            set { _address = value; }
            get { return _address; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? States
        {
            set { _states = value; }
            get { return _states; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Matter
        {
            set { _matter = value; }
            get { return _matter; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Needs
        {
            set { _needs = value; }
            get { return _needs; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string BusinessType
        {
            set { _businesstype = value; }
            get { return _businesstype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? CMId
        {
            set { _cmid = value; }
            get { return _cmid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? ContractType
        {
            set { _contracttype = value; }
            get { return _contracttype; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CMNumber
        {
            set { _cmnumber = value; }
            get { return _cmnumber; }
        }
        #endregion Model

    }
}

