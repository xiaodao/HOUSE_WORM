﻿<%@ Application Language="C#" %>

<script runat="server">

    private log4net.ILog logger = log4net.LogManager.GetLogger("_Default");
    protected void Application_Error(object sender, EventArgs e)
    {
        // 在出现未处理的错误时运行的代码 
        Exception objErr = Server.GetLastError().GetBaseException();
        logger.Error(objErr);
    }

    protected void Session_End(object sender, EventArgs e)
    {
        //更新缓存
        if (HttpContext.Current != null)
        {
            if (HttpContext.Current.Session["users"] != null)
            {
                xundh.Model.RBAC_Users user = (xundh.Model.RBAC_Users)HttpContext.Current.Session["users"];

                string cacheKey = "onlineusers";// EnumKeys.SessionKey.ONLINEUSERS;
                List<xundh.Model.RBAC_Users> list;
                object obList = Maticsoft.Common.DataCache.GetCache(cacheKey);
                if (obList != null)
                {
                    list = obList as List<xundh.Model.RBAC_Users>;
                    foreach (xundh.Model.RBAC_Users userInfo in list)
                    {
                        if (userInfo.UserId == user.UserId)
                        {
                            list.Remove(userInfo);
                            break;
                        }
                    }
                    Maticsoft.Common.DataCache.SetCache("onlineusers", list,
                 DateTime.Now.AddMinutes(Maticsoft.Common.ConfigHelper.GetConfigInt("ModelCache")), TimeSpan.Zero);
                }

            }
        }
    }
    void Application_AcquireRequestState(object source, EventArgs e)
    {
        //return;
        HttpApplication Application = (HttpApplication)source;
        HttpResponse Response = Application.Context.Response;
        try
        {
            #region 判断Session，缓存Agents
            if (HttpContext.Current.Session["users"] != null)
            {
                xundh.Model.RBAC_Users user = (xundh.Model.RBAC_Users)HttpContext.Current.Session["users"];
                user.PreOpTime = DateTime.Now;
                ///加载缓存里的在线用户List
                string cacheKey = "onlineusers";// EnumKeys.SessionKey.ONLINEUSERS;
                object objModel = Maticsoft.Common.DataCache.GetCache(cacheKey);
                List<xundh.Model.RBAC_Users> CacheUsers;
                ///查询是否已存在
                CacheUsers = (List<xundh.Model.RBAC_Users>)objModel;
                if (CacheUsers != null)
                {
                    //查看缓存里自己的Session是否存在
                    xundh.Model.RBAC_Users findIfExist = CacheUsers.Find(delegate(xundh.Model.RBAC_Users oneU)
                    {
                        return (oneU.UserName == user.UserName);
                    });
                    //如果存在，看SessionID是否一致
                    if (findIfExist != null)
                    {
                        if (findIfExist.UserSessionId != Session.SessionID)
                        {
                            HttpContext.Current.Session["users"] = null;
                            HttpContext.Current.Session.Abandon();
                            Response.Redirect("~/Templates/_Relogin.htm");
                        }
                    }
                }
                //else
                //{
                //    CacheUsers = new List<xundh.Model.RBAC_Users>();
                //    HttpContext.Current.Session["users"] = null;
                //    HttpContext.Current.Session.Abandon();
                //    Response.Redirect("~/Templates/_Relogin.htm");
                //}
            }
            #endregion
        }
        catch(Exception) { }

    }
    protected void Application_End(object sender, EventArgs e)
    {
        //xundh.API.SystemSet.ClearAllCache();
    }
    protected void Application_Start(object sender, EventArgs e)
    {
        // start log4net configure
        log4net.Config.XmlConfigurator.Configure(); 
        log4net.ILog logger = log4net.LogManager.GetLogger("Default");
        logger.Info("start web success");
    }
       
</script>
