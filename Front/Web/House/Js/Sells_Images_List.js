﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Sells_Images_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['id'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/House/Services/House_Sells_Images.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = [
             { text: '编号', dataIndex: 'SellId', width: X_WIDTH.Number, align: 'center' },
             { text: '区域', dataIndex: 'SellRegionArea', width: X_WIDTH.RegionArea, align: 'center', renderer: formatRegionArea },
             { text: '小区', dataIndex: 'BuildingName' },
             { text: '类型', dataIndex: 'SellFace', width: X_WIDTH.State, renderer: function (c) { return X_HOUSEFACE[c]; } },
             { text: '楼层', dataIndex: 'HouseFloor', width: X_WIDTH.Floor, align: 'center', renderer: function (v, cellmeta, r) { return v + "/" + r.data.HouseTotalFloor; } },
             { text: '户型', dataIndex: 'Shi', width: X_WIDTH.State, align: 'center', renderer: function (v, cellmeta, r) { return v + "-" + r.data.Ting; } },
             { text: '面积', dataIndex: 'HouseArea', width: X_WIDTH.Int, align: 'center' },
             { text: '单价', dataIndex: 'UnitPrice', width: X_WIDTH.Int, align: 'center', renderer: formatPrice },
             { text: '总价', dataIndex: 'TotalPrice', width: X_WIDTH.Int, align: 'center', renderer: formatPrice },
             { text: '装修', dataIndex: 'Decoration', width: X_WIDTH.State, align: 'center', renderer: function (v, r, k) { return X_DECORATEKEYS[v]; } },
             { text: '登记时间', dataIndex: 'AddTime', width: X_WIDTH.Time, renderer: function (value, cellmeta, record, rowIndex, columnIndex, store) { return formatShortTime(value); } },
             { text: '部门', dataIndex: 'AddDepartId', width: X_WIDTH.Depart, align: 'center', renderer: formatDepart },
             { text: '发布人', dataIndex: 'AddUser', width: X_WIDTH.UserName, align: 'center' },
             { text: '状态', dataIndex: 'State', width: X_WIDTH.State, align: 'center', renderer: function (v, c, r) { return "<span style='" + styleHouseState(v) + "'>" + X_SELLSTATE[v] + "</span>"; } }
        ];
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var searchHeight = $(".auto", view.dom).height();
        if (!searchHeight) searchHeight = 0;
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - searchHeight - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Sells_Images_View = Ext.create("House.Js.Sells_Images_View");
                    Sells_Images_View.show(row.data.id);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Sells_Images_Add = Ext.create("House.Js.Sells_Images_Add");
            Sells_Images_Add.show(0);
        });
        $("a[name=linkEdit]", view.dom).click(function () {
            if (selectRowData) {
                var Sells_Images_Add = Ext.create("House.Js.Sells_Images_Add");
                Sells_Images_Add.show(selectRowData.id);
            }
        });
        $("a[name=linkDel]", view.dom).click(function () {
            if (selectRowData) {
                $.ajax({
                    url: "House/Services/House_Sells_Images.asmx/Delete",
                    data: { id: selectRowData.id },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                //var RBAC_Module = Ext.create("RBAC.Js.RBAC_Module");
                                //RBAC_Module.render('House.Js.Sells_List,House.Js.Sells_Favs_List');
                                break;
                            case "Exists":
                                $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                break;
                        }
                    }
                });
            }
        });
    }
});