﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Rents_Favs_Add", {
    show: function (FavId, RentId) {

        $.post("/House/Services/House_Rents_Favs.asmx/Add",
            {RentsId:RentId}, function (data) {
                switch ($(data).find("ServiceStateKeys").text()) {
                    case "Success":
                        $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                        return true;
                        break;
                    case "Exists":
                        $.messager.show({ title: '操作提示', msg: "已经收藏过了" });
                        return false;
                        break;
                    default:
                        Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                        return false;
                        break;
                }
            });

    },
    constructor: function (config) {
        this.initConfig(config);
    }
});