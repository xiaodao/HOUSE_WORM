﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* arg0 要返回到的HouseId的id
* arg1 是否单选，如果单选，值为true
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Sells_Select", {
    $win: {},
    SelectedArray: [],
    Grid: {},
    isSingleSelect: false,
    show: function () {
        var _callbackid = arg0; //返回到那个id里去
        this.isSingleSelect = (arg1 == "true"); //是否单选
        var param = { SellId: 0 };
        var _module = this;
        var winDom;

        var win = Ext.create('Ext.window.Window', {
            title: '选择出售房源', height: 560, width: 700, modal: true,
            loader: {
                url: '/House/Templates/Sells_Select.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        _module.$win = winDom;
                        renderUserControls(winDom);

                        _module.renderList(Ext.get(winDom.find('.divMainList')[0]));
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    if (!$("input[name=SelectedHouseIds]", _win.dom).val()) {
                        $.messager.show({msg:"没有选定房源"});
                        return;
                    }
                    $("#" + _callbackid).val($("input[name=SelectedHouseIds]", _win.dom).val()).change();
                    $("#" + _callbackid).parent().next().find("input[name=HouseBuilds]").val($("input[name=SelectedBuilds]", _win.dom).val());
                    _win.close();
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    renderList: function (view) {
        var _module = this;
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['SellId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/House/Services/House_Sells.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmHouse_Sell_Select]", _module.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = [
             { text: '编号', dataIndex: 'SellId', width: X_WIDTH.Number * 1.4, align: 'center' },
             { text: '区域', dataIndex: 'SellRegionArea', width: X_WIDTH.RegionArea, align: 'center', renderer: formatRegionArea },
             { text: '小区', dataIndex: 'BuildingName' },
             { text: '类型', dataIndex: 'SellFace', width: X_WIDTH.State, renderer: function (c) { return X_HOUSEFACE[c]; } },
             { text: '楼层', dataIndex: 'HouseFloor', width: X_WIDTH.Floor, align: 'center', renderer: function (v, cellmeta, r) { return v + "/" + r.data.HouseTotalFloor; } },
             { text: '户型', dataIndex: 'Shi', width: X_WIDTH.State, align: 'center', renderer: function (v, cellmeta, r) { return v + "-" + r.data.Ting; } },
             { text: '面积', dataIndex: 'HouseArea', width: X_WIDTH.Int, align: 'center' },
             { text: '单价', dataIndex: 'UnitPrice', width: X_WIDTH.Int, align: 'center', renderer: formatPrice },
             { text: '总价', dataIndex: 'TotalPrice', width: X_WIDTH.Int * .8, align: 'center', renderer: formatPrice },
             { text: '装修', dataIndex: 'Decoration', width: X_WIDTH.State, align: 'center', renderer: function (v, r, k) { return X_DECORATEKEYS[v]; } },
             { text: '登记时间', dataIndex: 'AddTime', width: X_WIDTH.Time, renderer: function (value, cellmeta, record, rowIndex, columnIndex, store) { return formatShortTime(value); } },
             { text: '发布人', dataIndex: 'AddUser', width: X_WIDTH.UserName, align: 'center' },
             { text: '状态', dataIndex: 'State', width: X_WIDTH.State, align: 'center', renderer: function (v, c, r) { return "<span style='" + styleHouseState(v) + "'>" + X_SELLSTATE[v] + "</span>"; } }
        ];
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var searchHeight = $(".auto", _module.dom).height();
        if (!searchHeight) searchHeight = 0;
        var sm = new Ext.selection.CheckboxModel({ checkOnly: false, mode:this.isSingleSelect?"SINGLE": 'SIMPLE' });
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: 400,
            store: Store, width: '100%', forceFit: true,
            columns: Columns, selModel: sm,
            listeners: {
                cellclick: function (cell, td, cellIndex, record, tr, rowIndex, e, eopts) {
                    if (cellIndex > 0) {
                        var Sells_View = Ext.create("House.Js.Sells_View");
                        Sells_View.show(record.data.SellId);
                        selectRowData = record.data;
                    }
                },
                select: function (t, record, index, eOpts) {
                    var position = _module.SelectedArray.indexOf(record.data);
                    if (position < 0)
                        _module.SelectedArray.push(record.data);
                    renderResult();
                },
                deselect: function (t, record, index, eOpts) {
                    var _resultArray = [];
                    $.map(_module.SelectedArray, function (item) {
                        if (item.SellId == record.data.SellId) { }
                        else {
                            _resultArray.push(item);
                        }
                    });
                    _module.SelectedArray = _resultArray;
                    renderResult();
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });
        this.Grid = grid;
        function renderResult() {
            var _SellId = "", Buildings = "";
            $.map(_module.SelectedArray, function (item, k) {
                if (_SellId.ItemIsContain(item.SellId)) return true;
                if (_SellId != "") _SellId += ",";
                if (Buildings != "") Buildings += ",";
                _SellId += item.SellId;
                Buildings += item.BuildingName;
            });
            $("input[name=SelectedHouseIds]", _module.dom).val(_SellId);
            $("input[name=SelectedBuilds]", _module.dom).val(Buildings);
        }
        //events
        $("form[name=fmHouse_Sell_Select]", _module.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmHouse_Sell_Select]", _module.dom).serialize());
            grid.store.reload();
            return false;
        });
        $("a[data-name=linkEmpty]", _module.dom).click(function () {
            _module.SelectedArray = [];
            sm.deselectAll();
            $("input[name=SelectedHouseIds]", _module.dom).val('');
            $("input[name=SelectedBuilds]", _module.dom).val('');
        });
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});
