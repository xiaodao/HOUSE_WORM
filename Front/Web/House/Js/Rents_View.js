﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Rents_View", {
    show: function (RentId) {
        var winDom;

        var mRents = getRemoteObject("/House/Services/House_Rents.asmx/Query", { id: RentId });
        var userObj = getRemoteObject("/RBAC/Services/RBAC_Users.asmx/Query", { id: mRents.AddUserId });
        var IsAllowDisplayPhoneNumber = RBAC.Js.RBAC_Module.SystemGetBool("House", "IsAllowDisplayPhoneNumber");
        if (!mRents) {
            $.messager.show("操作提示", "房源编号不存在");
            return;
        }
        var buttons = []
        if (X_USER == mRents.AddUser) {
            if (mRents.ViewLevel > 0) {
                buttons.push({
                    text: '转私盘',
                    handler: function (btn) {
                        $.get("House/Services/House_Rents_Follows.asmx/Add", { RentId: RentId, Follow: '转私盘', FollowHouseState: 0, ViewLevel: 2, ChgHouseViewLevel: 0 }, function (r) {
                            switch ($(r).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                    btn.setText("转公盘").disable();;
                                    break;
                                case "TimeOut":
                                    break;
                                case "NoPermiss":
                                    $.messager.show({ title: '操作提示', msg: "没有权限。" });
                                    break;
                                default:
                                    break;
                            }
                        });
                    }
                });
            }
            else if (mRents.ViewLevel == 0) {
                buttons.push({
                    text: '转公盘',
                    handler: function (btn) {
                        $.get("House/Services/House_Rents_Follows.asmx/Add", { RentId: RentId, Follow: '转公盘', FollowHouseState: 0, ViewLevel: 2, ChgHouseViewLevel: 2 }, function (r) {
                            switch ($(r).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                    btn.setText("转私盘").disable();;
                                    break;
                                case "NoPermiss":
                                    $.messager.show({ title: '操作提示', msg: "没有权限。" });
                                    break;
                                case "TimeOut":
                                    break;
                                default:
                                    break;
                            }
                        });
                    }
                });
            }
        }
        buttons = buttons.concat([
                 {
                     text: '举报是中介',
                     handler: function () {
                         popMenu('/Tools/Js/AgentMobiles_Add', 0, RentId);
                     }
                 },
                 {
                     text: '跟进',
                     handler: function () {
                         popMenu('/House/Js/Rents_Follows_Add', 0, RentId, winDom);
                     }
                 }, {
                     text: '收藏',
                     handler: function () {
                         popMenu('/House/Js/Rents_Favs_Add', 0, RentId);
                     }
                 }, {
                     text: "号码",
                     handler: function () {
                         var control = $("span[name=tdTel]", winDom);
                         if (control.css("visibility") == 'hidden') {
                             control.css("visibility", "visible")
                             if (!control.attr("data-load")) {
                                 control.attr("data-load", "true");
                                 ///中介
                                 var obj = getRemoteObject("/Tools/Services/Tools_AgentMobiles.asmx/QueryTel", { tel: mRents.CustomerTel });
                                 if (obj) {
                                     control.append('[中介号码]');
                                 } else {
                                     ///手机号码所在地
                                     $.get("Tools/Services/Tools_MobileArea.asmx/Query", { number: mRents.CustomerTel }, function (v) {
                                         control.append($(v).find("city").text());
                                     });
                                 }
                             }
                         }
                         else
                             control.css("visibility", "hidden");
                     }
                 }, {
                     text: '关闭',
                     handler: function () {
                         this.up("window").close();
                     }
                 }]);

        var win = Ext.create('Ext.window.Window', {
            title: '查看出租房源', height: 500, width: 900, modal: true,
            loader: {
                url: '/House/Templates/Rents_View.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = obj.dom;
                        var Region_Module = Ext.create("Region.Js.Region_Module");
                        mRents.AreaName = Region_Module.AreaRowsObj[mRents.RentRegionArea].AreaName;
                        var BuildingObj = Region_Module.getBuildingObj(mRents.RentBuildingId);
                        mRents.BuildingName = BuildingObj.BuildingName;
                        mRents.StateName = X_RENTSTATE[mRents.State];
                        mRents.StateColor = X_SELLSTATE_COLOR[mRents.State];
                        mRents.ViewLevelName = X_VIEWLEVEL[mRents.ViewLevel];
                        mRents.BuildingAddr = BuildingObj.BuildingAddr;
                        if (mRents.PayType == -1) mRents.PayType = "不限"; else
                            mRents.PayType = X_RENTPAYTYPE[mRents.PayType]
                        mRents.GetPropertyDate = formatDate(mRents.GetPropertyDate);
                        mRents.AddTime = formatDate(mRents.AddTime);
                        if (mRents.RentFace == -1) mRents.RentFaceName = '未知类型';
                        else
                            mRents.RentFaceName = X_HOUSEFACE[mRents.RentFace];
                        if (X_HOUSEFACE_NOTINGCHUWEI.indexOf(mRents.RentFace) > -1)
                            mRents.Display = "none";
                        else
                            mRents.Display = "inline";
                        mRents.DecorationName = X_DECORATEKEYS[mRents.Decoration];
                        mRents.DepartName = X_DEPARTS['_' + mRents.AddDepartId].DepartName;
                        if (IsAllowDisplayPhoneNumber) {
                            mRents.Mobile = userObj.UserMobile;
                        } else
                            mRents.Mobile = "";

                        var _tempId = win.getId();
                        mRents.TempId = _tempId;
                        var $win = $(".x-autocontainer-innerCt", "#" + _tempId);
                        var html = template('House_Rents_View', mRents);
                        $win.html(html);
                        // 百度地图API功能
                        try {
                            var lat = 31.781192779541;
                            var lng = 117.215980529785;
                            if (BuildingObj.BuildingLatitude) lat = parseFloat(BuildingObj.BuildingLatitude);
                            if (BuildingObj.BuildingLongitude) lng = parseFloat(BuildingObj.BuildingLongitude);

                            if (!BuildingObj.BuildingLatitude) {
                                $("#map" + _tempId).html("无此小区位置信息");
                            } else {
                                var map = new BMap.Map("map" + _tempId);            // 创建Map实例
                                var point = new BMap.Point(lng, lat);    // 创建点坐标

                                map.centerAndZoom(point, 14);                     // 初始化地图,设置中心点坐标和地图级别。
                                map.enableScrollWheelZoom();                            //启用滚轮放大缩小
                                var marker = new BMap.Marker(new BMap.Point(lng, lat));  // 创建标注
                                map.addOverlay(marker);              // 将标注添加到地图中
                                map.addControl(new BMap.MapTypeControl({ mapTypes: [BMAP_NORMAL_MAP, BMAP_HYBRID_MAP] }));     //2D图，卫星图
                                setTimeout(function () {
                                    map.centerAndZoom(point, 15);
                                }, 500);
                            }
                        } catch (e) { }

                        //加载跟进
                        $.ajax({
                            type: "get",
                            url: "/House/Services/House_Rents_Follows.asmx/GetLists",
                            dataType: 'json',
                            data: { page: 1, rows: 200, txtRentId: RentId, tel: '' },
                            contentType: "application/json;utf-8",
                            success: function (data) {
                                var rows = loadFilter(data).rows;
                                if (rows) {
                                    for (var i = 0; i < rows.length; i++) {
                                        var row = rows[i];
                                        if (!row) continue;
                                        var _str = "<li>【{0}】[{1}][{3}]{2}</li>".format(row.AddUser,
                                         (row.AddTime.toDate()).format("yy-MM-dd hh:mm"),
                                         row.Follow,
                                         X_RENTSTATE[row.State]);
                                        $(".ulHouseRent", $win).append(_str);
                                    }
                                }
                            }
                        });

                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    constructor: function (config) {
        this.initConfig(config);
    }
});