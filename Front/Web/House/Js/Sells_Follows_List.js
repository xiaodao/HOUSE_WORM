﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.require('RBAC.Js.RBAC_Module');
Ext.define("House.Js.Sells_Follows_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var DongFangInList = RBAC.Js.RBAC_Module.SystemGetBool("House", "DongFangInList");
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['FollowId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/House/Services/House_Sells_Follows.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = easyui2extgrid([[
                    { field: 'SellId', title: '房源编号', width: 50 },
                    { field: 'SellRegionArea', title: '区域', width: 50, align: 'center', formatter: formatRegionArea },
                    { field: 'BuildingName', title: '小区', width: 150, formatter: formatBuilding },
                    { field: 'SellFace', title: '类型', width: 50, align: 'center', formatter: function (v, r, k) { return X_HOUSEFACE[v]; } },
                    { field: 'HouseFloor', title: '楼层', width: 50, align: 'center', formatter: function (v, r, k) { return v + "/" + r.HouseTotalFloor; } },
                    { field: 'Shi', title: '户型', width: 40, align: 'center', formatter: function (v, r, k) { return v + "-" + r.Ting; } },
                    { field: 'HouseArea', title: '面积', width: 60, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'UnitPrice', title: '价格', width: 60, align: 'left', formatter: formatPrice },
                    { field: 'Follow', title: '跟进内容', width: 200 },
                    { field: 'FollowHouseState', title: '跟进状态', width: 60, formatter: function (v, r, k) { return X_SELLSTATE[v]; }, styler: styleHouseState },
                    { field: 'State', title: '房源状态', width: 60, formatter: function (v, r, k) { return X_SELLSTATE[v]; }, styler: styleHouseState },
                    { field: 'AddUser', title: '跟进姓名', width: X_WIDTH.UserName },
                    { field: 'AddTime', title: '跟进时间', width: X_WIDTH.Time, formatter: formatShortTime }
        ]]);
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Sells_View = Ext.create("House.Js.Sells_View");
                    Sells_View.show(row.data.SellId);
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            if (!$("form[name=fmList]", view.dom).form("validate")) { return false; }
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
           // grid.store.reload();
            return false;
        });
        function formatBuilding(v, r, k) {
            if (DongFangInList) {
                return "{0} {1}#{2}".format(v, r.Dong, r.Fang);
            }
            else
                return v;
        }
    }
});
