﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 收藏
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Sells_Favs_Add", {
    show: function (FavId,SellId) {
        $.ajax({
            url: 'House/Services/House_Sells_Favs.asmx/Add',
            data: { SellsId: SellId },
            success: function (data) {
                switch ($(data).find("ServiceStateKeys").text()) {
                    case "Success":
                        $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                        return true;
                        break;
                    case "Exists":
                        $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                        return false;
                        break;
                    case "NoPermiss":
                        $.messager.show({ title: '操作提示', msg: "没有操作权限。" });
                        return false;
                        break;
                    default:
                        $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                        return false;
                        break;
                }
            }
        });
       
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});
