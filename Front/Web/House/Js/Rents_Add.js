﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Rents_Add", {
    show: function (RentId) {
        var winDom;
        var _title;
        if (RentId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '出租房源', height: 540, width: 900, modal: true,
            loader: {
                url: '/House/Templates/Rents_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=RentId]", winDom).val(RentId);
                        renderUserControls(winDom);


                        $("input[name=RentBuildingId]", winDom).change(function () {
                            var val = $(this).val();
                            var _dong = "";
                            var _fang = "";
                            if ($("input[name=Dong]", winDom).val()) {
                                _dong = $("input[name=Dong]", winDom).val();
                            }
                            if ($("input[name=Fang]", winDom).val()) {
                                _fang = $("input[name=Fang]", winDom).val();
                            }
                            if (val.chkType("number"))
                                renderSimilar({ Dong: _dong, Fang: _fang, RentBuildingId: val }, $("table[name=dgBuilding]", winDom));
                            else
                                renderSimilar({ Dong: _dong, Fang: _fang, BuildingName: val }, $("table[name=dgBuilding]", winDom));

                            if ($("select[name=RentRegionArea]", winDom).val() == "0" || $("select[name=RentRegionArea]", winDom).val() == "") {
                                $("select[name=RentRegionArea]", winDom).val($(this).attr("data-area"));
                            }
                        });

                        $("input[name=CustomerTel]", winDom).change(function () {
                            renderSimilar({ Mobile: $(this).val() }, $("table[name=dgMobile]"));
                        });
                        function renderSimilar(param, $dg) {
                            var _type;
                            if ($dg.attr("name") == 'dgMobile')
                                _type = 'mobile';
                            else
                                _type = 'building';
                            $dg.datagrid({
                                url: "House/Services/House_Rents.asmx/GetLists",
                                data: $.extend({}, param),
                                method: 'GET',
                                idFiled: 'RentId',
                                loadMsg: '正在加载，请稍候……',
                                pagination: false,
                                rownumbers: false,
                                pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
                                pageSize: 10,
                                toolbar: $("div[name=toolbar]", "#House-Rent_All"),
                                pagination: false, width: 525,
                                singleSelect: true, height: 200,
                                columns: [[
                                    { field: 'BuildingName', title: '小区', width: 150, align: 'left', hidden: _type == 'building' ? true : false },
                                    { field: 'HouseFloor', title: '楼层', width: 50, align: 'center', formatter: function (v, r, k) { return v + "/" + r.HouseTotalFloor; } },
                                    { field: 'Shi', title: '户型', width: 40, align: 'center', formatter: function (v, r, k) { return v + "-" + r.Ting; } },
                                    { field: 'HouseArea', title: '面积', width: 60, align: 'center', formatter: function (v, r, k) { return v + "平"; } },
                                    { field: 'TotalPrice', title: '价格', width: 60, align: 'left', formatter: function (v, r, k) { return v + "元"; } },
                                    { field: 'Decoration', title: '装修', width: 40, align: 'center', formatter: function (v, r, k) { return X_DECORATEKEYS[v]; } },
                                    { field: 'LastTime', title: '时间', width: 100, formatter: formatShortTime, align: 'center' },
                                    { field: 'State', title: '状态', width: 40, align: 'center', formatter: function (v, r, k) { return X_RENTSTATE[r.State]; } }
                                ]],
                                loadFilter: function (data) {
                                    return loadFilter(data);
                                },
                                onLoadSuccess: function (data) {
                                    var msg;
                                    if (data.rows.length) {
                                        msg = '加载 ' + data.rows.length + ' 条记录成功!';
                                    }
                                    else {
                                        msg = '没有数据!';
                                    }
                                    $.messager.show({ title: '操作提示', msg: msg });
                                }
                            });
                        }
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/House/Services/House_Rents.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "操作成功" });
                                    _win.close();
                                    RBAC.Js.RBAC_Module.render('House.Js.Rents_List');
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
       
    }
});
