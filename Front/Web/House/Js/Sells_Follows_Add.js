﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/**
* 
*/
Ext.define("House.Js.Sells_Follows_Add", {
    show: function (FollowId, _HouseId,ParentWin) {
        var winDom;
        var win = Ext.create('Ext.window.Window', {
            title: '新增跟进', height: 250, width: 295, modal: true,
            loader: {
                url: '/House/Templates/Sells_Follows_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = obj.dom;
                        $("input[name=HouseId]", winDom).val(_HouseId);
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    $.ajax({
                        url: 'House/Services/House_Sells_Follows.asmx/Add',
                        data: $("form[name=fm]", winDom).serializeArray(),
                        success: function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    if ($(".ulHouseSell", ParentWin).length > 0) {
                                        var _str = "<li>【{0}】[{1}][{3}]{2}</li>".format(X_USER, (new Date()).format("yy-MM-dd hh:mm"), $("textarea[name=follow]", winDom).val(),
                                        X_SELLSTATE[$("input[name='FollowHouseState']:checked", winDom).val()]);
                                        $(".ulHouseSell", ParentWin).prepend(_str);
                                    }
                                    $.messager.show({title:"操作提示",msg:"操作成功！"});
                                    _win.close();
                                    break;
                                default:
                                    Ext.Msg.show({title:"操作提示", message:"保存操作失败"});
                                    break;
                            }
                        }
                    });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});
