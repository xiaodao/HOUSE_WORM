﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Rents_Favs_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var DongFangInList = RBAC.Js.RBAC_Module.SystemGetBool("House", "DongFangInList");
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['FavId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/House/Services/House_Rents_Favs.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = easyui2extgrid([[
                    { field: 'RentId', title: '房源编号', width: 50 },
                    { field: 'RentRegionArea', title: '区域', width: 50, align: 'center', formatter: formatRegionArea },
                    { field: 'BuildingName', title: '小区', width: 150, formatter: formatBuilding },
                    { field: 'RentFace', title: '类型', width: 50, align: 'center', formatter: function (v, r, k) { return X_HOUSEFACE[v]; } },
                    { field: 'HouseFloor', title: '楼层', width: 50, align: 'center', formatter: function (v, r, k) { return v + "/" + r.HouseTotalFloor; } },
                    { field: 'Shi', title: '户型', width: 40, align: 'center', formatter: function (v, r, k) { return v + "-" + r.Ting; } },
                    { field: 'HouseArea', title: '面积', width: 60, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'TotalPrice', title: '月租', width: 60, align: 'left', formatter: formatPrice },
                    { field: 'Decoration', title: '装修', width: 40, align: 'center', formatter: function (v, r, k) { return X_DECORATEKEYS[v]; } },
                    { field: 'LastTime', title: '最后时间', width: 100, formatter: formatShortTime, align: 'center' },
                    { field: 'State', title: '状态', width: 60, formatter: function (v, r, k) { return X_RENTSTATE[v]; }, styler: styleHouseState }
        ]]);
        var tbl = view.query(".mainList");
        var selectRowData = {};
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Rents_View = Ext.create("House.Js.Rents_View");
                    Rents_View.show(row.data.RentId);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });
        $("a[name=linkDel]", view.dom).click(function () {
            if (selectRowData) {
                $.ajax({
                    url: "House/Services/House_Rents_Favs.asmx/Delete",
                    data: { RentsId: selectRowData.RentId },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                RBAC.Js.RBAC_Module.render('House.Js.Rents_List,House.Js.Rents_Favs_List');
                                break;
                            case "Exists":
                                $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                                break;
                            case "NoPermiss":
                                $.messager.show({ title: '操作提示', msg: "没有操作权限。" });
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                break;
                        }
                    }
                });
            }
        });
        function formatBuilding(v, r, k) {
            if (DongFangInList) {
                return "{0} {1}#{2}".format(v, r.Dong, r.Fang);
            }
            else
                return v;
        }
    }
});

//(function a(n) {
//    //按钮链接
//    return;
//    var $btnSearch = $("input[name=btnSearch]", "#" + n);
//    var $linkDel = $("a[name=linkDel]", "#" + n);
//    //表
//    var $dg = $("table[name=dg]", "#" + n);
//    //枚举
//    var enumUrl = {//s服务 w//窗体
//        sDel: "House/Services/House_Rents_Favs.asmx/Delete",
//        sList: 'House/Services/House_Rents_Favs.asmx/GetLists'
//    };
//    //系统设置
//    var DongFangInList = SystemGetBool("House", "DongFangInList");
//    //事件
//    $btnSearch.click(function () { render(); });
//    $linkDel.click(function () {
//        var row = $dg.datagrid('getSelected');
//        if (row) {
//            $.messager.confirm('操作提示', '确认要删除该收藏吗？', function (r) {
//                if (r) {
//                    $.ajax({
//                        data: { id: row.FavId },
//                        type: "GET",
//                        url: enumUrl.sDel,
//                        dataType: "json",
//                        headers: { 'Content-Type': 'application/json;utf-8' },
//                        success: function (result) {
//                            if (result.d) {
//                                $.messager.show({ title: '操作提示', msg: '删除操作成功!' });
//                                render();
//                            }
//                        }
//                    });
//                }
//            });
//        }
//    });
//    render();
//    //函数
//    function render() {
//        $dg.datagrid({
//            url: enumUrl.sList + "?" + $("form[name=fm]", "#House-Rent_Fav_List").serialize(),
//            //title: '收藏列表',
//            method: 'GET',
//            idFiled: 'RentId',
//            loadMsg: '正在加载，请稍候……',
//            pagination: true,
//            rownumbers: true,
//            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
//            pageSize: 20,
//            toolbar: $("div[name=toolbar]", "#" + n),
//            singleSelect: true,
//            columns: [[
//                    { field: 'RentId', title: '房源编号', width: 50 },
//                    { field: 'RentRegionArea', title: '区域', width: 50, align: 'center', formatter: formatRegionArea },
//                    { field: 'BuildingName', title: '小区', width: 150, formatter: formatBuilding },
//                    { field: 'RentFace', title: '类型', width: 50, align: 'center', formatter: function (v, r, k) { return X_HOUSEFACE[v]; } },
//                    { field: 'HouseFloor', title: '楼层', width: 50, align: 'center', formatter: function (v, r, k) { return v + "/" + r.HouseTotalFloor; } },
//                    { field: 'Shi', title: '户型', width: 40, align: 'center', formatter: function (v, r, k) { return v + "-" + r.Ting; } },
//                    { field: 'HouseArea', title: '面积', width: 60, align: 'center', formatter: function (v, r, k) { return v; } },
//                    { field: 'TotalPrice', title: '月租', width: 60, align: 'left', formatter: formatPrice },
//                    { field: 'Decoration', title: '装修', width: 40, align: 'center', formatter: function (v, r, k) { return X_DECORATEKEYS[v]; } },
//                    { field: 'LastTime', title: '最后时间', width: 100, formatter: formatShortTime, align: 'center' },
//                    { field: 'State', title: '状态', width: 60, formatter: function (v, r, k) { return X_RENTSTATE[v]; }, styler: styleHouseState }
//            ]],
//            loadFilter: function (data) {
//                return loadFilter(data);
//            },
//            onLoadSuccess: function (data) {
//                if (data.rows.length) {
//                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
//                }
//                else {
//                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
//                }
//            },
//            onClickRow: function (rowIndex,rowData) {
//                popMenu('/House/Js/Rent_View', rowData.RentId);
//            }
//        });
//    }
//    function formatBuilding(v, r, k) {
//        if (DongFangInList) {
//            return "{0} {1}#{2}".format(v, r.Dong, r.Fang);
//        }
//        else
//            return v;
//    }

//})('House-Rent_Fav_List');