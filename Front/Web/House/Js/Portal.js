﻿/***最新出售房源**/
$(document).ready(function () {
    var _height = 486;//4个模块 高度306  2个模块高度486
    var pSell = $('<div><table name="tblSell"></table></div>').appendTo('#Portal-Default');
    pSell.panel({
        title: '推荐房源',
        height: _height,
        collapsible: true
    });
    $('#content').portal('add', {
        panel: pSell,
        columnIndex: 1
    });
    var $dgSell = $("table[name=tblSell]", '#Portal-Default');
    var DongFangInList = SystemGetBool("House", "DongFangInList");
    $dgSell.datagrid({
        url: 'House/Services/House_Sells.asmx/GetLists' + "?Suggest=1&" + $("form[name=fm]", "#House-Sell_List").serialize(),
        method: 'GET', queryParams: { page: 1, rows: 8 },
        idFiled: 'SellId',
        loadMsg: '正在加载，请稍候……',
        pagination: true,
        pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
        pageSize:10,
        toolbar: $("div[name=toolbar]", "#House-Sell_List"),
        singleSelect: true,
        columns: [[
                { field: 'SellRegionArea', title: '区域', width: 50, align: 'center', formatter: formatRegionArea },
                 { field: 'Suggest', title: '类别', width: 50, align: 'left', formatter: function (c) { return X_SUGGESTSTATE[c]; } },
                { field: 'BuildingName', title: '小区', width: 100, align: 'left', formatter: formatBuilding },
                { field: 'SellFace', title: '类型', width: 40, align: 'center', formatter: function (v, r, k) { return X_HOUSEFACE[v]; } },
                { field: 'HouseFloor', title: '楼层', width: 45, align: 'center', formatter: function (v, r, k) { return v + "/" + r.HouseTotalFloor; } },
                { field: 'Shi', title: '户型', width: 40, align: 'center', formatter: function (v, r, k) { return v + "-" + r.Ting; } },
                { field: 'HouseArea', title: '面积', width: 40, align: 'center' },
                { field: 'TotalPrice', title: '总价', width: 30, align: 'left', formatter: formatPrice },
                { field: 'AddTime', title: '登记时间', width: 90, formatter: formatShortTime, align: 'center', styler: styleAddTime }
        ]],
        loadFilter: function (data) {
            return loadFilter(data);
        },
        onClickRow: function (rowIndex, rowData) {
            popMenu('/House/Js/Sells_View', rowData.SellId);
        }
    });

    function formatBuilding(v, r, k) {
        if (DongFangInList) {
            return "{0} {1}#{2}".format(v, r.Dong, r.Fang);
        }
        else
            return v;
    }
    function styleAddTime(v, r, k) {
        var addtime = v;
        var lasttime = r.LastTime;
        if (addtime != lasttime) {
            return 'color:red';
        } else
            return '';
    }
});