﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Rents_Follows_Add", {
    show: function (FollowId, _HouseId, ParentWin) {
        var winDom;
        var _title;
        if (FollowId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '跟进', height: 250, width: 295, modal: true,
            loader: {
                url: '/House/Templates/Rents_Follows_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=RentId]", winDom).val(_HouseId);
                        renderUserControls(winDom);
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    //确认
                    if (!$("form[name=fm]", winDom).form("validate")) {
                        $.messager.show({ title: '操作提示', msg: "请检查输入内容。" });
                        return;
                    }
                    $.ajax({
                        url: 'House/Services/House_Rents_Follows.asmx/Add',
                        data: $("form[name=fm]", winDom).serializeArray(),
                        success: function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                    var _str = "<li>【{0}】[{1}][{3}]{2}</li>".format(X_USER,
                                    (new Date()).format("yy-MM-dd hh:mm"),
                                    $("textarea[name=follow]", winDom).val(),
                                    X_RENTSTATE[$("input[name='FollowHouseState']:checked", winDom).val()]);
                                    $(".ulHouseRent", ParentWin).prepend(_str);
                                    _win.close();
                                    break;
                                default:
                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                    break;
                            }
                        }
                    });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});