﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.require('RBAC.Js.RBAC_Module');
Ext.define("House.Js.Sells_Views_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var DongFangInList = RBAC.Js.RBAC_Module.SystemGetBool("House", "DongFangInList");
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['ViewId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/House/Services/House_Sells_Views.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = easyui2extgrid([[
                               { field: 'SellId', title: '房源编号', width: 50 },
                               { field: 'SellRegionArea', title: '区域', width: 50, align: 'center', formatter: formatRegionArea },
                               { field: 'BuildingName', title: '小区', width: 150, align: 'left', formatter: formatBuilding },
                               { field: 'SellFace', title: '类型', width: 50, align: 'center', formatter: function (v, r, k) { return X_HOUSEFACE[v]; } },
                               { field: 'HouseFloor', title: '楼层', width: 50, align: 'center', formatter: function (v, r, k) { return v + "/" + r.HouseTotalFloor; } },
                               { field: 'Shi', title: '户型', width: 40, align: 'center', formatter: function (v, r, k) { return v + "-" + r.Ting; } },
                               { field: 'HouseArea', title: '面积', width: 60, align: 'center', formatter: function (v, r, k) { return v + "平"; } },
                               { field: 'UnitPrice', title: '单价(元/平)', width: 60, align: 'left' },
                               { field: 'TotalPrice', title: '总价(万元)', width: 60, align: 'left' },
                               { field: 'Decoration', title: '装修', width: 40, align: 'center', formatter: function (v, r, k) { return X_DECORATEKEYS[v]; } },
                               { field: 'AddTime', title: '浏览时间', width: 100, formatter: formatShortTime, align: 'center' },
                               { field: 'AddDepartId', title: '浏览部门', width: 50, align: 'center', formatter: formatDepart },
                               { field: 'AddUser', title: '浏览人', width: 50, align: 'center' },
                               { field: 'State', title: '状态', width: 40, align: 'center', styler: styleHouseState, formatter: function (v, r, k) { return X_SELLSTATE[r.State]; } }
        ]]);
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Sells_View = Ext.create("House.Js.Sells_View");
                    Sells_View.show(row.data.SellId);
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Sells_Add = Ext.create("House.Js.Sells_Add");
            Sells_Add.show(0);
        });
        function formatBuilding(v, r, k) {
            if (DongFangInList) {
                return "{0} {1}#{2}".format(v, r.Dong, r.Fang);
            }
            else
                return v;
        }
    }
});
