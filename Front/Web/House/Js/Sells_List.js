﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/>  
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('House.Js.Sells_View');
Ext.define("House.Js.Sells_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view, padding_top, is_desk) {
        var DongFangInList = RBAC.Js.RBAC_Module.SystemGetBool("House", "DongFangInList");
        if (!padding_top) padding_top = 0;
        if (!is_desk) is_desk = false;
        var Rows = [];
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            fields: ['SellId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/House/Services/House_Sells.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = [
             { text: '编号', dataIndex: 'SellId', width: X_WIDTH.Number, align: 'center', hidden: is_desk },
             { text: '区域', dataIndex: 'SellRegionArea', width: X_WIDTH.RegionArea, align: 'center', renderer: formatRegionArea },
             { text: '小区', dataIndex: 'BuildingName', renderer: formatBuilding },
             { text: '类型', dataIndex: 'SellFace', width: X_WIDTH.State, renderer: function (c) { return X_HOUSEFACE[c]; }, hidden: is_desk },
             { text: '楼层', dataIndex: 'HouseFloor', width: X_WIDTH.Floor, align: 'center', renderer: function (v, cellmeta, r) { return v + "/" + r.data.HouseTotalFloor; } },
             { text: '户型', dataIndex: 'Shi', width: X_WIDTH.State, align: 'center', renderer: function (v, cellmeta, r) { return v + "-" + r.data.Ting; } },
             { text: '面积', dataIndex: 'HouseArea', width: X_WIDTH.Int, align: 'center' },
             { text: '单价', dataIndex: 'UnitPrice', width: X_WIDTH.Int, align: 'center', renderer: formatPrice },
             { text: '总价', dataIndex: 'TotalPrice', width: X_WIDTH.Int, align: 'center', renderer: formatPrice },
             { text: '装修', dataIndex: 'Decoration', width: X_WIDTH.State, align: 'center', renderer: function (v, r, k) { return X_DECORATEKEYS[v]; } },
             { text: '登记时间', dataIndex: 'AddTime',width:X_WIDTH.Time, renderer: function (value, cellmeta, record, rowIndex, columnIndex, store) { return formatShortTime(value); } },
             { text: '部门', dataIndex: 'AddDepartId', width: X_WIDTH.Depart, align: 'center', renderer: formatDepart,hidden:is_desk },
             { text: '发布人', dataIndex: 'AddUser', width: X_WIDTH.UserName, align: 'center' },
             { text: '状态', dataIndex: 'State', width: X_WIDTH.State, align: 'center', renderer: function (v, c, r) { return "<span style='" + styleHouseState(v) + "'>" + X_SELLSTATE[v] + "</span>"; } }
        ];
        var tbl = view.query(".mainList");
        var sm = new Ext.selection.CheckboxModel({ checkOnly: false, mode: this.isSingleSelect ? "SINGLE" : 'SIMPLE' });
        var searchHeight = $(".auto", view.dom).height();
        if (!searchHeight) searchHeight = 0;
       
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - searchHeight - padding_top - X_TAB_HEIGHT + 30, 
            store: Store, width: '100%', forceFit: true,
            //viewConfig: {

            //    getRowClass: function () {
            //        // 在这里添加自定样式 改变这个表格的行高，样式在app.css中设置
            //        return 'x-grid-row custom-grid-row';
            //    }
            //},
            columns: Columns, selModel: RBAC.Js.RBAC_Module.CheckLimit("出售房源", "删") ? sm : null,
            listeners: {
                rowclick: function (grid, row) {
                    var Sells_View = Ext.create("House.Js.Sells_View");
                    Sells_View.show(row.data.SellId);
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });
        $(window).resize(function () {
            var mainListHeight = $(".mainList",view.dom).height();
            if (mainListHeight > 0) {
                grid.setHeight(mainListHeight);
            }
        });
        //events

        function formatBuilding(v, r, k) {
            if (DongFangInList) {
                if (k.data.ViewLevel == 0) {
                    return "<span style='color:red'>(私)</span>" + v + k.data.Dong + '#' + k.data.Fang;
                } else {
                    return "<span style='color:green'>(公)</span>" + v + k.data.Dong + '#' + k.data.Fang;
                }
            }
            else {
                if (k.data.ViewLevel == 0) {
                    return "<span style='color:red'>(私)</span>" + v;
                } else { 
                    return "<span style='color:green'>(公)</span>" + v;
                }
            }
        }
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });
        $("form[name=fmList] input[type=reset]", view.dom).click(function () {
            $("input[name=SellBuildingId]", view.dom).val('');
            var _id = $("select[name=SellRegionArea_T]", view.dom).attr("data-ext-id");
            var $id = Ext.getCmp(_id); 
            $id.clearValue();
            return true;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Sells_Add = Ext.create("House.Js.Sells_Add");
            Sells_Add.show(0);
        });
        $("a[name=linkDel]", view.dom).click(function () {
            var selected = grid.getView().getSelectionModel().getSelection();
            var _ids = "";
            Ext.each(selected, function (item) {
                if (_ids != "") _ids += ",";
                _ids += item.data.SellId;
            });
            if (!_ids) {
                if (selectRowData) _ids = selectRowData.SellId;
            }
            if (_ids) {
                $.ajax({
                    url: "House/Services/House_Sells.asmx/DeleteBatch",
                    data: { SellId: _ids },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "删除操作成功" });
                                RBAC.Js.RBAC_Module.render('House.Js.Sells_List');
                                break;
                            case "Exists":
                                $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                break;
                        }
                    }
                });
            }
        });
    }
});
