﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("House.Js.Rents_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var DongFangInList = RBAC.Js.RBAC_Module.SystemGetBool("House", "DongFangInList");
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['RentId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/House/Services/House_Rents.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = [
             { text: '编号', dataIndex: 'RentId', width: X_WIDTH.Number, align: 'center' },
             { text: '区域', dataIndex: 'RentRegionArea', width: X_WIDTH.RegionArea, align: 'center', renderer: formatRegionArea },
             { text: '小区', dataIndex: 'BuildingName',renderer: formatBuilding },
             { text: '类型', dataIndex: 'RentFace', width: X_WIDTH.State, renderer: function (c) { return X_HOUSEFACE[c]; } },
             { text: '楼层', dataIndex: 'HouseFloor', width: X_WIDTH.Floor, align: 'center', renderer: function (v, cellmeta, r) { return v + "/" + r.data.HouseTotalFloor; } },
             { text: '户型', dataIndex: 'Shi', width: X_WIDTH.State, align: 'center', renderer: function (v, cellmeta, r) { return v + "-" + r.data.Ting; } },
             { text: '面积', dataIndex: 'HouseArea', width: X_WIDTH.Int, align: 'center' },
             { text: '月租', dataIndex: 'TotalPrice', width: X_WIDTH.Int, align: 'center', renderer: formatPrice },
             { text: '装修', dataIndex: 'Decoration', width: X_WIDTH.State, align: 'center', renderer: function (v, r, k) { return X_DECORATEKEYS[v]; } },
             { text: '登记时间', dataIndex: 'AddTime', width: X_WIDTH.Time, renderer: function (value, cellmeta, record, rowIndex, columnIndex, store) { return formatShortTime(value); } },
             { text: '部门', dataIndex: 'AddDepartId', width: X_WIDTH.Depart, align: 'center', renderer: formatDepart },
             { text: '发布人', dataIndex: 'AddUser', width: X_WIDTH.UserName, align: 'center' },
             { text: '状态', dataIndex: 'State', width: X_WIDTH.State, align: 'center', renderer: function (v, c, r) { return "<span style='" + styleHouseState(v) + "'>" + X_RENTSTATE[v] + "</span>"; } }
        ];
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT+30,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Rents_View = Ext.create("House.Js.Rents_View");
                    Rents_View.show(row.data.RentId);
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        function formatBuilding(v, r, k) {
            if (DongFangInList) {
                if (k.data.ViewLevel == 0) {
                    return "<span style='color:red'>(私)</span>" + v + k.data.Dong + '#' + k.data.Fang;
                } else {
                    return "<span style='color:green'>(公)</span>" + v + k.data.Dong + '#' + k.data.Fang;
                }
            }
            else {
                if (k.data.ViewLevel == 0) {
                    return "<span style='color:red'>(私)</span>" + v;
                } else {
                    return "<span style='color:green'>(公)</span>" + v;
                }
            }
        }

        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);//设置回到首页
            grid.store.reload();
            return false;
        });
        $("form[name=fmList] input[type=reset]", view.dom).click(function () {
            $("input[name=RentBuildingId]", view.dom).val('');
            var _id = $("select[name=RentRegionArea_T]", view.dom).attr("data-ext-id");
            var $id = Ext.getCmp(_id);
            $id.clearValue();
            return true;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Rents_Add = Ext.create("House.Js.Rents_Add");
            Rents_Add.show(0);
        });
    }
});