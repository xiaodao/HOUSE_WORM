﻿using log4net;
using System;
using System.Web;

public partial class _Default : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        HttpContext.Current.Server.MapPath("~/");
    }
}