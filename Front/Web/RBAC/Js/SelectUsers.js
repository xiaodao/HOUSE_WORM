﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* controlid是id的名称，指userid控件id,
* 要求返回的必须是input name= SignUser SignUserId UserMobile SignDepartId，
* 它们的parent().parent()是同一控件。
*/
Ext.require('Region.Js.Region_Module');
Ext.define("RBAC.Js.SelectUsers", {
    show: function (controlid) {
        var winDom;
        var selectRow;
        var win = Ext.create('Ext.window.Window', {
            title: '选择用户', height: 560, width: 650, modal: true,
            loader: {
                url: '/RBAC/Templates/SelectUsers.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        renderUserControls(winDom);
                        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
                            fields: ['UserId'],
                            pageSize: 10,
                            autoLoad: true,
                            proxy: {
                                type: 'ajax',
                                enablePaging: true,//支持翻页
                                url: '/RBAC/Services/RBAC_Users.asmx/GetLists',
                                pageParam: 'page',
                                startParam: '{startParam}',
                                limitParam: 'rows',
                                headers: { "Content-Type": 'application/json' },
                                reader: {
                                    type: 'json',
                                    rootProperty: 'd.Table.rows',
                                    totalProperty: 'd.Total'
                                },
                                writer: {
                                    type: 'json'
                                },
                                extraParams: $.par2Json($("form[name=fmRBAC_User_Select]", winDom).serialize()),
                                sortParam: 'UserId'
                            }
                        });
                        var Columns = easyui2extgrid([[
                            { field: 'UserId', title: '编号', width: 40 },
                            { field: 'UserName', title: '姓名', width: 50, align: 'center' },
                            { field: 'DepartId', title: '部门', formatter: formatDepart, align: 'center' },
                            { field: 'UserMobile', title: '电话', align: 'center' }
                        ]]);
                        var grid = Ext.create('Ext.grid.Panel', {
                            scroll: true, height: 440,
                            store: Store, width: '100%', forceFit: true,
                            columns: Columns,
                            listeners: {
                                rowclick: function (grid, row) {
                                    selectRow = row.data;
                                }
                            },
                            bbar: Ext.create('Ext.PagingToolbar', {
                                store: Store,
                                displayInfo: true,
                                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                                emptyMsg: "没有数据"
                            }),
                            renderTo: $(".mainList", winDom)[0]
                        });
                        //工具 todo:
                        $("input[name=btnSearch]", winDom).click(function () {
                            render();
                        });
                        function render() {
                            grid.store.proxy.extraParams = $.par2Json($("form[name=fmRBAC_User_Select]", winDom).serialize());
                            grid.store.reload();
                            return false;
                        }
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');

                    if (selectRow) {
                        var container = $("#" + controlid).parent().parent();
                        var SignUser = container.find("input[name=SignUser]");
                        var SignUserId = container.find("input[name=SignUserId]");
                        var UserMobile = container.find("input[name=UserMobile]");
                        var SignDepartId = container.find("input[name=SignDepartId]");
                        if (SignUser) SignUser.val(selectRow.UserName);
                        if (SignUserId) SignUserId.val(selectRow.UserId);
                        if (UserMobile) UserMobile.val(selectRow.UserMobile);
                        if (SignDepartId) SignDepartId.val(selectRow.DepartId).change();
                    }
                    _win.close();
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});
