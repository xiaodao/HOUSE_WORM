﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("RBAC.Js.Roles_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['RoleId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/RBAC/Services/RBAC_Roles.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'RoleId'
            }
        });
        var Columns = [
             { text: '编号', dataIndex: 'RoleId', width: X_WIDTH.Number, align: 'center' },
             { text: '角色', dataIndex: 'RoleName',  align: 'center' }
         
        ];
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,border:true,scroll:true,
            listeners: {
                rowclick: function (grid, row) {
                    var Roles_View = Ext.create("RBAC.Js.Roles_View");
                    Roles_View.show(row.data.RoleId);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Roles_Add = Ext.create("RBAC.Js.Roles_Add");
            Roles_Add.show(0);
        });
    }
});

///// <reference path="/Js/jsUtil.js"/> 
///// <reference path="/Js/main.js"/> 
///// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
///// <reference path="/Js/app.js"/> 
///**
//* 
//*/
//Ext.require('Region.Js.Region_Module');
//Ext.define("RBAC.Js.Roles_List", {
//    constructor: function (config) {
//        this.initConfig(config);
//    },
//    renderList: function (view) {
//        var arrAllResource = getRemoteRows('RBAC/Services/RBAC_Resources.asmx/GetAllLists');
//        var arrRoles = getRemoteRows('/RBAC/Services/RBAC_Roles.asmx/GetLists');
//        var functionCols = [];
//        var _tree = [];

//        for (var j = 0; j < arrAllResource.length; j++) {
//            functionCols.push({
//                text: arrAllResource[j].ResourceName,
//                align: 'center',
//                width: X_WIDTH.Number * 2,
//                dataIndex: '_' + arrAllResource[j].ResourceId
//                ,
//                renderer: function (v, k, r) {
//                    //console.log(v, k.record.data.RoleName, r);
//                    if (['增', '删', '改', '查', '查部门', '查所有'].indexOf(k.record.data.RoleName) > -1) {
//                        var _type = typeof (r.data.RoleId);
//                        if (['string', 'int', 'number'].indexOf(_type) > -1)
//                            return "<input type='checkbox' name='chk' resourceId='" + functionCols[k.columnIndex].dataIndex.replace("_", "") + "' RoleId='" + r.data.RoleId + "' />";
//                    }
//                }
//            });
//        }
//        for (var _j = 0; _j < arrRoles.length; _j++) {
//            var _temp = arrRoles[_j];
//            _tree.push({
//                'RoleId': _temp.RoleId, "leaf": false,
//                'RoleName': "&nbsp;&lt;&nbsp;" + _temp.RoleName + "&nbsp;&gt;&nbsp;",
//                expanded: true,
//                children: [
//                   { 'RoleName': '增', 'RoleId': '' },
//                   { 'RoleName': '删', 'RoleId': '' },
//                   { 'RoleName': '改', 'RoleId': '' },
//                   { 'RoleName': '查', 'RoleId': '' },
//                   { 'RoleName': '查部门', 'RoleId': '' },
//                   { 'RoleName': '查所有', 'RoleId': '' }
//                ]
//            });
//        }
//        var Tree = { rows: _tree, total: _tree.length };
//        var Store = Ext.create('Ext.data.TreeStore', { //store和model一起创建
//            fields: ['RoleId'], collapsible: true,
//            rootVisible: false,
//            root: {
//                children: Tree.rows, expanded: true, rootVisible: false,
//                RoleName: '角色', RoleId: 0
//            }
//            //pageSize: 30,
//            //autoLoad: true,
//            //proxy: {
//            //    type: 'ajax',
//            //    enablePaging: true,//支持翻页
//            //    url: '/RBAC/Services/RBAC_Roles.asmx/GetLists',
//            //    pageParam: 'page',
//            //    startParam: '{startParam}',
//            //    limitParam: 'rows',
//            //    headers: { "Content-Type": 'application/json' },
//            //    reader: {
//            //        type: 'json',
//            //        rootProperty: 'd.Table.rows',
//            //        totalProperty: 'd.Total'
//            //    },
//            //    writer: {
//            //        type: 'json'
//            //    },
//            //    extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
//            //    sortParam: 'LastTime'
//            //},
//            //listeners: {
//            //    load: function (t, records, succesful, eOpts) {
//            //        //赋值
//            //        //$.ajax({
//            //        //    type: "GET",
//            //        //    url: 'RBAC/Services/RBAC_Role_Resource_Operation.asmx/GetLists',
//            //        //    data: { RoleIds: '' },
//            //        //    dataType: "json",
//            //        //    headers: { 'Content-Type': 'application/json;utf-8' },
//            //        //    success: function (result) {
//            //        //        if (result.d.Table.rows) {
//            //        //            var _rows = result.d.Table.rows;
//            //        //            for (var _i = 0; _i < _rows.length; _i++) {
//            //        //                //$("td[field='_" + _rows[_i].ResourceId + "']")
//            //        //                //    .find("input[name=chk][resourceid='" + _rows[_i].RoleId + "_" + _rows[_i].OperationId + "']").prop("checked", _rows[_i].Value);
//            //        //                $("input[name=chk][resourceid={0}][roleid={1}]".format(_rows[_i].ResourceId, _rows[_i].RoleId)).prop("checked", _rows[_i].value);
//            //        //                _rows[_i]['children'] = [{title:'增'}];
//            //        //            }
//            //        //        }
//            //        //    }
//            //        //});


//            //        //$("input[name=chk]").unbind("click").bind("click", function () {
//            //        //    var temp = $(this).attr("resourceid").split('_');
//            //        //    var roleId = parseInt(temp[0]);
//            //        //    var operateId = parseInt(temp[1]);
//            //        //    var resourceId = parseInt($(this).parent().parent().attr("field").replace("_", ""));
//            //        //    var value = $(this).prop("checked");
//            //        //    //提交服务器
//            //        //    $.ajax({
//            //        //        'url': 'RBAC/Services/RBAC_Role_Resource_Operation.asmx/Update',
//            //        //        data: {
//            //        //            RoleId: roleId,
//            //        //            ResourceId: resourceId,
//            //        //            OperationId: operateId,
//            //        //            Value: value
//            //        //        },
//            //        //        success: function (data1) {
//            //        //            switch ($(data1).find("ServiceStateKeys").text()) {
//            //        //                case "Success":
//            //        //                    $.messager.show({ title: '操作提示', msg: "操作成功" });
//            //        //                    break;
//            //        //                case "Exists":
//            //        //                    break;
//            //        //                default:
//            //        //                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
//            //        //                    break;
//            //        //            }
//            //        //        }
//            //        //    });
//            //        //});
//            //    }
//            //}
//        });
//        var Columns = [
//             {
//                 text: '编号', hidden: true, dataIndex: 'RoleId', width: X_WIDTH.Number, align: 'center',  renderer: function (v,  k,r) {
//                     if (['增', '删', '改', '查', '查部门', '查所有'].indexOf(k.record.data.RoleName) > -1) {
//                         return v + "<input type='checkbox' style='width:1px' />";
//                     } else return v;
//                 }
//             },
//             {
//                 text: '角色', dataIndex: 'RoleName', width: 120, align: 'center', locked: false, renderer: function (v, k,r) {
//                     if (['增', '删', '改', '查', '查部门', '查所有'].indexOf(k.record.data.RoleName) > -1) {
//                         return v + "<input type='checkbox' style='width:1px' />";
//                     } else return v;
//                 }
//             }
//        ];

//        var selectRowData = {};
//        var tbl = view.query(".mainList");
//        var grid = Ext.create('Ext.tree.Panel', {
//            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
//            store: Store, width: '100%', forceFit: true,
//            columns: Columns.concat(functionCols),
//            listeners: {
//                rowclick: function (grid, row) {
//                    var Roles_View = Ext.create("RBAC.Js.Roles_View");
//                    Roles_View.show(row.data.RoleId);
//                    selectRowData = row.data;

//                }
//            }, useArrows: true,
//            renderTo: tbl
//        });
//    }
//});

//(function a(n) {
//    //按钮链接
//    var $linkNew = $("a[name=linkNew]","#"+n);
//    var $linkEdit = $("a[name=linkEdit]","#"+n);
//    var $btnSearch = $("input[name=btnSearch]","#"+n);
//    var $linkDel = $("a[name=linkDel]","#"+n);
//    //搜索项
//    var $txtUserName = $("input[name=txtUserName]","#"+n);
//    //表
//    var $tree = $("table[name=tree]","#"+n);
//    //枚举
//    var enumUrl = {//s服务 w//窗体
//        sDel: "RBAC/Services/RBAC_Roles.asmx/Delete",
//        sList: 'RBAC/Services/RBAC_Roles.asmx/GetLists',
//        sAdd: 'RBAC/Services/RBAC_Roles.asmx/Add',
//        wAdd: 'RBAC/Roles_Add.aspx'
//    };

//    //普通代码
//    ///读取所有功能
//    var arrAllResource = [];
//    $.ajax({
//        url: 'RBAC/Services/RBAC_Resources.asmx/GetAllLists',
//        async: false,
//        dataType: "json",
//        headers: { 'Content-Type': 'application/json;utf-8' },
//        success: function (t) {
//            arrAllResource = t.d.Table.rows;
//        }
//    });

//    //事件
//    $linkNew.click(function () { edit(0); });
//    $linkEdit.click(function () {
//        var row = $tree.treegrid('getSelected');
//        if (row) {
//            if (typeof (row.RoleId) == 'number')
//                edit(row.RoleId);
//        }
//    });
//    $btnSearch.click(function () { render(); });
//    $linkDel.click(function () {
//        var row = $tree.treegrid('getSelected');
//        if (row) {
//            $.messager.confirm('操作提示', '确认要删除该角色吗？', function (r) {
//                if (r) {
//                    $.ajax({
//                        data: { id: row.RoleId },
//                        type: "GET",
//                        url: enumUrl.sDel,
//                        dataType: "json",
//                        headers: { 'Content-Type': 'application/json;utf-8' },
//                        success: function (result) {
//                            if (result.d) {
//                                $.messager.show({ title: '操作提示', msg: '删除操作成功!' });
//                                render();
//                            }
//                        }
//                    });
//                }
//            });
//        }
//    });
//    render();
//    //函数
//    function render() {
//        var param = {};
//        //把功能定义为列
//        var functionCols = [];
//        for (var j = 0; j < arrAllResource.length; j++) {
//            functionCols.push({
//                field: '_' + arrAllResource[j].ResourceId,
//                title: arrAllResource[j].ResourceName,
//                align: 'center',
//                width: 80,
//                rowspan: 1,
//                formatter: function (v, r, k) {
//                    if (typeof (r.RoleId) == 'string') return "<input type='checkbox' name='chk' resourceId='" + r.RoleId + "' />";
//                }
//            });
//        }

//        $tree.treegrid({
//            url: enumUrl.sList + "?" + jQuery.param(param),
//            treeField: 'RoleName', method: 'get',
//            idField: 'RoleId',
//            frozenColumns: [[{ field: 'RoleId', title: '编号', width: 120, align: 'left', hidden: true, rowspan: 2 },
//                    { field: 'RoleName', title: '角色', width: 120, align: 'left', rowspan: 2}]],
//            columns: [[
//                    { title: '功能', rowspan: 1, colspan: arrAllResource.length }
//                ], functionCols],
//            loadFilter: function (data) {
//                var _tree = [];

//                for (var _j = 0; _j < data.d.Table.rows.length; _j++) {
//                    var _temp = data.d.Table.rows[_j];
//                    //定义操作,对象Children：X_OPEREATES['_1']
//                    var CHILDREN = [];
//                    for (var operate in X_OPEREATES) {
//                        CHILDREN.push({ 'RoleId': _temp.RoleId + "_" + X_OPEREATES[operate].OperateId, 'RoleName': X_OPEREATES[operate].OperationName });
//                    }
//                    //
//                    _tree.push({
//                        'RoleId': _temp.RoleId,
//                        'RoleName': _temp.RoleName, 'state': 'closed',
//                        'children': CHILDREN
//                    });
//                }
//                return { rows: _tree, total: _tree.length };
//            }, onLoadSuccess: function () {
//                //赋值
//                $.ajax({
//                    type: "GET",
//                    url: 'RBAC/Services/RBAC_Role_Resource_Operation.asmx/GetLists',
//                    data:{RoleIds:''},
//                    dataType: "json",
//                    headers: { 'Content-Type': 'application/json;utf-8' },
//                    success: function (result) {
//                        if (result.d.Table.rows) {
//                            var _rows = result.d.Table.rows;
//                            for (var _i = 0; _i < _rows.length; _i++) {
//                                $("td[field='_" + _rows[_i].ResourceId + "']")
//                                    .find("input[name=chk][resourceid='" + _rows[_i].RoleId + "_" + _rows[_i].OperationId + "']").prop("checked", _rows[_i].Value);
//                            }
//                        }
//                    }
//                });


//                $("input[name=chk]").unbind("click").bind("click", function () {
//                    var temp = $(this).attr("resourceid").split('_');
//                    var roleId = parseInt(temp[0]);
//                    var operateId = parseInt(temp[1]);
//                    var resourceId = parseInt($(this).parent().parent().attr("field").replace("_", ""));
//                    var value = $(this).prop("checked");
//                    //提交服务器
//                    $.ajax({
//                        'url': 'RBAC/Services/RBAC_Role_Resource_Operation.asmx/Update',
//                        data: { RoleId: roleId,
//                            ResourceId: resourceId,
//                            OperationId: operateId,
//                            Value: value
//                        },
//                        success: function (data1) {
//                            switch ($(data1).find("ServiceStateKeys").text()) {
//                                case "Success":
//                                    $.messager.show({ title: '操作提示', msg: "操作成功" });
//                                    //                                    $dialog.dialog('close');
//                                    //                                    $tree.treegrid('reload');
//                                    break;
//                                case "Exists":
//                                    //                                    $.messager.show({ title: '操作提示', msg: "用户已经存在" });
//                                    break;
//                                default:
//                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
//                                    break;
//                            }
//                        }
//                    });
//                });
//            }
//        });
//    }
//    function edit(id) {
//        var win_id = winId();
//        var param = { RoleId: id };
//        $('#' + n).append("<div id='" + win_id + "'></div>");
//        var $dialog = $('#' + win_id);

//        $dialog.dialog({
//            title: id > 0 ? '修改角色' : '新增角色',
//            width: 300,
//            height: 150,
//            closed: false,
//            cache: false,
//            href: enumUrl.wAdd + "?" + $.param(param),
//            modal: true,
//            onLoad: function () {
//                renderUserControls();
//                $("input[name=RoleId]","#RBAC-Roles_Add").val(id);
//            },
//            onClose: function () {
//                $(this).dialog("destroy");
//                $dialog.remove();
//            },
//            buttons: [{
//                text: '保存',
//                handler: function () {
//                    $.ajax({
//                        url: enumUrl.sAdd,
//                        data: $("form[name=fm]","#RBAC-Roles_Add").serializeArray(),
//                        success: function (data) {
//                            switch ($(data).find("ServiceStateKeys").text()) {
//                                case "Success":
//                                    $.messager.show({ title: '操作提示', msg: "保存操作成功" });
//                                    $dialog.dialog('close');
//                                    $tree.treegrid('reload');
//                                    break;
//                                case "Exists":
//                                    $.messager.show({ title: '操作提示', msg: "用户已经存在" });
//                                    break;
//                                default:
//                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
//                                    break;
//                            }
//                        }
//                    });
//                }
//            }, {
//                text: '取消',
//                handler: function () {
//                    $dialog.dialog("close");
//                }
//            }]
//        });
//    }
//})('RBAC-Roles_List');