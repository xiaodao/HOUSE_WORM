﻿(function a(n) {
    //按钮链接
    var $linkNew = $("a[name=linkNew]","#"+n);
    var $linkEdit = $("a[name=linkEdit]","#"+n);
    var $btnSearch = $("input[name=btnSearch]","#"+n);
    var $linkDel = $("a[name=linkDel]","#"+n);
    //搜索项
    var $txtUserName = $("input[name=txtUserName]","#"+n);
    //表
    var $dg = $("table[name=dg]","#"+n);
    //枚举
    var enumUrl = {//s服务 w//窗体
        sDel: "RBAC/Services/RBAC_Users.asmx/Delete",
        sList: 'RBAC/Services/RBAC_Users.asmx/GetLists',
        sAdd: 'RBAC/Services/RBAC_Users.asmx/Add',
        wAdd: 'RBAC/Users_Add.aspx'
    };
    //事件
    $linkNew.click(function () { edit(0); });
    $linkEdit.click(function () { var row = $dg.datagrid('getSelected'); if (row) { edit(row.UserId); } });
    $btnSearch.click(function () { render(); });
    $linkDel.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            $.messager.confirm('操作提示', '确认要删除该用户吗？', function (r) {
                if (r) {
                    $.ajax({
                        data: { id: row.UserId },
                        type: "GET",
                        url: enumUrl.sDel,
                        dataType: "json",
                        headers: { 'Content-Type': 'application/json;utf-8' },
                        success: function (result) {
                            if (result.d) {
                                $.messager.show({ title: '操作提示', msg: '删除操作成功!' });
                                render();
                            }
                        }
                    });
                }
            });
        }
    });
    render();
    //函数
    function render() {
        var param = { txtUserName: $txtUserName.val() };
        $dg.datagrid({
            url: enumUrl.sList + "?" + jQuery.param(param),
            //title: '用户列表',
            method: 'GET',
            idFiled: 'UserId',
            loadMsg: '正在加载，请稍候……',
            pagination: true,
            rownumbers: true,
            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
            pageSize: 20,
            toolbar: $("div[name=toolbar]","#"+n),
            singleSelect: true,
            columns: [[
                    { field: 'UserId', title: '编号', width: 40 },
                    { field: 'UserName', title: '姓名', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'UserSex', title: '性别', width: X_WIDTH.Sex, align: 'center', formatter: formatSex },
                    { field: 'UserState', title: '状态', width: 50, align: 'center', formatter: function (v, r, k) { return X_USERSTATE[v]; } },
                    { field: 'UserMobile', title: '电话', width: X_WIDTH.Tel, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'UserEmail', title: '邮箱', width: 140, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'LastLoginTime', title: '最后登陆', width: X_WIDTH.Time, align: 'center', formatter: formatShortTime },
                    { field: 'LastLoginIp', title: '最后IP', width: X_WIDTH.Ip, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'UserAdmin', title: '是否管理员', width: 70, align: 'center', formatter: formatBool },
                    { field: 'UserOnline', title: '在线', width: 50, formatter: function (v, r, k) { return X_ONLINESTATE[v]; }, align: 'center' },
                    { field: 'DepartId', title: '部门', width: X_WIDTH.Depart, align: 'center', formatter: formatDepart }
                ]],
            loadFilter: function (data) {
                data.d.Table.total = data.d.Total;
                return data.d.Table;
            },
            onLoadSuccess: function (data) {
                if (data.rows.length) {
                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                }
                else {
                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                }
            }
        });
    }
    function edit(id) {
        var win_id = winId();
        var param = { UserId: id };
        $('#' + n).append("<div id='" + win_id + "'></div>");
        var $dialog = $('#' + win_id);

        $dialog.dialog({
            title: id > 0 ? '修改用户' : '新增用户',
            width: 700,
            height: 450,
            closed: false,
            cache: false,
            href: enumUrl.wAdd + "?" + $.param(param),
            modal: true,
            onLoad: function () {
                renderUserControls();
                $("input[name=UserId]","#RBAC-Users_Add").val(id);
            },
            onClose: function () {
                $(this).dialog("destroy");
                $dialog.remove();
            },
            buttons: [{
                text: '保存',
                handler: function () {
//                    if ($("form[name=fm]","#RBAC-Users_Add").form('validate')) {
//                        $.messager.show({ title: '操作提示', msg: "请检查输入格式是否正确。" });
//                        return false;
//                    }
                    $.ajax({
                        url: enumUrl.sAdd,
                        data: $("form[name=fm]","#RBAC-Users_Add").serializeArray(),
                        success: function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                    $dialog.dialog('close');
                                    $dg.datagrid('reload');
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "用户已经存在" });
                                    break;
                                default:
                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                    break;
                            }
                        }
                    });
                }
            }, {
                text: '取消',
                handler: function () {
                    $dialog.dialog("close");
                }
            }]
        });
    }
})('RBAC-Users_List');