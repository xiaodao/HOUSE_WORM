﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.ext.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/**
* 
*/
Ext.define("RBAC.Js.RBAC_Module", {
    Rows: [],
    singleton: true,
    Tree: [],    //树状目录
    query: function (UserId) {
        return getRemoteObject("/RBAC/Services/RBAC_Users.asmx/Query", {id:UserId});
    },
    constructor: function (config) {
        this.Rows = getRemoteRows('/RBAC/Services/RBAC_Resources.asmx/GetLists', { page: 1, rows: 25 });
        if (this.Rows==[]) {
            this.Timeout();
            return null;
        };
        var childrenData = [];
        Ext.each(this.Rows, function (item, k) {
            if (item.ParentResourceId == 0) {
                childrenData.push({
                    id: 'resource_' + item.ResourceId, text: item.ResourceName, Js: item.Js, leaf: false, iconCls: item.Icon, children: [], expanded: item.DefaultState == 1, path: ''
                });
            }
        });
        Ext.each(this.Rows, function (item) {
            if (item.ParentResourceId != 0) {
                for (var j = 0; j < childrenData.length; j++) {
                    if (childrenData[j].id == ('resource_' + item.ParentResourceId)) {
                        childrenData[j].children.push({
                            id: 'resource_' + item.ResourceId, text: item.ResourceName, Js: item.Js, leaf: true, iconCls: item.Icon, path: item.ResourcePath
                        });
                    }
                }
            }
        });
        this.Tree = childrenData;
        this.initConfig(config);
    },
    logout: function () {
        Ext.Msg.confirm("确认框", "确认要退出吗？", function (btn) {
            $.post("RBAC/Services/RBAC_Users.asmx/Logout", function (v) {
                switch ($(v).find("boolean").text()) {
                    case "true":
                        window.postMessage('app.quit()', window.location);
                        //location.href = "/Login.htm";
                        break;
                }
            });
        });
    },
    Timeout: function () {
        Ext.Msg.alert('Status', '登陆超时！');
        location.href = "Login.htm";//todo:
    },
    SystemGetString: function (module, key) {
        var _result;
        $.ajax({
            url: "SystemSet/Services/SystemSet.asmx/SystemGetString",
            data: { module: module, key: key },
            async: false,
            success: function (v) {
                _result = $(v).find("string").text();
            }
        });
        return _result;
    },
    SystemGetBool: function (module, key) {
        var _result;
        $.ajax({
            url: "SystemSet/Services/SystemSet.asmx/SystemGetBool?t=" + Math.random(),
            data: { module: module, key: key },
            async: false,
            success: function (v) {
                _result = $(v).find("boolean").text() == "true";  
            }
        });
        return _result;
    },
    SystemGetInt: function (module, key) {
        var _result;
        $.ajax({
            url: "SystemSet/Services/SystemSet.asmx/SystemGetInt",
            data: { module: module, key: key },
            async: false,
            success: function (v) {
                _result = parseInt($(v).find("int").text());
            }
        });
        return _result;
    },
    CheckLimit: function (ResourceName, OperationName) {
        for (var limit in X_SELFOPERATIONS) {
            if (X_SELFOPERATIONS[limit].ResourceName == ResourceName &&
                X_SELFOPERATIONS[limit].OperationName == OperationName)
                return X_SELFOPERATIONS[limit].Value;
        }
        return false;
    },
    getTab: function (Js) {
        var _tblId = 'tab_' + Js.replace(/\./g, "_");
        var _tab = Ext.getCmp(_tblId);
        if (_tab) return _tab;
        else return null;
    },
    closeTab: function (Js) {
        var _tab = this.getTab(Js);
        if (_tab) _tab.close();
    },
    addTab: function addTab(id, url, title, Js) {
        if (!Js) return;
        var _tblId = 'tab_' + Js.replace(/\./gm, '_');
        var _tab = Ext.getCmp(_tblId);
        if (!_tab) {
            var panel = Ext.create('Ext.panel.Panel', {
                id: _tblId, title: title, layout: 'fit', closable: true,
                autoScroll: false, //html: '<div class="mainList"></div>',
                loader: {
                    url: url, autoLoad: true, scripts: true,
                    listeners: {
                        load: function (obj, eOpts) {
                            var $div = Ext.get(_tblId);
                            var _module = Ext.create(Js);

                            renderUserControls($div.dom);
                            _module.renderList($div);
                        }
                    }
                }
            });
            _tab = deskExt.add(panel).show();
        } else
            deskExt.setActiveTab(_tab); // 设置当前tab页
    },
    render: function (JsSource) {
        if (JsSource.indexOf(",") > -1) {
            var _array = JsSource.split(',');
            for (var s = 0; s < _array.length; s++) {
                this.doit(_array[s]);
            }
        }
        else
            this.doit(JsSource);
    },
    doit: function (Js) {
        var tab = this.getTab(Js);

        if (tab) {
            var view = $(".mainList", tab.dom);
            if (!tab.dom) view = $(".mainList", tab.el.dom);
            if (!view) return;
            var grid = view.find("div.x-panel");
            
            if (!grid) return;
            var gridExt = Ext.getCmp($(grid[0]).attr("id"));
            if (!gridExt) return;
            if (gridExt.store.type == 'tree') {
                //gridExt.store.reload();//render();
                //tab.close();
                //setTimeout(function () {
                   
                //}, 500);
            }
            else {
                gridExt.store.reload();
            }
        }
    }
});