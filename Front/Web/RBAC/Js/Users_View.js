﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("RBAC.Js.Users_View", {
    show: function (UserId) {
        var winDom;
        var title = "客户";
        var mUsers = getRemoteObject("/RBAC/Services/RBAC_Users.asmx/Query", { id: UserId });

        if (!mUsers) {
            $.messager.show("操作提示", title + "编号不存在");
            return;
        }

        if (UserId > 0)
            title = "查看" + title;
        else
            title = "新增" + title;

        var buttons = [];
        if (X_USER == mUsers.AddUser) {
            if (mUsers.ViewLevel > 0) {
                buttons.push({
                    text: '转私盘',
                    handler: function (btn) {
                        $.get("RBAC/Services/RBACUsers.asmx/Add", { UserId: UserId, Follow: '转私盘', FollowHouseState: 0, ViewLevel: 2, ChgHouseViewLevel: 0, SuggestState: 0 }, function (r) {
                            switch ($(r).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                    btn.setText("转公盘").disable();;
                                    break;
                                case "TimeOut":
                                    break;
                                case "NoPermiss":
                                    $.messager.show({ title: '操作提示', msg: "没有权限。" });
                                    break;
                                default:
                                    break;
                            }
                        });
                    }
                });
            }
        }
        buttons = buttons.concat([
               {
                   text: "关闭",
                   handler: function () {
                       this.up("window").close();
                   }
               }
        ]);

        var win = Ext.create('Ext.window.Window', {
            title: title, height: 500, width: 900, modal: true,
            loader: {
                url: '/RBAC/Templates/Users_View.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = obj.dom;
                        var Region_Module = Ext.create("Region.Js.Region_Module");
                      
                        var _tempId = win.getId();
                        mUsers.TempId = _tempId;
                        var $win = $(".x-autocontainer-innerCt", "#" + _tempId);

                        var html = template('RBAC_Js_Users', mUsers);
                        $win.html(html);
                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    constructor: function (config) {
        this.initConfig(config);
    }
});