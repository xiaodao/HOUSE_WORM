﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("RBAC.Js.Roles_View", {
    show: function (RoleId) {
        var winDom;
        var title = "角色";
        var mRoles = getRemoteObject("/RBAC/Services/RBAC_Roles.asmx/Query", { id: RoleId });

        if (!mRoles) {
            $.messager.show("操作提示", title + "编号不存在");
            return;
        }

        if (RoleId > 0)
            title = "查看" + title;
        else
            title = "新增" + title;

        var buttons = [];

        buttons = buttons.concat([
               {
                   text: "关闭",
                   handler: function () {
                       this.up("window").close();
                   }
               }
        ]);

        var win = Ext.create('Ext.window.Window', {
            title: title, height: 500, width: 900, modal: true,
            loader: {
                url: '/RBAC/Templates/Roles_View.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = obj.dom;

                        var _tempId = win.getId();
                        mRoles.TempId = _tempId;
                        var $win = $(".x-autocontainer-innerCt", "#" + _tempId);
                        //加载所有资源
                        var arrAllResource = getRemoteRows('RBAC/Services/RBAC_Resources.asmx/GetAllLists');
                        mRoles.Resources = arrAllResource;
                        var html = template('RBAC_Roles_View', mRoles);
                        $win.html(html);
                        //加载该角色的所有权限动作
                        var ResourceOperation = getRemoteRows('RBAC/Services/RBAC_Role_Resource_Operation.asmx/GetLists', { RoleIds: RoleId }, function (rows) {
                            for (var i = 0; i < rows.length; i++) {
                                $("input[name=chk][value={0}_{1}]".format(rows[i].ResourceId, rows[i].OperationId), winDom).prop("checked", true);
                            }
                        });

                        $("input[name=chk]",winDom).unbind("click").bind("click", function () {
                            var temp = $(this).val().split('_');
                            var operateId = parseInt(temp[1]);
                            var resourceId = temp[0];
                            var value = $(this).prop("checked");
                            //提交服务器
                            $.ajax({
                                'url': 'RBAC/Services/RBAC_Role_Resource_Operation.asmx/Update',
                                data: {
                                    RoleId: RoleId,
                                    ResourceId: resourceId,
                                    OperationId: operateId,
                                    Value: value
                                },
                                success: function (data1) {
                                    switch ($(data1).find("ServiceStateKeys").text()) {
                                        case "Success":
                                            $.messager.show({ title: '操作提示', msg: "操作成功" });
                                            break;
                                        case "Exists":
                                            break;
                                        default:
                                            $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                            break;
                                    }
                                }
                            });
                        });
                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    constructor: function (config) {
        this.initConfig(config);
    }
});