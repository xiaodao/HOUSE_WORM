﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("RBAC.Js.Users_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['UserId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/RBAC/Services/RBAC_Users.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = easyui2extgrid([[
                    { field: 'UserId', title: '编号', width: 40 },
                    { field: 'UserName', title: '姓名', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'UserSex', title: '性别', width: X_WIDTH.Sex, align: 'center', formatter: formatSex },
                    { field: 'UserState', title: '状态', width: 50, align: 'center', formatter: function (v, r, k) { return X_USERSTATE[v]; } },
                    { field: 'UserMobile', title: '电话', width: X_WIDTH.Tel, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'UserEmail', title: '邮箱', width: 140, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'LastLoginTime', title: '最后登陆', width: X_WIDTH.Time, align: 'center', formatter: formatShortTime },
                    { field: 'LastLoginIp', title: '最后IP', width: X_WIDTH.Ip, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'UserAdmin', title: '是否管理员', width: 70, align: 'center', formatter: formatBool },
                    { field: 'UserOnline', title: '在线', width: 50, formatter: function (v, r, k) { return X_ONLINESTATE[v]; }, align: 'center' },
                    { field: 'DepartId', title: '部门', width: X_WIDTH.Depart, align: 'center', formatter: formatDepart }
                ]]);
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Users_View = Ext.create("RBAC.Js.Users_Add");
                    Users_View.show(row.data.UserId);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Users_Add = Ext.create("RBAC.Js.Users_Add");
            Users_Add.show(0);
        });
        $("a[name=linkDel]", view.dom).click(function () {
            if (selectRowData) {
                $.ajax({
                    url: "RBAC/Services/RBAC_Users.asmx/Delete",
                    data: { id: selectRowData.UserId },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                RBAC.Js.RBAC_Module.render('RBAC.Js.Users_List');
                                break;
                            case "Exists":
                                $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "删除操作失败" });
                                break;
                        }
                    }
                });
            }
        });
    }
});
