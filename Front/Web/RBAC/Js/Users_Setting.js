﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("RBAC.Js.Users_Setting", {
    renderList: function (view) {
        var tbl = view.query(".mainList");
        var obj = getRemoteObject('/RBAC/Services/RBAC_Users.asmx/Query', { id: X_USEROBJ.UserId });
        var html = template('RBAC_Users_Add', obj);
        tbl[0].innerHTML = html;
        var winDom = view.dom;
        $("input[type=button]", winDom).click(function () {
            //有效性验证
            if (!$("form[name=fmAdd]", winDom).form("validate")) {
            } else {
                if ($("input[name=DoubleUserPass]", winDom).val() != $("input[name=NewUserPass]", winDom).val()) {
                    $.messager.show({ msg: "请重复输入密码。" });
                    return false;
                }
                $.post("RBAC/Services/RBAC_Users.asmx/UpdateInfo", $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                    switch ($(data).find("ServiceStateKeys").text()) {
                        case "Success":
                            $.messager.show({ title: '操作提示', msg: "操作成功" });
                            //$("form[name=fmAdd]", winDom).up("window").close();
                            break;
                        case "Exists":
                            $.messager.show({ title: '操作提示', msg: "已经存在" });
                            break;
                        default:
                            Ext.Msg.show({ title: "操作提示", message: "保存操作失败，请检查原密码是否正确！" });
                            break;
                    }
                    return false;
                });
            }
            return false;
        });
    },
    show: function (view) {
        this.renderList(view);
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});