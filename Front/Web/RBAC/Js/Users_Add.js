﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("RBAC.Js.Users_Add", {
    show: function (UserId) {
        var winDom;
        var _title;
        if (UserId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '用户', height: 400, width: 455, modal: true,
            loader: {
                url: '/RBAC/Templates/Users_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=UserId]", winDom).val(UserId);
                        renderUserControls(winDom);
                        //加载角色
                        var RoleRows = getRemoteRows('/RBAC/Services/RBAC_Roles.asmx/GetLists');
                        $.map(RoleRows, function (item, k) {
                            if ((k % 4 == 0) && (k > 0)) $(".divRoles", winDom).append("<br />");
                            $(".divRoles", winDom).append(" <label style='display:inline-block;width:100px;overflow:hidden'><input type='checkbox' name='Roles' value='{1}' />{0}</label> ".format(item.RoleName, item.RoleId));
                        });
                        if (UserId) {
                            $("form[name=fmAdd]", winDom).fillForm({
                                "url": "RBAC/Services/RBAC_Users.asmx/Query",
                                data: { id: UserId }
                            });
                            //加载现在角色
                            var UserRoleRows = getRemoteRows('/RBAC/Services/RBAC_Roles.asmx/GetListsByUserId', { UserId: UserId });
                            $.map(UserRoleRows, function (item, k) {
                                $("input[name=Roles][value={0}]".format(item.RoleId)).prop("checked", true);
                            });
                            //$("input[name=Roles]", winDom).click(function () {
                            //    var has = $(this).prop("checked");
                            //    var _roleid=$(this).val();
                            //    var url = "/RBAC/Services/RBAC_User_Role.asmx/";
                            //    if (has) { url += "Add"; }
                            //    else { url += "Delete"; }
                            //    $.post(url, { UserRoleId: 0, UserId: UserId, RoleId: _roleid }, function (r) {
                            //        switch ($(r).find("ServiceStateKeys").text()) {
                            //            case "Success":
                            //                $.messager.show({ msg: '操作成功' });
                            //                break;
                            //            default:
                            //                $.messager.show({ msg: '操作失败' });
                            //                break;
                            //        }
                            //    });
                            //});
                        }
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    var userpass = $("input[name=UserPass]", winDom).val();
                    if (userpass) {
                        $("input[name=UserPass]", winDom).val(hex_md5((userpass)));
                    }
                    $.post("/RBAC/Services/RBAC_Users.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('RBAC.Js.Users_List');
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});