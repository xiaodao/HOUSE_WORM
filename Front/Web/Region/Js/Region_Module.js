﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.ext.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/**
* 
*/
Ext.define("Region.Js.Region_Module", {
    AreaRows: [],
    AreaRowsObj: {},
    getBuildingObj: function (BuildingId) {
        var obj = getRemoteObject("/Region/Services/Region_Buildings.asmx/Query", { BuildId: BuildingId });
        return obj;
    },
    constructor: function (config) {
        this.AreaRows = getRemoteRows('/Region/Services/Region_Area.asmx/GetLists', { page: 1, rows: 35535 });
        if (!this.AreaRows.length) {
            Timeout();
        };
        this.AreaRowsObj = rowsToObj(this.AreaRows, 'AreaId');
        this.initConfig(config);
    }
});