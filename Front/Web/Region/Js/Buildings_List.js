﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Region.Js.Buildings_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['BuildingId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/Region/Services/Region_Buildings.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'BuildingId'
            }
        });
        var Columns = easyui2extgrid([[
                    { field: 'BuildingId', title: '编号', width: 40 },
                    { field: 'BuildingArea', title: '区域', sortable: true, width: 50, formatter: formatRegionArea },
                    { field: 'BuildingName', title: '名称', sortable: true, width: 100, align: 'left' },
                    { field: 'BuildingAlice', title: '别名', sortable: true, width: 100, align: 'left' },
                    { field: 'BuildingAddr', title: '地址', sortable: true, width: 250, align: 'left' },
                    { field: 'BuildingLatitude', title: '经度', width: X_WIDTH.Building, align: 'left' },
                    { field: 'BuildingLongitude', title: '纬度', width: 100, align: 'left' },
                    { field: 'SellCount', title: '出售', sortable: true, width: X_WIDTH.Int, align: 'center' },
                    { field: 'RentCount', title: '出租', sortable: true, width: X_WIDTH.Int, align: 'center' }
        ]]);
        var tbl = view.query(".mainList");
        var selectRowData = {};
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    selectRowData = row.data;
                    var Buildings_Add = Ext.create("Region.Js.Buildings_Add");
                    Buildings_Add.show(row.data.BuildingId);
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Buildings_Add = Ext.create("Region.Js.Buildings_Add");
            Buildings_Add.show(0);
        });
        $("a[name=linkDel]", view.dom).click(function () {
            if (selectRowData) {
                $.ajax({
                    url: "Region/Services/Region_Buildings.asmx/Delete",
                    data: { BuildingId: selectRowData.BuildingId },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "操作成功" });
                                RBAC.Js.RBAC_Module.render('Region.Js.Buildings_List');
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                break;
                        }
                    }
                });
            }
        });
    }
});