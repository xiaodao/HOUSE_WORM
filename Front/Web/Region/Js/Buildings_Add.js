﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Region.Js.Buildings_Add", {
    show: function (BuildingId) {
        var winDom;
        var _title;
        if (BuildingId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '楼盘', height: 450, width: 700, modal: true,
            loader: {
                url: '/Region/Templates/Buildings_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=BuildingId]", winDom).val(BuildingId);
                        renderUserControls(winDom);

                        //
                        // 百度地图API功能
                        var lat = 31.781192779541;
                        var lng = 117.215980529785;
                        if (BuildingId) {
                            var obj = getRemoteObject("/Region/Services/Region_Buildings.asmx/Query", { BuildId: BuildingId });
                            if (obj) {
                                $("form[name=fmAdd]").fillForm({
                                    params: obj
                                });
                                if (obj.BuildingLatitude && obj.BuildingLongitude) {
                                    lat = parseFloat(obj.BuildingLatitude);
                                    lng = parseFloat(obj.BuildingLongitude);
                                }
                            }
                        }
                        var map = new BMap.Map("allmap");            // 创建Map实例
                        var point = new BMap.Point(lng, lat);    // 创建点坐标
                        map.centerAndZoom(point, 15);                     // 初始化地图,设置中心点坐标和地图级别。

                        map.addControl(new BMap.NavigationControl());  //添加默认缩放平移控件
                        map.enableScrollWheelZoom();                            //启用滚轮放大缩小

                        var marker = new BMap.Marker(new BMap.Point(lng, lat));  // 创建标注
                        map.addOverlay(marker);              // 将标注添加到地图中
                        marker.enableDragging();    //可拖拽
                        marker.addEventListener("dragend", function (e) {
                            fillMapLngLat(e.point);
                        });
                        map.addEventListener("click", function (e) {
                            fillMapLngLat(e.point);
                        });
                        var options = {
                            onSearchComplete: function (results) {
                                // 判断状态是否正确
                                if (local.getStatus() == BMAP_STATUS_SUCCESS) {
                                    var s = [];
                                    for (var i = 0; i < results.getCurrentNumPois() ; i++) {
                                        var _result = results.getPoi(i);
                                        $("input[name='BuildingAddr']", winDom).val(_result.address);
                                        fillMapLngLat(_result.point);
                                        map.centerAndZoom(_result.point, 15);
                                        break;
                                    }
                                }
                                else
                                    $.messager.show({
                                        title: '提示信息',
                                        msg: "定位失败"
                                    });
                            }
                        };
                        var local = new BMap.LocalSearch(map, options);

                        $("a[name=linkPosition]", winDom).click(function () {
                            local.search($("input[name='BuildingName']", winDom).val());
                        });
                        $("#BuildingArea", winDom).change(function () {
                            map.centerAndZoom(new BMap.Point(area['_' + $(this).val()].Longitude, area['_' + $(this).val()].Latitude), 15);
                        });
                        function fillMapLngLat(_point) {
                            $("input[name=BuildingLongitude]", winDom).val(_point.lng);
                            $("input[name=BuildingLatitude]", winDom).val(_point.lat);
                            marker.setPosition(new BMap.Point(_point.lng, _point.lat));
                        }
                        $("#Buildings_Merge", winDom).click(function () {
                            $.messager.confirm('确认', "确认要合并小区吗？合并后无法恢复。", function (r) {
                                $.get("/Region/Services/Region_Buildings.asmx/Merge", {
                                    fromBuildId: BuildingId,
                                    toBuildId: $("input[name=MergeToBuildingId]", winDom).val()
                                }, function (r) {
                                    switch ($(r).find("ServiceStateKeys").text()) {
                                        case "Success":
                                            RBAC.Js.RBAC_Module.render('Region.Js.Buildings_List');
                                            $.messager.show({ title: '操作提示', msg: "操作成功" });
                                            win.close();
                                            break;
                                        default:
                                            $.messager.show({ title: '操作提示', msg: "操作失败" });
                                            break;
                                    }
                                });
                            });


                        });
                        //
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/Region/Services/Region_Buildings.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    RBAC.Js.RBAC_Module.render('Region.Js.Buildings_List');
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});