﻿$(document).ready(function () {
    $('#form1').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            ip: {
                message: '地址校验错误',
                validators: {
                    notEmpty: { message: '地址不能为空' }
                }
            },
            database: {
                message: '数据库名校验错误',
                validators: {
                    notEmpty: { message: '数据库名不能为空' }
                }
            },
            account: {
                message: '账号校验错误',
                validators: {
                    notEmpty: { message: '账号不能为空' }
                }
            },
            password: {
                validators: {
                    notEmpty: { message: '密码不能为空' }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');
        var $noty=noty({
            type: 'info',
            text:'正在尝试连接数据库'
        });
        // Use Ajax to submit form data
        $.post('Services/SystemSetup.asmx/SetupDatabase', $("#form1").serializeArray(), function (r) {
            $noty.close();
            switch ($(r).find("boolean").text()) {
                case "false":
                    noty({
                        type:'error',
                        text: '连接数据库失败，请检查连接',
                        timeout: 2000
                    });
                    break;
                case "true":
                    noty({
                        type:'success',
                        text: '设置成功',
                        timeout:2000
                    });
                    setTimeout(function () {
                        location.href = "Step3.aspx";
                    }, 2000);
                    break;
            }
        });
    });;
    $("#submit").click(function () {
        $("#form1").submit();

    });
});