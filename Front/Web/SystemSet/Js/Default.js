﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.define("SystemSet.Js.Default", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        load();
        $("form[name='SystemSet-House']", view.dom).submit(function () {
            $.post("/House/Services/House_SystemSet.asmx/Save?" + $(this).serialize(), function (v) {
                var msg;
                switch ($(v).find("ServiceStateKeys").text()) {
                    case "Success":
                        msg = "操作成功！";
                        break;
                    case "NotExists":
                        msg = "配置文件路径不正确！";
                        break;
                    default:
                        msg = "操作失败！";
                        break;
                }
                $.messager.show({ msg: msg });
            });
            return false;
        });
        $("form[name='SystemSet-Daily']", view.dom).submit(function () {
            $.post("/Daily/Services/Daily_SystemSet.asmx/Save?" + $(this).serialize(), function (v) {
                var msg;
                switch ($(v).find("ServiceStateKeys").text()) {
                    case "Success":
                        msg = "操作成功！";
                        break;
                    case "NotExists":
                        msg = "配置文件路径不正确！";
                        break;
                    default:
                        msg = "操作失败！";
                        break;
                }
                $.messager.show({ msg: msg });
            });
            return false;
        });
        $("form[name='SystemSet-CM']", view.dom).submit(function () {
            $.post("/CM/Services/CM_SystemSet.asmx/Save?" + $(this).serialize(), function (v) {
                var msg;
                switch ($(v).find("ServiceStateKeys").text()) {
                    case "Success":
                        msg = "操作成功！";
                        break;
                    case "NotExists":
                        msg = "配置文件路径不正确！";
                        break;
                    default:
                        msg = "操作失败！";
                        break;
                }
                $.messager.show({ msg: msg });
            });
            return false;
        });
        //加载
        function load() {
            $("form[name='SystemSet-House']", view.dom).fillForm({
                params: {
                    DongFangInList: RBAC.Js.RBAC_Module.SystemGetBool("House", "DongFangInList"),
                    IsViewCountLimit: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsViewCountLimit"),
                    ViewCountLimit: RBAC.Js.RBAC_Module.SystemGetInt("House", "ViewCountLimit"),
                    IsAllowDeleteSelf: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsAllowDeleteSelf"),
                    IsAllowUserSetFollowViewLevel: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsAllowUserSetFollowViewLevel"),
                    IsAllowListPrivate: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsAllowListPrivate"),
                    IsAllowViewPrivateDongFang: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsAllowViewPrivateDongFang"),
                    IsAllowDisplayPhoneNumber: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsAllowDisplayPhoneNumber"),
                    IsUpdateHouseSellsList: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsUpdateHouseSellsList"),
                    IsAllowMemChangeHouseInfo: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsAllowMemChangeHouseInfo"),
                    IsChangeLoginStyle: RBAC.Js.RBAC_Module.SystemGetBool("House", "IsChangeLoginStyle")
                }
            });
            $("form[name='SystemSet-Daily']", view.dom).fillForm({
                params: {
                    DailyDaysBeforeCountAllow: RBAC.Js.RBAC_Module.SystemGetInt("Daily", "DailyDaysBeforeCountAllow")
                }
            });
            $("form[name='SystemSet-CM']", view.dom).fillForm({
                params: {
                    CMShowList: RBAC.Js.RBAC_Module.SystemGetBool("CM", "CMShowList"),
                    IsOrderFollow: RBAC.Js.RBAC_Module.SystemGetBool("CM", "IsOrderFollow")
                }
            });
        }


    }
});