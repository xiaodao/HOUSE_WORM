﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemSet/MasterPage.master" AutoEventWireup="true" CodeFile="Step1.aspx.cs" Inherits="SystemSet_Step1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>第1步:检查安装环境</title>
    <script type="text/javascript" src="Js/Step1.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="row">
            <div class="col-md-12 text-center">
                1.检查环境
            </div>
        </div>

        <div class="row" style="margin-top:50px;margin-bottom:50px">
            <div class="col-md-12 text-center">
                通过  <i style="color:green" class="glyphicon glyphicon-ok" ></i>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="Step2.aspx" class="btn-default btn">下一步</a>
            </div>
        </div>
</asp:Content>

