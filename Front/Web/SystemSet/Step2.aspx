﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemSet/MasterPage.master" AutoEventWireup="true" CodeFile="Step2.aspx.cs" Inherits="SystemSet_Step2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>第2步:连接数据库</title>
    <script type="text/javascript" src="Js/Step2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <fieldset>
                    <legend>2.连接数据库</legend>
                </fieldset>

            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="form-group">
                    <label>地址</label>
                        <input type="text" class="form-control" name="ip" placeholder="地址" />
                    
                </div>
                <div class="form-group">
                    <label>数据库名</label>
                        <input type="text" class="form-control" name="database" placeholder="数据库名称" />
                    
                </div>
                <div class="form-group">
                    <label>账号</label>
                        <input type="text" class="form-control" name="account" placeholder="账号" data-bv-field="account" />
                    
                </div>
                <div class="form-group">
                    <label>密码</label>
                        <input type="text" class="form-control" name="password" placeholder="密码" />
                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="javascript:;" class="btn-default btn" id="submit">下一步</a>
            </div>
        </div>
</asp:Content>

