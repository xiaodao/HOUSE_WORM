﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemSet/MasterPage.master" AutoEventWireup="true" CodeFile="Step3.aspx.cs" Inherits="SystemSet_Step3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title>第3步:安装模块</title>
    <script type="text/javascript" src="Js/Step3.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4 text-center">
                <fieldset>
                    <legend>3.安装模块</legend>
                    <div class="text-left">
                        <ul>
                            <li><label><input type="checkbox" name="Modules" value="Announcement" />公告</label></li>
                            <li><label><input type="checkbox" name="Modules" value="CM" />合同</label></li>
                            <li><label><input type="checkbox" name="Modules" value="CRM" />客户</label></li>
                            <li><label><input type="checkbox" name="Modules" value="Daily" />日志</label></li>
                            <li><label><input type="checkbox" name="Modules" value="Doc" />公文</label></li>
                            <li><label><input type="checkbox" name="Modules" value="FI" />财务</label></li>
                            <li><label><input type="checkbox" name="Modules" value="House" />房源</label></li>
                            <li><label><input type="checkbox" name="Modules" value="HR" />人力资源</label></li>
                            <li><label><input type="checkbox" name="Modules" value="Notice" />提醒</label></li>
                            <li><label><input type="checkbox" name="Modules" value="WF" />流程</label></li>
                        </ul>
                    </div>
                </fieldset>
            
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <input type="button" id="btn" value=" 安装 " class="btn-default btn"  />
            </div>
        </div>
</asp:Content>

