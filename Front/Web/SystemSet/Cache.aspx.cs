﻿using System;
using System.Collections.Generic;

public partial class SystemSet_Cache : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string cacheKey = EnumKeys.SessionKey.ONLINEUSERS;
        object objModel = Maticsoft.Common.DataCache.GetCache(cacheKey);
        List<xundh.Model.RBAC_Users> CacheUsers;
        CacheUsers = (List<xundh.Model.RBAC_Users>)objModel;
        if (CacheUsers == null) return;
        string s = "";
        foreach (xundh.Model.RBAC_Users m in CacheUsers)
        {
            s = s + m.UserName + "<br />";
        }
        Response.Write(s);
    }
}