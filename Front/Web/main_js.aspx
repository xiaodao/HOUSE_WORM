﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="main_js.aspx.cs" Inherits="main_js" %>
var X_AREA = {};
var X_DEPARTS={};
var X_OPEREATES={};
var X_POSTS={};
var X_USER="<%#U.UserName%>";
var X_SELFOPERATIONS={};
var xmpp_jid_pwd="<%#Webapp.StringUtil.EncryptMD5NotUnicode( U.UserPass+Webapp.StringUtil.EncryptMD5NotUnicode( U.UserId.ToString())).Substring(0,8).ToLower() %>";
var xmpp_jid_user="csharp_" + <%#U.UserId %>;
X_AREA['_-1']={'AreaId':-1,'AreaName':'','Latitude':0,'Longitude':0};
<asp:Repeater runat="server" id="repArea">
    <ItemTemplate>X_AREA['_<%#((System.Data.DataRowView)Container.DataItem)["AreaId"]%>']={'AreaId':<%#((System.Data.DataRowView)Container.DataItem)["AreaId"]%>,'AreaName':'<%#((System.Data.DataRowView)Container.DataItem)["AreaName"]%>','Latitude':<%#((System.Data.DataRowView)Container.DataItem)["RegionLatitude"]%>,'Longitude':<%#((System.Data.DataRowView)Container.DataItem)["RegionLongitude"]%>};</ItemTemplate>
</asp:Repeater>
<asp:Repeater runat="server" id="repDeparts">
    <ItemTemplate>
    X_DEPARTS['_<%#((System.Data.DataRowView)Container.DataItem)["DepartId"]%>']=
    {
        'DepartId':<%#((System.Data.DataRowView)Container.DataItem)["DepartId"]%>,
        'DepartName':'<%#((System.Data.DataRowView)Container.DataItem)["DepartName"]%>',
        'ParentId':'<%#((System.Data.DataRowView)Container.DataItem)["ParentId"]%>',
        'IsVirtual':'<%#((System.Data.DataRowView)Container.DataItem)["IsVirtual"]%>',
        'Telphone':'<%#((System.Data.DataRowView)Container.DataItem)["Telphone"]%>'
    };
    </ItemTemplate>
</asp:Repeater>
<asp:Repeater runat="server" id="repOpreate">
    <ItemTemplate>X_OPEREATES['_<%#((System.Data.DataRowView)Container.DataItem)["OperationId"]%>']={'OperateId':'<%#((System.Data.DataRowView)Container.DataItem)["OperationId"]%>','OperationName':'<%#((System.Data.DataRowView)Container.DataItem)["OperationName"]%>','OperationEn':'<%#((System.Data.DataRowView)Container.DataItem)["OperationEn"]%>'};</ItemTemplate>
</asp:Repeater>
<asp:Repeater runat="server" id="repPosts">
    <ItemTemplate>X_POSTS['_<%#((System.Data.DataRowView)Container.DataItem)["PostId"]%>']={'PostId':'<%#((System.Data.DataRowView)Container.DataItem)["PostId"]%>','PostName':'<%#((System.Data.DataRowView)Container.DataItem)["PostName"]%>','PostNameEn':'<%#((System.Data.DataRowView)Container.DataItem)["PostNameEn"]%>'};</ItemTemplate>
</asp:Repeater>

var X_USEROBJ={
    Name:X_USER,
    DepartId:<%#U.DepartId %>,
    Depart:X_DEPARTS['_<%#U.DepartId %>'],
    Sex:<%#U.UserSex %>,
    UserEmail:'<%#U.UserEmail %>',
    UserPostId:<%#U.PostId %>,
    UserPost:X_POSTS['_<%#U.PostId %>'],
    UserId:<%#U.UserId %>
};
<asp:Repeater runat="server" id="repOperations">
    <ItemTemplate>
    X_SELFOPERATIONS['_'+ <%#((System.Data.DataRowView)Container.DataItem)["RoleAuthorityId"]%>]={
        ResourceName:'<%#((System.Data.DataRowView)Container.DataItem)["ResourceName"]%>',
        RoleId:<%#((System.Data.DataRowView)Container.DataItem)["RoleId"]%>,
        ResourceId:<%#((System.Data.DataRowView)Container.DataItem)["ResourceId"]%>,
        OperationId:<%#((System.Data.DataRowView)Container.DataItem)["OperationId"]%>,
        OperationName:'<%#((System.Data.DataRowView)Container.DataItem)["OperationName"]%>',
        Value:<%#((System.Data.DataRowView)Container.DataItem)["Value"].ToString().ToLower()%>
        };
        </ItemTemplate>
</asp:Repeater>
var X_COP={};