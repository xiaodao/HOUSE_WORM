﻿using System;
using Webapp;

public partial class Daily_Templates_OutRecords_View : BasePage
{
    public xundh.Model.Daily_OutRecords mOutRecords;
    protected void Page_Load(object sender, EventArgs e)
    {
        xundh.API.Daily.Daily_OutRecords sOutRecords = new xundh.API.Daily.Daily_OutRecords();
        int OutRecordId = StringUtil.GetIntValue(Request["OutRecordId"]);
        mOutRecords = sOutRecords.Query(OutRecordId);
        if (mOutRecords != null)
        {
        }
        else
        {
            mOutRecords = new xundh.Model.Daily_OutRecords();
            mOutRecords.OutRecordId = 0;
            mOutRecords.CRMType = 0;
            mOutRecords.AgreementType = 0;
        }
        DataBind();
    }
}