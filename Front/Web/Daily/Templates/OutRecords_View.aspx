﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OutRecords_View.aspx.cs" Inherits="Daily_Templates_OutRecords_View" %>

    <form name="fm" id="OutRecords_View_Form" method="post" novalidate>
        <input name="BusinessType" type="hidden" value="0" />
        <div id="OutRecords_ViewTabs" style="width: 362px; height: 187px">
            <div title="看房" style="padding: 10px;">
                <div class="fitem">
                    <span>外出类型：</span>
                    <label>买卖<input name="HouseType" checked="checked" value="0" type="radio" /></label>
                    <label>租赁<input name="HouseType" type="radio" value="1" /></label>
                </div>
                <div class="fitem" style="height:auto;">
                    <span>房源编号：</span>
                    <label id="lblHouseIds" style="display:none"><%#mOutRecords.HouseId %></label>
                    <div id="divHouseInfo" style="padding-left:20px"></div>
                </div>
                <div class="fitem hide">
                    <span>小区名称：</span>
                    <label id="lblHouseBuilds" style="width:auto;overflow:hidden;height:auto"><%#mOutRecords.HouseBuilds %></label>
                </div>
            </div>
            <div title="合同业务" style="overflow: auto; padding: 10px;">
                <div class="fitem">
                    <span>合同类型：</span>
                    <label>买卖<input name="AgreementType" type="radio" value="<%#mOutRecords.AgreementType %>" checked="checked" /></label>
                    <label>租赁<input name="AgreementType" type="radio" /></label>
                </div>
                <div class="fitem">
                    <span>合同编号：</span>
                    <input name="AgreementIds" value="<%#mOutRecords.AgreementIds %>" />
                </div>
            </div>
            <div title="其它" style="padding: 10px;">
                <div class="fitem">
                    <span>地址：</span>
                    <input name="Address" value="<%#mOutRecords.Address %>" class="width-large" />
                </div>
                <div class="fitem">
                    <span>事件：</span>
                    <input name="DoThins" value="<%#mOutRecords.DoThins %>" class="width-large" />
                </div>
            </div>
        </div>
        <div style="padding:10px; width: 362px;height:180px" class="easyui-panel" title="其它信息">
            <input type="hidden" name="OutRecordId" value="<%#mOutRecords.OutRecordId %>" />
            <div class="fitem">
                <span>陪同同事：</span>
                <%#mOutRecords.AccompanyNames %>
                <input name="AccompanyIds" value="<%#mOutRecords.AccompanyIds %>" type="hidden" />
            </div>
            <div class="fitem">
                <span>客户类型：</span>
                <label>求购<input type="radio" name="CRMType" checked="checked" value="<%#mOutRecords.CRMType %>" /></label>
                <label>求租<input type="radio" name="CRMType" value="<%#mOutRecords.CRMType %>" /></label>
            </div>
            <div class="fitem">
                <span>客户姓名：</span>
                <label id="lblCRMNames" style="display:none"><%#mOutRecords.CRMNames %></label>
                <div id="divCRMIds"  class="hide"><%#mOutRecords.CRMIds %></div>
                <input name="CRMIds" value="<%#mOutRecords.CRMIds %>" type="hidden" />
                <div id="divCRMInfo" style="padding-left:20px"></div>
            </div>
        </div>
    </form>