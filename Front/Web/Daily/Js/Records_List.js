﻿(function a(n) {
    //按钮链接
    var $linkNew = $("a[name=linkNew]", "#" + n);
    var $linkEdit = $("a[name=linkEdit]", "#" + n);
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    var $linkDel = $("a[name=linkDel]", "#" + n);
    var $linkTemplateSet = $("a[name=linkTemplateSet]", "#" + n);

    //表
    var $dg = $("table[name=dg]", "#" + n);
    //枚举
    var enumUrl = {//s服务 w//窗体
        sDel: "Daily/Services/Daily_Records.asmx/Delete",
        sList: 'Daily/Services/Daily_Records.asmx/GetLists',
        sAdd: 'Daily/Services/Daily_Records.asmx/Add',
        wAdd: 'Daily/Records_Add.aspx'
    };
    ///允许书写哪天的日志
    var _DailyDaysBeforeCountAllow = SystemGetInt("Daily", "DailyDaysBeforeCountAllow");

    //事件
    $linkNew.click(function () { edit(0); });
    $linkEdit.click(function () { var row = $dg.datagrid('getSelected'); if (row) { edit(row.id); } });
    $btnSearch.click(function () { render(); });
    $linkDel.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            $.messager.confirm('操作提示', '确认要删除该日志吗？', function (r) {
                if (r) {
                    $.ajax({
                        data: { id: row.id },
                        type: "GET",
                        url: enumUrl.sDel,
                        dataType: "json",
                        headers: { 'Content-Type': 'application/json;utf-8' },
                        success: function (result) {
                            if (result.d) {
                                $.messager.show({ title: '操作提示', msg: '删除操作成功!' });
                                render();
                            }
                        }
                    });
                }
            });
        }
    });

    render();
    //函数
    function render() {
        //var param = { Contents: $("input[name=txtContents]", "#" + n).val() };
        $dg.datagrid({
            url: enumUrl.sList + "?" + $("form[name=fmSearch]", "#" + n).serialize(),
            //title: '日志列表',
            method: 'GET',
            idFiled: 'id',
            loadMsg: '正在加载，请稍候……',
            pagination: true,
            rownumbers: true,
            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
            pageSize: 20,
            toolbar: $("div[name=toolbar]", "#" + n),
            singleSelect: true,
            columns: [[
                    { field: 'id', title: '编号', width: 40 },
                    { field: 'DailyDate', title: '日志日期', width: X_WIDTH.Time, formatter: formatDate, align: 'center' },
                    { field: 'AddDepartId', title: '部门', width: X_WIDTH.Depart, formatter: formatDepart, align: 'center' },
                    { field: 'AddUserName', title: '姓名', width: X_WIDTH.UserName, align: 'center' },
                    {
                        field: 'Contents', title: '内容', width: 765, align: 'left', formatter: function (c, r, o) {
                            if (r.LastCommentContents) {
                                return r.LastCommentContents;
                            } else {
                                return c;
                            }
                        }
                    },
                    { field: 'LastCommentContents', hidden: true }
            ]],
            loadFilter: function (data) {
                return loadFilter(data);
            },
            onLoadSuccess: function (data) {
                if (data.rows.length) {
                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                }
                else {
                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                }
            },
            onClickRow: function (rowIndex, rowData) {
                popMenu('/Daily/Js/Records_View', rowData.id);
            }
        });
    }
    function edit(id) {
        var win_id = winId();
        var param = { id: id };
        $('#' + n).append("<div id='" + win_id + "'></div>");
        var $dialog = $('#' + win_id);
        var editor;
        $dialog.dialog({
            title: id > 0 ? '修改日志' : '书写日志',
            width: 900,
            height: 550,
            closed: false,
            cache: false,
            href: enumUrl.wAdd + "?" + $.param(param),
            modal: true,
            onLoad: function () {
                renderUserControls();
                $("input[name=id]", "#Daily-Records_Add").val(id);
                editor = KindEditor.create('#DailyContents', {
                    allowFileManager: true,
                    width: 883,
                    height: 430,
                    uploadJson: '/Services/upload_json.ashx',
                    fileManagerJson: '/Services/file_manager_json.ashx',
                });
            },
            onClose: function () {
                $(this).dialog("destroy");
                $dialog.remove();
            },
            buttons: [{
                text: '保存',
                handler: function () {
                    editor.sync();
                    var _date = $("input[name=DailyDate]", "#Daily-Records_Add").val();
                    var _contents = $("#DailyContents", "#Daily-Records_Add").val();
                    var _datediff = Date.daysBetween(_date, (new Date()).format("yyyy-MM-dd"));
                    if ((-_datediff) > _DailyDaysBeforeCountAllow) {
                        $.messager.show({ title: '操作提示', msg: "不能书写{0}天前的日志。".format(_DailyDaysBeforeCountAllow) });
                        return false;
                    }
                    if (!_contents) {
                        $.messager.show({ title: '操作提示', msg: "日志内容不能为空。" });
                        return false;
                    }
                    $.ajax({
                        type: 'post',
                        url: enumUrl.sAdd,
                        data: $("form[name=fm]", "#Daily-Records_Add").serializeArray(),
                        success: function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                    $dialog.dialog('close');
                                    $dg.datagrid('reload');
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "日志已经存在" });
                                    break;
                                case "Fail":
                                    $.messager.show({ title: '操作提示', msg: "保存操作失败，可能日期超出限制。" });
                                    break;
                                default:
                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                    break;
                            }
                        }
                    });
                }
            }, {
                text: '取消',
                handler: function () {
                    $dialog.dialog("close");
                }
            }]
        });
    }
})('Daily-Records_List');