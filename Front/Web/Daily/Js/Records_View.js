﻿/// <reference path="/script/jsUtil.js"/> 
/// <reference path="/script/main.js"/> 
/**
* 查看工作日志
*/

(function () {
    var id = arg0;
    LoadTemplate("/Daily", "查看工作日志", "Records_View", 900, 550, function load($dialog) {
        if (id) {
            $("form[name=fmView]").fillForm({
                "url": "/Daily/Services/Daily_Records.asmx/Query",
                "data": { id: id },
                "after": function (obj) {
                    $("div[name=Contents]", "#Daily-Records_View").html(obj.Contents);
                }
            });
        }
        $dialog.dialog({
            buttons: [
               {
                    text: '关闭',
                    handler: function () {
                        $dialog.dialog("close");
                    }
                }]
        });
    });
})();