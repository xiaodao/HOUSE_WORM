﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 外出记录
*/
(function a(n) {
    //按钮链接
    var $linkNew = $("a[name=linkNew]", "#" + n);
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    //表
    var $dg = $("table[name=dg]", "#" + n);
    //枚举
    var enumUrl = {//s服务 w//窗体
        sList: 'Daily/Services/Daily_OutRecords.asmx/GetLists'
    };
    //事件
    $linkNew.click(function () { popMenu('/Daily/Js/OutRecords_Add', 0); });

    $btnSearch.click(function () { render(); });
   
    render();
    //函数
    function render() {
        $dg.datagrid({
            url: enumUrl.sList + "?" + $("form[name=fmOutRecord]", "#Daily-OutRecords_List").serialize(),
            //title: '外出记录',
            method: 'GET',
            idFiled: 'OutRecordId',
            loadMsg: '正在加载，请稍候……',
            pagination: true,
            rownumbers: true,
            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
            pageSize: 20,
            toolbar: $("div[name=toolbar]", "#" + n),
            singleSelect: true,
            columns: [[
                    { field: 'OutRecordId', title: '编号', width: 40 },
                    { field: 'AddDepartId', title: '部门', formatter: formatDepart, width: X_WIDTH.Depart },
                    { field: 'AddUser', title: '姓名', width: X_WIDTH.UserName },
                    { field: 'BusinessType', title: '业务类型', formatter: formatBusinessType, width: 50 },
                    { field: 'HouseId', title: '房源编号', width: 90 },
                    { field: 'CRMType', title: '客户类型', width: 50, formatter: formatCRMType },
                    { field: 'CRMNames', title: '客户', width: X_WIDTH.UserName*2 },
                    { field: 'AccompanyNames', title: '陪同同事', width: X_WIDTH.UserName*2 },
                    { field: 'DoThins', title: '外出事由', width: X_WIDTH.Title-30,formatter:formatDoThins },
                    { field: 'AddTime', title: '外出时间', width: X_WIDTH.Time, formatter: formatShortTime },
                    { field: 'BackTime', title: '返回时间', width: X_WIDTH.Time, formatter: formatShortTime },
                    { field: 'State', title: '状态', width: X_WIDTH.State ,formatter:formatState, styler: styleHouseState}
         
            ]],
            loadFilter: function (data) {
                return loadFilter(data);
            },
            onLoadSuccess: function (data) {
                if (data.rows.length) {
                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                }
                else {
                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                }
            },
            onClickRow: function (rowIndex, rowData) {
                popMenu('/Daily/Js/OutRecords_View', rowData.OutRecordId);
            }
        });
    }
    function formatBusinessType(v,r,k) {
        var types = ["看房", "合同业务", "其它"];
        var c = types[v];
        if (v == 0) {
            if (r.CRMIds) {
                c = "带看";
            } else
                c = "自看";
        }
        return c;
    }
    function formatDoThins(v,r,k) {
        if (r.BusinessType == 0) {
            if (r.HouseBuilds)
                return "到" + r.HouseBuilds + "看房";
            else
                return "";
        } else

            return "到" + r.Address + " " + v;
    }
    function formatCRMType(c) {
        var types = ["购买", "租赁"];
        return types[c];
    }
    function formatState(c) {
        var types = ["外出", "返回", "取消"];
        return types[c];
    }
    function styleHouseState(v, r, k) {
        switch (v) { //{ "外出", "返回", "取消" }
            case 0:
                return 'background-color:green;color:#fff';
                break;
            case 1:
                return 'background-color:blue;color:#fff';
                break;
            case 2:
                return 'background-color:#fff;color:#666';
                break;
        }
    }
})('Daily-OutRecords_List');