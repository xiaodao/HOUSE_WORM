﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 文件说明
*/
(function a(n) {
    //按钮链接
    var $linkNew = $("a[name=linkNew]", "#" + n);
    var $linkEdit = $("a[name=linkEdit]", "#" + n);
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    var $linkDel = $("a[name=linkDel]", "#" + n);
    //表
    var $dg = $("table[name=dg]", "#" + n);
    //枚举
    var enumUrl = {//s服务 w//窗体
        sDel: "Daily/Services/Daily_RecordTemplate.asmx/Delete",
        sList: 'Daily/Services/Daily_RecordTemplate.asmx/GetLists'
    };
    //事件
    $linkNew.click(function () {
        popMenu('/Daily/Js/RecordTemplate_Add', 0);
    });
    $linkEdit.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            popMenu('/Daily/Js/RecordTemplate_Add', row.id);
        }
    });
    $btnSearch.click(function () { render(); });
    $linkDel.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            $.messager.confirm('操作提示', '确认要删除该**吗？', function (r) {
                if (r) {
                    $.ajax({
                        data: { id: row.id },
                        type: "GET",
                        url: enumUrl.sDel,
                        dataType: "json",
                        headers: { 'Content-Type': 'application/json;utf-8' },
                        success: function (result) {
                            if (result.d) {
                                $.messager.show({ title: '操作提示', msg: '删除操作成功!' });
                                render();
                            }
                        }
                    });
                }
            });
        }
    });
    window['Daily_RecordTemplate_List'] = { render: render };
    render();
    //函数
    function render() {
        var param = {};
        $dg.datagrid({
            url: enumUrl.sList + "?" + jQuery.param(param),
            title: '',
            method: 'GET',
            idFiled: 'id',
            loadMsg: '正在加载，请稍候……',
            pagination: true,
            rownumbers: true,
            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
            pageSize: 20,
            toolbar: $("div[name=toolbar]", "#" + n),
            singleSelect: true,
            columns: [[
                    { field: 'id', title: '编号', width: 40 },
                    { field: 'PostId', title: '岗位', width: X_WIDTH.UserName,formatter:formatPost },
                    { field: 'AddUserName', title: '添加人', width: X_WIDTH.UserName },
                    { field: 'AddTime', title: '添加时间', width: X_WIDTH.Time,formatter:formatShortTime },
            ]],
            loadFilter: function (data) {
                return loadFilter(data);
            },
            onLoadSuccess: function (data) {
                if (data.rows.length) {
                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                }
                else {
                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                }
            }
        });
    }

})('Daily-RecordTemplate_List');