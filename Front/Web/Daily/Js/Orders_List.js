﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 文件说明
*/
(function a(n) {
    //按钮链接
    var $linkNew = $("a[name=linkNew]", "#" + n);
    var States = ['预约', '通过', '取消', '拒绝'];
    //表
    var $dg = $("table[name=dg]", "#" + n);
    //枚举
    var enumUrl = {//s服务 w//窗体
        sDel: "Daily/Services/Daily_Orders.asmx/Delete",
        sList: 'Daily/Services/Daily_Orders.asmx/GetLists'
    };
    //事件
    $linkNew.click(function () {
        popMenu('/Daily/Js/Orders_Add', 0);
    });
    window["Daily_OrdersList"] = { render: render };
    render();
    //函数
    function render() {
        var param = {};
        $dg.datagrid({
            url: enumUrl.sList + "?" + jQuery.param(param),
            title: '',
            method: 'GET',
            idFiled: 'OrderId',
            loadMsg: '正在加载，请稍候……',
            pagination: true,
            rownumbers: true,
            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
            pageSize: 20,
            toolbar: $("div[name=toolbar]", "#" + n),
            singleSelect: true,
            columns: [[
                    { field: 'OrderId', title: '编号', width: 40 },
                    { field: 'AddUser', title: '申请人', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'OrderBeginTime', title: '预约时间', width: X_WIDTH.Time, formatter: formatShortTime, align: 'center' },
                    { field: 'OrderEndTime', title: '结束时间', width: X_WIDTH.Time, formatter: formatShortTime, align: 'center' },
                    { field: 'OrderUser', title: '预约对象', width: X_WIDTH.UserName, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'Matter', title: '事项', width: X_WIDTH.Title, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'States', title: '状态', width: X_WIDTH.State, align: 'center', formatter: function (v, r, k) { return States[v];} }
            ]],
            loadFilter: function (data) {
                return loadFilter(data);
            },
            onLoadSuccess: function (data) {
                if (data.rows.length) {
                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                }
                else {
                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                }
            },
            onClickRow: function (rowIndex, rowData) {
                popMenu('/Daily/Js/Orders_View', rowData.OrderId);
            }
        });
    }

})('Daily-Orders_List');