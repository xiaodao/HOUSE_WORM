﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 查看预约
*/
(function () {
    var States = ['预约', '通过', '取消', '拒绝'];
    var id = arg0;
    if (!id) return;

    LoadTemplate("/Daily", "查看预约", "Orders_View", 754, 500, function load($dialog) {
        var _obj;
        $("form[name=fm]", "#Daily-Orders_View").fillForm({
            "url": "/Daily/Services/Daily_Orders.asmx/Query",
            "data": { id: id }, filter: function (obj) {
                obj.States = States[obj.States];
                _obj = obj;
                return obj;
            }
        });
        $("form[name=fm] *", "#Daily-Orders_View").prop("readonly", true);
        $("form[name=fm] :input", "#Daily-Orders_View").prop("disabled", true);

        var btns = [];
        if (_obj.AddUserId == X_USEROBJ.UserId && _obj.State==0)
            btns.push({ text: '取消预约', handler: function () { action(2,'取消预约'); } });
        if (_obj.OrderUserId == X_USEROBJ.UserId && _obj.State==0) {
            btns.push({ text: '通过', handler: function () { action(1,'通过'); } });
            btns.push({ text: '驳回', handler: function () { action(3,'驳回'); } });
        }
        btns.push({ text: '关闭', handler: function () { $dialog.dialog("close"); } });
        $dialog.dialog({ buttons: btns });
        function action(state,act) {
            $.messager.confirm('确认', "您的操作将不能取消，确定要" + act + "吗？", function (r) {
                if (r) {
                    $.post("/Daily/Services/Daily_Orders.asmx/DoAction", {
                        id: id,
                        State: state
                    }, function (r) {
                        switch ($(r).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                callPageRender('Daily_OrdersList');
                                break;
                            case "TimeOut":
                                break;
                            default:
                                break;
                        }
                    });
                }
            });

           
        }
    });
})();