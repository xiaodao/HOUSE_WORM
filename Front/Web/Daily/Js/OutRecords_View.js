﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 
*/
(function () {
    var id = arg0;
    LoadTemplate("/Daily", "查看外出记录", "OutRecords_View.aspx?OutRecordId=" + id, 395, 460, function load($dialog) {
        //初始化
        $("#OutRecords_View_Form input").prop("disabled", true);
        $("#OutRecords_ViewTabs").tabs({
            onSelect: function (title, index) {
                $("input[name=BusinessType]", "#Daily-OutRecords_Add").val(index);
            }
        });
        $.ajax({
            type: "get",
            url: "/Daily/Services/Daily_OutRecords.asmx/Query",
            dataType: "json",
            contentType: "application/json;utf-8",
            data: { id: id },
            success: function (data) {
                var obj = data.d;
                $("#OutRecords_ViewTabs").tabs("disableTab", 0);
                $("#OutRecords_ViewTabs").tabs("disableTab", 1);
                $("#OutRecords_ViewTabs").tabs("disableTab", 2);
                $("#OutRecords_ViewTabs").tabs("enableTab", parseInt(obj.BusinessType));
                $("#OutRecords_ViewTabs").tabs("select", parseInt(obj.BusinessType));

                var _tempHouseIds = $("#lblHouseIds").text().trim();
                var _tempBuilds = $("#lblHouseBuilds").text().trim();
                var _tempCRMIds = $("#divCRMIds").text().trim();
                var _tempCRMNames = $("#lblCRMNames").text().trim();
                var _arrayHouseIds = _tempHouseIds.split(',');
                var _arrayBuilds = _tempBuilds.split(',');
                var _arrayCRMIds = _tempCRMIds.split(',');
                var _arrayCRMNames = _tempCRMNames.split(',');

                var _result = '';
                var _houseInfo = "";
                var _crmInfo = "";
                for (var s = 0; s < _arrayHouseIds.length; s++) {
                    if (_houseInfo != "") _houseInfo += "<br />";
                    if (obj.HouseType == 0) {
                        //出售
                        _result += " <a href='javascript:;' class='jsFunc' win='/House/Js/Sells_View' arg0='{0}'>{0}</a> ".format(_arrayHouseIds[s]);
                        //获取obj
                        var houseObj = getRemoteObject("/House/Services/House_Sells.asmx/Query", { id: _arrayHouseIds[s] });
                        _houseInfo = _houseInfo + "<a href='javascript:;' class='jsFunc' win='/House/Js/Sells_View' arg0='{0}'>[出售] {3} {1}#{2}</a>".format(_arrayHouseIds[s], houseObj.Dong, houseObj.Fang, _arrayBuilds[s]);
                    }
                    else {
                        _result += " <a href='javascript:;' class='jsFunc' win='/House/Js/Rent_View' arg0='{0}'>{0}</a> ".format(_arrayHouseIds[s]);
                        //获取obj
                        var houseObj = getRemoteObject("/House/Services/House_Rents.asmx/Query", { id: _arrayHouseIds[s] });
                        _houseInfo = _houseInfo + "<a href='javascript:;' class='jsFunc' win='/House/Js/Rent_View' arg0='{0}'>[出租] {3} {1}#{2}</a>".format(_arrayHouseIds[s], houseObj.Dong, houseObj.Fang, _arrayBuilds[s]);
                    }
                }
                for (var s = 0; s < _arrayCRMIds.length; s++) {
                    if (_crmInfo != "") _crmInfo += "<br />";
                    //var crmObj = getRemoteObject("/CRM/Services/CRM_Buyers.asmx/Query", { id: _arrayCRMIds[s] });
                    if (obj.CRMType == "0") {
                        //求购
                        var crmObj = getRemoteObject("/CRM/Services/CRM_Buyers.asmx/Query", { id: _arrayCRMIds[s] });
                        _crmInfo = _crmInfo + "<a href='javascript:;' class='jsFunc' win='/CRM/Js/Buyers_View' arg0='{3}'>[{0}] {1} {2}</a>".format("求购", crmObj.CustomerName, crmObj.WantBuildings, crmObj.CustomerId);
                    } else {
                        //求租
                        var crmObj = getRemoteObject("/CRM/Services/CRM_Buyers.asmx/Query", { id: _arrayCRMIds[s] });
                        _crmInfo = _crmInfo + "<a href='javascript:;' class='jsFunc' win='/CRM/Js/Buyers_View' arg0='{3}'>[{0}] {1} {2}</a>".format("求租", crmObj.CustomerName, crmObj.WantBuildings, crmObj.CustomerId);
                    }
                    $("#divCRMInfo").html(_crmInfo);

                }
                $("#divHouseInfo").html(_houseInfo);
                $("#lblHouseIds").html(_result);
                if (obj.State == 0) {
                    //按钮定义
                    $dialog.dialog({
                        buttons: [{
                            text: '外出返回',
                            handler: function () {
                                $.messager.confirm('操作提示', "确认外出返回操作吗？", function (r) {
                                    if (r) {
                                        $.get("/Daily/Services/Daily_OutRecords.asmx/Back", { id: id }, function (v) {
                                            switch ($(v).find("ServiceStateKeys").text()) {
                                                case "Success":
                                                    $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                                    var $dg = $("table[name=dg]", "#Daily-OutRecords_List");
                                                    if ($dg.length > 0)
                                                        $dg.datagrid('reload');
                                                    break;
                                                default:
                                                    $.messager.show({ title: '操作提示', msg: "操作失败，可能没有操作权限。" });
                                                    break;
                                            }
                                        });
                                        $dialog.dialog("close");
                                    }
                                });
                            }
                        }, {

                            text: '取消外出',
                            handler: function () {
                                $.messager.confirm('操作提示', "确认取消外出吗？", function (r) {
                                    if (r) {
                                        $.get("/Daily/Services/Daily_OutRecords.asmx/Cancel", { id: id }, function (v) {
                                            switch ($(v).find("ServiceStateKeys").text()) {
                                                case "Success":
                                                    $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                                    var $dg = $("table[name=dg]", "#Daily-OutRecords_List");
                                                    if ($dg.length > 0)
                                                        $dg.datagrid('reload');
                                                    break;
                                                default:
                                                    $.messager.show({ title: '操作提示', msg: "操作失败，可能没有操作权限。" });
                                                    break;
                                            }
                                        });
                                        $dialog.dialog("close");
                                    }
                                });
                            }
                        }, {
                            text: '关闭',
                            handler: function () {
                                $dialog.dialog("close");
                            }
                        }]
                    });
                }
                else {
                    //按钮定义
                    $dialog.dialog({
                        buttons: [{
                            text: '关闭',
                            handler: function () {
                                $dialog.dialog("close");
                            }
                        }]
                    });
                }
            }
        });
    });
})();