﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 针对本项目的一些公用变量及公用方法
*/
(function () {
    var id = arg0;
    LoadTemplate("/Daily", "设置日志模板", "RecordTemplate_Add.aspx?id=" + id, 700, 450, function load($dialog) {
        renderUserControls();
        editor = KindEditor.create('#DailyTemplates', {
            allowFileManager: true,
            width: 683,
            height: 330,
            uploadJson: '/Services/upload_json.ashx',
            fileManagerJson: '/Services/file_manager_json.ashx',
        });
        if (id) {
            $("form[name=fm]", "#Daily-RecordTemplate_Add").fillForm({
                params: null,
                url: "/Daily/Services/Daily_RecordTemplate.asmx/Query",
                data: { id: id },
                filter: function (obj) {
                    editor.html(obj.Templates);
                    return obj;
                },
                after: function () {

                }
            });
        }
        $dialog.dialog({
            buttons: [
                {
                    text: '保存',
                    handler: function () {
                        editor.sync();
                        var _Templates = $("#DailyTemplates", "#RecordTemplate_Add").val();
                        $.ajax({
                            type: 'post',
                            url: 'Daily/Services/Daily_RecordTemplate.asmx/Add',
                            data: $("form[name=fm]", "#Daily-RecordTemplate_Add").serializeArray(),
                            success: function (data) {
                                switch ($(data).find("ServiceStateKeys").text()) {
                                    case "Success":
                                        $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                        callPageRender('Daily_RecordTemplate_List');
                                        $dialog.dialog('close');
                                        break;
                                    default:
                                        $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                        break;
                                }
                            }
                        });
                    }
                }, {
                    text: '关闭',
                    handler: function () {
                        $dialog.dialog("close");
                    }
                }]
        });
    });
})();