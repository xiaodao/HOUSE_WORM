﻿(function () {
    var id = arg0;
    LoadTemplate("/Daily", id > 0 ? '修改外出记录' : '新增外出记录', "OutRecords_Add.aspx?OutRecordId=" + id, 400, 428, function load($dialog) {
        if (!id) id = 0;
        $("input[name=OutRecordId]", "#Daily-OutRecords_Add").val(id);
        $("#OutRecords_AddTabs").tabs({
            onSelect: function (title, index) {
                if (index == 1) index = 2;
                $("input[name=BusinessType]", "#Daily-OutRecords_Add").val(index);
            }
        });
        $("input[name=HouseType]", "#Daily-OutRecords_Add").change(function () {
            switch ($(this).val()) {
                case "0"://买卖
                    $("#linkSelectHouse").attr("win", "/House/Js/Sell_Select");
                    $("#linkAddHouse").attr("win", "/House/Js/Sell_Add");
                    break;
                case "1":
                    $("#linkSelectHouse").attr("win", "/House/Js/Rent_Select");
                    $("#linkAddHouse").attr("win", "/House/Js/Rent_Add");
                    break;
            }
        });
        $("input[name=CRMType]", "#Daily-OutRecords_Add").change(function () {
            switch ($(this).val()) {
                case "0"://买卖
                    $("#linkSelectCustomer").attr("win", "/CRM/Js/Select_Buyers");
                    break;
                case "1":
                    $("#linkSelectCustomer").attr("win", "/CRM/Js/Select_Rents");
                    break;
            }
        });
        $dialog.dialog({
            buttons: [{
                text: '保存',
                handler: function () {
                    ///有效性
                    ///当前选项卡位置
                    var tab = $("#OutRecords_AddTabs").tabs("getSelected");
                    var index = $("#OutRecords_AddTabs").tabs('getTabIndex', tab);
                    switch (index) {
                        case 0://看房
                            if (!$("input[name=HouseId]", "#Daily-OutRecords_Add").val()) {
                                $.messager.show({ title: '操作提示', msg: '请选择房源' });
                                return;
                            }
                            if ($("input[name=ToHouseType]:checked", "#Daily-OutRecords_Add").val()=="带看") {
                                if (!$("#OutRecords_Add_Customers").val()) {
                                    $.messager.show({ title: '操作提示', msg: '带看请选择客户！' });
                                    return;
                                }
                            }
                            break;
                        case 1://合同业务
                            break;
                        case 2://其它
                            break;
                    }
                    $.ajax({
                        url: 'Daily/Services/Daily_OutRecords.asmx/Add',
                        data: $("form[name=fm]", "#Daily-OutRecords_Add").serializeArray(),
                        success: function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                    $dialog.dialog('close');
                                    var $dg = $("table[name=dg]", "#Daily-OutRecords_List");
                                    if ($dg.length > 0)
                                        $dg.datagrid('reload');
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "用户已经存在" });
                                    break;
                                case "Fail":
                                    $.messager.show({ title: '操作提示', msg: "保存操作失败!" });
                                    break;
                                default:
                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                    break;
                            }
                        }
                    });
                }
            }, {
                text: '取消',
                handler: function () {
                    $dialog.dialog("close");
                }
            }]
        });
    })
})();