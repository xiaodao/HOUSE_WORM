﻿$(document).ready(function () {
    var pDoc = $('<div><table name="tblDoc"></table></div>').appendTo('#Portal-Default');
    pDoc.panel({
        title: '最新公文',
        height: 306,
        collapsible: true
    });
    $('#content').portal('add', {
        panel: pDoc,
        columnIndex: 0
    });
    var $dgDoc = $("table[name=tblDoc]", '#Portal-Default');
    $dgDoc.datagrid({
        url: '/Doc/Services/Doc_Items.asmx/GetLists',
        method: 'GET',
        queryParams: { page: 1, rows: 10 },
        idFiled: 'DocId',
        loadMsg: '正在加载，请稍候……',
        rownumbers: false,
        pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
        singleSelect: true,
        columns: [[
                { field: 'TypeName', title: '类别', align: 'center', width: 50 },
                { field: 'Title', title: '标题', width: X_WIDTH.Title },
                { field: 'AddDepartId', title: '发布部门', align: 'center', width: X_WIDTH.Depart, formatter: formatDepart }
        ]],
        loadFilter: function (data) {
            return loadFilter(data);
        },
        onClickRow: function (rowIndex, rowData) {
            popMenu('/Doc/Js/Items_View', rowData.DocId);
        }
    });
});
///**公文**/
