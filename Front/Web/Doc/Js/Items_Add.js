﻿(function () {
    var id = arg0;
    var title;
    var editor;
    if (id) title = "修改"; else title = "添加";
    LoadTemplate("/Doc", title + "公文", "Items_Add.aspx?DocId=" + id, 720, 600, function load($dialog) {
        renderUserControls();
        if (!editor)
            editor = KindEditor.create('textarea[name="Body"]', {
                allowFileManager: true,
                width: 683,
                height: 380,
                uploadJson: '/Services/upload_json.ashx',
                fileManagerJson: '/Services/file_manager_json.ashx',
            });
        $("input[name=DocId]", "#Doc-Items_Add").val(0);
        if (id) {
            $.ajax({
                type: "get",
                url: "/Doc/Services/Doc_Items.asmx/Query",
                dataType: "json",
                contentType: "application/json;utf-8",
                data: {id:id},
                success: function (data) {
                    var obj = data.d;
                    if (obj) {
                        $("input[name=TypeId][value=" + obj.TypeId + "]", "#Doc-Items_Add").prop("checked", true);
                        $("input[name=Title]", "#Doc-Items_Add").val(obj.Title);
                        $("input[name=DocId]", "#Doc-Items_Add").val(obj.DocId);
                        editor.html(obj.Body);
                    }
                }
            });
        }
        $dialog.dialog({
            buttons: [{
                text: '保存',
                handler: function () {
                    if (!$("input[name=TypeId]:checked").length) {
                        $.messager.show({ title: '操作提示', msg: "请选择公文类型！" });
                        return;
                    }
                    if (!editor.html()) {
                        $.messager.show({ title: '操作提示', msg: "请填写公文内容！" });
                        return;
                    }
                    editor.sync();
                    $.ajax({
                        type:'post',
                        url: "/Doc/Services/Doc_Items.asmx/Add",
                        data: $("form[name=fm]", "#Doc-Items_Add").serializeArray(),
                        success: function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                    callPageRender('Doc-Items_List');
                                    $dialog.dialog('close');
                                    KindEditor.remove('textarea[name="Body"]');
                                    editor = undefined;
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "用户已经存在" });
                                    break;
                                case "NoPermiss":
                                    $.messager.show({ title: '操作提示', msg: "没有权限！" });
                                    return;
                                default:
                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                    break;
                            }

                        }
                    });
                }
            }, {
                text: '取消',
                handler: function () {
                    KindEditor.remove('textarea[name="Body"]');
                    editor = undefined;
                    $dialog.dialog("close");
                }
            }]
        });
    });
})();