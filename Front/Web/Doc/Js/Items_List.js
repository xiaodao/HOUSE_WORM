﻿(function a(n) {
    //按钮链接
    var $linkNew = $("a[name=linkNew]", "#" + n);
    var $linkEdit = $("a[name=linkEdit]", "#" + n);
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    var $linkDel = $("a[name=linkDel]", "#" + n);
    //表
    var $dg = $("table[name=dg]", "#" + n);
    //枚举
    var enumUrl = {//s服务 w//窗体
        sDel: "Doc/Services/Doc_Items.asmx/Delete",
        sList: 'Doc/Services/Doc_Items.asmx/GetLists',
        sAdd: 'Doc/Services/Doc_Items.asmx/Add'
    };
    //事件
    $linkNew.click(function () {
        popMenu('/Doc/Js/Items_Add', 0);
    });
    $linkEdit.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            popMenu('/Doc/Js/Items_Add', row.DocId);
        }
    });
    $btnSearch.click(function () { render(); });
    $linkDel.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            $.messager.confirm('操作提示', '确认要删除该公文吗？', function (r) {
                if (r) {
                    $.ajax({
                        data: { id: row.DocId },
                        type: "GET",
                        url: enumUrl.sDel,
                        dataType: "json",
                        headers: { 'Content-Type': 'application/json;utf-8' },
                        success: function (result) {
                            if (result.d) {
                                $.messager.show({ title: '操作提示', msg: '删除操作成功!' });
                                render();
                            }
                        }
                    });
                }
            });
        }
    });
    render();
    //函数
    function render() {
        var param = {};
        $dg.datagrid({
            url: enumUrl.sList + "?" + jQuery.param(param),
            //title: '公文列表',
            method: 'GET',
            idFiled: 'DocId',
            loadMsg: '正在加载，请稍候……',
            rownumbers: true,
            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
            pageSize: 20,
            toolbar: $("div[name=toolbar]", "#" + n),
            pagination: true,
            singleSelect: true,
            columns: [[
                    { field: 'DocId', title: '编号', width: 40 },
                    { field: 'TypeName', title: '类别', align: 'center', width: 50 },
                    { field: 'Title', title: '标题', width: X_WIDTH.Title, formatter: formatTitle },
                    { field: 'Yinfa', title: '印发', align: 'center', width: X_WIDTH.Depart },
                    { field: 'AddUser', title: '发布人', align: 'center', width: X_WIDTH.UserName },
                    { field: 'AddTime', title: '发布时间', width: X_WIDTH.Time, formatter: formatShortTime }
            ]],
            loadFilter: function (data) {
                return loadFilter(data);
            },
            onLoadSuccess: function (data) {
                if (data.rows.length) {
                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                }
                else {
                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                }
            }
        });
    }
    function formatTitle(v, r, k) {
        return "<a href='javascript:;' onclick='showDocItem({0})'>{1}</a>".format(r.DocId, v);
    }
    window[n] = { render: render };
})('Doc-Items_List');

function showDocItem(id) {
    popMenu('/Doc/Js/Items_View', id);
}