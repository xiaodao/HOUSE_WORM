﻿(function () {
    var id = arg0;
    LoadTemplate("/Doc", '查看公文', 'Items_View', 750, 570, function load($dialog) {
        var _obj;
        $.ajax({
            type: "get",
            dataType: "json",
            async: false,
            contentType: "application/json;utf-8",
            url: "/Doc/Services/Doc_Items.asmx/Query",
            data: { id: id },
            success: function (data) {
                _obj = data.d;
            }
        });
        $("div[name=title]", "#Doc-Items_View").text(_obj.Title);
        $("div[name=body]", "#Doc-Items_View").html(_obj.Body);
        $("span[name=addTime]", "#Doc-Items_View").html(formatTimeBase(_obj.AddTime, 'yyyy-MM-dd'));
        $("span[name=addUser]", "#Doc-Items_View").html(_obj.AddUser);
        $("span[name=addDepart]", "#Doc-Items_View").html(X_DEPARTS['_' + _obj.AddDepartId].DepartName);
        $("span[name=Obj]", "#Doc-Items_View").html(_obj.Obj);
        $("label[name=Yinfa]", "#Doc-Items_View").html(_obj.Yinfa);
        $("label[name=Chaobao]", "#Doc-Items_View").html(_obj.Chaobao);
        $("label[name=Chaosong]", "#Doc-Items_View").html(_obj.Chaosong);
        $("label[name=Luokuan]", "#Doc-Items_View").html(_obj.Luokuan);
        $dialog.dialog({
            buttons: [{
                text: '关闭',
                handler: function () {
                    $dialog.dialog("close");
                }
            }]
        });
    });
})();


