﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CM.Js.Buy_FeeTypes_List", {
    $win: {},
    isSingleSelect: false,
    SelectedArray: [],
    Grid: {},
    show: function (RelatedIndex) {
        this.isSingleSelect = (arg1 == "true");
        var winDom;
        var buttons = [];
        var _module = this;
        buttons = buttons.concat([
               {
                   text: "关闭",
                   handler: function () {
                       this.up("window").close();
                   }
               }
        ]);

        var win = Ext.create('Ext.window.Window', {
            title: '查看费用列表', height: 500, width: 754, modal: true,
            loader: {
                url: '/CM/Templates/Buy_FeeTypes_List.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        _module.$win = winDom;
                        _module.renderList(RelatedIndex,Ext.get(winDom.find('.divMainList')[0]));
                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (RelatedIndex,view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            fields: ['FeeTypeId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/CM/Services/CM_Buy_FeeTypes.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'FeeTypeId'
            }
        });
        var Columns = easyui2extgrid([[
            { field: 'FeeTypeId', title: '编号', width: 40 },
            { field: 'Title', title: '类型名称', width: 100, align: 'center' },
            { field: 'Functions', title: '公式', width: 150, align: 'center' },
            { field: 'SubjectTitle', title: '会计科目', width: 150, align: 'center' },
            { field: 'SubjectCode', title: '会计科目代码', width: 150, align: 'center' },
            { field: 'Operate',width:50, title: '收款',formatter:formatOperate}
        ]]);
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: 400,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Vouchers_Add = Ext.create("FI.Js.Vouchers_Add");
                    Vouchers_Add.show(RelatedIndex,row.data.FISubjectId,row.data.FeeTypeId);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });
        function formatOperate(v, r, k) {
            return "<a href='javascript:;' >收款</a>";
        }
    }
});