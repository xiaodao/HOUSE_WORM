﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CM.Js.Buy_Follows_Add", {
    $win: {},
    show: function (FollowId,CMId) {
        var winDom;
        var _module = this;
        var win = Ext.create('Ext.window.Window', {
            title:  '合同跟进', height: 180, width: 295, modal: true,
            loader: {
                url: '/CM/Templates/Buy_Follows_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        _module.$win = winDom;
                        $("input[name=CMId]", winDom).val(CMId);
                        renderUserControls(winDom);
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/CM/Services/CM_Buy_Follows.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('CM.Js.Buy_Follows_List');

                                    var _str = "<li>【{0}】[{1}][{3}]{2}</li>".format(X_USER,
                                             (new Date()).format("yy-MM-dd hh:mm"),
                                             $("textarea[name=Follow]", winDom).val(),
                                             FOLLOWCMSTATE[$("input[name='FollowCMState']:checked", winDom).val()]);
                                    if ($("#ulCmBuyFollow", "#Buy_Items_View-body").length > 0)
                                        $("#ulCmBuyFollow", "#Buy_Items_View-body").prepend(_str);

                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});