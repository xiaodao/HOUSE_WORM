﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CM.Js.Rent_Items_View", {
    show: function (CMId) {
        var winDom;
        var mRent_Items = getRemoteObject("/CM/Services/CM_Rent_Items.asmx/Query", { id: CMId });

        if (!mRent_Items) {
            $.messager.show("操作提示", title + "编号不存在");
            return;
        }

        var buttons = [];
      
        buttons = buttons.concat([
               {
                   text: "关闭",
                   handler: function () {
                       this.up("window").close();
                   }
               }
        ]);

        var win = Ext.create('Ext.window.Window', {
            id:'Rent_Items_View',
            title: '查看出租合同', height: 500, width: 900, modal: true,
            loader: {
                url: '/CM/Templates/Rent_Items_View.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = obj.dom;
                        var _tempId = win.getId();
                        mRent_Items.TempId = _tempId;
                        mRent_Items.PayTypeStr = PAYTYPEARRAY[mRent_Items.PayType];
                        mRent_Items.ChargePayTypeStr = CHARGEPAYTYPEARRAY[mRent_Items.ChargePayType];

                        var $win = $(".x-autocontainer-innerCt", "#" + _tempId);
                        var modelHouse = getRemoteObject("/House/Services/House_Rents.asmx/Query", { id: mRent_Items.HouseId });
                        var modelBuilding = getRemoteObject("/Region/Services/Region_Buildings.asmx/Query", { BuildId: modelHouse.RentBuildingId });
                        var modelArea = X_AREA['_' + mRent_Items.HouseRegionArea];
                        var modelSignUser = getRemoteObject("/RBAC/Services/RBAC_Users.asmx/Query", { id: mRent_Items.SignUserId });
                        modelSignUser.DepartName = X_DEPARTS['_' + mRent_Items.SignDepartId];

                        var repFeeTypes = getRemoteRows("/CM/Services/CM_Rent_FeeTypes.asmx/GetLists", {});
                        var repFollow = getRemoteRows("/CM/Services/CM_Rent_Follows.asmx/GetLists", { CMId: mRent_Items.CMId });
                        var html = template('CM_Js_Rent_Items', {
                            modelHouse: modelHouse,
                            modelCMItem: mRent_Items,
                            modelBuilding: modelBuilding,
                            modelArea: modelArea,
                            modelSignUser: modelSignUser,
                            repFeeTypes: repFeeTypes,
                            repFollow: repFollow
                        });
                        $win.html(html);

                        var _str = "";
                        $(".FeeTypeSubjects").each(function () {
                            if (_str != "") _str += ",";
                            _str += $(this).attr("FISubjectId");
                        });
                        $("#linkFIVouchers").attr("arg1", _str.trimdot());

                        loadTotalFee();
                        $("a[name=reloadFeeTotal]", $win).click(function () {
                            loadTotalFee();
                        });

                        function loadTotalFee() {
                            $.get("/FI/Services/FI_Vouchers.asmx/QueryTotalAmount", {
                                SubjectIds: _str,
                                RelatedIndex: CMId
                            }, function (t) {
                                $("label[name=lblTotalFee]", $win.dom).text(parseFloat($(t).find("decimal").text()).toFixed(1));
                            });
                        }
                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    constructor: function (config) {
        this.initConfig(config);
    }
});