﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CM.Js.Buy_Items_Add", {
    show: function (CMId) {
        var winDom;
        var _title;
        if (CMId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '买卖合同', height: 525, width: 900, modal: true,
            loader: {
                url: '/CM/Templates/Buy_Items_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=CMId]", winDom).val(CMId);
                        renderUserControls(winDom);

                        $("input[name=HouseId]", winDom).change(function () {
                            var _houseid = $(this).val();
                            if (!_houseid) return;
                            $.ajax({
                                url: "/House/Services/House_Sells.asmx/Query",
                                dataType: 'json',
                                headers: { 'Content-Type': 'application/json;utf-8' },
                                data: { id: _houseid },
                                success: function (data) {
                                    var obj = data.d;
                                    if (!obj) return;
                                    $("select[name=SellRegionArea]", winDom).val(obj.SellRegionArea);
                                    //小区地址
                                    $.ajax({
                                        url: "/Region/Services/Region_Buildings.asmx/Query",
                                        dataType: 'json',
                                        headers: { 'Content-Type': 'application/json;utf-8' },
                                        data: { BuildId: obj.SellBuildingId },
                                        success: function (data1) {
                                            var _control = Ext.getCmp($("#SellAddSellBuildingId").attr("ext-comobobox-id"));
                                            _control.setValue(data1.d.BuildingId);
                                            _control.setRawValue(data1.d.BuildingName);
                                        }
                                    });
                                    $("input[name=Dong]", winDom).val(obj.Dong);
                                    $("input[name=Fang]", winDom).val(obj.Fang);
                                    $("input[name=HouseCustomerName]", winDom).val(obj.CustomerName);
                                    $("input[name=HouseTel]", winDom).val(obj.CustomerTel);
                                    $("input[name=HouseArea]", winDom).val(obj.HouseArea);
                                    $("input[name=UnitPrice]", winDom).val(obj.UnitPrice);
                                    $("input[name=TotalPrice]", winDom).val(obj.TotalPrice);
                                    if (obj.GetPropertyDate)
                                        Ext.getCmp($("input[name=GetPropertyDate]", winDom).attr("data-ext-id")).setValue(obj.GetPropertyDate.toDate());

                                }
                            });

                        });
                        $("#Items_Buy_Add_CustomerId", winDom).change(function () {
                            var _crmid = $(this).val();
                            $("input[name=CustomerId]", winDom).val(_crmid);
                            $.ajax({
                                url: "/CRM/Services/CRM_Buyers.asmx/Query",
                                dataType: 'json',
                                headers: { 'Content-Type': 'application/json;utf-8' },
                                data: { id: _crmid },
                                success: function (data) {
                                    $("input[name=Sex][value=" + data.d.Sex + "]", winDom).prop("checked", true);
                                    $("input[name=CustomerTel]", winDom).val(data.d.Tel);
                                    $("input[name=CustomerName]", winDom).val(data.d.CustomerName);
                                }
                            });
                        });

                        if (CMId) {
                            $("form[name=fmAdd]", winDom).fillForm({
                                "url": "CM/Services/CM_Buy_Items.asmx/Query",
                                data: { id: CMId },
                                filter: function (obj) {
                                    obj.CustomerIdHidden = obj.CustomerId;
                                    obj.SellRegionArea = obj.HouseRegionArea;
                                    obj.UnitPrice = (obj.TotalPrice * 10000 / obj.HouseArea).toFixed(2);
                                    $("input[name=SignDepartId]").val(obj.SignDepartId).change();

                                    var rbacuser = RBAC.Js.RBAC_Module.query(obj.SignUserId);
                                    obj.UserMobile = rbacuser.UserMobile;
                                    return obj;
                                },
                                after: function (obj) {
                                    $("input[name=HouseId]", winDom).change();
                                    $("#Items_Buy_Add_CustomerId", winDom).change();
                                }
                            });
                        }
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/CM/Services/CM_Buy_Items.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('CM.Js.Buy_Items_List');
                                    _win.close();
                                    break;
                                case "NoPermiss":
                                    $.messager.show({ title: '操作提示', msg: "没有操作权限" });
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});