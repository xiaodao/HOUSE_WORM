﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CM.Js.Buy_Steps_Logs_Add", {
    $win: {},
    show: function (LogId, CMId, $shouldStep) {
        var winDom;
        var _module = this;
        var IsOrderFollow = RBAC.Js.RBAC_Module.SystemGetBool("CM", "IsOrderFollow");
        var _step;
        var win = Ext.create('Ext.window.Window', {
            title: '买卖合同跟单', height: 240, width: 395, modal: true,
            loader: {
                url: '/CM/Templates/Buy_Steps_Logs_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        _module.$win = winDom;
                        $("input[name=LogId]", winDom).val(LogId);
                        $("input[name=CMId]", winDom).val(CMId);

                        renderUserControls(winDom);
                        if (IsOrderFollow) {
                            $shouldStep = $("#ulCmSteps li[Operate=0]:eq(0)");
                        }
                        _step = $shouldStep.attr("StepId");
                        if (!_step) {
                            console.log('eception');
                            return;
                        }
                        $("#legendStep", winDom).text("现在操作第" + _step + "步");
                        $("input[name=StepId]", winDom).val(_step);
                        $("#Steps_Log_Add_Description", winDom).text($shouldStep.attr("Description"));

                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.messager.confirm('此操作不能撤销，请确认操作', "是否确认提交跟单信息吗？", function () {
                        $.post("/CM/Services/CM_Buy_Steps_Logs.asmx/Add", $("form[name=fmAdd]", _win.dom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('CM.Js.Buy_Items_List');

                                    //刷新显示
                                    var _color, _state, _description;
                                    _state = parseInt($("input[name=State]:checked", _win.dom).val());
                                    _description = $("textarea[name=Description]", _win.dom).val();
                                    if (_state == 2) {
                                        _color = "red";
                                    }
                                    else if (_state == 1) {
                                        _color = "blue";
                                    }
                                    else {
                                        _color = "#666";
                                    }

                                    $shouldStep.find(".State").text(CM_STEPS_STATE_ARRAY[_state]);
                                    $shouldStep.find(".LogDescription").text(X_USER + " " + (new Date()).format("yy/MM/dd") + " " + _description).attr("title", _description);
                                    $shouldStep.find(".State").css("color", _color);
                                    $shouldStep.attr("Operate", "1");


                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                    });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});