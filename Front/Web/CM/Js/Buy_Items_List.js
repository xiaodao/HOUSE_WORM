﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CM.Js.Buy_Items_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['CMId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/CM/Services/CM_Buy_Items.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        
        var Columns = easyui2extgrid([[
                    { field: 'CMNumber', title: '合同编号', width: 50 },
                    { field: 'HouseId', title: '房源', width: 50, align: 'center' },
                    { field: 'HouseRegionArea', title: '区域', width: 50, formatter: formatRegionArea, align: 'center' },
                    { field: 'BuildingName', title: '小区名称', width: X_WIDTH.Building, align: 'center' },
                    { field: 'HouseArea', title: '面积', width: 50, align: 'center' },
                    { field: 'TotalPrice', title: '成交价', width: 50, align: 'center' },
                    { field: 'ChargeTotal', title: '佣金', width: 80, align: 'center', formatter: formatCharge },
                    { field: 'SignUser', title: '签单人', width: 140, align: 'center', formatter: formatSign },
                    { field: 'HouseAddUser', title: '房源', width: 50, align: 'center' },
                    { field: 'AddTime', title: '签单日期', width: 110, align: 'center', formatter: formatShortTime },
                    { field: 'AddUserId', title: '进行天数', width: 50, align: 'center', formatter: formatDays },
                    { field: 'State', title: '状态', width: 70, align: 'center', formatter: formatCmState },
                    { field: 'Title', title: '操作进度', width: 130, align: 'left', formatter: formatStep }
        ]]);
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Buy_Items_View = Ext.create("CM.Js.Buy_Items_View");
                    Buy_Items_View.show(row.data.CMId);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });
        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            //grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Buy_Items_Add = Ext.create("CM.Js.Buy_Items_Add");
            Buy_Items_Add.show(0);
        });
        $("a[name=linkEdit]", view.dom).click(function () {
            if (selectRowData.CMId) {
                var Buy_Items_Add = Ext.create("CM.Js.Buy_Items_Add");
                Buy_Items_Add.show(selectRowData.CMId);
            } else
                $.messager.show({ msg: '请选择行' });
        });
        function formatSign(v, r, k) {
            return "[" + X_DEPARTS['_' + r.SignDepartId].DepartName + "]" + r.SignUser;
        }
        var colors = ['green','red','#000','red'];
        function formatCmState(v, r, k) {
            return '<span style="color:{0}">'.format(colors[v]) + CM_STATE_ARRAY[r.State] + '</span>';
        }
        function formatDays(v, r, k) {
            return '<span style="color:red">' + Date.daysBetween((new Date()).format("yyyy-MM-dd"), r.AddTime.toDate().format("yyyy-MM-dd")) + '</span>';
        }
        function formatCharge(v, r, k) {
            if (v)
                return v + "(<span style='color:red'>" + parseFloat(v / (100 * r.TotalPrice)).toFixed(2) + "</span>)";
        }
        function formatStep(v, r, k) {
            
            if (v)
                return r.NowStepInt + "." + v;
            else
                return "0.未开始";
        }
    }
});