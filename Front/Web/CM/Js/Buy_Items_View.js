﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CM.Js.Buy_Items_View", {
    show: function (CMId) {
        var winDom;
        var mBuy_Items = getRemoteObject("/CM/Services/CM_Buy_Items.asmx/Query", { id: CMId });
        var IsOrderFollowForView = RBAC.Js.RBAC_Module.SystemGetBool("CM", "IsOrderFollow");
        if (!mBuy_Items) {
            $.messager.show("操作提示", "没有查看权限");
            return;
        }

        var buttons = [];

        buttons = buttons.concat([
               {
                   text: "关闭",
                   handler: function () {
                       this.up("window").close();
                   }
               }
        ]);

        var win = Ext.create('Ext.window.Window', {
            id: 'Buy_Items_View',
            title: '查看买卖合同', height: 660, width: 900, modal: true,
            loader: {
                url: '/CM/Templates/Buy_Items_View.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = obj.dom;
                        var _tempId = win.getId();
                        mBuy_Items.TempId = _tempId;
                        mBuy_Items.PayTypeStr = PAYTYPEARRAY[mBuy_Items.PayType];
                        mBuy_Items.ChargePayTypeStr = CHARGEPAYTYPEARRAY[mBuy_Items.ChargePayType];

                        var $win = $(".x-autocontainer-innerCt", "#" + _tempId);
                        var modelHouse = getRemoteObject("/House/Services/House_Sells.asmx/Query", { id: mBuy_Items.HouseId });
                        var modelBuilding = getRemoteObject("/Region/Services/Region_Buildings.asmx/Query", { BuildId: modelHouse.SellBuildingId });
                        var modelArea = X_AREA['_' + mBuy_Items.HouseRegionArea];
                        var modelSignUser = getRemoteObject("/RBAC/Services/RBAC_Users.asmx/Query", { id: mBuy_Items.SignUserId });
                        modelSignUser.DepartName = X_DEPARTS['_' + mBuy_Items.SignDepartId];

                        var repFeeTypes = getRemoteRows("/CM/Services/CM_Buy_FeeTypes.asmx/GetLists", {});
                        var repFollow = getRemoteRows("/CM/Services/CM_Buy_Follows.asmx/GetLists", { CMId: mBuy_Items.CMId });

                        var repSteps = getRemoteRows("/CM/Services/CM_Buy_Steps.asmx/GetLists", {});

                        var html = template('CM_Js_Buy_Items', {
                            modelHouse: modelHouse,
                            modelCMItem: mBuy_Items,
                            modelBuilding: modelBuilding,
                            modelArea: modelArea,
                            modelSignUser: modelSignUser,
                            repFeeTypes: repFeeTypes,
                            repFollow: repFollow,
                            repSteps: repSteps
                        });
                        $win.html(html);

                        var _str = "";
                        $(".FeeTypeSubjects").each(function () {
                            if (_str != "") _str += ",";
                            _str += $(this).attr("FISubjectId");
                        });
                        $("#linkFIVouchers").attr("arg1", _str.trimdot());

                        loadTotalFee();
                        $("a[name=reloadFeeTotal]", $win).click(function () {
                            loadTotalFee();
                        });
                        if (!IsOrderFollowForView) {
                            $("a[name=linkStepsLogAdds]").click(function () {
                                var $shouldStep = $(this).parent().parent();
                                var win_step = "/CM/Js/Buy_Steps_Logs_Add";
                                var obj = Ext.create(win_step.replaceAll("/", ".").trim("."));
                                obj.show(0, CMId, $shouldStep);
                            });
                            $("a[name=linkStepsLogAdd]", winDom).remove();
                        }
                            

                        //获取以进行的操作
                        $.ajax({
                            url: "/CM/Services/CM_Buy_Steps_Logs.asmx/GetLists",
                            data: { CMId: CMId },
                            dataType: 'json',
                            headers: { 'Content-Type': 'application/json;utf-8' },
                            success: function (data) {
                                var rows = loadFilter(data).rows;
                                $.map(rows, function (v) {
                                    //在ulCmSteps里看可能找到它
                                    var $tempcontrol = $("#ulCmSteps li[StepId=" + v.StepId + "] .State");
                                    var _color;
                                    var _operate;
                                    if (v.State == 2) {  //跳过
                                        _color = "red";
                                        _operate = 1;
                                    }
                                    else if (v.State == 1) { //完成
                                        _color = "blue"; _operate = 1;
                                    }
                                    else { //异常
                                        _color = "#666"; _operate = 0;
                                    }
                                    $tempcontrol.css("color", _color);
                                    $tempcontrol.parent().attr("Operate", _operate);
                                    $tempcontrol.text(CM_STEPS_STATE_ARRAY[v.State]);
                                    $("#ulCmSteps li[StepId=" + v.StepId + "] .LogDescription").text(v.AddUser + " " + formatTimeBase(v.AddTime, "yy/MM/dd") + " " + v.Description).attr("title", v.Description);
                                });
                                if ($("#ulCmSteps li[operate=0]").length == 0) {
                                    $("a[name=linkStepsLogAdd]", winDom).remove();
                                    $("a[name=linkStepsLogAdds]", winDom).remove();
                                }
                            }
                        });
                        function loadTotalFee() {
                            $.get("/FI/Services/FI_Vouchers.asmx/QueryTotalAmount", {
                                SubjectIds: _str,
                                RelatedIndex: CMId
                            }, function (t) {
                                $("label[name=lblTotalFee]", $win.dom).text(parseFloat($(t).find("decimal").text()).toFixed(1));
                            });
                        }
                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    constructor: function (config) {
        this.initConfig(config);
    }
});