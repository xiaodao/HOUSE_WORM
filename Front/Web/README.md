﻿# 项目说明
-----
## AutoPublicHouse
自动公盘的功能，要配置到系统的计划任务里去。
 
## cefsimple
webkit精简浏览器

## Client
C#编写的客户端，调用webkit。
使用方法：
修改Client的app.config文件，编译不同的客户系统。
```
<?xml version="1.0"?>
<configuration>
  <appSettings>
    <add key="url" value="http://xf.cnfangchong.com" />
    <add key="title" value="系统名称"/>
    <add key="encode" value="utf-8" />
  </appSettings>
  <startup><supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.0"/></startup>
  <runtime>
    <legacyCorruptedStateExceptionsPolicy enabled="true|false"/>
  </runtime>
</configuration>
```
编译生成的/bin/release目录程序使用WinRar打包成客户端安装包即可使用。

# 其它说明
F73A4FE6E34213A4012846AD7064AF54  谢厂节家里序列号
A1CF955B1E026DFF8612FE3CC2C9DBE1 谢厂节HP序列号

## xmpp strophejs plugin:
https://github.com/strophe/strophejs-plugins  

## xmpp jquery plugin:
https://github.com/maxpowel/jQuery-XMPP-plugin
# xmpp 密码：
信息员：csharp_2	4be55208
经理：	csharp_5 	05cbd3eb