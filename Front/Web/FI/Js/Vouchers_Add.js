﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("FI.Js.Vouchers_Add", {
    $win: {},
    show: function (RelatedIndex, SubjectId, FeeTypeId) { //RelatedIndex,row.data.FISubjectId,row.data.FeeTypeId
        var winDom;
        var _title;
        var _module = this;
        var win = Ext.create('Ext.window.Window', {
            title: '录入凭证', height: 300, width: 654, modal: true,
            loader: {
                url: '/FI/Templates/Vouchers_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        _module.$win = winDom;
                        renderUserControls(winDom);

                        $.ajax({
                            type: 'get',
                            url: '/FI/Services/FI_Subjects.asmx/Query',
                            data: { id: SubjectId },
                            dataType: "json",
                            async: false,
                            contentType: "application/json;utf-8",
                            success: function (data) {
                                var obj = data.d;
                                if (!obj) return;
                                $("input[name=SubjectTitle]", winDom).val(obj.SubjectTitle);
                                $("input[name=SubjectCode]", winDom).val(obj.SubjectCode);
                                $("input[name=SubjectId]", winDom).val(obj.SubjectId);
                                $("input[name=Direction]", winDom).val(obj.Direction);
                                $("input[name=RelatedIndex]", winDom).val(RelatedIndex);
                            }
                        });
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        $.messager.show({ title: '操作提示', msg: "请检查凭证内容。" });
                        return;
                    }
                    if (!$("input[name=AccountOfficer]", winDom).val()) {
                        $.messager.show({ title: '操作提示', msg: "请选择会计主管。" });
                        return;
                    }
                    if (!$("input[name=Cashier]", winDom).val()) {
                        $.messager.show({ title: '操作提示', msg: "请选择出纳人员。" });
                        return;
                    }
                    $.messager.confirm('此操作无法撤销', '确认新增收费凭证吗？', function () {
                        $.post("/FI/Services/FI_Vouchers.asmx/Add", $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('FI.Js.Vouchers_List');
                                    if ($("label[name=lblTotalFee]").length) {
                                        var fee = parseFloat($("label[name=lblTotalFee]").text());
                                        $("label[name=lblTotalFee]").text(fee + parseFloat( $("input[name=Amount]",winDom).val()));
                                    }
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                    })
                    //if () {
                        
                    //}
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});