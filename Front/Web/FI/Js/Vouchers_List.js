﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("FI.Js.Vouchers_List", {
    $win: {},
    isSingleSelect: false,
    SelectedArray: [],
    Grid: {},
    constructor: function (config) {
        this.initConfig(config);
    },
    show: function (RelatedIndex, SubjectIds) {
        var _callbackid = arg0; //返回到那个id里去
        this.isSingleSelect = (arg1 == "true");
        var winDom;
        var buttons = [];
        var _module = this;
        buttons = buttons.concat([
               {
                   text: "关闭",
                   handler: function () {
                       this.up("window").close();
                   }
               }
        ]);

        var win = Ext.create('Ext.window.Window', {
            title: '查看凭证', height: 500, width: 754, modal: true,
            loader: {
                url: '/FI/Templates/Vouchers_List.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        _module.$win = winDom;
                        _module.renderList(Ext.get(winDom.find('.divMainList')[0]).parent(), SubjectIds, RelatedIndex);
                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    renderList: function (view, SubjectIds, RelatedIndex) {
        var _total = 0;
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            fields: ['VoucherId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: "/FI/Services/FI_Vouchers.asmx/GetLists?" + $.param({ SubjectIds: "'" + SubjectIds + "'", RelatedIndex: RelatedIndex }),
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'VoucherId'
            },
            listeners: {
                load: function (t, records, successful, eOpts) {
                    $("#FI_Vouchers_List_Total", view.dom).text(_total);
                    
                    _total = 0;
                }
            }
        });
        var Columns = easyui2extgrid([[
            { field: 'VoucherId', title: '编号', width: 40 },
            { field: 'VoucherCode', title: '凭证编号', width: 100, align: 'center' },
            { field: 'Amount', title: '金额', width: 50, align: 'center', formatter: formatAmount },
            { field: 'AccountOfficer', title: '会计主管', width: 50, align: 'center' },
            { field: 'CashierUser', width: 50, title: '出纳' },
            { field: 'AddTime', width: X_WIDTH.Time, title: '日期', align: 'center', formatter: formatShortTime },
            { field: 'Description', title: '摘要', width: 350, align: 'center' }
        ]]);
        
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: 400,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });
        function formatAmount(v, r, k) {
            _total += v;
            return v;
        }
        function formatOperate(v, r, k) {
            return "<a href='javascript:;' >**</a>";
        }
    }
});