﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* RBAC.Js.RBAC_Module.addTab(0, 'Announcement/Templates/Items_List.htm', '公告', 'Announcement.Js.Items_List');
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Announcement.Js.Items_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    grid:null,
    renderList: function (view, padding_top,isFromDesk) {
        var _module = this;
        if (!padding_top) padding_top = 0;
        var VIEW_HEIGHT = view.parent().getHeight();
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['AnnoucementId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/Announcement/Services/Announcement_Items.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });

        var Columns = easyui2extgrid([[
                    { field: 'AnnoucementId', title: '编号', width: 40 },
                    { field: 'AnnoucementTitle', title: '标题', width: X_WIDTH.Title, align: 'center' },
                    { field: 'AddDepartId', title: '发布部门', width: X_WIDTH.Depart, align: 'center', formatter: formatDepart },
                    { field: 'AddUser', title: '发布人', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'AddTime', title: '发布时间', width: X_WIDTH.Time, align: 'center', formatter: formatShortTime }
        ]]);
        var selectRowData = {};

        var tbl = view.query(".mainList");
        var sm = new Ext.selection.CheckboxModel({ checkOnly: false, mode: this.isSingleSelect ? "SINGLE" : 'SIMPLE' });
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: VIEW_HEIGHT - X_TAB_HEIGHT - padding_top+30,
            store: Store, width: '100%', forceFit: true,
            columns: Columns, selModel:isFromDesk?null: sm,
            listeners: {
                rowclick: function (grid, row) {
                    var Items_View = Ext.create("Announcement.Js.Items_View");
                    Items_View.show(row.data.AnnoucementId);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });
        _module.grid = grid;
        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });

        $("a[name=linkNew]", view.dom).click(function () {
            var Items_Add = Ext.create("Announcement.Js.Items_Add");
            Items_Add.show(0);
        });
        $("a[name=linkEdit]", view.dom).click(function () {
            if (selectRowData) {
                var Items_Add = Ext.create("Announcement.Js.Items_Add");
                Items_Add.show(selectRowData.AnnoucementId);
            }
        });
        $("a[name=linkDel]", view.dom).click(function () {
            var grid = _module.grid;
            var selected = grid.getView().getSelectionModel().getSelection();
            var _ids = "";
            Ext.each(selected, function (item) {
                if (_ids != "") _ids += ",";
                _ids += item.data.AnnoucementId;
            });
            if (!_ids) {
                if(selectRowData) _ids = selectRowData.AnnoucementId;
            }
            if (_ids) {
                $.ajax({
                    url: "Announcement/Services/Announcement_Items.asmx/Delete",
                    data: { id: _ids },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "删除操作成功" });
                                RBAC.Js.RBAC_Module.render('Announcement.Js.Items_List');
                                break;
                            case "Exists":
                                $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                break;
                        }
                    }
                });
            }
        });

    }
});