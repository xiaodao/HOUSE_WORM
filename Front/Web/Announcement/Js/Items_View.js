﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Announcement.Js.Items_View", {
    show: function (AnnoucementId) {
        var winDom;
        var title = "公告";
        var mItems = getRemoteObject("/Announcement/Services/Announcement_Items.asmx/Query", { id: AnnoucementId });

        if (!mItems) {
            $.messager.show("操作提示", title + "编号不存在");
            return;
        }

        if (AnnoucementId > 0)
            title = "查看" + title;
        else
            title = "新增" + title;

        var buttons = [];
        if (X_USER == mItems.AddUser) {
            if (mItems.ViewLevel > 0) {
                buttons.push({
                    text: '转私盘',
                    handler: function (btn) {
                        $.get("Announcement/Services/AnnouncementItems.asmx/Add", { AnnoucementId: AnnoucementId, Follow: '转私盘', FollowHouseState: 0, ViewLevel: 2, ChgHouseViewLevel: 0, SuggestState: 0 }, function (r) {
                            switch ($(r).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                    btn.setText("转公盘").disable();;
                                    break;
                                case "TimeOut":
                                    break;
                                case "NoPermiss":
                                    $.messager.show({ title: '操作提示', msg: "没有权限。" });
                                    break;
                                default:
                                    break;
                            }
                        });
                    }
                });
            }
        }
        buttons = buttons.concat([
               {
                   text: "关闭",
                   handler: function () {
                       this.up("window").close();
                   }
               }
        ]);

        var win = Ext.create('Ext.window.Window', {
            title: title, height: 500, width: 900, modal: true,
            loader: {
                url: '/Announcement/Templates/Items_View.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = obj.dom;

                        var _tempId = win.getId();
                        mItems.TempId = _tempId;
                        if (mItems.AddTime) {
                            mItems.AddTimeStr = mItems.AddTime.toDate().format("yyyy年MM月dd日");
                        }
                        mItems.AddDepart = X_DEPARTS['_' + mItems.AddDepartId].DepartName;
                        var $win = $(".x-autocontainer-innerCt", "#" + _tempId);

                        var html = template('Announcement_Items_View', mItems);
                        $win.html(html);
                        $("div[name=body]", winDom).html(mItems.AnnoucementBody);

                        $.ajax({
                            type: "get",
                            dataType: "json",
                            async: false,
                            contentType: "application/json;utf-8",
                            url: "/Announcement/Services/Announcement_Items_Views.asmx/GetLists",
                            data: { ItemId: AnnoucementId },
                            success: function (data) {
                                var rows = loadFilter(data).rows;
                                $.map(rows, function (v) {
                                    $("div[name=viewUsers]",winDom).append("<label>{0}[{1}]</label>".format(v.AddUser, v.AddTime.toDate().format("yy/MM/dd hh:mm")));
                                });
                            }
                        });
                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    constructor: function (config) {
        this.initConfig(config);
    }
});