﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Announcement.Js.Items_Add", {
    show: function (AnnoucementId) {
        var winDom, editor;
        var _title;
        if (AnnoucementId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '公告', height: 450, width: 700, modal: true,
            loader: {
                url: '/Announcement/Templates/Items_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=AnnoucementId]", winDom).val(AnnoucementId);
                        renderUserControls(winDom);
                        if (!editor)
                            editor = KindEditor.create('#AnnoucementBody', {
                                allowFileManager: true,
                                width: 695,
                                height: 330,
                                uploadJson: '/Services/upload_json.ashx',
                                fileManagerJson: '/Services/file_manager_json.ashx',
                            });
                        if (AnnoucementId) {
                            $("form[name=fmAdd]", winDom).fillForm({
                                "url": "Announcement/Services/Announcement_Items.asmx/Query",
                                data: { id: AnnoucementId },
                                filter: function (obj) {
                                    editor.html(obj.AnnoucementBody);
                                    return obj;
                                }
                            });
                        }
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    editor.sync();
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/Announcement/Services/Announcement_Items.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('Announcement.Js.Items_List');
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});

//(function () {
//    var id = arg0;
//    var editor;
//    LoadTemplate("/Announcement", id > 0 ? '修改公告' : '新增公告', 'Items_Add.aspx?AnnoucementId=' + id, 700, 450, function load($dialog) {
//        renderUserControls();
//        $("input[name=AnnoucementId]", "#Announcement-Items_Add").val(id);
//        if (!editor)
//            editor = KindEditor.create('#AnnoucementBody', {
//                allowFileManager: true,
//                width: 683,
//                height: 330,
//                uploadJson: '/Services/upload_json.ashx',
//                fileManagerJson: '/Services/file_manager_json.ashx',
//            });

//        $dialog.dialog({
//            buttons: [{
//                text: '保存',
//                handler: function () {
//                    editor.sync();
//                    $.ajax({
//                        type: 'post',
//                        url: '/Announcement/Services/Announcement_Items.asmx/Add',
//                        data: $("form[name=fm]", "#Announcement-Items_Add").serializeArray(),
//                        success: function (data) {
//                            switch ($(data).find("ServiceStateKeys").text()) {
//                                case "Success":
//                                    $.messager.show({ title: '操作提示', msg: "保存操作成功" });
//                                    KindEditor.remove('#AnnoucementBody');
//                                    //editor.remove();
//                                    $dialog.dialog('close');
//                                    //$dg.datagrid('reload');
//                                    ///把页面上的公告也更新
//                                    loadLastAnnounce();
//                                    break;
//                                case "Exists":
//                                    $.messager.show({ title: '操作提示', msg: "公告已经存在" });
//                                    break;
//                                case "NoPermiss":
//                                    $.messager.show({ title: '操作提示', msg: "没有权限！" });
//                                    return;
//                                default:
//                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
//                                    break;
//                            }
//                        }
//                    });
//                }
//            }, {
//                text: '取消',
//                handler: function () {
//                    KindEditor.remove('#AnnoucementBody');
//                    //editor.remove();
//                    $dialog.dialog("close");
//                }
//            }]
//        });
//    });
//})();
