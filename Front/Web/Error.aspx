﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Error.aspx.cs" Inherits="Error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>出错提示</title>
    <link href="Css/main.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server" style="padding-top:200px;">
        <div class="divNav" style="text-align:center" >
            <%#information %>
        </div>
        <div class="commonNav" style="margin:20px auto;border:0px;text-align:center">
            2秒后自动跳转，<a href="<%#go %>?go=<%#refer %>">您也可以点击这里跳转。</a>

        </div>
    </form>
    <script type="text/javascript">
        setTimeout(function () {
            location.href = "<%#go%>?go=<%#refer%>";
        }, 2000);

    </script>
</body>
</html>
