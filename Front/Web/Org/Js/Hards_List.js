﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Org.Js.Hards_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['HardId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/Org/Services/Org_Hards.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = easyui2extgrid([[
                    { field: 'DepartId', title: '部门', width: X_WIDTH.Depart, formatter: formatDepart, align: 'center', sortable: true },
                    { field: 'HardStr', title: '硬件', width: X_WIDTH.HardStr, align: 'center', sortable: true },
                    { field: 'HardState', title: '状态', width: X_WIDTH.State, align: 'center', formatter: formatState, styler: styleBool, sortable: true },
                    { field: 'LastLoginUser', title: '最后登陆', width: X_WIDTH.UserName, align: 'center', formatter: function (v, r, k) { return v; }, sortable: true },
                    { field: 'LastLoginTime', title: '最后时间', width: X_WIDTH.Time, align: 'center', formatter: formatShortTime, sortable: true },
                    { field: 'LastLoginIp', title: '最后IP', width: X_WIDTH.Ip, align: 'center', formatter: function (v, r, k) { return v; }, sortable: true },
                    { field: 'AddUser', title: '添加人', width: X_WIDTH.UserName, align: 'center', sortable: true },
                    { field: 'AddTime', title: '添加时间', width: X_WIDTH.Time, formatter: formatShortTime, align: 'center', sortable: true },
                    { field: 'Mem', title: '备注', width: 100, align: 'center', sortable: true }
                ]]);
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Hards_View = Ext.create("Org.Js.Hards_Add");
                    Hards_View.show(row.data.HardId);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Hards_Add = Ext.create("Org.Js.Hards_Add");
            Hards_Add.show(0);
        });
        $("a[name=linkDel]", view.dom).click(function () {
            if (selectRowData) {
                $.ajax({
                    url: "Org/Services/Org_Hards.asmx/Delete",
                    data: { id: selectRowData.HardId },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                RBAC.Js.RBAC_Module.render('Org.Js.Hards_List');
                                break;
                            case "Exists":
                                $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                break;
                        }
                    }
                });
            }
        });
    }
});