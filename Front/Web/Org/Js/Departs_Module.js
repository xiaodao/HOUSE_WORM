﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.ext.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/**
* 
*/
Ext.define("Org.Js.Departs_Module", {
    Rows: [],
    Tree: [],    //树状目录
    singleton: true,
    refreshTree: function () {
        var depart_rows = this.Rows = getRemoteRows('Org/Services/Org_Departs.asmx/GetLists');
        if (!this.Rows.length) {
            return null;
        };
        this.Tree = getTree();
        function getTree() {
            var _tree = [];
            for (var _j = 0; _j < depart_rows.length; _j++) {
                var _temp = depart_rows[_j];
                if (_temp.ParentId == 0) {
                    _tree.push({
                        'id': 'DepartId' + _temp.DepartId,
                        'DepartId': _temp.DepartId,
                        'DepartName': _temp.DepartName,
                        'text': _temp.DepartName,
                        'Telphone': _temp.Telphone,
                        'IsDelete': _temp.IsDelete,
                        'IsVirtual': _temp.IsVirtual, leaf: false,
                        iconCls: 'monitor', expanded: true,
                        'children': getChildren(_temp)
                    });
                    //break;
                }
            }
            return { rows: _tree, total: _tree.length };
        }
        function getChildren(node) {
            var _children = [];
            for (var _j = 0; _j < depart_rows.length; _j++) {
                if (depart_rows[_j].ParentId == node.DepartId) {
                    _children.push({
                        'id': 'DepartId' + depart_rows[_j].DepartId,
                        'DepartId': depart_rows[_j].DepartId,
                        'DepartName': depart_rows[_j].DepartName,
                        'text': depart_rows[_j].DepartName,
                        'Telphone': depart_rows[_j].Telphone,
                        'IsDelete': depart_rows[_j].IsDelete,
                        'IsVirtual': depart_rows[_j].IsVirtual,
                        leaf: false, iconCls: 'monitor', expanded: true,
                        'children': getChildren(depart_rows[_j])
                    });
                }
            }
            return _children;
        }
        function getChildrenUser(node) {
            var _children = [];
            for (var i = 0; i < depart_users.length; i++) {
                if (_user[i].departid == node.departid) {

                    _children.push({
                        'id': 'userid' + depart_users[i].userid,
                        'username': depart_users[i].username,
                        'text': depart_users[i].username,
                        'departid': depart_users[i].departid, leaf: false, iconcls: 'monitor',
                        'children': getChildrenUser(depart_users[i])
                    });
                }
            }

        }
    },
    constructor: function (config) {
        this.refreshTree();
        this.initConfig(config);
    }
});