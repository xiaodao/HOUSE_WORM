﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Org.Js.Users_Add", {
    show: function (UserId) {
        var winDom;
        var _title;
        if (UserId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '人事档案', height: 390, width: 550, modal: true,
            loader: {
                url: '/Org/Templates/Users_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=UserId]", winDom).val(UserId);
                        
                        if (UserId) {
                            $("form[name=fmAdd]", winDom).fillForm({
                                url: "/Org/Services/Org_Users.asmx/Query",
                                data: { id: UserId },
                                filter: function (obj) {
                                    console.log(obj);
                                },
                                after: function () {
                                    $("img[name=imgThumb]", winDom).attr("src", this.params.Thumb);
                                    $("input[name=UserName]", winDom).prop("readonly", true);
                                }
                            });
                        }
                        renderUserControls(winDom);
                        //头像上传
                        var uploadbutton = KindEditor.uploadbutton({
                            button: KindEditor('#Org_Users_Add_Thumb')[0],
                            fieldName: 'imgFile',
                            url: '/Services/upload_json.ashx',
                            afterUpload: function (data) {
                                if (data.error === 0) {
                                    var url = KindEditor.formatUrl(data.url, 'absolute');
                                    $('#Org_Users_Add_urlThumb').val(url);
                                    $("img[name=imgThumb]", winDom).attr("src", url);
                                } else {
                                    //alert(data.message);
                                }
                            },
                            afterError: function (str) {
                                //alert('自定义错误信息: ' + str);
                            }
                        });
                        uploadbutton.fileBox.change(function (e) {
                            uploadbutton.submit();
                        });//onChange
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/Org/Services/Org_Users.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "操作成功" });
                                    RBAC.Js.RBAC_Module.render("Org.Js.Users_List");
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});
