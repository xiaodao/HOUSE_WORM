﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.require('Org.Js.Departs_Module');

Ext.define("Org.Js.Departs_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Columns =
            [
                {
                    xtype: 'treecolumn', //this is so we know which column will show the tree
                    text: '部门',
                    width: X_WIDTH.Depart * 4,
                    sortable: true,
                    dataIndex: 'DepartName',
                    locked: true
                },
                { text: '电话', field: 'Telphone', width: X_WIDTH.Tel },
                { text: '状态', field: 'IsDelete', align: 'center', renderer: formatIsDelete, styler: styleIsDelete, editor: { type: 'checkbox', options: { on: true, off: false } } },
                { text: '是否虚拟', field: 'IsVirtual', align: 'center', renderer: formatBool, styler: styleBool, editor: { type: 'checkbox', options: { on: true, off: false } } }
            ];

        var tbl = view.query(".mainList");
        var tree = Org.Js.Departs_Module.Tree;
        var grid = new Ext.tree.TreePanel({
            height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            collapsible: true, rootVisible: false, width: '100%', forceFit: true,
            columns: Columns, expanded: true, lines: true,
            root: {
                expanded:true,text:'部门',
                children: tree.rows[0].children
            },
            listeners: {
                itemclick: function (grid, row) {
                    var Departs_Add = Ext.create("Org.Js.Departs_Add");
                    Departs_Add.show(row.data.DepartId);
                }
            },
            renderTo: tbl //,
            //store: Ext.create('Ext.data.Store', {
            //    children: tree.rows[0].children
            //})
            //, loader: new Ext.tree.TreeLoader({
                
            //})
        });

        $("a[name=linkNew]", view.dom).click(function () {
            var Departs_Add = Ext.create("Org.Js.Departs_Add");
            Departs_Add.show(0);
        });
        
    }
});