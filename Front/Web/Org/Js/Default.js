﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 
*/

Ext.require('.RBAC.Js.RBAC_Module');

Fingerprint2.getV18({}, function (finger, components) {
    finger = hex_md5(finger);

    var HardStr = siteTracker.q.HardStr;
    $("input[name=HardStr]").val(HardStr);
    var d = (new Date()).getTime();
    var max = 99999; var min = 11111;
    var rand = Math.floor(Math.random() * (max - min + 1) + min);
    var source = finger + ";" + HardStr + ";" + d + ";" + rand;
    var CheckStr = hex_md5(source);


    $("input[name=CheckStr]").val(CheckStr);
    $("input[name=Timestamp]").val(d);
    $("input[name=Rand]").val(rand);
    $("form[name=formLogin]").submit(function () {
        if (!$("form[name=formLogin]").form('validate')) {
            $.messager.show({ title: '操作提示', msg: "请检查输入格式是否正确。" });
            return false;
        }
        var source = $("input[name=UserPass]").val();
        var md5 = hex_md5(source);
        $("input[name=UserPass]").val(md5);
        $("#btn").prop("disabled", true);

        $.post("/RBAC/Services/RBAC_Users.asmx/CheckLogin", $("form[name=formLogin]").serializeArray(), function (r) {
            switch ($(r).find("HardStr").text()) {
                case "Success":
                    $.messager.show({ title: '操作提示', msg: "操作成功，现在进入安装界面。" });
                    setTimeout(function () {
                        if (!siteTracker.q.HardStr) {
                            $.messager.show({ msg: "没有获取到硬件标识参数。" });
                        }
                        else { location.href = "Hards_Install.htm?HardStr={0}&Finger={1}".format(siteTracker.q.HardStr,finger); }
                    }, 1500);
                    break;
                case "UserErr":
                    $.messager.show({ title: '操作提示', msg: "用户名或密码错误。" });
                    break;
                case "NoPermiss":
                    $.messager.show({ title: '操作提示', msg: "没有安装权限。" });
                    break;
                case "TimeOut":
                    break;
                default:
                    $.messager.show({ title: '操作提示', msg: "服务端错误。" });
                    break;
            }
            $("input[name=UserPass]").val(source);
            $("#btn").prop("disabled", false);
        });
        return false;
    });
});
