﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Org.Js.Hards_Add", {
    show: function (HardId) {
        var winDom;
        var _title;
        if (HardId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '终端', height: 200, width: 300, modal: true,
            loader: {
                url: '/Org/Templates/Hards_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=HardId]", winDom).val(HardId);
                        
                        if (HardId) {
                            $("form[name=fmAdd]", winDom).fillForm({
                                "url": "Org/Services/Org_Hards.asmx/Query",
                                data: { id: HardId },
                                filter: function (obj) {
                                    obj.HardsId=obj.HardId;
                                    return obj;
                                }
                            });
                        }
                        renderUserControls(winDom);
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/Org/Services/Org_Hards.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('Org.Js.Hards_List');
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});