﻿/// <reference path="/script/jsUtil.js"/> 
/// <reference path="/script/main.js"/> 
/**
* 
*/
var departRows = getRemoteRows("/Org/Services/Org_Departs.asmx/GetLists");
$("input[name=HardStr]").val(siteTracker.q.HardStr);
$("input[name=CheckStr]").val(siteTracker.q.CheckStr);
$.each(departRows, function (k, item) {
    if (item.ParentId != 0)
        $("#divStores").append("<div style='float:left;width:150px;text-align:left'><label><input type='radio' class='need' name='DepartId' value='{1}' />{0}</label></div>".format(item.DepartName, item.DepartId));
});
$("form[name=fm]").submit(function () {
    $("#btn").prop("disabled", true);
    if (!$("form[name=fm]").form('validate')) {
        $.messager.show({ title: '操作提示', msg: "请检查输入格式是否正确。" });
        $("#btn").prop("disabled", false);
        return false;
    }
    if (!$("input[name=DepartId]:checked").val()) {
        $.messager.show({ title: '操作提示', msg: "请选择门店。" });
        $("#btn").prop("disabled", false);
        return false;
    }
    Fingerprint2.getV18({}, function (finger, components) {
        finger = hex_md5(finger);
        $("input[name=Finger]").val(finger);
        $.post("/Org/Services/Org_Hards.asmx/Add", $("form[name=fm]").serializeArray(), function (r) {
            switch ($(r).find("ServiceStateKeys").text()) {
                case "Success":
                    $.messager.show({ title: '操作提示', msg: "操作成功，3秒后自动转向登陆。" });
                    location.href = "/Login.htm?HardStr=" + siteTracker.q.HardStr;
                    break;
                case "TimeOut":
                    $.messager.show({ title: '操作提示', msg: "登陆超时，请重新登陆并尝试安装系统。" });
                    location.href = "/Org/Default.aspx?t=" + Math.random() + "&HardStr=" + siteTracker.q.HardStr + "&CheckStr=" + siteTracker.q.CheckStr;
                    break;
                case "Exists":
                    $.messager.show({ title: '操作提示', msg: "此机器已经注册，并允许登陆系统，3秒后自动转向登陆系统。" });
                    location.href = "/Login.htm?HardStr=" + siteTracker.q.HardStr;
                    break;
                case "NoPermiss":
                    $.messager.show({ title: '操作提示', msg: "此机器被禁止登陆，可到系统后台解除禁止。机器编号：" + siteTracker.q.HardStr });
                    break;
                default:
                    break;
            }
        });
    });
   
    return false;
});