﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Org.Js.Users_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['UserId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/Org/Services/Org_Users.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = easyui2extgrid([[
                    { field: 'UserId', title: '人员编号', width: 40 },
                    { field: 'UserName', title: '姓名', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'UserSex', title: '性别', width: X_WIDTH.Sex, align: 'center', formatter: formatSex },
                    { field: 'State', title: '状态', width: 50, align: 'center', formatter: function (v, r, k) { return X_STAFFSTATE[v]; }, sortable: true },
                    { field: 'UserMobile', title: '电话', width: X_WIDTH.Tel, align: 'center', formatter: function (v, r, k) { return v; } },
                    { field: 'EnterDeate', title: '入职日期', width: 140, align: 'center', formatter: formatShortTime },
                    { field: 'UserState', title: '帐号状态', width: 50, align: 'center', formatter: function (v, r, k) { return X_USERSTATE[v]; }, sortable: true },
                    { field: 'DepartId', title: '部门', width: X_WIDTH.Depart, align: 'center', formatter: formatDepart }

        ]]);
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Users_Add = Ext.create("Org.Js.Users_Add");
                    Users_Add.show(row.data.UserId);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Users_Add = Ext.create("Org.Js.Users_Add");
            Users_Add.show(0);
        });
        $("a[name=linkDel]", view.dom).click(function () {
            if (selectRowData) {
                $.ajax({
                    url: "Org/Services/Org_Users.asmx/Delete",
                    data: { UserId: selectRowData.UserId },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                RBAC.Js.RBAC_Module.render('Org.Js.Users_List');
                                break;
                            case "Exists":
                                $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                break;
                        }
                    }
                });
            }
        });
    }
});