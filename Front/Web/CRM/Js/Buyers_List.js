﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CRM.Js.Buyers_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['CustomerId'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/CRM/Services/CRM_Buyers.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'LastTime'
            }
        });
        var Columns = easyui2extgrid([[
                    { field: 'CustomerId', title: '编号', width: 40 },
                    { field: 'CustomerName', title: '姓名', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'CRMType', title: '业务类型', width: X_WIDTH.UserName, align: 'center', formatter: formatCRMType },
                    { field: 'Sex', title: '性别', width: X_WIDTH.Sex, align: 'center', formatter: formatSex },
                    { field: 'State', title: '状态', width: 50, align: 'center', styler: styleHouseState, formatter: function (v, r, k) { return X_USERSTATE[v]; } },
                    { field: 'WantRegionArea', title: '期望区域', width: 60, align: 'center', formatter: formatMultiRegionArea },
                    { field: 'WantBuildings', title: '期望小区', width: 140, align: 'center' },
                    { field: 'WantHouseAreaBegin', title: '期望面积', width: 50, align: 'center', formatter: formatHouseArea },
                    { field: 'WantFloor', title: '期望楼层', width: 60, align: 'center' },
                    { field: 'WantUnitPriceBegin', title: '期望单价', width: 80, align: 'center', formatter: formatUnitPrice },
                    { field: 'WantTotalPriceBegin', title: '总价', width: 80, align: 'center', formatter: formatTotalPrice },
                    { field: 'WantDecoration', title: '装修', width: 50, align: 'center', formatter: formatDecoration },
                    { field: 'AddDepartId', title: '登记部门', width: X_WIDTH.Depart, align: 'center', formatter: formatDepart },
                    { field: 'AddUser', title: '登记人', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'AddTime', title: '登记时间', width: 110, align: 'center', formatter: formatShortTime },
                    { field: 'ManageUser', title: '分配给', width: X_WIDTH.UserName, align: 'center' }
        ]]);
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Buyers_View = Ext.create("CRM.Js.Buyers_View");
                    Buyers_View.show(row.data.CustomerId);
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });

        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var Buyers_Add = Ext.create("CRM.Js.Buyers_Add");
            Buyers_Add.show(0);
        });

        function formatHouseArea(v, r, k) {
            return v + "-" + r.WantHouseAreaEnd;
        }
        function formatUnitPrice(v, r, k) {
            return v + "-" + r.WantUnitPriceEnd;
        }
        function formatTotalPrice(v, r, k) {
            return v + "-" + r.WantTotalPriceEnd;
        }
        function formatMultiRegionArea(v, r, k) {
            if (!v) return "";

            var AreaArray = v.split(',');
            var result = "";
            for (var _i = 0; _i < AreaArray.length; _i++) {
                result += "," + X_AREA['_' + AreaArray[_i]].AreaName;
            }
            return result.trimdot();
        }
        function formatDecoration(v, r, k) {
            if (!v) return '';
            var DecorationArray = v.split(',');
            var result = "";
            for (var _i = 0; _i < DecorationArray.length; _i++) {
                result += "," + X_DECORATEKEYS[DecorationArray[_i]];
            }
            return result.trimdot();

        }
        function formatCRMType(v, r, k) {
            if (v)
                return getCrmTypes(v);
            else
                return "求购";
        }
        function getCrmTypes(ids) {
            var result = "";
            var idsArray = ids.split(',');

            for (var i = 0; i < idsArray.length; i++) {
                if (idsArray[i]) {
                    var k = parseInt(idsArray[i]);
                    result = result.ItemAdd(CRMTypeArray[k]);
                }
            }
            return result;
        }
    }
});