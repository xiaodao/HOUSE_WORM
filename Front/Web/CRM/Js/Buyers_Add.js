﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CRM.Js.Buyers_Add", {
    show: function (CustomerId) {
        var winDom;
        var _title;
        if (CustomerId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '客户', height: 390, width: 700, modal: true,
            loader: {
                url: '/CRM/Templates/Buyers_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=CustomerId]", winDom).val(CustomerId);

                        if (!id) {
                            $("input[name=CRMType][value='0']", winDom).prop("checked", true);
                            $("input[name=IsFirstBuy][value='true']", winDom).prop("checked", true);
                            $("input[name=WantUnitPriceBegin]", winDom).val("0");
                            $("input[name=WantUnitPriceEnd]", winDom).val("0");
                            $("input[name=WantTotalPriceBegin]", winDom).val("0");
                            $("input[name=WantTotalPriceEnd]", winDom).val("0");
                            $("input[name=WantShi]", winDom).val("0");
                            $("input[name=WantTing]", winDom).val("0");
                        }

                        $("input[name=WantBuildingIds]").prev().prev().attr("name", "WantBuildings");

                        renderUserControls(winDom);
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                   
                    var _CRMType = $("input[name=CRMType]:checked").val();

                    if (!$("input[name=WantUnitPrice]", winDom).val()) {
                        $("input[name=WantUnitPrice]", winDom).val('0');
                    }
                    if (!$("input[name=WantTotalPrice]", winDom).val()) {
                        $("input[name=WantTotalPrice]", winDom).val('0');
                    }
                    if (!$("input[name=WantHouseArea]", winDom).val()) {
                        $("input[name=WantHouseArea]", winDom).val('0');
                    }
                    if (!$("input[name=WantFloor]", winDom).val()) {
                        $("input[name=WantFloor]", winDom).val('0');
                    }
                    var paramobj = $("form[name=fmAdd]", winDom).serializeArray();
                    //if (_CRMType == "0" || _CRMType == "1") {


                    //} else {
                    if (!$("input[name=WantDecoration]", winDom).val()) {
                        setval('WantDecoration', '');
                    }
                    if (!$("input[name=WantRegionArea]", winDom).val()) {
                        setval('WantRegionArea', '');
                    }
                    if (!$("input[name=WantBuildingIds]", winDom).val()) {
                        setval('WantBuildingIds', '');
                    }
                    //setval('WantFloor', '');
                    //setval('WantUnitPriceBegin', '0');
                    //setval('WantUnitPriceEnd', '0');
                    //setval('WantTotalPriceBegin', '0');
                    //setval('WantTotalPriceEnd', '0');
                    //}
                    //if (_CRMType == '1') {
                    //    setval('WantUnitPriceBegin', '0');
                    //    setval('WantUnitPriceEnd', '0');
                    //}
                    if (!id) {
                        if (!$("form[name=fmAdd]", winDom).form('validate')) {
                            $.messager.show({ title: '操作提示', msg: "请检查输入格式是否正确。" });
                            return false;
                        }
                    }
                    function setval(name, value) {
                        var ishas = false;
                        for (var i = 0; i < paramobj.length; i++) {
                            if (paramobj[i].name == name) {
                                paramobj[i].value = value;
                                ishas = true;
                                break;
                            }
                        }
                        if (!ishas)
                            paramobj.push({ name: name, value: value });
                    }
                    $.ajax({
                        url: 'CRM/Services/CRM_Buyers.asmx/Add',
                        data: paramobj,
                        success: function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ title: '操作提示', msg: "保存操作成功" });

                                    RBAC.Js.RBAC_Module.render('CRM.Js.Buyers_List');
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "用户已经存在" });
                                    break;
                                default:
                                    $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                    break;
                            }
                        }
                    });

                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});