﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CRM.Js.Buyers_View", {
    show: function (CustomerId) {
        var winDom,win;
        var title = "客户";
        var mBuyers = getRemoteObject("/CRM/Services/CRM_Buyers.asmx/Query", { id: CustomerId });

        if (!mBuyers) {
            $.messager.show("操作提示", title + "编号不存在");
            return;
        }
       
        if (CustomerId > 0)
            title = "查看" + title;
        else
            title = "新增" + title;

        var buttons = [];

        buttons = buttons.concat([{
                text: '举报是中介',
                handler: function () {
                    popMenu('/Tools/Js/AgentMobiles_Add', 0, $("#lblBuyers_View_Tel").text().trim());
                }
            },
            {
                text: '跟进',
                handler: function () {
                    var Buyers_Follows_Add = Ext.create("CRM.Js.Buyers_Follows_Add");
                    Buyers_Follows_Add.show(0, CustomerId, this.up("window").el.dom);
                }
            },
            {
                text: '重分配',
                handler: function () {
                    var _win = this.up("window");
                    popMenu('/RBAC/Js/SelectUsers', 'Buyers_View_ManageUser');
                }
            },
            {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }
        ]);

        win = Ext.create('Ext.window.Window', {
            title: title, height: 450, width: 680, modal: true,
            loader: {
                url: '/CRM/Templates/Buyers_View.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = obj.dom;
                        var Region_Module = Ext.create("Region.Js.Region_Module");
                        mBuyers.Sex = X_CRM_SEX[mBuyers.Sex];
                        mBuyers.State = X_USERSTATE[mBuyers.State];
                        mBuyers.CRMType = CRMTypeArray[mBuyers.CRMType];
                        mBuyers.InformationComeFrom = InformationComeFrom[mBuyers.InformationComeFrom];
                        mBuyers.AddTime = formatDate(mBuyers.AddTime);
                        mBuyers.LastTime = formatDate(mBuyers.LastTime);
                        mBuyers.WantDecoration = formatDecorations(mBuyers.WantDecoration);
                        mBuyers.WantRegionArea = formatRegionAreas(mBuyers.WantRegionArea);
                        mBuyers.IsFirstBuy = mBuyers.IsFirstBuy?"是":"否";
                        var _tempId = win.getId();
                        mBuyers.TempId = _tempId;
                        var $win = $(".x-autocontainer-innerCt", "#" + _tempId);
                        var html = template('CRM_Js_Buyers_View', mBuyers);
                        $win.html(html);

                        $("#Buyers_View_ManageUserId",winDom).change(function () {
                            var _id = $(this).val();
                            $.get("/CRM/Services/CRM_Buyers.asmx/Distribute", { id: CustomerId, ManageUserId: _id }, function (data) {
                                switch ($(data).find("ServiceStateKeys").text()) {
                                    case "Success":
                                        $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                        //var _win = Ext.getCmp(_tempId);
                                        //_win.close();
                                        break;
                                    case "NotExists":
                                        $.messager.show({ title: '操作提示', msg: "可能客户或用户不存在！" });
                                        break;
                                    default:
                                        $.messager.show({ title: '操作提示', msg: "保存操作失败" });
                                        break;
                                }
                            });
                        });
                        //加载跟进
                        $.ajax({
                            type: "get",
                            url: "/CRM/Services/CRM_Buyers_Follows.asmx/GetLists",
                            dataType: 'json',
                            data: { page: 1, rows: 200, CustomerId: CustomerId },
                            contentType: "application/json;utf-8",
                            success: function (data) {
                                var rows = loadFilter(data).rows;
                                if (rows) {
                                    for (var i = 0; i < rows.length; i++) {
                                        var row = rows[i];
                                        if (!row) continue;
                                        var _str = "<li>【{0}】[{1}][{3}]{2}</li>".format(row.AddUser,
                                         (row.AddTime.toDate()).format("yy-MM-dd hh:mm"),
                                         row.Follow,
                                         X_CUSTOMERSTATE[row.FollowCustomerState]);
                                        $(".ulBuyerFollow", $win).append(_str);
                                    }
                                }
                            }
                        });

                    }
                }
            },
            buttons: buttons,
            listeners: {
                close: function () {
                }
            }
        }).show();

    },
    constructor: function (config) {
        this.initConfig(config);
    }
});
