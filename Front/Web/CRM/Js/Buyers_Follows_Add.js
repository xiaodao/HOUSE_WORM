﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("CRM.Js.Buyers_Follows_Add", {
    show: function (FollowId, CustomerId, ParentWin) {
        var winDom;
        var win = Ext.create('Ext.window.Window', {
            title: '跟进客户信息', height: 225, width: 285, modal: true,
            loader: {
                url: '/CRM/Templates/Buyers_Follows_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=FollowId]", winDom).val(FollowId);
                        $("input[name=CustomerId]", winDom).val(CustomerId);
                        renderUserControls(winDom);
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/CRM/Services/CRM_Buyers_Follows.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    if ($(".ulBuyerFollow", ParentWin).length > 0) {
                                        var _str = "<li>【{0}】[{1}][{3}]{2}</li>".format(X_USER, (new Date()).format("yy-MM-dd hh:mm"), $("textarea[name=follow]", winDom).val(),
                                        X_CUSTOMERSTATE[$("input[name='FollowCustomerState']:checked", winDom).val()]);
                                        $(".ulBuyerFollow", ParentWin).prepend(_str);
                                    }
                                    $.messager.show({ title: "操作提示", msg: "操作成功！" });
                                    _win.close();
                                    RBAC.Js.RBAC_Module.render("CRM.Js.Buyers_List");
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                case "NoPermiss":
                                    $.messager.show({ title: '操作提示', msg: "没有操作权限" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});