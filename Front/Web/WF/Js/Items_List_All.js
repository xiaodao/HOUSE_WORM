﻿/**
我的请求
*/
(function a(n) {
    //按钮链接
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    var $linkDel = $("a[name=linkDel]", "#" + n);
    //表
    var $dg = $("table[name=dg]", "#" + n);
    //枚举

    $btnSearch.click(function () { render(); });

    render();
    //函数
    function render() {
        var param = { action: "'All'" };
        WF_DATAGRID_OBJ.url = "WF/Services/WF_Items.asmx/GetLists?" + jQuery.param(param);
        WF_DATAGRID_OBJ.toolbar = $("div[name=toolbar]", "#" + n);
        WF_DATAGRID_OBJ.onClickRow = function (rowIndex, rowData) {
            popMenu('/WF/Js/WorkFlow_View', rowData.ItemId, rowData.FlowId);
        };
        $dg.datagrid(WF_DATAGRID_OBJ);
    }
    window[n] = { render: render };
})('WF-Items_List_All');