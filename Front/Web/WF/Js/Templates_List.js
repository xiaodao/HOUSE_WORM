﻿var WF_TypesArray = [];
(function a(n) {
    //按钮链接
    var $linkNew = $("a[name=linkNew]", "#" + n);
    var $linkEdit = $("a[name=linkEdit]", "#" + n);
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    var $linkDel = $("a[name=linkDel]", "#" + n);
    //表
    var $dg = $("table[name=dg]", "#" + n);
    //枚举
    var enumUrl = {//s服务 w//窗体
        sDel: "WF/Services/WF_Templates.asmx/Delete",
        sList: 'WF/Services/WF_Templates.asmx/GetLists'
    };
    //事件
    $linkNew.click(function () {
        popMenu('/WF/Js/Templates_Add', 0);
    });
    $linkEdit.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            popMenu('/WF/Js/Templates_Add', row.TemplateId);
        }
    });
    $btnSearch.click(function () { render(); });
    $.ajax({
        type: "get",
        url: "/WF/Services/WF_Types.asmx/GetLists",
        dataType: 'json',
        data: { page: 1 },
        async: false,
        contentType: "application/json;utf-8",
        success: function (data) {
            var rows = loadFilter(data).rows;
            WF_TypesArray = rows;
        }
    });
    function loadWFTypeName(id) {
        var TypeName = "";
        for (var j = 0; j < WF_TypesArray.length; j++) {
            if (WF_TypesArray[j].TypeId == id) {
                TypeName = WF_TypesArray[j].Name;
                break;
            }
        }
        return TypeName;
    }
    render();
    //函数
    function render() {
        var param = $("form[name=fm]", "#WF-Templates_List").serializeArray();
        $dg.datagrid({
            url: enumUrl.sList + "?" + jQuery.param(param),
            method: 'GET',
            idFiled: 'TemplateId',
            loadMsg: '正在加载，请稍候……',
            pagination: true,
            rownumbers: true,
            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
            pageSize: 20,
            toolbar: $("div[name=toolbar]", "#" + n),
            singleSelect: true,
            columns: [[
                    { field: 'TemplateId', title: '编号', width: 40 },
                    {
                        field: 'TypeId', title: '类别', width: 140, formatter: function (c, o, r) {
                            return loadWFTypeName(c);
                        }
                    },
                    { field: 'Title', title: '名称', width: 140 }
            ]],
            loadFilter: function (data) {
                return loadFilter(data);
            },
            onLoadSuccess: function (data) {
                if (data.rows.length) {
                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                }
                else {
                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                }
            }
        });
    }
    window[n] = { render: render };
})('WF-Templates_List');