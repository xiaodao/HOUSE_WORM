﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 查看流程
*/
//(function () {
var FlowId = arg1;
var ItemId = arg0;
var WFItem = {};
var WorkFlow = {};
var Template = {};
var GraphJsonObj = {};//params对象，要对全局可见
var mode = "Verify";
if (ItemId) {
    $.ajax({
        type: "get",
        url: "/WF/Services/WF_Items.asmx/Query",
        dataType: 'json',
        data: { id: ItemId },
        contentType: "application/json;utf-8",
        success: function (data) {
            WFItem = data.d;
            if (WFItem) {
                var hasSvg = true;// hasSVG();
                LoadTemplate("/WF", "查看流程", "WorkFlow_View",854, 610, function load($dialog) {
                    //如果是查看流程，不要显示按钮
                    if (!FlowId) {
                        mode = "View";
                    }
                    //graph
                    var property = {
                        width: 805,
                        height: 470,
                        haveHead: false,
                        haveTool: false,
                        haveGroup: true,
                        initLabelText: '查看流程'
                    };
                    if (hasSvg)
                        graph = $.createGooFlow($("#graph"), property);

                    if (WFItem.State) {
                        $("span#spanState").text(X_WFSTATE[WFItem.State]);
                    }
                    else {
                        $("span#NowToUsers").text("当前位置：" + WFItem.NowToUsers);
                    }
                    if (WFItem.NowWorkFlowId)
                        WorkFlow = getRemoteObject("/WF/Services/WF_WorkFlows.asmx/Query", { id: WFItem.NowWorkFlowId });
                    Template = getRemoteObject("/WF/Services/WF_Templates.asmx/Query", { id: WFItem.TemplateId });
                   
                    //显示graph流程图
                    if (Template.GraphJson && hasSvg) {
                        GraphJsonObj = $.parseJSON(Template.GraphJson);
                        graph.loadData(GraphJsonObj);
                    }
                    var ParamsObj = $.par2Json(WFItem.Params);

                    Template.Form=Template.Form.replaceAll('\n','');
                    $("#divFormDesignerVisual").html(Template.Form);
                    var mergeObj = $.extend({}, ParamsObj, WFItem);
                    $("#divFormDesignerVisual").fillForm({ params: mergeObj });
                    $("#divFormDesignerVisual input").prop("readonly", true);
                    $("#divFormDesignerVisual textarea").prop("readonly", true);
                    $("#divFormDesignerVisual *:radio").prop("disabled", true);
                    //加载处理流程
                    renderWorkFlow();
                    if (mode == "Verify") {
                        $dialog.dialog({
                            buttons: [
                                {
                                    text: '通过',
                                    handler: function () {
                                        $.post("/WF/Services/WF_Items.asmx/Verify", { ItemId: ItemId, Suggest: $("input[name=Suggest]", "#WF-WorkFlow_View").val(), State: 1 }, function (r) {
                                            switch ($(r).find("ServiceStateKeys").text()) {
                                                case "Success":
                                                    $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                                    callPageRender('WF-WorkFlow_List_Need_My');
                                                    $dialog.dialog("close");
                                                    break;
                                                case "TimeOut":
                                                    break;
                                                default:
                                                    break;
                                            }
                                        });
                                    }
                                },
                                {
                                    text: '驳回',
                                    handler: function () {
                                        $.post("/WF/Services/WF_Items.asmx/Verify", { ItemId: ItemId, Suggest: $("input[name=Suggest]", "#WF-WorkFlow_View").val(), State: 4 }, function (r) {
                                            switch ($(r).find("ServiceStateKeys").text()) {
                                                case "Success":
                                                    $.messager.show({ title: '操作提示', msg: "操作成功，流程被驳回。" });
                                                    callPageRender('WF-WorkFlow_List_Need_My');
                                                    $dialog.dialog("close");
                                                    break;
                                                case "TimeOut":
                                                    break;
                                                default:
                                                    break;
                                            }
                                        });
                                    }
                                }, {
                                    text: '关闭',
                                    handler: function () {
                                        $dialog.dialog("close");
                                    }
                                }]
                        });
                    } else {
                        $("#divSuggest").hide();
                        $dialog.dialog({
                            buttons: [
                                   {
                                       text: '关闭',
                                       handler: function () {
                                           $dialog.dialog("close");
                                       }
                                   }]
                        });
                    }
                    function renderWorkFlow() {
                        var param = { ItemId: WFItem.ItemId, action: "'All'" };
                        var $dg = $("table[name=dgWorkFlow]", "#WF-WorkFlow_View");
                        $dg.datagrid({
                            url: "/WF/Services/WF_WorkFlows.asmx/GetLists" + "?" + jQuery.param(param),
                            title: '',
                            method: 'GET',
                            idFiled: 'FlowId',
                            loadMsg: '正在加载，请稍候……',
                            pagination: true,
                            rownumbers: true,
                            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
                            pageSize: 20,
                            toolbar: $("div[name=toolbar]", "#WF-WorkFlow_View"),
                            singleSelect: true,
                            columns: [[
                            { field: 'Title', title: '名称', width: 80 },
                            { field: 'NodeName', title: '节点名称', width: 80 },
                            { field: 'FlowAddTime', title: '到达时间', width: 80, formatter: formatShortTime },
                            { field: 'State', title: '流程状态', align: 'center', width: 80, styler: styleHouseState, formatter: formatWFState },
                            { field: 'Suggest', title: '审批意见', align: 'center', width: 50 },
                            { field: 'NowToUsers', title: '当前位置', align: 'center', width: X_WIDTH.UserName * 2 },
                            { field: 'VerifyTime', title: '审批时间', align: 'center', width: X_WIDTH.Time, formatter: formatShortTime },
                            { field: 'VerifyUser', title: '审批人', align: 'center', width: X_WIDTH.UserName },
                            ]],
                            loadFilter: function (data) {
                                return loadFilter(data);
                            },
                            onLoadSuccess: function (data) {
                                if (data.rows.length) {
                                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                                }
                                else {
                                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                                }
                            }
                        });
                    }
                }, function afterRender() {
                    //$("#divFormDesignerVisual").html(decodeURIComponent(WFItem.Form));
                    //$("#divFormDesignerVisual input").prop("readonly", true);
                });
            }
        }, async: false
    });
}
//})();