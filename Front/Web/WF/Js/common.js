﻿var WF_DATAGRID_OBJ = {
    url: '',
    method: 'GET',
    idFiled: 'ItemId',
    loadMsg: '正在加载，请稍候……',
    pagination: true,
    rownumbers: true,
    pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
    pageSize: 20,
    singleSelect: true,
    columns: [[
            { field: 'ItemId', title: '编号', width: 40 },
            { field: 'Name', title: '类别', align: 'center', width: 50 },
            { field: 'Title', title: '流程名称', align: 'center', width: 100 },
             { field: 'AddTime', title: '添加时间', width: 100, formatter: formatShortTime, align: 'center' },
             { field: 'AddUser', title: '申请人', width: 100, align: 'center' },
             { field: 'State', title: '状态', width: 100, align: 'center', styler: styleWFState, formatter: formatWFState },
             { field: 'NowToUsers', title: '当前位置', width: X_WIDTH.UserName*3, align: 'center'},
             { field: 'LastUser', title: '最后操作', width: X_WIDTH.UserName, align: 'center' },
             { field: 'LastTime', title: '最后时间', width: X_WIDTH.Time, formatter: formatShortTime, align: 'center' }
    ]],
    loadFilter: function (data) {
        return loadFilter(data);
    },
    onLoadSuccess: function (data) {
        if (data.rows.length) {
            $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
        }
        else {
            $.messager.show({ title: '操作提示', msg: '没有数据!' });
        }
    }
}
