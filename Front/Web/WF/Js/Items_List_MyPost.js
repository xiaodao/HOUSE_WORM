﻿/**
我的请求
*/
(function a(n) {
    //按钮链接
    var $linkNew = $("a[name=linkNew]", "#" + n);
    var $linkEdit = $("a[name=linkEdit]", "#" + n);
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    var $linkDel = $("a[name=linkDel]", "#" + n);
    //表
    var $dg = $("table[name=dg]", "#" + n);

    $btnSearch.click(function () { render(); });
    //事件
    $linkNew.click(function () {
        popMenu('/WF/Js/Items_Add_Step1', 0);
    });
    $linkEdit.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            popMenu('/WF/Js/Items_Add_Step2', {
                TemplateId: row.TemplateId,
                TemplateName: row.Title,
                ItemId: row.ItemId
            });
        }
    });
    render();
    //函数
    function render() {
        var param = { action: "'MyPost'" };
        WF_DATAGRID_OBJ.url = "WF/Services/WF_Items.asmx/GetLists?" + jQuery.param(param);
        WF_DATAGRID_OBJ.toolbar = $("div[name=toolbar]", "#" + n);
        WF_DATAGRID_OBJ.onClickRow = function (rowIndex, rowData) {
            popMenu('/WF/Js/WorkFlow_View', rowData.ItemId, rowData.FlowId);
        };
        $dg.datagrid(WF_DATAGRID_OBJ);
    }
    window[n] = { render: render };
})('WF-Items_List_MyPost');