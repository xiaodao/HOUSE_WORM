﻿(function () {
    var Template = {
        TemplateId: arg0.TemplateId,
        Title: arg0.TemplateName,
        Form: "",
        GraphJson: "",
        TypeId: 0,
        Params: "",
        ItemId: arg0.ItemId
    };
    var WFItem = { Form: "" };
    var graph;
    LoadTemplate("/WF", "填写流程单:" + Template.Title, "Items_Add_Step2.aspx?ItemId=" + Template.ItemId, 700, 580,
        function load($dialog) {
            //graph
            var property = {
                width: 670,
                height: 415,
                haveHead: false,
                haveTool: false,
                haveGroup: true,
                initLabelText: '查看流程'
            };
            graph = $.createGooFlow($("#graph"), property);
            if (Template.ItemId) {
                $.ajax({
                    type: "get",
                    url: "/WF/Services/WF_Items.asmx/Query",
                    dataType: 'json',
                    data: { id: Template.ItemId },
                    contentType: "application/json;utf-8",
                    success: function (data) {
                        WFItem = data.d;
                        WFItem.Form = decodeURIComponent(WFItem.Form);
                    }, async: false
                });
            }
            $.ajax({
                type: "get",
                url: "/WF/Services/WF_Templates.asmx/Query",
                dataType: 'json',
                data: { id: Template.TemplateId },
                contentType: "application/json;utf-8",
                success: function (data) {
                    Template.GraphJson = data.d.GraphJson;
                    Template.Form = data.d.Form;
                    Template.Title = data.d.Title;
                    Template.TypeId = data.d.TypeId;
                }, async: false
            });
            if (!Template.ItemId) {
                WFItem.Form = Template.Form;
                WFItem.GraphJson = Template.GraphJson;
                WFItem.ItemId = Template.ItemId;
                WFItem.TemplateId = Template.TemplateId;
            }
            //显示graph流程图
            if (Template.GraphJson)
                graph.loadData($.parseJSON(Template.GraphJson));
            //上传文件
            var uploadbutton = KindEditor.uploadbutton({
                button: KindEditor('#uploadButton')[0],
                fieldName: 'imgFile',
                url: '/SystemSet/upload_json.ashx?dir=file',
                afterUpload: function (data) {
                    if (data.error === 0) {
                        var url = KindEditor.formatUrl(data.url, 'absolute');
                        WFItem.FilePath = url;
                    } else {
                        //alert(data.message);
                    }
                },
                afterError: function (str) {
                    //alert('自定义错误信息: ' + str);
                }
            });
            uploadbutton.fileBox.change(function (e) {
                uploadbutton.submit();
            });

            $dialog.dialog({
                buttons: [
                    {
                        text: '提交',
                        handler: function () {
                            WFItem.Params = $("form[name=formDesignerVisual]", "#WF-Items_Add_Step2").serialize();
                            var source = $("#divFormDesignerVisual", "#WF-Items_Add_Step2").formhtml();
                            WFItem.Form = encodeURIComponent(source);
                            if (!WFItem.FilePath) {
                                WFItem.FilePath = "";
                            }
                            if (!$("form[name=formDesignerVisual]", "#WF-Items_Add_Step2").form('validate')) {
                                $.messager.show({ title: '操作提示', msg: "请检查输入格式是否正确。" });
                            } else {
                                $.post("/WF/Services/WF_Items.asmx/Add", WFItem, function (r) {
                                    switch ($(r).find("ServiceStateKeys").text()) {
                                        case "Success":
                                            $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                            callPageRender('WF-Items_List_MyPost');
                                            callPageRender('WF-Items_List_Need_My');
                                            callPageRender('WF-Items_List_All');
                                            $dialog.dialog("close");
                                            break;
                                        case "HardErr":
                                            $.messager.show({ title: '操作提示', msg: "流程模版异常，无法启动工作流程！" });
                                            break;
                                        case "TimeOut":
                                            break;
                                        default:
                                            break;
                                    }
                                });
                            }
                        }
                    }, {
                        text: '关闭',
                        handler: function () {
                            $dialog.dialog("close");
                        }
                    }]
            });
        },
        function afterRender() {             //显示Form表单
            renderFormUI();
            function renderFormUI() {
                $("#divFormDesignerVisual").html(replaceJsTemplate(WFItem.Form));
                //renderUserControls($("#divFormDesignerVisual"));
                $.parser.parse($("#divFormDesignerVisual"));
            }
        });
})();