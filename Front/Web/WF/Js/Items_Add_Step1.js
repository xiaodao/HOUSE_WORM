﻿(function () {
    var id = arg0;
    LoadTemplate("/WF", "新建流程", "Items_Add_Step1.aspx?ItemId=" + id, 654, 550, function load($dialog) {
        //$("select[name=selType]", "#WF-Items_Add_Step1").change(function () {
        //    $("select[name=selTemplateId]", "#WF-Items_Add_Step1").empty();
        //    $("select[name=selTemplateId]", "#WF-Items_Add_Step1").addOption("选择流程", "");
        //    var typeid = $(this).val();
           
        //});

        $.ajax({
            type: "get",
            url: "/WF/Services/WF_Templates.asmx/GetLists",
            dataType: 'json',
            data: { page: 1, rows: 10000, TypeId: 0 },
            contentType: "application/json;utf-8",
            success: function (data) {
                var rows = loadFilter(data).rows;
                $.map(rows, function (item) {
                    $("#ul_" + item.TypeId, "#WF-Items_Add_Step1").append(
                        ' <a href="javascript:;" class="linkSelTemplate" style="float:left;width:100px;height:22px;line-height:22px;overflow:hidden;display:inline-block" templateid="{1}">{0}</a> '.format(item.Title, item.TemplateId));
                });
                $(".linkSelTemplate").click(function () {
                    var obj = {
                        TemplateId: $(this).attr("templateid"),
                        TemplateName: $(this).text().trim(),
                        ItemId:0
                    };
                    console.log(obj);
                    popMenu('/WF/Js/Items_Add_Step2', obj);
                    $dialog.dialog("close");
                });

            }
        });
        

        $dialog.dialog({
            buttons: [
                {
                    text: '关闭',
                    handler: function () {
                        $dialog.dialog("close");
                    }
                }]
        });
    });
})();