﻿/*WF 流程使用的，为了在工作台正确调用，把它们从common.js里移到这里
*/
function formatWFState(v, r, k) {
    return X_WFSTATE[v];
}
/*工作台使用的房源状态*/

function styleWFState(v, r, k) {
    switch (v) { //{ "正常", "停售","已售", "删除" }
        case 0:
            return 'background-color:#fff;color:#000';
            break;
        case 1:
            return 'background-color:#fff;color:red';
            break;
        case 2:
            return 'background-color:#fff;color:red';
            break;
        case 3:
            return 'background-color:#fff;color:blue';
            break;
        case 4:
            return 'background-color:#fff;color:red';
            break;
    }
}
$(document).ready(function () {
    var pworkflow = $('<div><table name="dgWorkFlow"></table></div>').appendTo('#Portal-Default');
    pworkflow.panel({
        title: '流程信息',
        height: 286,
        collapsible: true
    });
    var dgWorkFlow = $("table[name=dgWorkFlow]", "#Portal-Default");
    var param = { ItemId: 0, action: "'NeedMyVerifyAndNotPass'" };
    var WF_DATAGRID_OBJ = {
        url: '',
        method: 'GET',
        idFiled: 'ItemId',
        loadMsg: '正在加载，请稍候……', pagination: true,
        queryParams: { page: 1, rows: 10 },
        pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
        pageSize: 10,
        singleSelect: true,
        columns: [[
                { field: 'Name', title: '类别', align: 'center', width: 50 },
                { field: 'Title', title: '流程名称', align: 'center', width: 70 },
                 { field: 'AddTime', title: '提交时间', width: 80, formatter: formatShortTime, align: 'center' },
                 { field: 'AddUser', title: '申请人', width: 55, align: 'center' },
                 { field: 'State', title: '状态', width: 40, align: 'center', styler: styleWFState, formatter: formatWFState },
                 { field: 'NowToUsers', title: '当前位置', width: 55, align: 'center' },
                { field: 'AddDepartId', title: '部门', align: 'center', width: X_WIDTH.Depart, formatter: formatDepart }
        ]],
        loadFilter: function (data) {
            return loadFilter(data);
        }
    }

    WF_DATAGRID_OBJ.url = "/WF/Services/WF_WorkFlows.asmx/GetLists?" + jQuery.param(param);
    WF_DATAGRID_OBJ.onClickRow = function (rowIndex, rowData) {
        popMenu('/WF/Js/WorkFlow_View', rowData.ItemId, rowData.FlowId);
    }
    $('#content').portal('add', {
        panel: pworkflow,
        columnIndex: 1
    });
    dgWorkFlow.datagrid(WF_DATAGRID_OBJ);
});