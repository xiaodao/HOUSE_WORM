﻿/**
待办事宜
*/
(function a(n) {
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    //表
    var $dg = $("table[name=dg]", "#" + n);
    $("select[name=TypeId]", "#" + n).xBindData({
        "url": '/WF/Services/WF_Types.asmx/GetLists',
        "text": "Name",
        "value": "TypeId"
    });
    $("select[name=TypeId]", "#" + n).change(function () {
        var _typeid = $(this).val();
        if (!_typeid) return;
        $("select[name=Title]").xBindData({
            "url": '/WF/Services/WF_Templates.asmx/GetLists',
            "data": { page: 1, rows: 35535, TypeId: _typeid },
            "text": "Title",
            "value": "TemplateId"
        });
    });
    $btnSearch.click(function () { render(); });
    render();
    //函数
    function render() {
        var param = {
            ItemId: 0, action: "'NeedMyVerify'", TypeId: $("select[name=TypeId]", "#" + n).val()
        , Title: $("select[name=Title]", "#" + n).val()
        };
        WF_DATAGRID_OBJ.url = "WF/Services/WF_WorkFlows.asmx/GetLists?" + jQuery.param(param);
        WF_DATAGRID_OBJ.toolbar = $("div[name=toolbar]", "#" + n);
        WF_DATAGRID_OBJ.onClickRow = function (rowIndex, rowData) {
            popMenu('/WF/Js/WorkFlow_View', rowData.ItemId, rowData.FlowId);
        }
        $dg.datagrid(WF_DATAGRID_OBJ);
    }
    window[n] = { render: render };
    function formatWFState(v, r, k) {
        return X_WFSTATE[v];
    }
})('WF-WorkFlow_List_Need_My');