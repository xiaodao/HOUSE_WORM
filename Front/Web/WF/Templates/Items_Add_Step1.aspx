﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Items_Add_Step1.aspx.cs" Inherits="WF_Templates_Items_Add" %>

<form name="fm" method="post" novalidate>
    <div style="float: left; width: 635px">
        <asp:repeater runat="server" id="repTypes">
            <ItemTemplate>
                <fieldset id="ul_<%#DataBinder.Eval(Container.DataItem,"TypeId") %>">
                    <legend>
                       <%#DataBinder.Eval(Container.DataItem,"Name") %>
                    </legend>
                </fieldset>   
            </ItemTemplate>
        </asp:repeater>
    </div>
</form>
