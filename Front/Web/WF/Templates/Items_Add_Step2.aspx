﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Items_Add_Step2.aspx.cs" Inherits="WF_Templates_Items_Add_Template" %>

<div class="easyui-tabs" style="width: 680px; height: 485px;">
    <div title="填写表单" style="padding: 0px;">
        <div id="divFormDesign" class="easyui-layout" style="width: 100%; height:450px;" >
            <div data-options="region:'center',title:''">
                <form name="formDesignerVisual">
                    <div id="divFormDesignerVisual"></div>
                </form>
            </div>
            <div data-options="region:'south',title:''" style="overflow:hidden" >
               <input type="button" id="uploadButton" value="上传附件" />
            </div>
        </div>
    </div>
    <div title="查看流程" style="overflow: auto; padding: 0px;">
        <div class="easyui-layout" style="width: 100%; height: 450px;">
            <div data-options="region:'center',title:'设计视图'" style="background: #eee;" id="Design">
                <div id="graph"></div>
            </div>
        </div>
    </div>
</div>
