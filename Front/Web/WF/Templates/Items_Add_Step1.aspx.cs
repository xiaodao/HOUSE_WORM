﻿using System;
using System.Data;
using Webapp;

public partial class WF_Templates_Items_Add : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        xundh.API.WF.WF_Types serviceTypes = new xundh.API.WF.WF_Types();

        int ItemId = StringUtil.GetIntValue(Request["ItemId"]);
        using (DataTable dt = serviceTypes.GetLists().Table) {
            repTypes.DataSource = dt;
            repTypes.DataBind();
        }
      
    }
}