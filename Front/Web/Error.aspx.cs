﻿using System;
using Webapp;

public partial class Error  : System.Web.UI.Page//不要继承BasePage
{
    public string go = "";
    public string information = "";
    public string refer = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        go = StringUtil.GetNullToString(Request["go"]);
        information = StringUtil.GetNullToString(Request["information"]);
        if (Request.UrlReferrer != null)
            refer = Request.UrlReferrer.PathAndQuery.ToString();
        DataBind();
    }
}