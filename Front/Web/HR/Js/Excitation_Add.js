﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 激励添加
*/
(function () {
    var id = arg0;
    LoadTemplate("/HR", "开激励", "Excitation_Add", 354, 260, function load($dialog) {
        if (id) {
            $("form[name=fmAdd]", "#HR-Excitation_Add").fillForm({
                "url": "/HR/Services/HR_Excitation.asmx/Query",
                "data": { id: id },
                filter: function (obj) {
                    obj.wf_depart = obj.ObjectDepartId;
                    obj.UserId = obj.ObjectUserId;
                    obj.wf_time = obj.ObjectTime;
                    obj.wf_address = obj.ObjectAddress;
                    obj.wf_matter = obj.Matter;
                    obj.wf_type = obj.Type;
                    obj.wf_amount = obj.Amount;
                    return obj;
                },
                after: function (obj) {
                    $("select[name=wf_depart]", "#HR-Excitation_Add").val(obj.ObjectDepartId);
                    $("select[name=wf_depart]", "#HR-Excitation_Add").attr("value1", obj.ObjectDepartId).attr("userid",obj.UserId);
                    //$("select[name=UserId]", "#HR-Excitation_Add").attr("value1", obj.UserId);
                }
            });
        }
        $dialog.dialog({
            buttons: [
               {
                   text: '确定',
                   handler: function () {
                       $.post("/HR/Services/HR_Excitation.asmx/Add", $("form[name=fmAdd]", "#HR-Excitation_Add").serializeArray(), function (r) {
                           switch ($(r).find("ServiceStateKeys").text()) {
                               case "Success":
                                   $.messager.show({ title: '操作提示', msg: "操作成功。" });
                                   callPageRender('HR_ExcitationList');
                                   $dialog.dialog("close");
                                   break;
                               case "TimeOut":
                                   break;
                               default:
                                   break;
                           }
                       });
                   }
               }, {
                   text: '关闭',
                   handler: function () {
                       $dialog.dialog("close");
                   }
               }]
        });
    });
})();