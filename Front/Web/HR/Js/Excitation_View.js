﻿/// <reference path="/script/jsUtil.js"/> 
/// <reference path="/script/main.js"/> 
/**
* 查看激励详情
*/
(function () {
    var id = arg0;
    LoadTemplate("/HR", "查看激励", "Excitation_View", 354, 260, function load($dialog) {
        if (id) {
            $("form[name=fmView]", "#HR-Excitation_View").fillForm({
                'url': '/HR/Services/HR_Excitation.asmx/Query',
                'data': { id: id },
                filter: function (obj) {
                    return obj;
                }
            });
        }
        $dialog.dialog({
            buttons: [ {
                    text: '关闭',
                    handler: function () {
                        $dialog.dialog("close");
                    }
                }]
        });
    });
})();