﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/**
* 激励列表
*/
(function a(n) {
    var States = ['待处理', '通过', '取消'];
    //按钮链接
    var $linkNew = $("a[name=linkNew]", "#" + n);
    var $linkEdit = $("a[name=linkEdit]", "#" + n);
    var $btnSearch = $("input[name=btnSearch]", "#" + n);
    //表
    var $dg = $("table[name=dg]", "#" + n);
    //枚举
    var enumUrl = {//s服务 w//窗体
        sDel: "HR/Services/HR_Excitation.asmx/Delete",
        sList: 'HR/Services/HR_Excitation.asmx/GetLists'
    };
    //事件
    $linkNew.click(function () {
        popMenu('/HR/Js/Excitation_Add', 0);
    });
    $linkEdit.click(function () {
        var row = $dg.datagrid('getSelected');
        if (row) {
            popMenu('/HR/Js/Excitation_Add', row.ExcitationId);
        }
    });
    $btnSearch.click(function () { render(); });
   
    render();
    window["HR_ExcitationList"] = { render: render };
    //函数
    function render() {
        var param = $("form[name=fm]", "#HR-Excitation_List").serialize();
        $dg.datagrid({
            url: enumUrl.sList + "?" + (param),
            title: '',
            method: 'GET',
            idFiled: 'ExcitationId',
            loadMsg: '正在加载，请稍候……',
            pagination: true,
            rownumbers: true,
            pageNumber: 1, headers: { 'Content-Type': 'application/json;utf-8' },
            pageSize: 20,
            toolbar: $("div[name=toolbar]", "#" + n),
            singleSelect: true,
            columns: [[
                    { field: 'ExcitationId', title: '编号', width: 40 },
                    { field: 'ObjectUser', title: '激励对象', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'ObjectDepartId', title: '激励部门', width: X_WIDTH.Depart, align: 'center', formatter: formatDepart },
                    { field: 'AddUser', title: '开激励人', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'ObjectTime', title: '事件时间', width: X_WIDTH.Time, align: 'center', formatter: formatTime },
                    { field: 'Matter', title: '事件', width: X_WIDTH.Title, align: 'left' },
                    { field: 'Type', title: '类型', width: X_WIDTH.UserName, align: 'center', formatter: function (v) { return v == 0 ? "正激励" : "负激励" } },
                    { field: 'Amount', title: '金额(元)', width: X_WIDTH.Int, align: 'center' },
                    { field: 'VerifyUser', title: '审核人', width: X_WIDTH.UserName, align: 'center' },
                    { field: 'State', title: '状态', width: X_WIDTH.State * 1.5, align: 'center', formatter: formatState }
            ]],
            loadFilter: function (data) {
                return loadFilter(data);
            },
            onLoadSuccess: function (data) {
                if (data.rows.length) {
                    $.messager.show({ title: '操作提示', msg: '加载 ' + data.rows.length + ' 条记录成功!' });
                }
                else {
                    $.messager.show({ title: '操作提示', msg: '没有数据!' });
                }
            },
            onClickRow: function (rowIndex, rowData) {
                popMenu('/HR/Js/Excitation_View', rowData.ExcitationId);
            }
        });
    }

    function formatState(v, r, k) {
        return States[v];
    }
})('HR-Excitation_List');