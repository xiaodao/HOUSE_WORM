﻿using System;

public partial class main_js : System.Web.UI.Page
{
    public xundh.Model.RBAC_Users U;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[EnumKeys.SessionKey.USER] == null)
        {
            U = new xundh.Model.RBAC_Users
            {
                UserId = 0,
                DepartId = 0,
                UserSex=1,
                PostId=0
            };
            DataBind();
            return;
        }
        U = (xundh.Model.RBAC_Users)Session[EnumKeys.SessionKey.USER];
        ///区域
        xundh.API.Region.Region_Area serviceArea = new xundh.API.Region.Region_Area();
        try
        {
            Webapp.JsonTable list = serviceArea.GetLists();
            if (list == null)
            {
                Response.Clear();
                Response.Write("document.write('序列号错误。');var X_USER='';var X_USEROBJ={DepartId:0}");
                DataBind();
                return;
            }
            else
            {
                repArea.DataSource = list.Table;
                ///部门
                xundh.API.Org.Org_Departs serviceDeparts = new xundh.API.Org.Org_Departs();
                repDeparts.DataSource = serviceDeparts.GetLists().Table;
                ///操作
                xundh.API.RBAC.RBAC_Operation serviceOperates = new xundh.API.RBAC.RBAC_Operation();
                repOpreate.DataSource = serviceOperates.GetLists().Table;
                //职位
                xundh.API.Org.Org_Posts servicePosts = new xundh.API.Org.Org_Posts();
                repPosts.DataSource = servicePosts.GetLists().Table;
                //用户权限
                xundh.API.RBAC.RBAC_Role_Resource_Operation op = new xundh.API.RBAC.RBAC_Role_Resource_Operation();
                repOperations.DataSource = op.GetUserOperations(U.UserId).Table;
                DataBind();
            }
        }
        catch (Exception e1)
        {
            Response.Clear();
            Response.Write( "document.write('序列号错误。<br />"+e1.ToString()+"');var X_USER='';var X_USEROBJ={}");
            Response.End();
        }
    }
}