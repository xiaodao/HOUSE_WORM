﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Tools.Js.MobileArea_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        $(".btn-primary[name=btnQuery]", view.dom).click(function () {
            var tel = $("input[name=tel]", view.dom).val();
            $.get("/Tools/Services/PhoneLocation.asmx/Query?phone=" + tel, function (t) {
                if ($(t).find("string").text())
                    $("label[name=result]", view.dom).text($(t).find("string").text());
                else
                    $("label[name=result]", view.dom).text("没有找到！");
            });
            return false;
        });
    }
});