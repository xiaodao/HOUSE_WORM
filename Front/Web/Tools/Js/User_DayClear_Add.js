﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Tools.Js.User_DayClear_Add", {
    show: function (DayId) {
        var winDom, editor;
        var _title;
        if (DayId) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '日清', height: 550, width: 700, modal: true,
            loader: {
                url: '/Tools/Templates/User_DayClear_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=DayId]", winDom).val(DayId);
                        renderUserControls(winDom);
                        if (!editor) {
                            editor = KindEditor.create('#DayContent', {
                                allowFileManager: true,
                                width: 695,
                                height: 330,
                                uploadJson: '/Services/upload_json.ashx',
                                fileManagerJson: '/Services/file_manager_json.ashx',
                            });

                            $("form[name=fmAdd]", winDom).fillForm({
                                "url": "Tools/Services/Tools_DayClear.asmx/QueryByPostId",
                                data: { postId: X_USEROBJ.UserPostId },
                                filter: function (obj1) {
                                    if (obj1) {
                                        editor.html(obj1.day_close);
                                    } else {
                                        $.messager.show({ title: '提示', msg: "模板还未完成，请示上级尽快完成！" });
                                    }

                                }
                            });
                        }
                        if (DayId) {
                            $("form[name=fmAdd]", winDom).fillForm({
                                "url": "Tools/Services/Tools_User_DayClear.asmx/Query",
                                data: { id: DayId },
                                filter: function (obj) {
                                    editor.html(obj.DayContent);
                                    if (obj.ApprovalUserId > 0) {
                                        var rowObj = getRemoteObject("/RBAC/Services/RBAC_Users.asmx/Query", { id: obj.ApprovalUserId });
                                        $("#ApprovalUserId", winDom).html(rowObj.UserName);
                                        $("#ApprovalTime", winDom).html(formatShortTime(obj.ApprovalTime));
                                        return obj;
                                    }
                                    else {
                                        $("#ApprovalUserId", winDom).html("");
                                        $("#ApprovalTime", winDom).html(formatShortTime(""));
                                    }
                                }
                            });
                        }
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    editor.sync();
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/Tools/Services/Tools_User_DayClear.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('Tools.Js.User_DayClear_List');
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});