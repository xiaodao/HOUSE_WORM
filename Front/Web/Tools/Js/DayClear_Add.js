﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Tools.Js.DayClear_Add", {
    show: function (id) {
        var winDom, editor;
        var _title;
        if (id) _title = '修改'; else _title = '新增';
        var win = Ext.create('Ext.window.Window', {
            title: _title + '模板', height: 450, width: 700, modal: true,
            loader: {
                url: '/Tools/Templates/DayClear_Add.htm', autoLoad: true, scripts: true,
                listeners: {
                    load: function (obj, eOpts) {
                        winDom = $("#" + obj.target.id);
                        $("input[name=id]", winDom).val(id);
                        renderUserControls(winDom);
                        if (!editor)
                            editor = KindEditor.create('#day_close', {
                                allowFileManager: true,
                                width: 695,
                                height: 330,
                                uploadJson: '/Services/upload_json.ashx',
                                fileManagerJson: '/Services/file_manager_json.ashx',
                            });
                        if (id) {
                            $("form[name=fmAdd]", winDom).fillForm({
                                "url": "Tools/Services/Tools_DayClear.asmx/Query",
                                data: { id: id },
                                filter: function (obj) {
                                    editor.html(obj.day_close);
                                    return obj;
                                }
                            });
                        }
                    }
                }
            },
            buttons: [
            {
                text: "确认",
                handler: function () {
                    var _win = this.up('window');
                    editor.sync();
                    //有效性验证
                    if (!$("form[name=fmAdd]", winDom).form("validate")) {
                        return;
                    }
                    $.post("/Tools/Services/Tools_DayClear.asmx/Add",
                        $("form[name=fmAdd]", winDom).serializeArray(), function (data) {
                            switch ($(data).find("ServiceStateKeys").text()) {
                                case "Success":
                                    $.messager.show({ msg: '操作成功' });
                                    RBAC.Js.RBAC_Module.render('Tools.Js.DayClear_List');
                                    _win.close();
                                    break;
                                case "Exists":
                                    $.messager.show({ title: '操作提示', msg: "已经存在" });
                                    break;
                                default:
                                    Ext.Msg.show({ title: "操作提示", message: "保存操作失败" });
                                    break;
                            }
                        });
                }
            }, {
                text: "关闭",
                handler: function () {
                    this.up("window").close();
                }
            }],
            listeners: {
                close: function () {
                }
            }
        }).show();
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});