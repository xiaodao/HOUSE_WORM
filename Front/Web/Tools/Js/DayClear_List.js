﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/// <reference path="/Js/app.js"/> 
/**
* 
*/
Ext.require('Region.Js.Region_Module');
Ext.define("Tools.Js.DayClear_List", {
    constructor: function (config) {
        this.initConfig(config);
    },
    renderList: function (view) {
        var Store = Ext.create('Ext.data.Store', { //store和model一起创建
            //storeId: 'simpsonsStore',
            fields: ['id'],
            pageSize: 30,
            autoLoad: true,
            proxy: {
                type: 'ajax',
                enablePaging: true,//支持翻页
                url: '/Tools/Services/Tools_DayClear.asmx/GetLists',
                pageParam: 'page',
                startParam: '{startParam}',
                limitParam: 'rows',
                headers: { "Content-Type": 'application/json' },
                reader: {
                    type: 'json',
                    rootProperty: 'd.Table.rows',
                    totalProperty: 'd.Total'
                },
                writer: {
                    type: 'json'
                },
                extraParams: $.par2Json($("form[name=fmList]", view.dom).serialize()),
                sortParam: 'postId'
            }
        });
        var Columns = easyui2extgrid([[
                    { field: 'id', title: '编号', width: 40 },
                    { field: 'postId', title: '职位', width: X_WIDTH.Depart, align: 'center', formatter: formatPost },
                    { field: 'dayclose', title: '日清模板', width: X_WIDTH.Depart, align: 'center', formatter: formatDayClose },
                    { field: 'AddUser', title: '发布人', width: X_WIDTH.Ip, align: 'center' },
                    { field: 'AddDepartId', title: '发布部门', width: X_WIDTH.Depart, align: 'center', formatter: formatDepart },
                    { field: 'AddTime', title: '发布时间', width: X_WIDTH.Time, align: 'center', formatter: formatShortTime }
                ]]);
        var selectRowData = {};
        var tbl = view.query(".mainList");
        var grid = Ext.create('Ext.grid.Panel', {
            scroll: true, height: deskExt.getHeight() - $(".auto", tbl.dom).height() - X_TAB_HEIGHT,
            store: Store, width: '100%', forceFit: true,
            columns: Columns,
            listeners: {
                rowclick: function (grid, row) {
                    var Users_View = Ext.create("Tools.Js.DayClear_Add");
                    Users_View.show(row.data.id);
                    selectRowData = row.data;
                }
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: Store,
                displayInfo: true,
                displayMsg: '显示 {0} - {1} 条，共计 {2} 条',
                emptyMsg: "没有数据"
            }),
            renderTo: tbl
        });
        //events
        $("form[name=fmList]", view.dom).submit(function () {
            grid.store.proxy.extraParams = $.par2Json($("form[name=fmList]", view.dom).serialize());
            grid.store.loadPage(1);
            grid.store.reload();
            return false;
        });
        $("a[name=linkNew]", view.dom).click(function () {
            var DayClear_Add = Ext.create("Tools.Js.DayClear_Add");
            DayClear_Add.show(0);
        });
        $("a[name=linkDel]", view.dom).click(function () {
            if (selectRowData) {
                $.ajax({
                    url: "Tools/Services/Tools_DayClear.asmx/Delete",
                    data: { id: selectRowData.id },
                    success: function (data) {
                        switch ($(data).find("ServiceStateKeys").text()) {
                            case "Success":
                                $.messager.show({ title: '操作提示', msg: "保存操作成功" });
                                RBAC.Js.RBAC_Module.render('Tools.Js.DayClear_List');
                                break;
                            case "Exists":
                                $.messager.show({ title: '操作提示', msg: "已经收藏过了。" });
                                break;
                            default:
                                $.messager.show({ title: '操作提示', msg: "删除操作失败" });
                                break;
                        }
                    }
                });
            }
        });
    }
});