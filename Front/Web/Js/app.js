﻿/// <reference path="/Js/jsUtil.js"/> 
/// <reference path="/Js/Main.js"/> 
/// <reference path="/Js/ext-5.0.0-gpl/build/ext-all.js"/> 
/**
* 
*/
var deskExt;
Ext.require('RBAC.Js.RBAC_Module');
Ext.require('Org.Js.Departs_Module');
Ext.application({
    name: 'xundhHouse',
    launch: function () {
        if (!X_USER) { location.href = '/Error.aspx?go=Login.htm&information=' + encodeURIComponent('登陆超时，请重新登陆。'); return; }
        var TOPHEIGHT = 49;
        var ALLHEIGHT = $(document).height();
        if (X_USER == '') { RBAC.Js.RBAC_Module.Timeout(); return; }
        Ext.Loader.setConfig({ enabled: true, paths: {} });
        ///取公司名称X_COP
        $.each(X_DEPARTS, function (k, item) {
            if (item.ParentId == 0) { X_COP = item; return false; }
        });
        ///去掉外边框边距
        //$(parent).css({"padding":"0px","margin":"0px"});
        var topPanelConfig = {
            region: 'north', xtype: 'panel', height: TOPHEIGHT, title: '', layout: 'column',
            bodyStyle: 'line-height:49px', bodyCls: 'north-header', html: '', items: [
                {
                    bodyCls: "transparent", columnWidth: 0.88, padding: "auto auto auto 10",
                    bodyStyle: "font-size:18px;color:#fff;font-family:微软雅黑", html: X_COP.DepartName,
                },
                {
                    bodyCls: "transparent", columnWidth: 0.12, layout: 'column', padding: "10 auto auto auto",
                    items: [
                        { text: '个人设置', xtype: 'button', columnWidth: '0.6', margin: '2', listeners: { click: function () { RBAC.Js.RBAC_Module.addTab(0, 'RBAC/Templates/Users_Setting.htm', '修改个人资料', 'RBAC.Js.Users_Setting') } } },
                        { text: '退出', xtype: 'button', columnWidth: '0.4', margin: '2', listeners: { click: function () { RBAC.Js.RBAC_Module.logout(); } } }
                    ]
                }
            ]
        };
        var menuPanel = new Ext.tree.TreePanel({
            id: 'menu', region: 'west', title: '功能菜单', width: 230, split: true, overflowY: true, collapsible: true
            , rootVisible: false, autoScroll: false, enableDD: false, containerScroll: false, draggable: false, root: {
                children: RBAC.Js.RBAC_Module.Tree// childrenData
            },
            listeners: {
                itemclick: function (event, node) {
                    if (!node) return false;
                    if (!node.data) return false;
                    RBAC.Js.RBAC_Module.addTab(node.data.id, node.data.path, node.data.text, node.data.Js);
                }
            }
        });

        var deskPanelConfig = {
            region: 'center', xtype: 'tabpanel', id: 'desk', items: [{
                title: '工作台', bodyPadding: 5,
                items: [
                  {
                      title: '', layout: 'column',
                      items: [
                          { title: '公告', columnWidth: .5, bodyPadding: 5, height: ALLHEIGHT - TOPHEIGHT, html: '<div id="Desk_Announcement"><div class="mainList"></div></div>' },
                          { title: '推荐房源', columnWidth: .5, bodyPadding: 5, height: ALLHEIGHT - TOPHEIGHT, html: '<div id="Desk_Sells"><form name="fmList"><input type="hidden" name="Suggest" value="1" /></form><div class="mainList"></div></div>' }
                      ]
                  }
                ]
            }]
        };
        if (!X_USEROBJ.Depart) {
            $.messager.alert("提示", "所在部门为空。");
        }
        var statusPanelConfig = {
            region: 'south', xtype: 'panel', layout: 'column', items: [
             { columnWidth: .2, title: '当前用户：【' + X_USEROBJ.Depart.DepartName + '】' + X_USER + ' ', xtype: 'panel' },
             {
                 //columnWidth: 0.8,  xtype: 'panel',style:'line-height:36px', html:'<a href="javascript:;" id="linkOpenFriendPanel">好友列表</a>'
             }
            ]
        };
        var ViewPort = new Ext.Viewport({
            title: "Viewport", layout: "border", defaults: { bodyStyle: "background-color: #FFFFFF;", frame: true },
            items: [topPanelConfig, menuPanel, deskPanelConfig, statusPanelConfig]
        });

        deskExt = ViewPort.getChildByElement('desk');
        console.log("deskExt===", deskExt.getHeight());
        var Announce = Ext.create('Announcement.Js.Items_List');
        Announce.renderList(Ext.get('Desk_Announcement'), 42, true);
        var House_Sells = Ext.create('House.Js.Sells_List');
        House_Sells.renderList(Ext.get('Desk_Sells'), 42, true);

        template.helper('GetShortDate', function (val) {
            return formatShortDate(val);
        });
    }
});